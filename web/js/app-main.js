var app = angular.module('includeExample', []);

app.controller('ExampleController', function ($scope, $http) {
    //Se inicializan los grados    
    $scope.grades = [
        {
            name: "4to grado de primaria",
            value: 0,
            classesCh: "n_form-button--options",
            sections:[
                {id: 1, name: "A"},
                {id: 2, name: "B"},
                {id: 3, name: "C"},
            ]
        },
        {
            name: "5to grado de primaria",
            value: 1,
            classesCh: "n_form-button--options",
            sections:[
                {id: 1, name: "A"},
                {id: 2, name: "B"},
                {id: 3, name: "C"},
            ]
        },
        {
            name: "6to grado de primaria",
            value: 2,
            classesCh: "n_form-button--options",
            sections:[
                {id: 1, name: "A"},
                {id: 2, name: "B"},
                {id: 3, name: "C"},
            ]
        },
    ];    

    //funciones para añadir  remover o mostrar secciones.
    $scope.addNewChoice = function () {
        var newItemNo = $scope.choices.length + 1;
        $scope.choices.push({ 'id': 'choice' + newItemNo, 'name': 'choice' + newItemNo });
    };
    $scope.addNewChoice2 = function () {
        var newItemNo2 = $scope.grades[0].sections.length + 1;
        let lastSection = $scope.grades[0].sections[$scope.grades[0].sections.length-1].name
        $scope.grades[0].sections.push({ 'id': newItemNo2 , 'name': nextChar(lastSection) });
    };

    $scope.removeNewChoice = function () {
        var newItemNo = $scope.choices.length - 1;
        if (newItemNo !== 0) {
            $scope.choices.pop();
        }
    };

    $scope.showAddChoice = function (choice) {
        return choice.id === $scope.choices[$scope.choices.length - 1].id;
    };


    //conjunto de funciones para la funcionalidad de activar o desactivar botones.

    $scope.btn1 = function () {
        if ($("#container-inlineCheckbox-1").hasClass("active")) {
            document.getElementById("container-inlineCheckbox-0").classList.remove("active");
            document.getElementById("n_check_box_1").style.display="none";
        } else {
            document.getElementById("container-inlineCheckbox-0").classList.add("active");
            document.getElementById("n_check_box_1").style.display="block";
        }
    }
    $scope.btn2 = function () {
        if ($("#container-inlineCheckbox-2").hasClass("active")) {
            document.getElementById("container-inlineCheckbox-1").classList.remove("active");
            document.getElementById("n_check_box_2").style.display="none";
        } else {
            document.getElementById("container-inlineCheckbox-1").classList.add("active");
            document.getElementById("n_check_box_2").style.display="block";
        }
    }
    $scope.btn3 = function () {
        if ($("#container-inlineCheckbox-3").hasClass("active")) {
            document.getElementById("container-inlineCheckbox-2").classList.remove("active");
            document.getElementById("n_check_box_3").style.display="none";
        } else {
            document.getElementById("container-inlineCheckbox-2").classList.add("active");
            document.getElementById("n_check_box_3").style.display="block";
        }
    }

    $scope.deleteStart = function (){

    }

    //transicion de primera pagina a segunda
    $scope.nextPage = function () {
        var error                       = "";
        var dni                         = $("[name=\"Registro[dni]\"]").val();
        var apellido_paterno            = $("[name=\"Registro[apellido_paterno]\"]").val();
        var apellido_materno            = $("[name=\"Registro[apellido_materno]\"]").val();
        var nombres                     = $("[name=\"Registro[nombres]\"]").val();
        var correo_electronico          = $("[name=\"Registro[correo_electronico]\"]").val();
        var celular                     = $("[name=\"Registro[celular]\"]").val();
        var fecha_nacimiento            = $("[name=\"Registro[fecha_nacimiento]\"]").val();

        if(!dni){
            $('.error_dni').addClass('alert-danger');
            $("#dni").focus();
            error = error + "error 1";
        }else{
            $('.error_dni').removeClass('alert-danger');
        }

        if(!apellido_paterno){
            $('.error_apellido_paterno').addClass('alert-danger');
            $("#apellido_paterno").focus();
            error = error + "error 1";
        }else{
            $('.error_apellido_paterno').removeClass('alert-danger');
        }

        if(!nombres){
            $('.error_nombres').addClass('alert-danger');
            $("#nombres").focus();
            error = error + "error 1";
        }else{
            $('.error_nombres').removeClass('alert-danger');
        }

        if(!correo_electronico){
            $('.error_correo_electronico').addClass('alert-danger');
            $("#correo_electronico").focus();
            error = error + "error 1";
        }else{
            $('.error_correo_electronico').removeClass('alert-danger');
        }

        filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test(correo_electronico)) {
            $('.error_correo_electronico').addClass('alert-danger');
            $("#correo_electronico").focus();
            error = error + "error 1";
        }else{
            $('.error_correo_electronico').removeClass('alert-danger');
        }


        if(!celular){
            $('.error_celular').addClass('alert-danger');
            $("#celular").focus();
            error = error + "error 1";
        }else{
            $('.error_celular').removeClass('alert-danger');
        }

        if(!fecha_nacimiento){
            $('.error_fecha_nacimiento').addClass('alert-danger');
            $("#fecha_nacimiento").focus();
            error = error + "error 1";
        }else{
            $('.error_fecha_nacimiento').removeClass('alert-danger');
        }

        if(error != ""){
            console.log(error);
            return true;
        }

        $('.svg_opcion_1').html(`<div class="n_elipse">
                                    <div class="container-image">
                                        <img src="<?= \Yii::$app->request->BaseUrl ?>/images/elipse.png" alt="">
                                    </div>
                                </div>
                                <div class="n_check">
                                    <div class="container-image">
                                        <img src="<?= \Yii::$app->request->BaseUrl ?>/images/check.png" alt="">
                                    </div>
                                </div>`);
        $('.svg_opcion_2').html(`<div class="n_elipse">
                                    <div class="container-image">
                                        <img src="<?= \Yii::$app->request->BaseUrl ?>/images/elipse-limpia.png" alt="">
                                    </div>
                                </div>
                                <div class="n_check">
                                    <div class="container-text">
                                        2
                                    </div>
                                </div>`);
        
        document.getElementById("cont1").style.display = "none";
        document.getElementById("cont2").style.display = "block"
    }
    $scope.paso02 = function(){
        console.log("222");
        //document.getElementById("codieMaster").style.display ="block";
    }

    $scope.step4 = function () {
        document.getElementById("paso4").style.display = "block";
    }


    $scope.step5 = function () {
        document.getElementById("paso5").style.display = "flex";
        document.getElementById("container-bs5").style.display = "flex";
    }

    $scope.step6 = function () {
        document.getElementById("container-bs5").style.display = "none";
        document.getElementById("paso5").style.display = "none";
        
        document.getElementById("paso6").style.display = "block";
    }
    $scope.step7 = function () {
        document.getElementById("container-bs5").style.display = "none";
        document.getElementById("paso6").style.display = "none";
        document.getElementById("paso7").style.display = "block";
    }
    $scope.step8 = function(){
        document.getElementById("paso7").style.display = "none";
        document.getElementById("cont3").style.display = "none";        
        document.getElementById("cont4").style.display = "block";
    }

    $scope.stepfinal = function () {
        window.location.href = "http://127.0.0.1:5500/final.html";
    }


    $scope.btn4a = function () {
        if ($("#container-inlineCheckbox-seccion-4a").hasClass("active")) {
            document.getElementById("container-inlineCheckbox-seccion-4a").classList.remove("active");
            document.getElementById("n_check_box_4a").style.display="none";            
        } else {
            document.getElementById("container-inlineCheckbox-seccion-4a").classList.add("active");
            document.getElementById("n_check_box_4a").style.display="block";
        }
    }
    $scope.btn4b = function () {
        if ($("#container-inlineCheckbox-seccion-4b").hasClass("active")) {
            document.getElementById("container-inlineCheckbox-seccion-4b").classList.remove("active");
            document.getElementById("n_check_box_4b").style.display="none";  
        } else {
            document.getElementById("container-inlineCheckbox-seccion-4b").classList.add("active");
            document.getElementById("n_check_box_4b").style.display="block";
        }
    }
    $scope.btn4c = function () {
        if ($("#container-inlineCheckbox-seccion-4c").hasClass("active")) {
            document.getElementById("container-inlineCheckbox-seccion-4c").classList.remove("active");
            document.getElementById("n_check_box_4c").style.display="none";  
        } else {
            document.getElementById("container-inlineCheckbox-seccion-4c").classList.add("active");
            document.getElementById("n_check_box_4c").style.display="block";  
        }
    }
    $scope.btn5a = function () {
        if ($("#container-inlineCheckbox-seccion-5a").hasClass("active")) {
            document.getElementById("container-inlineCheckbox-seccion-5a").classList.remove("active");
            document.getElementById("n_check_box_5a").style.display="none";
        } else {
            document.getElementById("container-inlineCheckbox-seccion-5a").classList.add("active");
            document.getElementById("n_check_box_5a").style.display="block";
        }
    }
    $scope.btn5b = function () {
        if ($("#container-inlineCheckbox-seccion-5b").hasClass("active")) {
            document.getElementById("container-inlineCheckbox-seccion-5b").classList.remove("active");
            document.getElementById("n_check_box_5b").style.display="none";
        } else {
            document.getElementById("container-inlineCheckbox-seccion-5b").classList.add("active");
            document.getElementById("n_check_box_5b").style.display="block";
        }
    }
    $scope.btn5c = function () {
        if ($("#container-inlineCheckbox-seccion-5c").hasClass("active")) {
            document.getElementById("container-inlineCheckbox-seccion-5c").classList.remove("active");
            document.getElementById("n_check_box_5c").style.display="none";
        } else {
            document.getElementById("container-inlineCheckbox-seccion-5c").classList.add("active");
            document.getElementById("n_check_box_5c").style.display="block";
        }
    }
    $scope.btn6a = function () {
        if ($("#container-inlineCheckbox-seccion-6a").hasClass("active")) {
            document.getElementById("container-inlineCheckbox-seccion-6a").classList.remove("active");
            document.getElementById("n_check_box_6a").style.display="none";
        } else {
            document.getElementById("container-inlineCheckbox-seccion-6a").classList.add("active");
            document.getElementById("n_check_box_6a").style.display="block";
        }
    }
    $scope.btn6b = function () {
        if ($("#container-inlineCheckbox-seccion-6b").hasClass("active")) {
            document.getElementById("container-inlineCheckbox-seccion-6b").classList.remove("active");
            document.getElementById("n_check_box_6b").style.display="none";
        } else {
            document.getElementById("container-inlineCheckbox-seccion-6b").classList.add("active");
            document.getElementById("n_check_box_6b").style.display="block";
        }
    }
    $scope.btn6c = function () {
        if ($("#container-inlineCheckbox-seccion-6c").hasClass("active")) {
            document.getElementById("container-inlineCheckbox-seccion-6c").classList.remove("active");
            document.getElementById("n_check_box_6c").style.display="none";
        } else {
            document.getElementById("container-inlineCheckbox-seccion-6c").classList.add("active");
            document.getElementById("n_check_box_6c").style.display="block";
        }
    }

    $scope.validateformBasic = function () {
        
        var error                       = "";
        var elegir_medio                = $("#elegir_medio").val();
        var departamento                = $("[name=\"Registro[departamento]\"]").val();
        var provincia                   = $("[name=\"Registro[provincia]\"]").val();
        var distrito                    = $("[name=\"Registro[distrito]\"]").val();
        var institucion_educativa_id    = $("[name=\"Registro[institucion_educativa_id]\"]").val();
        var tipo_institucion_educativa  = $("[name=\"Registro[tipo_institucion_educativa]\"]").val();

        if(!elegir_medio){
            $('.error_elegir_medio').addClass('alert-danger');
            $("#elegir_medio").focus();
            error = error + "error 1";
        }else{
            $('.error_elegir_medio').removeClass('alert-danger');
        }

        if(!departamento){
            $('.error_departamento').addClass('alert-danger');
            $("#departamento").focus();
            error = error + "error 1";
        }else{
            $('.error_departamento').removeClass('alert-danger');
        }

        if(!provincia){
            $('.error_provincia').addClass('alert-danger');
            $("#provincia").focus();
            error = error + "error 1";
        }else{
            $('.error_provincia').removeClass('alert-danger');
        }

        if(!distrito){
            $('.error_distrito').addClass('alert-danger');
            $("#distrito").focus();
            error = error + "error 1";
        }else{
            $('.error_distrito').removeClass('alert-danger');
        }

        if(!institucion_educativa_id){
            $('.error_institucion_educativa_id').addClass('alert-danger');
            $("#institucion_educativa_id").focus();
            error = error + "error 1";
        }else{
            $('.error_institucion_educativa_id').removeClass('alert-danger');
        }

        if(!tipo_institucion_educativa){
            $('.error_tipo_institucion_educativa').addClass('alert-danger');
            $("#tipo_institucion_educativa").focus();
            error = error + "error 1";
        }else{
            $('.error_tipo_institucion_educativa').removeClass('alert-danger');
        }

        if(error != ""){
            console.log(error);
            return false;
        }


        document.getElementById("cont2").style.display = "none";
        document.getElementById("cont3").style.display = "block";

        //     return;

        // if (v_depa == null || v_depa == "" || v_provi == null || v_provi == "" || v_distr == null || v_distr == "" || v_nameie == null || v_nameie == "" || v_typeie == null || v_typeie == "") {
        //     document.getElementById("alert_verify").style.display = "none";
        //     document.getElementById("alert_verify_sign").style.display = "none";
        //     document.getElementById("alert_empty").style.display = "inline-block";
        //     return false;
        // } else {
            
            
        // }
    }
});

function nextChar(c) {
    return String.fromCharCode(c.charCodeAt(0) + 1);
}
// funcionalidad que puede leer cuando el usuario manda enter
app.directive('myEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                /* scope.$apply(function () {
                    scope.obtenerDetalleOT();
                    scope.$eval(attrs.myEnter);
                }); */
                event.preventDefault();
            }
        });
    };
});
