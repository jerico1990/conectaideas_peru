<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['options' => ['id' => 'formExperiencia']]); ?>

    <div class="modal-header">
        <h4 class="modal-title"><?= $model->titulo ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label >Número de experiencia:</label>
                    <input type="number" id="correlativo" name="Experiencia[correlativo]" class="form-control numerico" maxlength="8">
                </div>
            </div>
        </div>
        
    </div>
    <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary btn-grabar-experiencia">Grabar</button>
    </div>

<?php ActiveForm::end(); ?>