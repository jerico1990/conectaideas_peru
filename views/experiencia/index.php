
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Experiencias</h4>
                </div>
                <br><br>
                <div class="col-12">
                    <button class="btn btn-success btn-agregar-experiencia">Agregar</button>
                </div>
            </div>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Experiencias</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
               


                <table id="lista-experiencias" class="table table-bordered dt-responsive ">
                    <thead>
                        <tr>
                            <th>Experiencia</th>
                            <th># Actividades</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>


                    <tbody>
                    </tbody>
                </table>


                <!-- <a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/reporte-registros-estudiantes" class="btn btn-success btn-block">Descargar lista de estudiantes</a> -->
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<div id="modal" class="fade modal" role="dialog"  tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>



<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('#staticBackdrop');


$('#lista-experiencias').DataTable();

Experiencias();
async function Experiencias(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/experiencia/get-lista-exp',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var experiencias ="";
                        $('#lista-experiencias').DataTable().destroy();
                        $.each(results.experiencias, function( index, value ) {
                            
                            experiencias = experiencias + "<tr>";
                                experiencias = experiencias + "<td> Experiencia " + value.correlativo + "</td>";
                                experiencias = experiencias + "<td>" + value.cantidad_actividades_activos + "</td>";

                                experiencias = experiencias + "<td>";
                                    experiencias = experiencias + '<a href="#" data-id="' + value.id + '" class="btn btn-primary btn-sm btn-modificar-estudiante" > <i class="fas fa-pen-square"></i> </a> ';
                                    experiencias = experiencias + '<a href="<?= \Yii::$app->request->BaseUrl ?>/actividad" class="btn btn-danger btn-sm"><i class="fas fa-eye"></i></a> ';
                                    // if(estado_registro=='1' ){
                                    //     estudiantes = estudiantes + '<a title="deshabilitar" href="#" data-matricula_id="' + value.matricula_id + '" class="btn btn-danger btn-sm btn-deshabilitar-estudiante" ><i class="fas fa-trash"></i></a>';
                                    // }else if (estado_registro=='2' || estado_registro=='0'){
                                    //     estudiantes = estudiantes + '<a title="habilitar" href="#" data-matricula_id="' + value.matricula_id + '" class="btn btn-success btn-sm btn-habilitar-estudiante" ><i class="fas fa-check"></i></a>';
                                    // }
                                    

                                experiencias = experiencias +"</td>";
                            experiencias = experiencias + "</tr>";
                        });
                        
                        $('#lista-experiencias tbody').html(experiencias);
                        $('#lista-experiencias').DataTable({
                            "paging": true,
                            "lengthChange": true,
                            "searching": false,
                            "ordering": false,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 10,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });

                        loading.modal('hide');
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}


async function Experiencia(id_experiencia){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/experiencia/get-experiencia',
                method: 'POST',
                data:{_csrf:csrf,id_experiencia:id_experiencia},
                dataType:'Json',
                beforeSend:function(xhr, settings)
                {
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var form = $('#formExperiencia');
                        form.loadJSON(results.experiencia);
                        //loading.modal('hide');
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}


//masivamente

$('body').on('click', '.btn-habilitar-masivamente-estudiante', function (e) {
    e.preventDefault();
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/estudiante/masivamente');
    $('#modal').modal('show');
});


//agregar

$('body').on('click', '.btn-agregar-experiencia', function (e) {
    e.preventDefault();
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/experiencia/create');
    $('#modal').modal('show');
});

//modificar
$('body').on('click', '.btn-modificar-estudiante', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/experiencia/update?id='+id,function(){
        Experiencia(id);
    });
    $('#modal').modal('show');
});

//grabar

$('body').on('click', '.btn-grabar-experiencia', function (e) {

    e.preventDefault();
    var form = $('#formExperiencia');
    var formData = $('#formExperiencia').serializeArray();
    if (form.find('.has-error').length) {
        return false;
    }
    
    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success: function (results) {
            if(results.success){
                $('#modal').modal('toggle');
                Experiencias();
            }
        },
    });
});




</script>
