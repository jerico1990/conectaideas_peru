<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Foto */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
html,body{
    background: #E5E5E5;
}

.btn-primario{
    color:black;
    background-color:white;
    border-color:black;
}

.btn-primario:not(:disabled):not(.disabled).active, .btn-primario:not(:disabled):not(.disabled):active{
    color:#fff;
    background-color:#2AB7CA;
    border-color:#2AB7CA;
}

.btn-primario:hover{
    color:#fff;
    background-color:#2AB7CA;
    border-color:#2AB7CA;
}

@media (min-width: 576px) {
  
}

@media (min-width: 768px) {
  
}

@media (min-width: 992px) {
    .container{
        max-width:1440px;
        height: 1074px;
        padding-left:0px;
        padding-right:0px;
        position: relative;
    }

    .col-xl-4{
        width: 457px;
        height: 1075px;
        left: 0px;
        top: -1px;
        background: #2AB7CA;
    }

    .col-xl-8{
        width: 983px;
        height: 1075px;
        left: 0px;
        top: -1px;
        background: white;
    }

    .titulo_izquierdo{
        /* Formulario de registro */


        position: absolute;
        width: 225px;
        height: 65px;
        left: 160px;
        top: 305px;

        /* Dweb/Heading 2 */

        font-family: Montserrat;
        font-style: normal;
        font-weight: bold;
        font-size: 26px;
        line-height: 32px;

        color: #FFFFFF;
    }

    .subtitulo_izquierdo{
        /* Completa las secciones del formulario para inscribirte al programa Conecta Ideas. */
        position: absolute;
        width: 227px;
        height: 71px;
        left: 160px;
        top: 394px;

        /* Dweb/Body */

        font-family: Montserrat;
        font-style: normal;
        font-weight: normal;
        font-size: 14px;
        line-height: 20px;
        /* or 143% */


        color: #FFFFFF;
    }

    .contenedor_opcion_1{
        /* Completa las secciones del formulario para inscribirte al programa Conecta Ideas. */
        position: absolute;
        width: 250px;
        height: 32px;
        left: 160px;
        top: 489px;
    }

    .contenedor_opcion_2{
        /* Completa las secciones del formulario para inscribirte al programa Conecta Ideas. */
        position: absolute;
        width: 250px;
        height: 32px;
        left: 160px;
        top: 545px;
    }

    .contenedor_opcion_3{
        /* Completa las secciones del formulario para inscribirte al programa Conecta Ideas. */
        position: absolute;
        width: 250px;
        height: 32px;
        left: 160px;
        top: 601px;
    }

    .contenedor_opcion_4{
        /* Completa las secciones del formulario para inscribirte al programa Conecta Ideas. */
        position: absolute;
        width: 250px;
        height: 32px;
        left: 160px;
        top: 657px;
    }

    .contenedor_opcion_1 .opcion{
        /* Datos de tu cargo */


        position: absolute;
        left: 19.2%;
        right: 0%;
        top: 18.75%;
        bottom: 18.75%;

        font-family: Montserrat;
        font-style: normal;
        font-weight: 500;
        font-size: 16px;
        line-height: 20px;
        /* identical to box height */

        text-transform: uppercase;

        color: #FFFFFF;
    }

    .contenedor_opcion_2 .opcion{
        /* Datos de tu cargo */


        position: absolute;
        left: 19.2%;
        right: 0%;
        top: 18.75%;
        bottom: 18.75%;

        font-family: Montserrat;
        font-style: normal;
        font-weight: 500;
        font-size: 16px;
        line-height: 20px;
        /* identical to box height */

        text-transform: uppercase;

        color: #FFFFFF;
    }

    .contenedor_opcion_3 .opcion{
        /* Datos de tu cargo */


        position: absolute;
        left: 19.2%;
        right: 0%;
        top: 18.75%;
        bottom: 18.75%;

        font-family: Montserrat;
        font-style: normal;
        font-weight: 500;
        font-size: 16px;
        line-height: 20px;
        /* identical to box height */

        text-transform: uppercase;

        color: #FFFFFF;
    }

    .contenedor_opcion_4 .opcion{
        /* Datos de tu cargo */


        position: absolute;
        left: 19.2%;
        right: 0%;
        top: 18.75%;
        bottom: 18.75%;

        font-family: Montserrat;
        font-style: normal;
        font-weight: 500;
        font-size: 16px;
        line-height: 20px;
        /* identical to box height */

        text-transform: uppercase;

        color: #FFFFFF;
    }



    .titulo_derecho{
        /* Datos personales */


        position: absolute;
        width: 568px;
        height: 30px;
        left: 108px;
        top: 96px;

        /* Dweb/Heading 2 */

        font-family: Montserrat;
        font-style: normal;
        font-weight: bold;
        font-size: 26px;
        line-height: 32px;

        color: #000000;
    }

    .subtitulo_derecho{
        /* Empecemos llenando la información sobre ti */


        position: absolute;
        width: 568px;
        height: 24px;
        left: 108px;
        top: 134px;

        /* Dweb/Subheading 2 */

        font-family: Montserrat;
        font-style: normal;
        font-weight: 500;
        font-size: 16px;
        line-height: 24px;
        /* identical to box height, or 150% */


        color: #000000;
    }

    .contenedor_derecho_1{
        /* Rectangle 221 */


        position: absolute;
        width: 568px;
        height: 440px;
        left: 108px;
        top: 190px;

        /* fondo campos */

        background: #F4F9FB;
        border-radius: 5px;
    }

    .contenedor_derecho_2{
        /* Rectangle 256 */


        position: absolute;
        width: 568px;
        height: 200px;
        left: 108px;
        top: 660px;

        /* fondo campos */

        background: #F4F9FB;
        border-radius: 5px;
    }

    .contenedor_derecho_3{


        position: absolute;
        width: 568px;
        height: 185px;
        left: 108px;
        top: 190px;

        /* fondo campos */

        background: #F4F9FB;
        border-radius: 5px;
    }

    .contenedor_derecho_4{


        position: absolute;
        width: 568px;
        height: 110px;
        left: 108px;
        top: 190px;

        /* fondo campos */

        background: #F4F9FB;
        border-radius: 5px;
    }

    .contenedor_derecho_5{


        position: absolute;
        width: 568px;
        height: 180px;
        left: 108px;
        top: 200px;

        /* fondo campos */

        background: #F4F9FB;
        border-radius: 5px;
    }

    .contenedor_derecho_6{


        position: absolute;
        width: 568px;
        height: 340px;
        left: 108px;
        top: 400px;

        /* fondo campos */

        background: #F4F9FB;
        border-radius: 5px;
    }

    .contenedor_derecho_7{


        position: absolute;
        width: 568px;
        height: 100px;
        left: 108px;
        top: 770px;

        /* fondo campos */

        background: #F4F9FB;
        border-radius: 5px;
    }

    .contenedor_derecho_8{

        position: absolute;
        width: 568px;
        height: 100px;
        left: 108px;
        top: 320px;

        /* fondo campos */

        background: #F4F9FB;
        border-radius: 5px;
    }

    .contenedor_boton{
        position: absolute;
        width: 568px;
        left: 108px;
    }

    
    .container .titulo_exitoso{
        position: absolute;
        width: 468px;
        height: 30px;
        left: 485px;
        top: 153px;

        /* Dweb/Heading 2 */

        font-family: Montserrat;
        font-style: normal;
        font-weight: bold;
        font-size: 26px;
        line-height: 32px;
        text-align: center;

        color: #000000;
    }

    .container .subtitulo_exitoso{
        position: absolute;
        width: 306px;
        height: 54px;
        left: 567px;
        top: 199px;

        /* Dweb/ Subheading 1 */

        font-family: Montserrat;
        font-style: normal;
        font-weight: 500;
        font-size: 16px;
        line-height: 24px;
        /* or 150% */

        text-align: center;

        color: #000000;
    }

    .container .primer_parrafo_exitoso{
        position: absolute;
        width: 446.25px;
        height: 43.51px;
        left: 510px;
        top: 292px;

        /* Dweb/Body */

        font-family: Montserrat;
        font-style: normal;
        font-weight: normal;
        font-size: 14px;
        line-height: 20px;
        /* or 143% */


        color: #000000;

        transform: matrix(1, 0, 0, 1, 0, 0);
    }
    
    .container .segundo_parrafo_exitoso{
        position: absolute;
        width: 445.4px;
        height: 43.51px;
        left: 510px;
        top: 370px;

        /* Dweb/Body */

        font-family: Montserrat;
        font-style: normal;
        font-weight: normal;
        font-size: 14px;
        line-height: 20px;
        /* or 143% */


        color: #000000;

        transform: matrix(1, 0, 0, 1, 0, 0);
    }
    .container .boton_exitoso{
        position: absolute;
        width: 445.4px;
        height: 43.51px;
        left: 510px;
        top: 450px;

    }
}

@media (min-width: 1200px) {
    .container{
        max-width:1440px;
        height: 1074px;
        padding-left:0px;
        padding-right:0px;
        position: relative;
    }

    .col-xl-4{
        width: 457px;
        height: 1075px;
        left: 0px;
        top: -1px;
        background: #2AB7CA;
    }

    .col-xl-8{
        width: 983px;
        height: 1075px;
        left: 0px;
        top: -1px;
        background: white;
    }

    .titulo_izquierdo{
        /* Formulario de registro */


        position: absolute;
        width: 225px;
        height: 65px;
        left: 160px;
        top: 305px;

        /* Dweb/Heading 2 */

        font-family: Montserrat;
        font-style: normal;
        font-weight: bold;
        font-size: 26px;
        line-height: 32px;

        color: #FFFFFF;
    }

    .subtitulo_izquierdo{
        /* Completa las secciones del formulario para inscribirte al programa Conecta Ideas. */
        position: absolute;
        width: 227px;
        height: 71px;
        left: 160px;
        top: 394px;

        /* Dweb/Body */

        font-family: Montserrat;
        font-style: normal;
        font-weight: normal;
        font-size: 14px;
        line-height: 20px;
        /* or 143% */


        color: #FFFFFF;
    }

    .contenedor_opcion_1{
        /* Completa las secciones del formulario para inscribirte al programa Conecta Ideas. */
        position: absolute;
        width: 250px;
        height: 32px;
        left: 160px;
        top: 489px;
    }

    .contenedor_opcion_2{
        /* Completa las secciones del formulario para inscribirte al programa Conecta Ideas. */
        position: absolute;
        width: 250px;
        height: 32px;
        left: 160px;
        top: 545px;
    }

    .contenedor_opcion_3{
        /* Completa las secciones del formulario para inscribirte al programa Conecta Ideas. */
        position: absolute;
        width: 250px;
        height: 32px;
        left: 160px;
        top: 601px;
    }

    .contenedor_opcion_4{
        /* Completa las secciones del formulario para inscribirte al programa Conecta Ideas. */
        position: absolute;
        width: 250px;
        height: 32px;
        left: 160px;
        top: 657px;
    }

    .contenedor_opcion_1 .opcion{
        /* Datos de tu cargo */


        position: absolute;
        left: 19.2%;
        right: 0%;
        top: 18.75%;
        bottom: 18.75%;

        font-family: Montserrat;
        font-style: normal;
        font-weight: 500;
        font-size: 16px;
        line-height: 20px;
        /* identical to box height */

        text-transform: uppercase;

        color: #FFFFFF;
    }

    .contenedor_opcion_2 .opcion{
        /* Datos de tu cargo */


        position: absolute;
        left: 19.2%;
        right: 0%;
        top: 18.75%;
        bottom: 18.75%;

        font-family: Montserrat;
        font-style: normal;
        font-weight: 500;
        font-size: 16px;
        line-height: 20px;
        /* identical to box height */

        text-transform: uppercase;

        color: #FFFFFF;
    }

    .contenedor_opcion_3 .opcion{
        /* Datos de tu cargo */


        position: absolute;
        left: 19.2%;
        right: 0%;
        top: 18.75%;
        bottom: 18.75%;

        font-family: Montserrat;
        font-style: normal;
        font-weight: 500;
        font-size: 16px;
        line-height: 20px;
        /* identical to box height */

        text-transform: uppercase;

        color: #FFFFFF;
    }

    .contenedor_opcion_4 .opcion{
        /* Datos de tu cargo */


        position: absolute;
        left: 19.2%;
        right: 0%;
        top: 18.75%;
        bottom: 18.75%;

        font-family: Montserrat;
        font-style: normal;
        font-weight: 500;
        font-size: 16px;
        line-height: 20px;
        /* identical to box height */

        text-transform: uppercase;

        color: #FFFFFF;
    }



    .titulo_derecho{
        /* Datos personales */


        position: absolute;
        width: 568px;
        height: 30px;
        left: 108px;
        top: 96px;

        /* Dweb/Heading 2 */

        font-family: Montserrat;
        font-style: normal;
        font-weight: bold;
        font-size: 26px;
        line-height: 32px;

        color: #000000;
    }

    .subtitulo_derecho{
        /* Empecemos llenando la información sobre ti */


        position: absolute;
        width: 568px;
        height: 24px;
        left: 108px;
        top: 134px;

        /* Dweb/Subheading 2 */

        font-family: Montserrat;
        font-style: normal;
        font-weight: 500;
        font-size: 16px;
        line-height: 24px;
        /* identical to box height, or 150% */


        color: #000000;
    }

    .contenedor_derecho_1{
        /* Rectangle 221 */


        position: absolute;
        width: 568px;
        height: 440px;
        left: 108px;
        top: 190px;

        /* fondo campos */

        background: #F4F9FB;
        border-radius: 5px;
    }

    .contenedor_derecho_2{
        /* Rectangle 256 */


        position: absolute;
        width: 568px;
        height: 200px;
        left: 108px;
        top: 660px;

        /* fondo campos */

        background: #F4F9FB;
        border-radius: 5px;
    }

    .contenedor_derecho_3{


        position: absolute;
        width: 568px;
        height: 185px;
        left: 108px;
        top: 190px;

        /* fondo campos */

        background: #F4F9FB;
        border-radius: 5px;
    }

    .contenedor_derecho_4{


        position: absolute;
        width: 568px;
        height: 110px;
        left: 108px;
        top: 190px;

        /* fondo campos */

        background: #F4F9FB;
        border-radius: 5px;
    }

    .contenedor_derecho_5{


        position: absolute;
        width: 568px;
        height: 180px;
        left: 108px;
        top: 200px;

        /* fondo campos */

        background: #F4F9FB;
        border-radius: 5px;
    }

    .contenedor_derecho_6{


        position: absolute;
        width: 568px;
        height: 340px;
        left: 108px;
        top: 400px;

        /* fondo campos */

        background: #F4F9FB;
        border-radius: 5px;
    }

    .contenedor_derecho_7{


        position: absolute;
        width: 568px;
        height: 100px;
        left: 108px;
        top: 770px;

        /* fondo campos */

        background: #F4F9FB;
        border-radius: 5px;
    }

    .contenedor_derecho_8{

        position: absolute;
        width: 568px;
        height: 100px;
        left: 108px;
        top: 320px;

        /* fondo campos */

        background: #F4F9FB;
        border-radius: 5px;
    }

    .contenedor_boton{
        position: absolute;
        width: 568px;
        left: 108px;
    }

    
    .container .titulo_exitoso{
        position: absolute;
        width: 468px;
        height: 30px;
        left: 485px;
        top: 153px;

        /* Dweb/Heading 2 */

        font-family: Montserrat;
        font-style: normal;
        font-weight: bold;
        font-size: 26px;
        line-height: 32px;
        text-align: center;

        color: #000000;
    }

    .container .subtitulo_exitoso{
        position: absolute;
        width: 306px;
        height: 54px;
        left: 567px;
        top: 199px;

        /* Dweb/ Subheading 1 */

        font-family: Montserrat;
        font-style: normal;
        font-weight: 500;
        font-size: 16px;
        line-height: 24px;
        /* or 150% */

        text-align: center;

        color: #000000;
    }

    .container .primer_parrafo_exitoso{
        position: absolute;
        width: 446.25px;
        height: 43.51px;
        left: 510px;
        top: 292px;

        /* Dweb/Body */

        font-family: Montserrat;
        font-style: normal;
        font-weight: normal;
        font-size: 14px;
        line-height: 20px;
        /* or 143% */


        color: #000000;

        transform: matrix(1, 0, 0, 1, 0, 0);
    }
    
    .container .segundo_parrafo_exitoso{
        position: absolute;
        width: 445.4px;
        height: 43.51px;
        left: 510px;
        top: 370px;

        /* Dweb/Body */

        font-family: Montserrat;
        font-style: normal;
        font-weight: normal;
        font-size: 14px;
        line-height: 20px;
        /* or 143% */


        color: #000000;

        transform: matrix(1, 0, 0, 1, 0, 0);
    }
    .container .boton_exitoso{
        position: absolute;
        width: 445.4px;
        height: 43.51px;
        left: 510px;
        top: 450px;

    }
}

@media (min-width: 1400px) {
    .container{
        max-width:1440px;
        height: 1074px;
        padding-left:0px;
        padding-right:0px;
        position: relative;
    }

    .col-xl-4{
        width: 457px;
        height: 1075px;
        left: 0px;
        top: -1px;
        background: #2AB7CA;
    }

    .col-xl-8{
        width: 983px;
        height: 1075px;
        left: 0px;
        top: -1px;
        background: white;
    }

    .titulo_izquierdo{
        /* Formulario de registro */


        position: absolute;
        width: 225px;
        height: 65px;
        left: 160px;
        top: 305px;

        /* Dweb/Heading 2 */

        font-family: Montserrat;
        font-style: normal;
        font-weight: bold;
        font-size: 26px;
        line-height: 32px;

        color: #FFFFFF;
    }

    .subtitulo_izquierdo{
        /* Completa las secciones del formulario para inscribirte al programa Conecta Ideas. */
        position: absolute;
        width: 227px;
        height: 71px;
        left: 160px;
        top: 394px;

        /* Dweb/Body */

        font-family: Montserrat;
        font-style: normal;
        font-weight: normal;
        font-size: 14px;
        line-height: 20px;
        /* or 143% */


        color: #FFFFFF;
    }

    .contenedor_opcion_1{
        /* Completa las secciones del formulario para inscribirte al programa Conecta Ideas. */
        position: absolute;
        width: 250px;
        height: 32px;
        left: 160px;
        top: 489px;
    }

    .contenedor_opcion_2{
        /* Completa las secciones del formulario para inscribirte al programa Conecta Ideas. */
        position: absolute;
        width: 250px;
        height: 32px;
        left: 160px;
        top: 545px;
    }

    .contenedor_opcion_3{
        /* Completa las secciones del formulario para inscribirte al programa Conecta Ideas. */
        position: absolute;
        width: 250px;
        height: 32px;
        left: 160px;
        top: 601px;
    }

    .contenedor_opcion_4{
        /* Completa las secciones del formulario para inscribirte al programa Conecta Ideas. */
        position: absolute;
        width: 250px;
        height: 32px;
        left: 160px;
        top: 657px;
    }

    .contenedor_opcion_1 .opcion{
        /* Datos de tu cargo */


        position: absolute;
        left: 19.2%;
        right: 0%;
        top: 18.75%;
        bottom: 18.75%;

        font-family: Montserrat;
        font-style: normal;
        font-weight: 500;
        font-size: 16px;
        line-height: 20px;
        /* identical to box height */

        text-transform: uppercase;

        color: #FFFFFF;
    }

    .contenedor_opcion_2 .opcion{
        /* Datos de tu cargo */


        position: absolute;
        left: 19.2%;
        right: 0%;
        top: 18.75%;
        bottom: 18.75%;

        font-family: Montserrat;
        font-style: normal;
        font-weight: 500;
        font-size: 16px;
        line-height: 20px;
        /* identical to box height */

        text-transform: uppercase;

        color: #FFFFFF;
    }

    .contenedor_opcion_3 .opcion{
        /* Datos de tu cargo */


        position: absolute;
        left: 19.2%;
        right: 0%;
        top: 18.75%;
        bottom: 18.75%;

        font-family: Montserrat;
        font-style: normal;
        font-weight: 500;
        font-size: 16px;
        line-height: 20px;
        /* identical to box height */

        text-transform: uppercase;

        color: #FFFFFF;
    }

    .contenedor_opcion_4 .opcion{
        /* Datos de tu cargo */


        position: absolute;
        left: 19.2%;
        right: 0%;
        top: 18.75%;
        bottom: 18.75%;

        font-family: Montserrat;
        font-style: normal;
        font-weight: 500;
        font-size: 16px;
        line-height: 20px;
        /* identical to box height */

        text-transform: uppercase;

        color: #FFFFFF;
    }



    .titulo_derecho{
        /* Datos personales */


        position: absolute;
        width: 568px;
        height: 30px;
        left: 108px;
        top: 96px;

        /* Dweb/Heading 2 */

        font-family: Montserrat;
        font-style: normal;
        font-weight: bold;
        font-size: 26px;
        line-height: 32px;

        color: #000000;
    }

    .subtitulo_derecho{
        /* Empecemos llenando la información sobre ti */


        position: absolute;
        width: 568px;
        height: 24px;
        left: 108px;
        top: 134px;

        /* Dweb/Subheading 2 */

        font-family: Montserrat;
        font-style: normal;
        font-weight: 500;
        font-size: 16px;
        line-height: 24px;
        /* identical to box height, or 150% */


        color: #000000;
    }

    .contenedor_derecho_1{
        /* Rectangle 221 */


        position: absolute;
        width: 568px;
        height: 440px;
        left: 108px;
        top: 190px;

        /* fondo campos */

        background: #F4F9FB;
        border-radius: 5px;
    }

    .contenedor_derecho_2{
        /* Rectangle 256 */


        position: absolute;
        width: 568px;
        height: 200px;
        left: 108px;
        top: 660px;

        /* fondo campos */

        background: #F4F9FB;
        border-radius: 5px;
    }

    .contenedor_derecho_3{


        position: absolute;
        width: 568px;
        height: 185px;
        left: 108px;
        top: 190px;

        /* fondo campos */

        background: #F4F9FB;
        border-radius: 5px;
    }

    .contenedor_derecho_4{


        position: absolute;
        width: 568px;
        height: 110px;
        left: 108px;
        top: 190px;

        /* fondo campos */

        background: #F4F9FB;
        border-radius: 5px;
    }

    .contenedor_derecho_5{


        position: absolute;
        width: 568px;
        height: 180px;
        left: 108px;
        top: 200px;

        /* fondo campos */

        background: #F4F9FB;
        border-radius: 5px;
    }

    .contenedor_derecho_6{


        position: absolute;
        width: 568px;
        height: 340px;
        left: 108px;
        top: 400px;

        /* fondo campos */

        background: #F4F9FB;
        border-radius: 5px;
    }

    .contenedor_derecho_7{


        position: absolute;
        width: 568px;
        height: 100px;
        left: 108px;
        top: 770px;

        /* fondo campos */

        background: #F4F9FB;
        border-radius: 5px;
    }

    .contenedor_derecho_8{

        position: absolute;
        width: 568px;
        height: 100px;
        left: 108px;
        top: 320px;

        /* fondo campos */

        background: #F4F9FB;
        border-radius: 5px;
    }

    .contenedor_boton{
        position: absolute;
        width: 568px;
        left: 108px;
    }

    
    .container .titulo_exitoso{
        position: absolute;
        width: 468px;
        height: 30px;
        left: 485px;
        top: 153px;

        /* Dweb/Heading 2 */

        font-family: Montserrat;
        font-style: normal;
        font-weight: bold;
        font-size: 26px;
        line-height: 32px;
        text-align: center;

        color: #000000;
    }

    .container .subtitulo_exitoso{
        position: absolute;
        width: 306px;
        height: 54px;
        left: 567px;
        top: 199px;

        /* Dweb/ Subheading 1 */

        font-family: Montserrat;
        font-style: normal;
        font-weight: 500;
        font-size: 16px;
        line-height: 24px;
        /* or 150% */

        text-align: center;

        color: #000000;
    }

    .container .primer_parrafo_exitoso{
        position: absolute;
        width: 446.25px;
        height: 43.51px;
        left: 510px;
        top: 292px;

        /* Dweb/Body */

        font-family: Montserrat;
        font-style: normal;
        font-weight: normal;
        font-size: 14px;
        line-height: 20px;
        /* or 143% */


        color: #000000;

        transform: matrix(1, 0, 0, 1, 0, 0);
    }
    
    .container .segundo_parrafo_exitoso{
        position: absolute;
        width: 445.4px;
        height: 43.51px;
        left: 510px;
        top: 370px;

        /* Dweb/Body */

        font-family: Montserrat;
        font-style: normal;
        font-weight: normal;
        font-size: 14px;
        line-height: 20px;
        /* or 143% */


        color: #000000;

        transform: matrix(1, 0, 0, 1, 0, 0);
    }
    .container .boton_exitoso{
        position: absolute;
        width: 445.4px;
        height: 43.51px;
        left: 510px;
        top: 450px;

    }
}
</style>
<?php $form = ActiveForm::begin(['options' => ['id' => 'formRegistro','class' => 'form-horizontal','autocomplete'=>'off']]); ?>

<div class="account-pages my-5">
    <div class="container">
        <div class="row justify-content-center msg_exitoso">
            <div class="col-md-4 col-lg-4 col-xl-4 col-xxl-4">
                <div class="col-md-12 col-lg-12 col-xl-12 col-xxl-12">
                    <svg width="460" height="297" viewBox="0 0 460 297" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <rect y="-1" width="460" height="297" fill="#2AB7CA"/>
                        <g clip-path="url(#clip0)">
                        <path d="M160.671 56.1926H165.712H165.893C165.893 56.1926 165.893 56.1459 165.939 56.1459C165.848 56.0525 165.712 56.0058 165.621 55.9125C164.168 55.0723 163.577 53.7187 163.532 52.0851C163.396 46.8107 163.441 41.5363 163.441 36.2152C163.441 34.5815 163.441 32.9479 163.487 31.3609C163.487 30.334 163.986 29.5405 164.713 28.887C165.394 28.3269 166.166 27.9535 167.074 28.0002C168.437 28.1402 169.708 28.4669 170.707 29.4938C170.98 29.7739 171.161 30.1473 171.343 30.474C171.616 30.9408 171.888 31.2675 172.478 31.3142C173.659 31.5009 174.431 32.3411 175.022 33.3213C175.566 34.2081 175.748 35.1416 175.703 36.1685C175.703 36.3552 175.703 36.5886 175.703 36.7753C175.703 37.0554 175.839 37.2887 176.111 37.3821C177.474 37.8955 178.155 38.9691 178.428 40.3694C178.655 41.3496 178.609 42.3298 178.2 43.2633C177.928 43.8701 177.474 44.3368 176.929 44.7102C176.293 45.0836 176.293 45.0836 176.475 45.8305C177.111 48.2109 176.066 50.1713 174.068 51.2916C173.205 51.7583 172.297 51.945 171.343 52.0384C170.571 52.1317 170.48 52.1317 170.299 52.8785C169.935 54.3255 169.072 55.3524 167.665 55.8191C167.392 55.9125 167.21 56.0525 167.165 56.3793C167.12 56.6126 166.938 56.7527 166.711 56.7527C164.531 56.7527 162.397 56.7527 160.217 56.7527C159.808 56.7527 159.717 56.6593 159.581 56.2859C159.536 56.0992 159.354 55.9592 159.172 55.9125C157.764 55.6324 156.947 54.6989 156.447 53.392C156.357 53.1586 156.266 52.9252 156.175 52.6918C156.039 52.3184 155.812 52.1784 155.448 52.1784C153.813 52.1317 152.36 51.665 151.27 50.4047C150.725 49.7512 150.226 49.0511 150.09 48.1643C149.953 47.3708 149.999 46.5306 150.226 45.7371C150.362 45.2237 150.407 45.2237 149.953 44.9436C148.909 44.3368 148.273 43.45 148.137 42.1897C148.001 40.9761 148.137 39.8559 148.863 38.829C149.227 38.2689 149.681 37.7555 150.362 37.5221C150.862 37.3354 151.089 36.8686 150.952 36.2152C150.816 35.4684 150.998 34.8149 151.225 34.1614C151.634 33.0879 152.224 32.201 153.269 31.6409C153.405 31.5476 153.541 31.5009 153.723 31.5009C154.495 31.3142 155.085 30.8941 155.448 30.1473C155.993 29.0737 156.856 28.4203 158.082 28.1869C158.446 28.1402 158.764 28.0002 159.127 27.9535C160.035 27.7668 160.807 28.0002 161.534 28.5136C162.351 29.1204 162.896 29.9139 163.123 30.9408C163.169 31.1742 163.214 31.4075 163.214 31.6409C163.169 38.4556 163.169 45.2703 163.169 52.0851C163.169 53.7187 162.487 54.9323 161.17 55.8191C160.943 56.0058 160.853 56.0992 160.671 56.1926ZM173.568 39.1558C173.886 39.3892 174.159 39.5292 174.477 39.7159C174.976 39.9959 175.158 40.5094 175.022 41.0228C174.794 41.6296 174.34 41.9097 173.75 41.7696C173.205 41.6296 172.978 41.2095 172.842 40.6961C172.751 40.3227 172.751 39.9493 172.524 39.5759C170.934 37.1487 167.982 36.6353 165.712 38.3156C165.076 38.7824 164.667 39.3892 164.576 40.2293C164.531 40.6027 164.576 40.9761 164.576 41.3496C164.576 43.0766 164.622 44.8036 164.622 46.5773C164.622 48.5377 164.622 50.4981 164.622 52.5051C164.622 53.0186 164.713 53.532 165.031 53.9988C165.53 54.7456 166.484 55.4457 167.755 54.7923C168.482 54.4189 168.891 53.8121 169.163 53.0653C169.481 52.2251 169.754 52.1317 168.664 51.3849C168.21 51.1049 167.846 50.7314 167.528 50.3114C166.802 49.4245 166.438 48.351 165.939 47.3708C165.712 46.9507 165.712 46.4839 166.03 46.0638C166.348 45.6438 166.847 45.5504 167.301 45.8305C167.846 46.1105 168.028 46.5773 167.846 47.1374C167.801 47.3241 167.71 47.4641 167.665 47.6041C167.574 47.8375 167.528 48.0242 167.574 48.2576C168.164 49.938 169.481 51.0582 171.298 51.1515C172.66 51.1982 173.75 50.5914 174.613 49.6112C175.794 48.351 175.839 46.7173 174.84 45.317C174.522 44.8503 174.068 44.4769 173.614 44.1034C173.296 43.8701 173.25 43.59 173.477 43.3099C173.705 42.9832 173.932 42.9365 174.25 43.1699C174.522 43.3566 174.749 43.5433 174.976 43.73C175.703 44.1968 176.475 44.0568 176.929 43.3099C177.746 42.003 177.383 40.2293 176.611 39.2491C176.339 38.9224 175.975 38.6423 175.521 38.5957C174.794 38.3156 174.204 38.689 173.568 39.1558ZM161.942 46.2972C161.942 44.2901 161.942 42.2831 161.942 40.276C161.942 39.7159 161.761 39.2958 161.398 38.9224C160.489 38.0355 159.399 37.5688 158.173 37.4754C156.947 37.3821 155.948 37.8488 154.994 38.5957C154.177 39.2025 153.677 39.9493 153.677 41.0228C153.677 41.2095 153.586 41.3496 153.496 41.4429C153.178 41.863 152.724 41.9097 152.224 41.8163C151.77 41.723 151.588 41.3496 151.543 40.9295C151.452 40.4627 151.679 40.136 152.042 39.9026C152.179 39.8092 152.36 39.7626 152.496 39.6692C152.905 39.4825 152.951 39.2025 152.542 38.9224C151.724 38.3156 150.635 38.4556 149.908 39.2958C148.909 40.416 148.954 42.003 149.545 43.2633C149.908 44.0568 150.771 44.2901 151.497 43.7767C151.77 43.59 152.042 43.4033 152.315 43.1699C152.633 42.9365 152.86 42.9365 153.087 43.2166C153.314 43.4967 153.314 43.7767 153.041 44.0568C152.905 44.1968 152.769 44.2901 152.633 44.4302C152.179 44.7569 151.815 45.177 151.543 45.6904C151.043 46.7173 150.816 47.7442 151.452 48.8177C152.269 50.1713 153.45 51.0115 154.994 51.1515C156.447 51.2916 157.583 50.5914 158.491 49.4245C158.991 48.7711 159.263 48.0242 158.854 47.2307C158.809 47.1841 158.809 47.1374 158.809 47.0907C158.764 46.764 158.764 46.4839 158.991 46.2039C159.309 45.8305 159.853 45.6904 160.353 45.8771C160.762 46.0638 160.943 46.4372 160.898 46.9974C160.898 47.2774 160.762 47.4641 160.625 47.6975C160.535 47.8375 160.398 47.9309 160.353 48.1176C159.899 49.7046 158.991 50.9182 157.628 51.805C157.401 51.945 157.31 52.1784 157.356 52.4118C157.492 53.3453 157.946 54.1388 158.718 54.6989C159.445 55.2124 160.217 55.2124 160.989 54.6989C161.806 54.1388 162.079 53.2986 162.079 52.3651C161.942 50.3114 161.942 48.3043 161.942 46.2972ZM161.942 34.1148C161.897 33.3213 161.988 32.2944 161.897 31.3142C161.806 30.3807 161.08 29.4938 160.171 29.3071C159.49 29.1671 158.809 29.1671 158.173 29.3538C157.265 29.6339 156.538 30.1473 156.175 31.0808C155.993 31.5009 156.039 31.7343 156.357 32.061C156.856 32.5744 157.401 32.9945 157.992 33.4613C158.264 33.648 158.446 33.8814 158.446 34.2548C158.491 34.7682 157.992 35.3283 157.447 35.4217C156.947 35.515 156.402 35.1883 156.22 34.6749C156.13 34.3481 156.175 34.0681 156.175 33.788C156.175 33.4613 156.039 33.2279 155.812 32.9945C155.13 32.3877 154.131 32.3877 153.405 32.9479C152.496 33.648 152.042 34.6282 151.906 35.7484C151.77 36.8686 152.042 37.3821 152.996 37.8955C153.496 38.1756 153.768 38.1289 154.177 37.7555C156.266 35.9818 158.809 35.8418 161.125 37.2887C161.307 37.3821 161.488 37.6155 161.67 37.4754C161.897 37.3354 161.897 37.0554 161.897 36.822C161.897 36.7753 161.897 36.6819 161.897 36.6353V34.1148H161.942ZM164.622 34.2548C164.622 35.1416 164.622 36.0285 164.622 36.8686C164.622 37.0554 164.531 37.3354 164.758 37.4754C164.94 37.5688 165.121 37.3821 165.303 37.2887C166.302 36.6353 167.392 36.2619 168.573 36.2619C169.981 36.2619 171.252 36.7286 172.297 37.6621C172.569 37.8955 172.842 38.0822 173.25 37.9889C173.932 37.8022 174.749 37.102 174.704 36.0285C174.658 34.9083 174.25 33.9281 173.432 33.1346C172.751 32.4811 171.933 32.2944 171.071 32.7611C170.526 33.0412 170.208 33.5546 170.299 34.0681C170.48 34.8149 169.981 35.1883 169.39 35.2817C168.936 35.3283 168.437 35.095 168.255 34.6749C168.028 34.1614 168.119 33.7413 168.573 33.3679C169.118 32.9012 169.708 32.4344 170.253 31.921C170.571 31.6409 170.616 31.3142 170.435 30.9408C170.162 30.3807 169.754 29.9139 169.163 29.6339C168.618 29.3538 167.982 29.2604 167.392 29.1671C166.075 28.9804 164.667 30.194 164.667 31.5476C164.622 32.4811 164.622 33.3679 164.622 34.2548Z" fill="white"/>
                        <path d="M166.574 57.3592C166.211 57.8726 165.802 58.3394 165.53 58.8995C165.484 58.9928 165.439 59.0862 165.439 59.2262C165.484 59.5996 165.348 59.9263 165.121 60.2064C164.803 60.6732 164.349 61.0932 163.668 60.9999C163.259 60.9532 162.85 60.9532 162.442 60.8599C161.987 60.7665 161.76 60.3464 161.624 59.973C161.488 59.6463 161.397 59.2729 161.306 58.8995C161.17 58.4327 160.898 58.0126 160.443 57.8259C160.262 57.7326 160.125 57.5459 160.171 57.3592C160.216 57.1725 160.398 57.1258 160.58 57.0791C161.76 57.0791 162.941 57.0791 164.122 57.0791C164.667 57.0791 165.257 57.0791 165.802 57.0791C166.075 57.0791 166.347 57.1725 166.574 57.3592Z" fill="white"/>
                        <path d="M170.616 40.1359C170.616 40.5093 170.525 40.7893 170.343 41.0227C169.889 41.7695 169.526 42.563 169.526 43.4965C169.526 44.15 169.753 44.8035 170.071 45.3636C170.253 45.6903 170.48 45.737 170.888 45.6436C171.479 45.5036 172.115 45.877 172.251 46.4371C172.387 46.8572 172.16 47.3707 171.797 47.604C171.343 47.8841 170.843 47.8841 170.48 47.464C169.935 46.8105 169.344 46.2038 168.936 45.4569C168.436 44.5234 168.345 43.4965 168.618 42.4697C168.799 41.7695 168.89 41.0694 168.527 40.3692C168.391 40.1359 168.527 39.8558 168.663 39.6224C168.799 39.4824 168.936 39.2957 169.117 39.249C169.344 39.1557 169.662 39.109 169.935 39.249C170.207 39.4357 170.616 39.6691 170.616 40.1359Z" fill="white"/>
                        <path d="M157.038 39.249C157.764 39.249 158.309 40.0892 157.991 40.7893C157.764 41.3028 157.764 41.8162 157.946 42.3296C158.445 43.9633 158.082 45.4103 156.901 46.6705C156.674 46.9039 156.447 47.184 156.266 47.464C155.721 48.1175 154.994 48.0708 154.54 47.3707C154.131 46.7639 154.449 45.9237 155.13 45.7837C155.267 45.737 155.494 45.737 155.721 45.737C156.356 45.6903 156.674 45.4569 156.856 44.8502C157.174 43.6366 157.083 42.423 156.356 41.3494C156.22 41.1627 156.084 40.976 156.039 40.7427C155.766 39.9958 156.311 39.2957 157.038 39.249Z" fill="white"/>
                        <path d="M326.974 54.8388C326.974 55.6323 326.929 56.6591 327.065 57.686C327.11 58.1528 327.065 58.5728 327.201 59.0396C327.292 59.4597 327.065 59.8798 326.52 59.8798C325.566 59.8798 324.567 59.8798 323.568 59.8798C323.023 59.8798 322.887 59.8331 322.887 59.133C322.887 58.1528 322.932 57.1726 322.932 56.1924C323.023 51.9915 323.023 47.8373 322.978 43.6365C322.978 42.0495 322.932 40.4625 322.887 38.8755C322.887 38.5488 322.796 38.2221 322.887 37.8953C322.932 37.662 323.023 37.5686 323.25 37.5686C324.249 37.4752 325.249 37.5219 326.248 37.5219C326.566 37.5219 326.656 37.802 326.702 38.082C326.747 38.2687 326.747 38.5021 326.656 38.6888C326.611 38.7822 326.611 38.9222 326.656 39.0622C326.656 39.2489 326.747 39.3423 326.929 39.4356C327.11 39.529 327.201 39.389 327.292 39.2956C327.61 38.9222 327.973 38.5955 328.427 38.4554C329.29 38.1754 330.153 38.1287 331.016 38.4088C331.879 38.6888 332.651 39.1089 333.196 39.9024C333.559 40.4625 333.786 41.0693 334.013 41.7228C334.467 43.1697 334.467 44.6634 334.467 46.157C334.467 47.744 334.24 49.331 333.65 50.8246C332.651 53.1584 330.607 53.8586 328.473 53.2051C327.973 53.0651 327.61 52.645 327.201 52.3182C327.156 52.2716 327.11 52.1782 327.02 52.2249C326.929 52.2716 326.974 52.3649 326.974 52.4583C326.974 53.1584 326.974 53.8586 326.974 54.8388ZM326.974 46.297C326.974 47.0438 326.974 47.604 326.974 48.1174C327.02 48.9109 327.474 49.3777 328.11 49.6577C328.7 49.8911 329.29 49.7511 329.744 49.2376C330.062 48.9109 330.244 48.4908 330.335 48.0707C330.698 46.297 330.653 44.5233 330.108 42.7963C329.926 42.1895 329.608 41.7694 328.927 41.7228C327.928 41.6294 326.883 42.5629 326.883 43.5898C326.883 44.57 327.065 45.5502 326.974 46.297Z" fill="white"/>
                        <path d="M355.176 44.8034C354.404 44.6167 353.632 44.57 352.815 44.6633C351.861 44.8034 351.089 45.2234 350.453 46.0636C350.408 45.9703 350.408 45.9236 350.363 45.8769C350.226 45.5035 350.317 45.0834 350.317 44.71C350.317 44.2899 350.181 44.1499 349.818 44.1966C349.136 44.2432 348.455 44.3366 347.774 44.4299C347.456 44.4766 347.274 44.71 347.274 45.0834C347.32 45.7369 347.365 46.3903 347.411 47.0438C347.456 48.024 347.501 49.0042 347.547 49.9377C347.592 51.1046 347.683 52.2715 347.683 53.4384C347.729 55.5855 347.729 57.7326 347.774 59.8797C347.774 60.0198 347.774 60.1598 347.774 60.2998C347.774 60.6266 347.956 60.7666 348.183 60.7666C349.091 60.7666 349.999 60.7666 350.907 60.7666C351.452 60.7666 351.543 60.6266 351.543 60.0198C351.543 59.0862 351.452 58.1994 351.452 57.2659C351.452 55.0254 351.452 52.7383 351.407 50.4978C351.407 50.4045 351.407 50.2645 351.407 50.1711C351.225 49.1442 351.634 48.4441 352.679 48.2574C353.405 48.1174 353.451 48.3507 353.95 48.6308C355.585 49.611 356.266 45.0367 355.176 44.8034Z" fill="white"/>
                        <path d="M364.168 58.8529C363.578 59.6931 362.805 60.1132 361.806 60.2532C360.989 60.3932 360.217 60.2999 359.445 60.1132C357.901 59.7398 357.129 58.5729 356.811 57.0792C356.629 56.1457 356.584 55.1655 356.493 54.1853C356.357 51.9915 356.448 49.7978 356.448 47.6507C356.448 46.9038 356.448 46.1103 356.538 45.3635C356.584 45.0368 356.72 44.8501 357.129 44.8501C357.81 44.8501 358.491 44.8501 359.127 44.8501C359.309 44.8501 359.536 44.8034 359.717 44.8034C360.217 44.8034 360.489 44.9434 360.444 45.5969C360.353 46.4838 360.308 47.3706 360.308 48.3041C360.262 50.0778 360.262 51.8982 360.262 53.6719C360.262 54.4187 360.353 55.1655 360.626 55.9123C360.807 56.4258 361.216 56.8458 361.943 56.7058C362.942 56.5191 363.396 55.819 363.214 54.7921C363.214 54.6987 363.214 54.5587 363.214 54.4654C363.214 52.2249 363.214 49.9378 363.169 47.6973C363.169 46.7638 363.123 45.877 363.078 44.9434C363.078 44.3833 363.169 44.2433 363.714 44.1966C364.622 44.15 365.53 44.15 366.439 44.1966C366.711 44.1966 366.893 44.3367 366.847 44.6634C366.847 44.8034 366.847 44.9434 366.847 45.0835C366.893 47.2306 366.893 49.3777 366.938 51.5248C366.938 52.6917 367.029 53.8586 367.074 55.0255C367.12 56.0057 367.165 56.9859 367.211 57.9194C367.256 58.5729 367.301 59.2263 367.347 59.8798C367.392 60.2532 367.211 60.4866 366.847 60.5333C366.166 60.6266 365.485 60.72 364.804 60.7666C364.44 60.8133 364.304 60.6733 364.304 60.2532C364.304 59.8798 364.35 59.4597 364.259 59.0863C364.213 58.9929 364.213 58.9463 364.168 58.8529Z" fill="white"/>
                        <path d="M342.506 53.7189C341.325 53.7189 340.144 53.7189 338.964 53.7189C338.328 53.7189 338.192 53.9522 338.237 54.6057C338.283 55.3059 338.373 56.006 338.6 56.6595C338.828 57.2663 339.282 57.6397 339.781 57.5463C340.326 57.4996 340.78 56.9395 340.871 56.3794C340.962 55.9593 340.917 55.4926 340.917 55.0725C340.917 54.7924 341.007 54.6057 341.28 54.6057C341.371 54.6057 341.461 54.559 341.552 54.559C342.37 54.559 343.233 54.559 344.05 54.559C344.413 54.559 344.595 54.6991 344.595 55.0725C344.595 56.6128 344.232 58.1064 343.187 59.32C342.506 60.1135 341.643 60.4402 340.689 60.6736C340.281 60.767 339.827 60.8603 339.372 60.8136C338.373 60.767 337.42 60.5336 336.602 59.8801C335.603 59.0866 335.058 58.0597 334.786 56.7995C334.422 55.1192 334.332 53.3921 334.422 51.7118C334.513 49.9848 334.786 48.2578 335.649 46.7175C336.33 45.5039 337.42 44.8037 338.737 44.5704C340.054 44.337 341.28 44.617 342.37 45.3638C343.687 46.204 344.368 47.4643 344.686 49.0046C344.867 50.0781 344.913 51.105 344.958 52.1786C344.958 52.552 344.867 52.9254 344.867 53.2988C344.867 53.6255 344.64 53.7655 344.323 53.7655H342.506V53.7189ZM339.418 50.6849C339.827 50.6382 340.281 50.7316 340.689 50.6382C341.007 50.5449 341.189 50.3582 341.144 50.0315C341.098 49.4247 340.917 48.8646 340.644 48.3044C340.462 47.9777 340.19 47.791 339.872 47.7443C339.145 47.6043 338.6 47.931 338.283 48.6312C338.101 49.0979 337.919 49.5647 337.828 50.0781C337.738 50.4515 337.874 50.5916 338.237 50.6849C338.646 50.7316 339.009 50.6849 339.418 50.6849Z" fill="white"/>
                        <path d="M360.989 42.3297C361.216 41.9096 361.353 41.7696 361.534 41.2094C361.67 40.836 361.716 40.6027 361.852 40.2293C361.897 40.0425 361.943 39.9492 362.034 39.7625C362.079 39.5758 362.079 39.3891 362.215 39.2024C362.261 39.109 362.442 39.1557 362.715 39.2957C363.169 39.5291 363.578 39.8092 364.032 40.0892C364.259 40.2292 364.486 40.3226 364.668 40.5093C364.94 40.6493 364.986 40.7427 364.94 40.836C364.849 40.9761 364.759 41.0694 364.668 41.2094C364.486 41.4895 364.259 41.7696 364.123 42.0496C363.941 42.423 363.714 42.7498 363.487 43.0765C363.214 43.4499 362.987 43.8233 362.76 44.2434C362.67 44.3834 362.533 44.3367 362.034 44.0567C361.58 43.7766 361.443 43.6833 360.989 43.4032C360.717 43.2632 360.626 43.1232 360.671 43.0298C360.762 42.8431 360.898 42.6564 360.989 42.4697C360.898 42.423 361.035 42.3297 360.989 42.3297Z" fill="white"/>
                        <path d="M181.242 41.8159C181.606 40.8823 182.06 39.9488 182.741 39.1553C183.967 37.755 185.511 37.2883 187.328 37.5217C188.145 37.615 188.872 37.8017 189.598 38.2218C190.915 39.0153 191.597 40.2756 192.005 41.7225C192.323 42.9361 192.414 44.1963 192.414 45.5033C192.414 46.0634 192.232 46.2501 191.687 46.2034C190.915 46.2034 190.189 46.2034 189.417 46.2501C188.872 46.2968 188.781 46.1567 188.781 45.5966C188.781 44.5698 188.645 43.6362 188.1 42.7494C187.509 41.8625 186.238 41.9092 185.693 42.7961C185.239 43.5896 185.057 44.4297 184.921 45.3166C184.648 47.277 184.739 49.2374 184.784 51.1978C184.784 52.5047 185.012 53.7649 185.42 55.0252C185.557 55.4453 185.784 55.8187 186.056 56.1454C186.601 56.7055 187.328 56.6589 188.009 56.1921C188.735 55.632 188.963 54.8385 189.053 53.9516C189.144 53.1582 189.144 52.4113 189.19 51.6178C189.19 51.3378 189.326 51.2444 189.598 51.2444C190.597 51.2444 191.596 51.2444 192.55 51.2444C192.913 51.2444 193.05 51.4311 193.095 51.8512C193.141 52.5514 193.095 53.2515 193.05 53.9983C192.959 54.9785 192.777 55.9587 192.414 56.8922C191.597 59.086 190.007 60.2062 187.782 60.673C187.146 60.813 186.51 60.8597 185.874 60.813C185.012 60.7664 184.194 60.4396 183.468 59.9262C182.378 59.086 181.742 57.9191 181.333 56.6589C180.924 55.492 180.697 54.2784 180.561 53.0648C180.516 52.5047 180.425 51.9446 180.425 51.3845C180.425 51.3378 180.425 51.2444 180.334 51.2444V47.277C180.47 47.1369 180.379 46.9502 180.379 46.7635C180.516 45.1299 180.743 43.5429 181.197 42.0026C181.242 41.9092 181.242 41.8625 181.242 41.8159Z" fill="white"/>
                        <path d="M257.855 59.4598C257.038 60.2533 256.084 60.5333 255.039 60.58C254.54 60.58 253.995 60.5333 253.495 60.3933C252.769 60.1599 252.088 59.9265 251.588 59.3198C250.998 58.6663 250.771 57.8728 250.589 56.986C250.498 56.4258 250.498 55.9124 250.544 55.3523C250.68 53.1585 251.588 51.7582 253.813 51.2915C254.903 51.0581 255.993 51.0581 257.128 51.1981C257.537 51.2448 257.81 51.0114 257.81 50.5913C257.81 49.9379 257.855 49.2377 257.764 48.5843C257.673 48.0241 257.219 47.464 256.72 47.3707C256.039 47.2307 255.448 47.5107 255.176 48.1175C254.994 48.5376 254.903 49.0043 254.903 49.4711C254.903 49.9379 254.812 50.0779 254.313 50.0312C253.359 49.9845 252.405 49.8912 251.452 49.7978C251.179 49.7512 250.998 49.6111 251.043 49.3311C251.134 47.9775 251.361 46.6705 252.224 45.597C252.95 44.7101 253.904 44.3367 254.903 44.1967C256.266 43.9633 257.583 44.0567 258.9 44.5234C260.534 45.1302 261.67 46.6705 261.715 48.4442C261.897 52.3183 261.761 56.1925 261.806 60.0199C261.806 60.44 261.579 60.6734 261.17 60.6734C260.307 60.6267 259.49 60.8601 258.627 60.8134C258.309 60.8134 258.082 60.6734 257.991 60.3933C257.991 60.1133 257.946 59.8332 257.855 59.4598ZM257.855 55.2123C257.855 54.9789 257.855 54.7455 257.855 54.5121C257.855 53.9987 257.673 53.812 257.174 53.7653C256.901 53.7653 256.674 53.812 256.402 53.812C255.584 53.8587 254.949 54.4188 254.767 55.2123C254.54 56.0991 254.903 56.8459 255.675 57.0326C256.22 57.1727 256.765 57.0793 257.31 56.8926C257.583 56.7993 257.764 56.5659 257.855 56.2858C257.855 55.9591 257.855 55.5857 257.855 55.2123Z" fill="white"/>
                        <path d="M208.581 46.0637C209.217 45.2235 209.989 44.7568 210.943 44.6167C211.76 44.4767 212.532 44.5701 213.35 44.7568C214.939 45.1302 215.666 46.2971 215.984 47.8374C216.165 48.8176 216.211 49.7978 216.302 50.778C216.438 52.9718 216.347 55.2122 216.347 57.406C216.347 58.1528 216.347 58.9463 216.256 59.6931C216.211 60.0199 216.075 60.2066 215.666 60.2066C214.985 60.2066 214.303 60.2066 213.622 60.2066C213.441 60.2066 213.213 60.2532 213.032 60.2532C212.532 60.2532 212.214 60.1132 212.305 59.4131C212.396 58.5262 212.441 57.5927 212.441 56.7059C212.487 54.8855 212.487 53.0651 212.487 51.2914C212.487 50.4979 212.396 49.7511 212.124 49.0043C211.942 48.4442 211.533 48.0708 210.761 48.2108C209.717 48.3975 209.308 49.0976 209.49 50.1712C209.49 50.2646 209.49 50.4046 209.49 50.4979C209.49 52.7851 209.49 55.0722 209.535 57.3593C209.535 58.2928 209.58 59.2264 209.626 60.1599C209.626 60.7667 209.535 60.86 208.99 60.9067C208.082 60.9534 207.173 60.9534 206.22 60.9067C205.947 60.9067 205.766 60.7667 205.811 60.4399C205.811 60.2999 205.811 60.1599 205.811 60.0199C205.766 57.8261 205.766 55.679 205.72 53.4852C205.72 52.3183 205.629 51.1047 205.584 49.9378C205.539 48.9576 205.493 47.9774 205.448 46.9972C205.402 46.3438 205.357 45.6903 205.312 44.9902C205.266 44.6167 205.448 44.3834 205.811 44.3367C206.492 44.2433 207.219 44.15 207.9 44.1033C208.263 44.0566 208.4 44.1967 208.4 44.6167C208.4 45.0368 208.354 45.4102 208.445 45.8303C208.536 45.9237 208.536 45.9703 208.581 46.0637Z" fill="white"/>
                        <path d="M224.567 53.8584C223.658 53.8584 222.75 53.8584 221.797 53.8584C221.206 53.8584 221.07 53.9984 221.07 54.6052C221.07 55.352 221.206 56.0521 221.433 56.7523C221.66 57.4057 222.024 57.7791 222.523 57.7791C223.068 57.7791 223.568 57.3591 223.704 56.7056C223.795 56.2855 223.886 55.9121 223.795 55.4453C223.704 55.0253 223.976 54.6985 224.385 54.6985C225.293 54.6518 226.202 54.6518 227.11 54.6985C227.382 54.6985 227.564 54.8386 227.564 55.1653C227.609 56.7056 227.246 58.1526 226.292 59.3661C225.747 60.0663 224.93 60.4397 224.113 60.7197C222.75 61.1398 221.433 61.1398 220.162 60.4864C219.026 59.9262 218.345 58.9927 217.936 57.8258C217.664 56.9857 217.528 56.0988 217.437 55.212C217.301 53.9517 217.301 52.6448 217.391 51.3845C217.437 50.311 217.664 49.2374 217.982 48.2105C218.254 47.3237 218.663 46.4835 219.299 45.8301C220.071 45.0832 220.979 44.6632 222.069 44.5698C223.749 44.4298 225.157 44.9432 226.338 46.1568C227.155 46.997 227.564 48.0705 227.746 49.2374C228.018 50.5444 228.018 51.898 227.973 53.2516C227.973 53.7183 227.746 53.905 227.292 53.905C226.429 53.8584 225.475 53.8584 224.567 53.8584ZM222.341 50.7777C222.75 50.7311 223.159 50.8244 223.568 50.7311C223.976 50.6377 224.113 50.451 224.067 50.0309C224.022 49.5642 223.886 49.0974 223.704 48.6306C223.522 48.1639 223.25 47.8371 222.705 47.7905C222.16 47.7438 221.66 47.8371 221.342 48.3506C220.979 48.9107 220.797 49.5642 220.707 50.2176C220.661 50.4977 220.797 50.6377 221.07 50.7311C221.479 50.7777 221.887 50.7311 222.341 50.7777Z" fill="white"/>
                        <path d="M193.686 52.9719C193.641 51.2915 193.868 49.5178 194.504 47.8842C195.185 46.0171 196.502 44.8502 198.5 44.6168C199.59 44.4768 200.635 44.5702 201.679 44.9903C202.86 45.5037 203.586 46.4372 203.995 47.6508C204.404 48.911 204.631 50.1713 204.722 51.4782C204.858 53.672 204.54 55.7724 203.723 57.7795C202.996 59.5065 201.724 60.5801 199.953 60.9068C198.636 61.1869 197.319 61.0002 196.184 60.16C195.321 59.5532 194.685 58.7597 194.367 57.7328C193.868 56.1925 193.686 54.6522 193.686 52.9719ZM201.179 52.4118C201.179 51.3382 201.089 50.3113 200.816 49.2378C200.635 48.5376 200.271 48.2109 199.681 48.1642C199.045 48.1176 198.5 48.3976 198.228 48.9577C198.046 49.2845 197.91 49.6579 197.819 50.0313C197.41 51.7583 197.41 53.532 197.546 55.3057C197.592 55.8191 197.728 56.2859 197.955 56.7526C198.5 57.6862 199.635 57.7795 200.317 56.8927C200.498 56.6126 200.635 56.3326 200.725 56.0058C201.043 54.8856 201.179 53.672 201.179 52.4118Z" fill="white"/>
                        <path d="M232.514 52.0384C232.469 53.5321 232.559 54.979 233.014 56.3793C233.15 56.7527 233.377 57.0795 233.74 57.2195C234.24 57.4062 234.739 57.1728 235.012 56.6594C235.33 56.1459 235.375 55.5391 235.33 54.979C235.284 54.1389 235.33 54.2789 236.011 54.2789C236.874 54.3256 237.782 54.4189 238.645 54.4656C239.008 54.5123 239.099 54.7456 239.099 55.0257C239.008 56.0526 238.826 57.0795 238.327 58.013C237.691 59.2732 236.647 59.88 235.33 60.2534C234.648 60.4401 233.922 60.4868 233.195 60.3935C231.152 60.1134 229.835 59.1332 229.29 56.8461C229.017 55.7725 228.79 54.699 228.745 53.5787C228.699 52.6452 228.699 51.7117 228.79 50.8249C228.926 49.658 229.199 48.5377 229.562 47.3708C230.243 45.1304 231.878 43.8701 234.149 43.8701C235.148 43.8701 236.102 44.0568 236.964 44.6169C238.009 45.3171 238.599 46.344 238.781 47.5575C238.872 48.3043 239.008 49.0045 238.872 49.7513C238.826 49.9847 238.736 50.078 238.554 50.1247C238.009 50.1714 237.464 50.2647 236.919 50.2647C236.556 50.2647 236.238 50.3581 235.875 50.3581C235.557 50.3581 235.42 50.2181 235.42 49.8913C235.466 49.2845 235.33 48.7244 235.148 48.1643C234.921 47.4175 234.285 47.0441 233.604 47.2308C233.241 47.3241 233.059 47.6042 232.923 47.9309C232.514 48.9111 232.378 49.9847 232.423 51.0116C232.514 51.4316 232.514 51.7117 232.514 52.0384Z" fill="white"/>
                        <path d="M242.914 52.9249C242.914 51.3379 242.914 50.0777 242.914 48.7707C242.914 48.1639 242.823 48.0706 242.233 48.0706C242.005 48.0706 241.778 48.0706 241.551 48.1173C240.779 48.304 240.371 47.9306 240.325 47.0904C240.325 46.4369 240.325 45.7835 240.325 45.13C240.325 44.7099 240.552 44.4765 240.961 44.4299C241.324 44.4299 241.642 44.4299 242.005 44.4299C242.641 44.4299 242.868 44.1965 242.868 43.543C242.868 42.4228 242.868 41.2559 242.868 40.1357C242.868 39.5289 242.959 39.4822 243.55 39.4355C244.322 39.3888 245.094 39.3422 245.866 39.2488C246.229 39.2021 246.501 39.3422 246.501 39.7622C246.501 40.9758 246.501 42.1894 246.456 43.4497C246.411 44.2432 246.683 44.4765 247.41 44.4299C247.909 44.3832 248.454 44.3832 248.954 44.4299C249.272 44.4299 249.408 44.6632 249.453 44.9433C249.453 45.6434 249.453 46.3436 249.453 47.0437C249.453 47.4171 249.226 47.6505 248.727 47.6038C248.136 47.6038 247.591 47.7439 247.001 47.6972C246.683 47.6505 246.547 47.8372 246.547 48.1639C246.547 50.5911 246.547 53.0649 246.592 55.4921C246.592 56.4723 247.455 57.2658 248.409 57.2658C248.636 57.2658 248.863 57.2658 249.135 57.2658C249.499 57.2658 249.635 57.3591 249.59 57.7325C249.499 58.5727 249.59 59.4596 249.499 60.2997C249.453 60.6265 249.362 60.8132 249.045 60.8598C248.091 60.9065 247.137 60.9532 246.184 60.7198C244.549 60.2997 243.277 58.8528 243.095 57.1258C242.823 55.6321 242.914 54.1385 242.914 52.9249Z" fill="white"/>
                        <path d="M279.971 43.4495C279.971 42.656 280.016 41.5825 279.88 40.5556C279.835 40.0888 279.88 39.6221 279.744 39.202C279.653 38.7352 279.88 38.3618 280.425 38.3618C281.424 38.3618 282.423 38.3618 283.377 38.3618C283.922 38.3618 284.058 38.4085 284.058 39.1086C284.058 40.0888 284.013 41.069 284.013 42.0959C283.922 46.3434 283.922 50.5443 283.967 54.7918C283.967 56.4255 284.013 58.0125 284.058 59.6461C284.058 59.9729 284.149 60.2996 284.058 60.6263C284.013 60.8597 283.922 60.953 283.695 60.953C282.696 61.0464 281.651 60.9997 280.652 60.9997C280.334 60.9997 280.198 60.673 280.198 60.3929C280.152 60.2062 280.152 59.9728 280.243 59.7861C280.289 59.6928 280.289 59.5528 280.243 59.4127C280.243 59.226 280.152 59.1327 279.971 59.0393C279.789 58.946 279.698 59.086 279.607 59.1794C279.29 59.5528 278.926 59.8795 278.427 60.0195C277.564 60.2996 276.701 60.3463 275.793 60.0662C274.884 59.7861 274.112 59.3194 273.613 58.5259C273.25 57.9658 272.977 57.359 272.795 56.7055C272.341 55.2586 272.341 53.7183 272.296 52.2246C272.296 50.591 272.523 49.004 273.159 47.5103C274.158 45.1765 276.247 44.4297 278.381 45.0832C278.881 45.2699 279.244 45.6433 279.653 45.97C279.698 46.0167 279.744 46.11 279.88 46.0634C279.971 46.0167 279.925 45.9233 279.925 45.83C279.971 45.1765 279.971 44.4297 279.971 43.4495ZM279.971 52.0846C279.971 51.3378 279.971 50.7777 279.971 50.2175C279.925 49.4241 279.471 48.9573 278.79 48.6772C278.2 48.4439 277.609 48.5839 277.11 49.0973C276.792 49.4241 276.61 49.8441 276.519 50.3109C276.156 52.1313 276.201 53.905 276.746 55.6786C276.928 56.2854 277.291 56.7055 277.927 56.7989C278.972 56.8922 280.016 55.9587 280.016 54.8852C280.062 53.8583 279.88 52.8781 279.971 52.0846Z" fill="white"/>
                        <path d="M307.9 54.4653V59.7397C307.9 59.8797 307.9 60.0664 307.855 60.2065C307.764 60.5799 307.582 60.7666 307.219 60.7199C306.629 60.7199 306.038 60.7199 305.493 60.7666C305.221 60.7666 304.994 60.8599 304.721 60.8599C304.267 60.8133 304.04 60.5799 303.995 60.1131C303.995 59.9731 303.995 59.8797 303.995 59.7397C303.995 59.6464 303.995 59.5997 303.949 59.553C303.858 59.5063 303.813 59.5997 303.768 59.6464C302.632 60.5799 301.315 60.7199 299.953 60.4865C298.818 60.3465 297.955 59.7864 297.319 58.8529C296.91 58.2461 296.729 57.5926 296.683 56.8925C296.592 56.0056 296.638 55.1655 296.819 54.2786C297.183 52.6916 298.136 51.8048 299.635 51.3847C300.816 51.058 302.042 51.058 303.268 51.198C303.631 51.2447 303.949 51.0113 303.949 50.5912C303.949 49.7977 304.04 49.0042 303.768 48.2107C303.586 47.6973 303.132 47.3705 302.632 47.3705C301.542 47.4172 301.088 47.9307 301.043 49.1442C301.043 49.2843 301.043 49.4243 301.043 49.5643C300.997 49.8444 300.861 50.0311 300.543 50.0311C300.044 50.0311 299.544 49.9844 299.045 49.8911C298.59 49.8444 298.136 49.8444 297.682 49.7977C297.364 49.751 297.183 49.611 297.183 49.2843C297.183 47.744 297.546 46.3437 298.636 45.2701C299.272 44.6167 300.135 44.3366 300.997 44.1966C302.36 44.0099 303.722 44.0099 305.039 44.5233C306.583 45.1301 307.491 46.2503 307.764 47.9307C308.036 49.7043 307.9 51.478 307.946 53.2517C307.9 53.6718 307.9 54.0452 307.9 54.4653ZM303.904 55.3055C303.904 55.0254 303.904 54.7454 303.904 54.512C303.904 54.0919 303.813 53.9519 303.404 53.8585C302.859 53.7652 302.314 53.9052 301.815 54.0919C301.179 54.3253 300.861 54.792 300.725 55.4455C300.543 56.2857 301.088 57.0792 301.906 57.1725C302.451 57.2659 302.996 57.1258 303.495 56.8458C303.858 56.6591 303.995 56.379 303.949 56.0056C303.904 55.7722 303.904 55.5389 303.904 55.3055Z" fill="white"/>
                        <path d="M293.05 53.9056C291.87 53.9056 290.644 53.9056 289.463 53.9056C288.827 53.9056 288.691 54.1389 288.736 54.7924C288.782 55.4926 288.872 56.1927 289.099 56.8462C289.327 57.453 289.781 57.8264 290.326 57.7797C290.871 57.733 291.37 57.1729 291.461 56.6128C291.552 56.146 291.506 55.7259 291.506 55.2592C291.506 54.9791 291.597 54.7924 291.915 54.7457C292.006 54.7457 292.097 54.6991 292.188 54.6991C293.05 54.6991 293.868 54.6991 294.731 54.6991C295.139 54.6991 295.276 54.8391 295.276 55.2125C295.276 56.7995 294.912 58.2931 293.868 59.5067C293.187 60.3002 292.278 60.6269 291.325 60.8603C290.871 60.9537 290.462 61.047 290.008 61.0003C288.963 60.9537 288.055 60.7203 287.192 60.0668C286.148 59.2733 285.648 58.1998 285.376 56.9395C285.012 55.2125 284.921 53.4855 285.012 51.7585C285.103 49.9848 285.376 48.2578 286.284 46.7175C286.965 45.5039 288.055 44.8037 289.417 44.5704C290.734 44.337 291.961 44.617 293.096 45.3638C294.413 46.2507 295.139 47.5109 295.412 49.0513C295.594 50.1248 295.684 51.1984 295.684 52.2719C295.684 52.6453 295.594 53.0187 295.594 53.3921C295.594 53.7189 295.367 53.8589 295.049 53.8589H293.05V53.9056ZM289.962 50.7783C290.371 50.7316 290.825 50.8249 291.234 50.7316C291.552 50.6382 291.733 50.4515 291.688 50.1248C291.643 49.518 291.461 48.9112 291.188 48.3978C291.007 48.0711 290.734 47.8844 290.416 47.8377C289.69 47.6976 289.145 48.0244 288.827 48.7245C288.6 49.1913 288.464 49.7047 288.373 50.1715C288.282 50.5449 288.418 50.7316 288.782 50.7783C289.145 50.8249 289.554 50.7783 289.962 50.7783Z" fill="white"/>
                        <path d="M313.849 61.0001C312.987 61.0468 312.124 60.9068 311.306 60.5334C309.899 59.8799 309.217 58.713 308.899 57.2194C308.809 56.7526 308.718 56.2859 308.718 55.7724C308.763 55.3523 308.899 55.119 309.308 55.119C309.944 55.0723 310.58 55.0723 311.216 55.0256C311.397 55.0256 311.533 55.0256 311.715 54.9789C312.124 54.8856 312.396 55.119 312.442 55.539C312.442 55.9591 312.532 56.3325 312.669 56.7059C312.85 57.2661 313.305 57.5928 313.895 57.6395C314.394 57.6395 314.849 57.4528 315.076 56.9393C315.348 56.4259 315.166 55.5857 314.712 55.119C314.122 54.4655 313.35 54.1388 312.578 53.8587C311.76 53.532 310.988 53.2052 310.353 52.6451C309.671 51.9917 309.217 51.1515 309.036 50.218C308.809 49.0511 308.854 47.9308 309.399 46.8573C309.944 45.6904 310.852 44.8969 312.124 44.6168C313.713 44.2434 315.257 44.3835 316.711 45.1769C317.982 45.8771 318.572 46.9973 318.663 48.491C318.663 48.8177 318.663 49.1444 318.663 49.4711C318.663 49.8446 318.482 49.9846 318.164 50.0313C317.664 50.0779 317.119 50.0779 316.62 50.0779C316.393 50.0779 316.166 50.0779 315.938 50.1246C315.484 50.218 315.212 49.9379 315.166 49.4711C315.166 49.1911 315.212 48.8644 315.121 48.5843C314.939 48.0242 314.667 47.7441 314.258 47.7441C313.486 47.6975 313.123 47.9308 312.896 48.5376C312.578 49.3311 312.805 50.0779 313.441 50.5914C314.167 51.1982 314.985 51.6182 315.848 51.945C317.392 52.5518 318.436 53.5786 318.845 55.259C319.208 56.7059 318.754 57.9662 317.982 59.1331C317.255 60.16 315.711 61.1402 313.849 61.0001Z" fill="white"/>
                        <path d="M266.802 56.3325C266.802 53.952 266.847 51.5248 266.802 49.1443C266.756 47.6507 266.756 46.1571 266.756 44.6168C266.756 43.7766 266.756 42.8897 266.666 42.0496C266.575 41.0694 266.529 40.0425 266.529 39.0623C266.529 38.5955 266.756 38.2221 267.256 38.1288C267.983 37.9888 268.709 37.9421 269.436 37.8487C269.799 37.802 270.162 37.7554 270.526 37.7554C270.934 37.7554 271.116 37.8954 271.071 38.3622C271.025 39.1557 271.025 39.9491 270.98 40.7426C270.98 42.423 270.889 44.1033 270.934 45.7837C270.98 47.7907 270.934 49.7978 270.889 51.8516C270.844 54.1387 270.844 56.4258 270.844 58.7596C270.844 59.6465 270.617 59.8798 269.799 59.8798C269.027 59.8798 268.255 59.9732 267.528 60.1132C267.074 60.2066 266.847 59.9732 266.847 59.5064C266.847 58.4329 266.847 57.3593 266.847 56.3325H266.802Z" fill="white"/>
                        </g>
                        <defs>
                        <clipPath id="clip0">
                        <rect width="230" height="33" fill="white" transform="translate(148 28)"/>
                        </clipPath>
                        </defs>

                        <g style="mix-blend-mode:multiply">
                        <rect x="149" y="96" width="230" height="185" rx="10" fill="#D9E2EF"/>
                        </g>
                        <path d="M264.698 197.408C269.644 191.872 274.318 186.653 279.219 181.208C289.474 195.139 299.593 208.888 309.985 223.001C271.187 223.001 232.843 223.001 194 223.001C207.976 202.172 221.726 181.662 235.656 160.879C245.413 173.131 254.942 185.201 264.698 197.408Z" fill="white"/>
                        <path d="M301 155.842C300.91 162.83 295.192 168.457 288.159 168.412C281.216 168.366 275.544 162.603 275.589 155.57C275.635 148.582 281.397 142.955 288.431 143C295.374 143.046 301.046 148.854 301 155.842Z" fill="white"/>
                    </svg>

                    <div class="titulo_izquierdo">Formulario de registro </div>
                    <div class="subtitulo_izquierdo">Completa las secciones del formulario para inscribirte al programa Conecta Ideas. </div>

                    <div class="contenedor_opcion_1">
                        <!-- <svg width="250" height="32" viewBox="0 0 250 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="16" cy="16" r="16" fill="white"/>
                            <circle cx="16" cy="16" r="16" stroke="black"/>
                            <path d="M13.4422 23C13.0989 23 12.7555 22.855 12.4809 22.6011L8.42957 18.5399C7.88024 17.996 7.8459 17.0532 8.39523 16.473C8.91023 15.8928 9.80289 15.8566 10.3522 16.4368L13.4079 19.5189L21.6135 10.4537C22.1284 9.87351 23.0211 9.83725 23.5704 10.4174C24.1198 10.9613 24.1541 11.9041 23.6048 12.4843L14.4378 22.5649C14.1632 22.855 13.8199 23 13.4422 23Z" fill="#2AB7CA"/>
                        </svg> -->
                        <div class="svg_opcion_1">
                            <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="16" cy="16" r="15" stroke="white" stroke-width="2"/>
                                <text x="14" y="20" fill="white">1</text>
                            </svg>
                        </div>
                        

                        <div class="opcion">
                            datos personales
                        </div>
                    </div>

                    <div class="contenedor_opcion_2">
                        <!-- <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="16" cy="16" r="15" stroke="white" stroke-width="2"/>
                            <text x="12" y="20" fill="white">2</text>
                        </svg> -->
                        <div class="svg_opcion_2">
                            <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="16" cy="16" r="16" fill="#67CEDC"/>
                                <circle cx="16" cy="16" r="16" stroke="#67CEDC"/>
                                <text x="12" y="20" fill="white">2</text>
                            </svg>
                        </div>
                        <div class="opcion">
                            Datos de tu I.E
                        </div>
                    </div>

                    <div class="contenedor_opcion_3">
                        <div class="svg_opcion_3">
                            <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="16" cy="16" r="16" fill="#67CEDC"/>
                                <circle cx="16" cy="16" r="16" stroke="#67CEDC"/>
                                <text x="12" y="20" fill="white">3</text>
                            </svg>
                        </div>
                        <div class="opcion">
                            Datos de tu cargo
                        </div>
                    </div>

                    <div class="contenedor_opcion_4">
                        <div class="svg_opcion_4">
                            <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="16" cy="16" r="16" fill="#67CEDC"/>
                                <circle cx="16" cy="16" r="16" stroke="#67CEDC"/>
                                <text x="12" y="20" fill="white">4</text>
                            </svg>
                        </div>
                        <div class="opcion">
                            contraseña
                        </div>
                    </div>



                    

                    
                    


                </div>
            </div>

            <div class="col-md-8 col-lg-8 col-xl-8 col-xxl-8">
                <div class="row formulario_1" >
                    <div class="col-md-12 col-lg-12 col-xl-12 col-xxl-12">
                        <div class="titulo_derecho">Datos personales</div>
                    </div>

                    <div class="col-md-12 col-lg-12 col-xl-12 col-xxl-12">
                        <div class="subtitulo_derecho">Empecemos llenando la información sobre ti</div>
                    </div>

                    <div class="col-md-12 col-lg-12 col-xl-12 col-xxl-12">
                        <div class="contenedor_derecho_1">
                            <div class="row justify-content-center">
                                <div class="col-md-11" style="top:16px">
                                    <div class="form-group">
                                        <label for="">Número de DNI</label>
                                        <input type="text" class="form-control numerico error_dni" id="dni" name="Registro[dni]" placeholder="Ingresa tu número de DNI" maxlength="8">
                                    </div>
                                </div>

                                <div class="col-md-11" style="top:16px">
                                    <div class="form-group">
                                        <label for="">Nombre</label>
                                        <input type="text" class="form-control texto error_nombres" id="nombres" name="Registro[nombres]" placeholder="Escribe tu nombre" maxlength="30">
                                    </div>
                                </div>

                                <div class="col-md-11" style="top:16px">
                                    <div class="form-group">
                                        <label for="">Apellido paterno</label>
                                        <input type="text" class="form-control texto error_apellido_paterno" id="apellido_paterno" name="Registro[apellido_paterno]" placeholder="Escribe tu apellido paterno" maxlength="30">
                                    </div>
                                </div>

                                <div class="col-md-11" style="top:16px">
                                    <div class="form-group">
                                        <label for="">Apellido materno</label>
                                        <input type="text" class="form-control texto error_apellido_materno" id="apellido_materno" name="Registro[apellido_materno]" placeholder="Escribe tu apellido paterno" maxlength="30">
                                    </div>
                                </div>

                                <div class="col-md-11" style="top:16px">
                                    <div class="form-group">
                                        <label for="">Fecha de nacimiento</label>
                                        <input type="text" class="form-control error_fecha_nacimiento" id="fecha_nacimiento" name="Registro[fecha_nacimiento]" placeholder="Ingrese fecha nacimiento" maxlength="10" data-date-format="dd-mm-yyyy">

                                        <!-- <input type="date" class="form-control error_fecha_nacimiento" id="fecha_nacimiento" name="Registro[fecha_nacimiento]" placeholder="Ingrese fecha nacimiento" maxlength="10" data-provide="datepicker"> -->
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="contenedor_derecho_2">
                            <div class="row justify-content-center">
                                <div class="col-md-11" style="top:16px">
                                    <div class="form-group">
                                        <label for="">Correo electrónico</label>
                                        <input type="email" class="form-control error_correo_electronico" id="correo_electronico" name="Registro[correo_electronico]" placeholder="Escribe tu correo electrónico">
                                    </div>
                                </div>

                                <div class="col-md-11" style="top:16px">
                                    <div class="form-group">
                                        <label for="">Número de celular</label>
                                        <input type="text" class="form-control numerico error_celular" id="celular" name="Registro[celular]" placeholder="Ingresa tu número de celular" maxlength="9">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="contenedor_boton text-right" style="top: 930px;">
                            <button type="button" class="btn btn-primary form_1">Siguiente</button>
                        </div>
                    </div>
                </div>


                <div class="row formulario_2" style="display:none" >
                    <div class="col-md-12 col-lg-12 col-xl-12 col-xxl-12">
                        <div class="titulo_derecho">Datos de tu institución educativa</div>
                    </div>

                    <div class="col-md-12 col-lg-12 col-xl-12 col-xxl-12">
                        <div class="subtitulo_derecho">Cuéntanos sobre la institución educativa en donde trabajas</div>
                    </div>

                    <div class="col-md-12 col-lg-12 col-xl-12 col-xxl-12">
                        <div class="contenedor_derecho_3">
                            <div class="row justify-content-center">
                                <div class="col-md-11" style="top:16px">
                                    <div class="form-group">
                                        <label for="">Elige la forma en que deseas completar la información</label>
                                        <select id="elegir_medio" class="form-control error_elegir_medio">
                                            <option value>Selecciona una opción</option>
                                            <option value="1">Por código modular</option>
                                            <option value="2">Por ubicación</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-9 " style="top:16px">
                                    <div class="form-group">
                                        <label for="">Ingresa el código modular de tu I.E:</label>
                                        <input type="text" class="form-control error_codigo_modular" id="codigo_modular" name="Registro[codigo_modular]" disabled placeholder="Ingresa el código">
                                    </div>
                                </div>
                                <div class="col-md-2 " style="top:16px">
                                    <div class="form-group">
                                        <label for="">&nbsp</label>
                                        <button type="button" class="btn btn-primary consultar_codigo" disabled>Consultar</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="contenedor_derecho_6">
                            <div class="row justify-content-center">
                                <div class="col-md-11" style="top:16px">
                                    <div class="form-group">
                                        <label for="">Departamento</label>
                                        <select class="form-control error_departamento" id="departamento" name="Registro[departamento]" disabled>
                                            <option value>Selecciona una opción</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-11" style="top:16px">
                                    <div class="form-group">
                                        <label for="provincia">Provincia</label>
                                        <select class="form-control error_provincia" id="provincia" name="Registro[provincia]" disabled>
                                            <option value>Selecciona una opción</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-11" style="top:16px">
                                    <div class="form-group">
                                        <label for="distrito">Distrito</label>
                                        <select class="form-control error_distrito" id="distrito" name="Registro[distrito]" disabled>
                                            <option value>Selecciona una opción</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-11" style="top:16px">
                                    <div class="form-group">
                                        <label for="institucion_educativa_id">Nombre de tu Institución Educativa (I.E.)</label>
                                        <select class="form-control error_institucion_educativa_id" id="institucion_educativa_id" name="Registro[institucion_educativa_id]" disabled>
                                            <option value>Selecciona una opción</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="contenedor_derecho_7">
                            <div class="row justify-content-center">
                                <div class="col-md-11" style="top:16px">
                                    <div class="form-group">
                                        <label for="tipo_institucion_educativa">Tipo de Institución Educativa:</label>
                                        <select class="form-control error_tipo_institucion_educativa" id="tipo_institucion_educativa" name="Registro[tipo_institucion_educativa]">
                                            <option value>Selecciona una institución educativa</option>
                                        </select>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="contenedor_boton text-right" style="top: 930px;">
                            <button type="button" class="btn btn-primary form_2">Siguiente</button>
                        </div>
                    </div>
                </div>


                <div class="row formulario_3" style="display:none">
                    <div class="col-md-12 col-lg-12 col-xl-12 col-xxl-12">
                        <div class="titulo_derecho">Datos de tu cargo</div>
                    </div>

                    <div class="col-md-12 col-lg-12 col-xl-12 col-xxl-12">
                        <div class="subtitulo_derecho">Cuéntanos qué cargo tienes en la institución educativa </div>
                    </div>

                    <div class="col-md-12 col-lg-12 col-xl-12 col-xxl-12">
                        <div class="contenedor_derecho_4">
                            <div class="row justify-content-center">
                                <div class="col-md-11" style="top:16px">
                                    <div class="form-group">
                                        <label for="">Selecciona tu cargo</label>
                                        <select class="form-control error_cargo_id" id="cargo_id" name="Registro[cargo_id]">
                                            <option value>Selecciona una opción</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-lg-12 col-xl-12 col-xxl-12">
                        <div class="contenedor_derecho_8" style="display:none">
                            <div class="row justify-content-center tipo_grado">
                            </div>
                        </div>

                        <div class="contenedor_boton text-right" style="top: 930px;">
                            <button type="button" class="btn btn-primary form_3">Siguiente</button>
                        </div>
                    </div>
                </div>


                <div class="row formulario_4" style="display:none">
                    <div class="col-md-12 col-lg-12 col-xl-12 col-xxl-12">
                        <div class="titulo_derecho">¡Ya casi has terminado!</div>
                    </div>

                    <div class="col-md-12 col-lg-12 col-xl-12 col-xxl-12">
                        <div class="subtitulo_derecho">Como paso final, crea la contraseña que te servirá para ingresar al programa, asegúrate de recordarla:</div>
                    </div>

                    <div class="col-md-12 col-lg-12 col-xl-12 col-xxl-12">
                        <div class="contenedor_derecho_5">
                            <div class="row justify-content-center">
                                <div class="col-md-11" style="top:16px">
                                    <div class="form-group">
                                        <label for="">Crea tu contraseña:</label>
                                        <input type="password" class="form-control error_clave" id="clave" name="Registro[clave]" placeholder="Escribe tu contraseña" maxlength="20">
                                        <p class="error_clave_text text-danger"></p>
                                    </div>
                                </div>

                                <div class="col-md-11" style="top:16px">
                                    <div class="form-group">
                                        <label for="">Confirma tu contraseña:</label>
                                        <input type="password" class="form-control" id="clave_confirma" placeholder="Escribe tu contraseña" maxlength="20">
                                    </div>
                                </div>

                            </div>
                        </div>


                        <div class="contenedor_boton text-right" style="top: 930px;">
                            <button type="button" class="btn btn-primary btn-grabar-registro">Finalizar</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
<!-- /.content -->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog text-center" role="document">
        <i class="fa fa-refresh fa-spin"></i>
        Cargando
    </div>
</div>



<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =  $('#staticBackdrop');
var final = $('#myModal');
// //loading.modal("show");
// loading.modal('hide');
const delay = ms => new Promise(res => setTimeout(res, ms));

$("#fecha_nacimiento").datepicker({
    language : 'es',
    closeText: 'Cerrar',
    prevText: '<Ant',
    nextText: 'Sig>',
    currentText: 'Hoy',
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
    dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
    dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
    weekHeader: 'Sm',
    dateFormat: 'dd-mm-yyyy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
});


// $("#codigo_modular").select2({
//     ajax: {
//         url: '<?= \Yii::$app->request->BaseUrl ?>/institucion-educativa/get-lista-instituciones-educativas',
//         dataType: "json",
//         delay: 250,
//         data: function (params) {
//             return {
//                 q: params.term, // search term
//             };
//         },
//         processResults: function (data, params) {
//             // parse the results into the format expected by Select2
//             // since we are using custom formatting functions we do not need to
//             // alter the remote JSON data, except to indicate that infinite
//             // scrolling can be used
//             //params.page = params.page || 1;

//             return {
//                 results: data.results,
//             };
//         },
//         cache: true,
//     },
//     placeholder: "Buscar código modular",
// });

$('body').on('click', '.consultar_codigo', function (e,data) {
    InstitucionEducativa($('#codigo_modular').val());
});


// $('#codigo_modular').on('select2:select', function (e) {
//     var data = e.params.data;

//     departamento = $('#departamento').val(data.departamento);
//     Provincias(departamento);
//     // provincia = $('#provincia').val(data.provincia);
//     // Distritos(provincia);
//     // distrito = $('#distrito').val(data.distrito);


//     console.log(data);
// });


TipoInstitucionEducativa()
async function TipoInstitucionEducativa(){
    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/tipo-institucion-educativa/get-tipos-instituciones-educativas',
        method: 'POST',
        data:{"_csrf":csrf},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {
            if(results.success){
                var tipo_institucion_educativa_options = "<option value>Selecciona un tipo de institución educativa</option>";
                $.each(results.data, function( index, value ) {
                    tipo_institucion_educativa_options = tipo_institucion_educativa_options +  "<option value='" + value.id + "'>" + value.descripcion + "</option>";
                });
                $('#tipo_institucion_educativa').html(tipo_institucion_educativa_options);
            }
        },
        error:function(){
            alert('No hay conectividad con el sistema');
        }
    });
}



Regiones()
async function Regiones(){
    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-regiones',
        method: 'POST',
        data:{"_csrf":csrf},
        dataType:'Json',
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success:function(results)
        {
            if(results.success){
                var regiones_options = "<option value>Selecciona una opción</option>";
                $.each(results.data, function( index, value ) {
                    regiones_options +=  "<option value='" + value.departamento + "'>" + value.departamento + "</option>";
                });
                $('#departamento').html(regiones_options);
                loading.modal('hide');
            }
        },
        error:function(){
            alert('No hay conectividad con el sistema');
        }
    });
}


async function Provincias(departamento,provincia){
    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-provincias',
        method: 'POST',
        data:{"_csrf":csrf,departamento:departamento},
        dataType:'Json',
        beforeSend:function()
        {
            
            //loading.modal("show");
        },
        success:function(results)
        {
            if(results.success){
                var provincias_options = "<option value>Selecciona una opción</option>";
                $.each(results.data, function( index, value ) {
                    provincias_options +=  "<option value='" + value.provincia + "'>" + value.provincia + "</option>";
                });
                $('#provincia').html(provincias_options);
                if(provincia){
                    $('#provincia').val(provincia);
                }
                loading.modal('hide');
            }
        },
        error:function(){
            alert('No hay conectividad con el sistema');
        }
    });
}


async function Distritos(provincia,distrito){
    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-distritos',
        method: 'POST',
        data:{"_csrf":csrf,provincia:provincia},
        dataType:'Json',
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success:function(results)
        {
            if(results.success){
                var distritos_options = "<option value>Selecciona una opción</option>";
                $.each(results.data, function( index, value ) {
                    distritos_options +=  "<option value='" + value.distrito + "'>" + value.distrito + "</option>";
                });
                $('#distrito').html(distritos_options);
                if(distrito){
                    $('#distrito').val(distrito);
                }
                loading.modal('hide');
            }
        },
        error:function(){
            alert('No hay conectividad con el sistema');
        }
    });
}

function wait(timeout) {
    return new Promise(resolve => {
        setTimeout(resolve, timeout);
    });
}


function InstitucionEducativa(codigo_modular){
    codigo_modular = ("0000000" + codigo_modular).substr(-7,7)

    $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/institucion-educativa/get-institucion-educativa-codigo-modular',
        method: 'POST',
        data:{"_csrf":csrf,codigo_modular:codigo_modular},
        dataType:'Json',
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success:function(results)
        {
            if(results.success){

                // async () => {
                //     await delay(5000);
                //     console.log("Waited 5s");

                //     await delay(5000);
                //     console.log("Waited an additional 5s");
                // };
                if(results.data.length>0){
                    departamento = results.data[0].departamento;
                    provincia = results.data[0].provincia;
                    distrito = results.data[0].distrito;
                    institucion_educativa = results.data[0].id;
                    $('#departamento').val(departamento);
                    Provincias(departamento,provincia);
                    Distritos(provincia,distrito);
                    InstitucionesEducativas(departamento,provincia,distrito,institucion_educativa);
                }

                if(results.data.length==0){
                    alert("El código modular no se encuentra.")
                }
                loading.modal('hide');
            }
        },
        error:function(){
            alert('No hay conectividad con el sistema');
        }
    });
}


async function InstitucionesEducativas(departamento,provincia,distrito,institucion_educativa){
    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/institucion-educativa/get-instituciones-educativas',
        method: 'POST',
        data:{"_csrf":csrf,departamento:departamento,provincia:provincia,distrito:distrito},
        dataType:'Json',
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success:function(results)
        {
            if(results.success){
                var instituciones_educativas_options = "<option value>Selecciona una opción</option>";
                $.each(results.data, function( index, value ) {
                    instituciones_educativas_options +=  "<option value='" + value.id + "' data-cod_modular='" + value.cod_mod + "'> " + value.nombre_ie + "</option>";
                });
                $('#institucion_educativa_id').html(instituciones_educativas_options);
                // $('#institucion_educativa_id').select2({multiple:false,placeholder: "Seleccione institución educativa"});
                // $('#institucion_educativa_id').val(null).trigger('change');

                if(institucion_educativa){
                    $('#institucion_educativa_id').val(institucion_educativa).trigger('change');
                }

                loading.modal('hide');
            }
        },
        error:function(){
            alert('No hay conectividad con el sistema');
        }
    });
}

async function Cargos(tipo_institucion_id){
    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/cargo/get-cargos-tipo-institucion',
        method: 'POST',
        data:{"_csrf":csrf,tipo_institucion_id:tipo_institucion_id},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {
            if(results.success){
                var cargos_options = "<option value>Selecciona una opción</option>";
                $.each(results.data, function( index, value ) {
                    cargos_options +=  "<option value='" + value.id + "'>" + value.descripcion + "</option>";
                });
                $('#cargo_id').html(cargos_options);
            }
        },
        error:function(){
            alert('No hay conectividad con el sistema');
        }
    });
}



var tipo;

async function Grados(){
    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/grado/get-grados',
        method: 'POST',
        data:{"_csrf":csrf},
        dataType:'Json',
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success:function(results)
        {
            //$('#grado').select2('destroy');
            if(results.success){
                var grados_options="";
                
                

                if(tipo==1){
                    $(".contenedor_derecho_8").css("height", "100px");
                    // $.each(results.data, function( index, value ) {
                    //     grados_options +=  "<option value='" + value.grado + "'>" + value.descripcion + "</option>";
                    // });
                    // // $('#grado').select2({multiple:false,placeholder: "Seleccione grado"});
                    // // $('#grado').val(null).trigger('change');

                    // $('#grado').html(grados_options);

                    grados_options += `<div class="col-md-5" style="top:16px">
                                            <div class="form-group">
                                                <label for="">Selecciona el grado:</label> <br>`
                            grados_options += ` <select class="form-control error_grado" id="grado" name="Registro[grado]">`;
                                grados_options += `<option value>Seleccione una opción</option>`;
                                $.each(results.data, function( index, value ) {
                                    var descripcion = value.descripcion;
                                    var grado = value.grado;
                                    grados_options +=  `<option value="${grado}">${descripcion} </option>`;
                                });
                            grados_options += ` </select>`;
                        grados_options += `</div>`;
                    grados_options += `</div>`;

                    grados_options += `<div class="col-md-5" style="top:16px">
                                            <div class="form-group">
                                                <label for="">Selecciona el aula:</label> <br>`;
                            grados_options += ` <select class="form-control error_seccion" id="seccion" name="Registro[seccion]">`;
                                grados_options += `<option value>Seleccione una opción</option>`;
                                grados_options += `<option value="A">Sección A</option>`;
                                grados_options += `<option value="B">Sección B</option>`;
                                grados_options += `<option value="C">Sección C</option>`;
                                grados_options += `<option value="D">Sección D</option>`;
                                grados_options += `<option value="E">Sección E</option>`;
                                grados_options += `<option value="F">Sección F</option>`;
                                grados_options += `<option value="G">Sección G</option>`;
                                grados_options += `<option value="H">Sección H</option>`;
                                grados_options += `<option value="I">Sección I</option>`;
                                grados_options += `<option value="J">Sección J</option>`;
                            grados_options += ` </select>`;
                        grados_options += `</div>`;
                    grados_options += `</div>`;

                    $('.tipo_grado').html(grados_options);
                }

                if(tipo==2){
                    // $.each(results.data, function( index, value ) {
                    //     grados_options +=  "<option value='" + value.grado + "'>" + value.descripcion + "</option>";
                    // });
                    // $('#grado').select2({multiple:true,placeholder: "Seleccione grado"});
                    // $('#grado').val(null).trigger('change');
                    // $('#grado').html(grados_options);
                    $(".contenedor_derecho_8").css("height", "530px");
                    grados_options += ` <div class="col-md-11" style="top:16px">
                                            <div class="form-group">
                                                <label for="">Selecciona los grados que deseas inscribir al programa:</label>
                                             </div>
                                         </div>`;
                    grados_options += ` <div class="col-md-11" style="top:16px">
                                            <div class="form-group">
                                                <label for="" class="mt-3">4to de Primaria:</label>`;
                            grados_options += ` <div class="btn-group-toggle seccion_4_contenido" data-toggle="buttons">`;
                                grados_options +=  `<label class="btn btn-primario mt-3"><input type="checkbox" value="A" id="seccion_4_A" name="Registro[seccion_4]"> A</label>&nbsp&nbsp`;
                                grados_options +=  `<label class="btn btn-primario mt-3"><input type="checkbox" value="B" id="seccion_4_B" name="Registro[seccion_4]"> B</label>&nbsp&nbsp`;
                                grados_options +=  `<label class="btn btn-primario mt-3"><input type="checkbox" value="C" id="seccion_4_C" name="Registro[seccion_4]"> C</label>&nbsp&nbsp`;
                                grados_options +=  `<label class="btn btn-primario mt-3"><input type="checkbox" value="D" id="seccion_4_D" name="Registro[seccion_4]"> D</label>&nbsp&nbsp`;
                                grados_options +=  `<label class="btn btn-primario mt-3"><input type="checkbox" value="E" id="seccion_4_E" name="Registro[seccion_4]"> E</label>&nbsp&nbsp`;
                                grados_options +=  `<label class="btn btn-primario mt-3"><input type="checkbox" value="F" id="seccion_4_F" name="Registro[seccion_4]"> F</label>&nbsp&nbsp`;
                                grados_options +=  `<label class="btn btn-primario mt-3"><input type="checkbox" value="G" id="seccion_4_G" name="Registro[seccion_4]"> G</label>&nbsp&nbsp`;
                                grados_options +=  `<label class="btn btn-primario mt-3"><input type="checkbox" value="H" id="seccion_4_H" name="Registro[seccion_4]"> H</label>&nbsp&nbsp`;
                                grados_options +=  `<label class="btn btn-primario mt-3"><input type="checkbox" value="I" id="seccion_4_I" name="Registro[seccion_4]"> I</label>&nbsp&nbsp`;
                                grados_options +=  `<label class="btn btn-primario mt-3"><input type="checkbox" value="J" id="seccion_4_J" name="Registro[seccion_4]"> J</label>&nbsp&nbsp`;
                            grados_options += ` </div>`;
                        grados_options += ` </div>`;
                    grados_options += ` </div>`;

                    grados_options += ` <div class="col-md-11" style="top:16px">
                                                <div class="input-group">
                                                    <input type="text" class="form-control"  id="otros_seccion_4" placeholder="Agregar sección">
                                                    <div class="input-group-prepend">
                                                        <div class="btn input-group-text btn_agregar_seccion_4">+</div>
                                                        <!--<div class="btn input-group-text btn_agregar_seccion_demo">+</div>-->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>`;

                    grados_options += ` <div class="col-md-11" style="top:16px">
                                            <div class="form-group">
                                                <label for="" class="mt-3">5to de Primaria:</label>`;
                            grados_options += ` <div class="btn-group-toggle seccion_5_contenido" data-toggle="buttons">`;
                                grados_options +=  `<label class="btn btn-primario mt-3"><input type="checkbox" value="A" id="seccion_5_A" name="Registro[seccion_5]"> A</label>&nbsp&nbsp`;
                                grados_options +=  `<label class="btn btn-primario mt-3"><input type="checkbox" value="B" id="seccion_5_B" name="Registro[seccion_5]"> B</label>&nbsp&nbsp`;
                                grados_options +=  `<label class="btn btn-primario mt-3"><input type="checkbox" value="C" id="seccion_5_C" name="Registro[seccion_5]"> C</label>&nbsp&nbsp`;
                                grados_options +=  `<label class="btn btn-primario mt-3"><input type="checkbox" value="D" id="seccion_5_D" name="Registro[seccion_5]"> D</label>&nbsp&nbsp`;
                                grados_options +=  `<label class="btn btn-primario mt-3"><input type="checkbox" value="E" id="seccion_5_E" name="Registro[seccion_5]"> E</label>&nbsp&nbsp`;
                                grados_options +=  `<label class="btn btn-primario mt-3"><input type="checkbox" value="F" id="seccion_5_F" name="Registro[seccion_5]"> F</label>&nbsp&nbsp`;
                                grados_options +=  `<label class="btn btn-primario mt-3"><input type="checkbox" value="G" id="seccion_5_G" name="Registro[seccion_5]"> G</label>&nbsp&nbsp`;
                                grados_options +=  `<label class="btn btn-primario mt-3"><input type="checkbox" value="H" id="seccion_5_H" name="Registro[seccion_5]"> H</label>&nbsp&nbsp`;
                                grados_options +=  `<label class="btn btn-primario mt-3"><input type="checkbox" value="I" id="seccion_5_I" name="Registro[seccion_5]"> I</label>&nbsp&nbsp`;
                                grados_options +=  `<label class="btn btn-primario mt-3"><input type="checkbox" value="J" id="seccion_5_J" name="Registro[seccion_5]"> J</label>&nbsp&nbsp`;
                            grados_options += ` </div>`;
                        grados_options += ` </div>`;
                    grados_options += ` </div>`;

                    grados_options += ` <div class="col-md-11" style="top:16px">
                                                <div class="input-group">
                                                    <input type="text" class="form-control"  id="otros_seccion_5" placeholder="Agregar sección">
                                                    <div class="input-group-prepend">
                                                        <div class="btn input-group-text btn_agregar_seccion_5">+</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>`;

                    grados_options += ` <div class="col-md-11" style="top:16px">
                                            <div class="form-group">
                                                <label for="" class="mt-3">6to de Primaria:</label>`;
                            grados_options += ` <div class="btn-group-toggle seccion_6_contenido" data-toggle="buttons">`;
                                grados_options +=  `<label class="btn btn-primario mt-3"><input type="checkbox" value="A" id="seccion_6_A" name="Registro[seccion_6]"> A</label>&nbsp&nbsp`;
                                grados_options +=  `<label class="btn btn-primario mt-3"><input type="checkbox" value="B" id="seccion_6_B" name="Registro[seccion_6]"> B</label>&nbsp&nbsp`;
                                grados_options +=  `<label class="btn btn-primario mt-3"><input type="checkbox" value="C" id="seccion_6_C" name="Registro[seccion_6]"> C</label>&nbsp&nbsp`;
                                grados_options +=  `<label class="btn btn-primario mt-3"><input type="checkbox" value="D" id="seccion_6_D" name="Registro[seccion_6]"> D</label>&nbsp&nbsp`;
                                grados_options +=  `<label class="btn btn-primario mt-3"><input type="checkbox" value="E" id="seccion_6_E" name="Registro[seccion_6]"> E</label>&nbsp&nbsp`;
                                grados_options +=  `<label class="btn btn-primario mt-3"><input type="checkbox" value="F" id="seccion_6_F" name="Registro[seccion_6]"> F</label>&nbsp&nbsp`;
                                grados_options +=  `<label class="btn btn-primario mt-3"><input type="checkbox" value="G" id="seccion_6_G" name="Registro[seccion_6]"> G</label>&nbsp&nbsp`;
                                grados_options +=  `<label class="btn btn-primario mt-3"><input type="checkbox" value="H" id="seccion_6_H" name="Registro[seccion_6]"> H</label>&nbsp&nbsp`;
                                grados_options +=  `<label class="btn btn-primario mt-3"><input type="checkbox" value="I" id="seccion_6_I" name="Registro[seccion_6]"> I</label>&nbsp&nbsp`;
                                grados_options +=  `<label class="btn btn-primario mt-3"><input type="checkbox" value="J" id="seccion_6_J" name="Registro[seccion_6]"> J</label>&nbsp&nbsp`;
                            grados_options += ` </div>`;
                        grados_options += ` </div>`;
                    grados_options += ` </div>`;

                    grados_options += ` <div class="col-md-11" style="top:16px">
                                                <div class="input-group">
                                                    <input type="text" class="form-control"  id="otros_seccion_6" placeholder="Agregar sección">
                                                    <div class="input-group-prepend">
                                                        <div class="btn input-group-text btn_agregar_seccion_6">+</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>`;
                    $('.tipo_grado').html(grados_options);
                }

                if(tipo==3){
                    $(".contenedor_derecho_8").css("height", "100px");
                    grados_options += `<div class="col-md-11" style="top:16px">
                                            <div class="form-group">
                                                <label for="">Selecciona los grados que deseas inscribir al programa:</label> <br>`
                            grados_options += `<div class="btn-group-toggle" data-toggle="buttons">`;
                            $.each(results.data, function( index, value ) {
                                var descripcion = value.descripcion;
                                var grado = value.grado;
                                grados_options +=  `<label class="btn btn-primario">
                                                        <input type="checkbox" value="${grado}" id="cuarto_multiple_${grado}" name="Registro[grado]"> ${descripcion} 
                                                    </label>&nbsp&nbsp`;
                            });
                            grados_options += `</div>`;
                        grados_options += `</div>`;
                    grados_options += `</div>`;
                    $('.tipo_grado').html(grados_options);
                }

                

                loading.modal('hide');
            }
        },
        error:function(){
            alert('No hay conectividad con el sistema');
        }
    });
}

$('body').on('change', '#departamento', function (e) {
    departamento = $(this).val();
    Provincias(departamento);
});


$('body').on('change', '#provincia', function (e) {
    provincia = $(this).val();
    Distritos(provincia);
});

$('body').on('change', '#distrito', function (e) {
    departamento = $('#departamento').val();
    provincia = $('#provincia').val();
    distrito = $('#distrito').val();

    InstitucionesEducativas(departamento,provincia,distrito);
});






$('body').on('change', '#tipo_institucion_educativa', function (e) {
    
    Cargos($(this).val());

    // $('.cargo_id').show();

    // if($('#tipo_institucion_educativa').val()=='1' && ($('#cargo_id').val()=='1' || $('#cargo_id').val()=='2' || $('#cargo_id').val()=='5')){
    //     $('.grado').hide();
    //     $('.seccion_4').hide();
    //     $('.seccion_5').hide();
    //     $('.seccion_6').hide();

    //     $('#grado').val(null).trigger('change');
    //     $('#seccion_4').val(null).trigger('change');
    //     $('#seccion_5').val(null).trigger('change');
    //     $('#seccion_6').val(null).trigger('change');
    // }

    // if($('#tipo_institucion_educativa').val()=='1' && $('#cargo_id').val()=='3'){
    //     $('.grado').show();
    //     $('.seccion_4').hide();
    //     $('.seccion_5').hide();
    //     $('.seccion_6').hide();
    //     $('#seccion_4').val(null).trigger('change');
    //     $('#seccion_5').val(null).trigger('change');
    //     $('#seccion_6').val(null).trigger('change');
    //     tipo=1;
    //     Grados();
    // }


    // if($('#tipo_institucion_educativa').val()=='1' && $('#cargo_id').val()=='4'){
    //     $('.grado').show();
    //     $('.seccion_4').hide();
    //     $('.seccion_5').hide();
    //     $('.seccion_6').hide();
    //     $('#seccion_4').val(null).trigger('change');
    //     $('#seccion_5').val(null).trigger('change');
    //     $('#seccion_6').val(null).trigger('change');
    //     tipo=2;
    //     Grados();
    // }


    // if($('#tipo_institucion_educativa').val()=='2' && ($('#cargo_id').val()=='1' || $('#cargo_id').val()=='2' || $('#cargo_id').val()=='4' || $('#cargo_id').val()=='5')){
    //     $('.grado').hide();
    //     $('.seccion_4').hide();
    //     $('.seccion_5').hide();
    //     $('.seccion_6').hide();
        
    //     $('#grado').val(null).trigger('change');
    //     $('#seccion_4').val(null).trigger('change');
    //     $('#seccion_5').val(null).trigger('change');
    //     $('#seccion_6').val(null).trigger('change');
    // }

    // if($('#tipo_institucion_educativa').val()=='2' && $('#cargo_id').val()=='3'){
    //     $('.grado').show();
    //     $('.seccion_4').hide();
    //     $('.seccion_5').hide();
    //     $('.seccion_6').hide();
        
    //     $('#seccion_4').val(null).trigger('change');
    //     $('#seccion_5').val(null).trigger('change');
    //     $('#seccion_6').val(null).trigger('change');

    //     tipo=3;
    //     Grados();
    // }


    // if($('#tipo_institucion_educativa').val()=='3'){
    //     $('.cargo_id').hide();
    //     $('.grado').hide();
    //     $('.seccion_4').hide();
    //     $('.seccion_5').hide();
    //     $('.seccion_6').hide();

    //     $('#cargo_id').val('');
    //     $('#grado').val(null).trigger('change');
    //     $('#seccion_4').val(null).trigger('change');
    //     $('#seccion_5').val(null).trigger('change');
    //     $('#seccion_6').val(null).trigger('change');
    // }

});

$('body').on('change', '#cargo_id', function (e) {
    
    // if($('#tipo_institucion_educativa').val()=='1' && ($('#cargo_id').val()=='1' || $('#cargo_id').val()=='2' || $('#cargo_id').val()=='5')){
    //     $('.grado').hide();
    //     $('.seccion_4').hide();
    //     $('.seccion_5').hide();
    //     $('.seccion_6').hide();

    //     $('#grado').val(null).trigger('change');
    //     $('#seccion_4').val(null).trigger('change');
    //     $('#seccion_5').val(null).trigger('change');
    //     $('#seccion_6').val(null).trigger('change');
    // }

    if($('#tipo_institucion_educativa').val()=='1' && $('#cargo_id').val()=='3'){
        // $('.grado').show();
        // $('.seccion_4').hide();
        // $('.seccion_5').hide();
        // $('.seccion_6').hide();
        // $('#seccion_4').val(null).trigger('change');
        // $('#seccion_5').val(null).trigger('change');
        // $('#seccion_6').val(null).trigger('change');
        $('.contenedor_derecho_8').show();
        tipo=1;
        Grados();
    }


    if($('#tipo_institucion_educativa').val()=='1' && $('#cargo_id').val()=='4'){
        // $('.grado').show();
        // $('.seccion_4').hide();
        // $('.seccion_5').hide();
        // $('.seccion_6').hide();
        // $('#seccion_4').val(null).trigger('change');
        // $('#seccion_5').val(null).trigger('change');
        // $('#seccion_6').val(null).trigger('change');
        $('.contenedor_derecho_8').show();
        tipo=2;
        Grados();
    }

    // if($('#tipo_institucion_educativa').val()=='2' && ($('#cargo_id').val()=='1' || $('#cargo_id').val()=='2' || $('#cargo_id').val()=='4' || $('#cargo_id').val()=='5')){
    //     $('.grado').hide();
    //     $('.seccion_4').hide();
    //     $('.seccion_5').hide();
    //     $('.seccion_6').hide();
        
    //     $('#grado').val(null).trigger('change');
    //     $('#seccion_4').val(null).trigger('change');
    //     $('#seccion_5').val(null).trigger('change');
    //     $('#seccion_6').val(null).trigger('change');
    // }

    if($('#tipo_institucion_educativa').val()=='2' && $('#cargo_id').val()=='8'){
        // $('.grado').show();
        // $('.seccion_4').hide();
        // $('.seccion_5').hide();
        // $('.seccion_6').hide();
        
        // $('#seccion_4').val(null).trigger('change');
        // $('#seccion_5').val(null).trigger('change');
        // $('#seccion_6').val(null).trigger('change');
        $('.contenedor_derecho_8').show();
        tipo=3;
        Grados();
    }
});










// $('body').on('change', '#grado', function (e) {
//     if($(this).val()=="4" && $('#tipo_institucion_educativa').val()=="1" && $('#cargo_id').val()=="3"){
//         $('.seccion_4').show();
//         $('.seccion_5').hide();
//         $('.seccion_6').hide();
//         $('#seccion_4').select2({tags:true});
//         $('#seccion_5').val(null).trigger('change');
//         $('#seccion_6').val(null).trigger('change');
//     }else if($(this).val()=="5" && $('#tipo_institucion_educativa').val()=="1" && $('#cargo_id').val()=="3"){
//         $('.seccion_4').hide();
//         $('.seccion_5').show();
//         $('.seccion_6').hide();
//         $('#seccion_4').val(null).trigger('change');
//         $('#seccion_5').select2({tags:true});
//         $('#seccion_6').val(null).trigger('change');
//     }else if($(this).val()=="6" && $('#tipo_institucion_educativa').val()=="1" && $('#cargo_id').val()=="3"){
//         $('.seccion_4').hide();
//         $('.seccion_5').hide();
//         $('.seccion_6').show();
//         $('#seccion_4').val(null).trigger('change');
//         $('#seccion_5').val(null).trigger('change');
//         $('#seccion_6').select2({tags:true});
//     }

//     if($('#tipo_institucion_educativa').val()=="1" && $('#cargo_id').val()=="4"){
//         $('.seccion_4').hide();
//         $('.seccion_5').hide();
//         $('.seccion_6').hide();

//         $.each($(this).val(), function( index, value ) {
//             if(value=="4"){
//                 $('.seccion_4').show();
//                 $('#seccion_4').select2({tags:true,multiple:true});
//                 $('#seccion_4').val(null).trigger('change');
//             }

//             if(value=="5"){
//                 $('.seccion_5').show();
//                 $('#seccion_5').select2({tags:true,multiple:true});
//                 $('#seccion_5').val(null).trigger('change');
//             }

//             if(value=="6"){
//                 $('.seccion_6').show();
//                 $('#seccion_6').select2({tags:true,multiple:true});
//                 $('#seccion_6').val(null).trigger('change');
//             }
//         });
//     }
    


    
// });

$('body').on('click', '.btn-grabar-registro', function (e) {
    //e.preventDefault();
    var form                        = $('#formRegistro');
    var error                       = "";
    var dni                         = $("[name=\"Registro[dni]\"]").val();
    var apellido_paterno            = $("[name=\"Registro[apellido_paterno]\"]").val();
    var apellido_materno            = $("[name=\"Registro[apellido_materno]\"]").val();
    var nombres                     = $("[name=\"Registro[nombres]\"]").val();
    var correo_electronico          = $("[name=\"Registro[correo_electronico]\"]").val();
    var celular                     = $("[name=\"Registro[celular]\"]").val();
    var fecha_nacimiento            = $("[name=\"Registro[fecha_nacimiento]\"]").val();
    var clave                       = $("[name=\"Registro[clave]\"]").val();
    //var codigo_verificador          = $("[name=\"Registro[codigo_verificador]\"]").val();
    var tipo_institucion_educativa  = $("[name=\"Registro[tipo_institucion_educativa]\"]").val();
    var departamento                = $("[name=\"Registro[departamento]\"]").val();
    var provincia                   = $("[name=\"Registro[provincia]\"]").val();
    var distrito                    = $("[name=\"Registro[distrito]\"]").val();
    var institucion_educativa_id    = $("[name=\"Registro[institucion_educativa_id]\"]").val();
    var cargo_id                    = $("[name=\"Registro[cargo_id]\"]").val();
    var cod_mod                     = $( "#institucion_educativa_id option:selected" ).attr('data-cod_modular');
    var grado                       = $("[name=\"Registro[grado]\"]");
    var Array_grado                 = [];
    var seccion                     = $("[name=\"Registro[seccion]\"]").val();
    var seccion_4                   = $("[name=\"Registro[seccion_4]\"]");
    var seccion_5                   = $("[name=\"Registro[seccion_5]\"]");
    var seccion_6                   = $("[name=\"Registro[seccion_6]\"]");
    var Array_seccion_4             = [];
    var Array_seccion_5             = [];
    var Array_seccion_6             = [];

    var clave_confirma              = $('#clave_confirma').val();

    if(tipo_institucion_educativa=="2" && cargo_id=="8"){
        $.each(grado, function( index, value ) {
            if(value.checked){
                Array_grado.push(value.value)
            }
        });
    }

    if(tipo_institucion_educativa=="1" && cargo_id=="4"){
        if(seccion_4.length>=10){
            $.each(seccion_4, function( index, value ) {
                if(value.checked){
                    Array_seccion_4.push(value.value)
                }
            });
            if(Array_seccion_4.length>0){
                Array_grado.push("4");
            }
        }

        if(seccion_5.length>=10){
            $.each(seccion_5, function( index, value ) {
                if(value.checked){
                    Array_seccion_5.push(value.value)
                }
            });
            if(Array_seccion_5.length>0){
                Array_grado.push("5");
            }
        }

        if(seccion_6.length>=10){
            $.each(seccion_6, function( index, value ) {
                if(value.checked){
                    Array_seccion_6.push(value.value)
                }
            });
            if(Array_seccion_6.length>0){
                Array_grado.push("6");
            }
        }
    }
    

    
    
    //formData.push({name: 'username', value: 'this is username'});
    
    if(!dni){
        $('.error_dni').addClass('alert-danger');
        $("#dni").focus();
        error = error + "error 1";
    }else{
        $('.error_dni').removeClass('alert-danger');
    }

    if(!apellido_paterno){
        $('.error_apellido_paterno').addClass('alert-danger');
        $("#apellido_paterno").focus();
        error = error + "error 1";
    }else{
        $('.error_apellido_paterno').removeClass('alert-danger');
    }

    // if(!apellido_materno){
    //     $('.error_apellido_materno').addClass('alert-danger');
    //     $("#apellido_paterno").focus();
    //     error = error + "error 1";
    // }else{
    //     $('.error_apellido_materno').removeClass('alert-danger');
    // }

    if(!nombres){
        $('.error_nombres').addClass('alert-danger');
        $("#nombres").focus();
        error = error + "error error_nombres";
    }else{
        $('.error_nombres').removeClass('alert-danger');
    }

    if(!correo_electronico){
        $('.error_correo_electronico').addClass('alert-danger');
        $("#correo_electronico").focus();
        error = error + "error error_correo_electronico";
    }else{
        $('.error_correo_electronico').removeClass('alert-danger');
    }

    filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!filter.test(correo_electronico)) {
        $('.error_correo_electronico').addClass('alert-danger');
        $("#correo_electronico").focus();
        error = error + "error error_correo_electronico";
    }else{
        $('.error_correo_electronico').removeClass('alert-danger');
    }


    if(!celular){
        $('.error_celular').addClass('alert-danger');
        $("#celular").focus();
        error = error + "error error_celular";
    }else{
        $('.error_celular').removeClass('alert-danger');
    }

    if(!fecha_nacimiento){
        $('.error_fecha_nacimiento').addClass('alert-danger');
        $("#fecha_nacimiento").focus();
        error = error + "error error_fecha_nacimiento";
    }else{
        $('.error_fecha_nacimiento').removeClass('alert-danger');
    }

    // if(!codigo_verificador){
    //     $('.error_codigo_verificador').addClass('alert-danger');
    //     $("#codigo_verificador").focus();
    //     error = error + "error error_codigo_verificador";
    // }else{
    //     $('.error_codigo_verificador').removeClass('alert-danger');
    // }

    if(!clave){
        $('.error_clave').addClass('alert-danger');
        $("#clave").focus();
        error = error + "error error_clave";
    }else{
        $('.error_clave').removeClass('alert-danger');
    }

    if(clave.length<6 || clave.length>20){
        $('.error_clave_text').html('La contraseña no debe ser menor de 6 y no mayor de 20 caracteres');
        $('.error_clave').addClass('alert-danger');
        $("#clave").focus();
        error = error + "error error_clave_text";
    }else{
        $('.error_clave').removeClass('alert-danger');
    }

    if(clave != clave_confirma){
        $('.error_clave_text').html('Las contaseñas no coinciden');
        $('.error_clave').addClass('alert-danger');
        $("#clave").focus();
        error = error + "error error_clave";
    }else{
        $('.error_clave').removeClass('alert-danger');
    }
    
    if(!tipo_institucion_educativa){
        $('.error_tipo_institucion_educativa').addClass('alert-danger');
        $("#tipo_institucion_educativa").focus();
        error = error + "error error_tipo_institucion_educativa";
    }else{
        $('.error_tipo_institucion_educativa').removeClass('alert-danger');
    }

    if(!departamento){
        $('.error_departamento').addClass('alert-danger');
        $("#departamento").focus();
        error = error + "error error_departamento";
    }else{
        $('.error_departamento').removeClass('alert-danger');
    }

    if(!provincia){
        $('.error_provincia').addClass('alert-danger');
        $("#provincia").focus();
        error = error + "error error_provincia";
    }else{
        $('.error_provincia').removeClass('alert-danger');
    }

    if(!distrito){
        $('.error_distrito').addClass('alert-danger');
        $("#distrito").focus();
        error = error + "error error_distrito";
    }else{
        $('.error_distrito').removeClass('alert-danger');
    }

    if(!institucion_educativa_id){
        $('.error_institucion_educativa_id').addClass('alert-danger');
        $("#institucion_educativa_id").focus();
        error = error + "error error_institucion_educativa_id";
    }else{
        $('.error_institucion_educativa_id').removeClass('alert-danger');
    }

    if(!cargo_id && (tipo_institucion_educativa=="1" || tipo_institucion_educativa=="2")){
        $('.error_cargo_id').addClass('alert-danger');
        $("#cargo_id").focus();
        error = error + "error error_cargo_id";
    }else{
        $('.error_cargo_id').removeClass('alert-danger');
    }

    

    //Criterios de validación
    if(tipo_institucion_educativa == '3'){
        //$('.grado').hide();
    }

    console.log(error);
    if(error != ""){
        console.log(error);
        return false;
    }
    //$(this).prop('disabled', true);
    
    if(tipo_institucion_educativa && tipo_institucion_educativa=="1" && cargo_id && cargo_id == "3"){
        grado = grado.val();
        if(grado=="4"){
            seccion_4 = seccion;
        }
        if(grado=="5"){
            seccion_5 = seccion;
        }
        if(grado=="6"){
            seccion_6 = seccion;
        }
    }

    if(tipo_institucion_educativa && tipo_institucion_educativa=="1" && cargo_id && cargo_id == "4"){
        seccion_4 = Array_seccion_4.toString();
        seccion_5 = Array_seccion_5.toString();
        seccion_6 = Array_seccion_6.toString();
        grado = Array_grado.toString();
    }

    if(tipo_institucion_educativa && tipo_institucion_educativa=="2" && cargo_id && cargo_id == "8"){
        grado = Array_grado.toString();
    }

    $('.formulario_4').hide();

    $('.svg_opcion_4').html(`<svg width="250" height="32" viewBox="0 0 250 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="16" cy="16" r="16" fill="white"/>
                            <circle cx="16" cy="16" r="16" stroke="black"/>
                            <path d="M13.4422 23C13.0989 23 12.7555 22.855 12.4809 22.6011L8.42957 18.5399C7.88024 17.996 7.8459 17.0532 8.39523 16.473C8.91023 15.8928 9.80289 15.8566 10.3522 16.4368L13.4079 19.5189L21.6135 10.4537C22.1284 9.87351 23.0211 9.83725 23.5704 10.4174C24.1198 10.9613 24.1541 11.9041 23.6048 12.4843L14.4378 22.5649C14.1632 22.855 13.8199 23 13.4422 23Z" fill="#2AB7CA"/>
                        </svg>`);


    var form_data = new FormData(); // $('#formEncuesta01').serializeArray();
    form_data.append("_csrf", csrf);
    form_data.append("Registro[dni]", dni);
    form_data.append("Registro[apellido_paterno]", apellido_paterno);
    form_data.append("Registro[apellido_materno]", apellido_materno);
    form_data.append("Registro[nombres]", nombres);
    form_data.append("Registro[correo_electronico]", correo_electronico);
    form_data.append("Registro[celular]", celular);
    form_data.append("Registro[fecha_nacimiento]", fecha_nacimiento);
    form_data.append("Registro[clave]", clave);
    //form_data.append("Registro[codigo_verificador]", codigo_verificador);
    form_data.append("Registro[tipo_institucion_educativa]", tipo_institucion_educativa);
    form_data.append("Registro[departamento]", departamento);
    form_data.append("Registro[provincia]", provincia);
    form_data.append("Registro[distrito]", distrito);
    form_data.append("Registro[institucion_educativa_id]", institucion_educativa_id);
    form_data.append("Registro[cargo_id]", cargo_id);
    form_data.append("Registro[cod_mod]", cod_mod);
    form_data.append("Registro[grado]", grado);
    form_data.append("Registro[seccion_4]", seccion_4);
    form_data.append("Registro[seccion_5]", seccion_5);
    form_data.append("Registro[seccion_6]", seccion_6);
    

    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        cache: false,
		contentType: false,
		processData: false,
        data: form_data,
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success: function (results) {
            if(results.success){
                loading.modal('hide');
                // const message = JSON.stringify({
                //     success: true,
                // });
                // window.parent.postMessage(message, '*');

                //window.top.postMessage('hello', '*')
                //results.success;
                //location.reload();
                
                $('.container').css('background','white');
                $('.msg_exitoso').html(`
                    <div class="col-md-12 col-lg-12 col-xl-12 col-xxl-12" style="background:white">
                        <div class="titulo_exitoso">¡Felicidades!</div>
                        <div class="subtitulo_exitoso">Has completado exitosamente tu registro al programa Conecta Ideas</div>
                        <div class="primer_parrafo_exitoso"> 
                            Recibirás tu usuario en un tiempo máximo de 5 días útiles en el correo y el celular que registraste.
                        </div>
                        <div class="segundo_parrafo_exitoso">Con este usuario, podrás ingresar al Intranet para Docentes y a la aplicación de Conecta Ideas Perú.</div>

                        <div class="boton_exitoso text-center">
                            <button class="btn btn-success">Volver al inicio</button>
                        </div>
                    </div>
                `);
                
            }
        },
    });
});


//formulario

$('body').on('click', '.form_1', function (e) {
    

    var error                       = "";
    var dni                         = $("[name=\"Registro[dni]\"]").val();
    var apellido_paterno            = $("[name=\"Registro[apellido_paterno]\"]").val();
    var apellido_materno            = $("[name=\"Registro[apellido_materno]\"]").val();
    var nombres                     = $("[name=\"Registro[nombres]\"]").val();
    var correo_electronico          = $("[name=\"Registro[correo_electronico]\"]").val();
    var celular                     = $("[name=\"Registro[celular]\"]").val();
    var fecha_nacimiento            = $("[name=\"Registro[fecha_nacimiento]\"]").val();

    if(!dni){
        $('.error_dni').addClass('alert-danger');
        $("#dni").focus();
        error = error + "error 1";
    }else{
        $('.error_dni').removeClass('alert-danger');
    }

    if(!apellido_paterno){
        $('.error_apellido_paterno').addClass('alert-danger');
        $("#apellido_paterno").focus();
        error = error + "error 1";
    }else{
        $('.error_apellido_paterno').removeClass('alert-danger');
    }

    if(!nombres){
        $('.error_nombres').addClass('alert-danger');
        $("#nombres").focus();
        error = error + "error 1";
    }else{
        $('.error_nombres').removeClass('alert-danger');
    }

    if(!correo_electronico){
        $('.error_correo_electronico').addClass('alert-danger');
        $("#correo_electronico").focus();
        error = error + "error 1";
    }else{
        $('.error_correo_electronico').removeClass('alert-danger');
    }

    filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!filter.test(correo_electronico)) {
        $('.error_correo_electronico').addClass('alert-danger');
        $("#correo_electronico").focus();
        error = error + "error 1";
    }else{
        $('.error_correo_electronico').removeClass('alert-danger');
    }


    if(!celular){
        $('.error_celular').addClass('alert-danger');
        $("#celular").focus();
        error = error + "error 1";
    }else{
        $('.error_celular').removeClass('alert-danger');
    }

    if(!fecha_nacimiento){
        $('.error_fecha_nacimiento').addClass('alert-danger');
        $("#fecha_nacimiento").focus();
        error = error + "error 1";
    }else{
        $('.error_fecha_nacimiento').removeClass('alert-danger');
    }

    if(error != ""){
        console.log(error);
        return false;
    }

    $('.formulario_2').show();
    $('.formulario_1').hide();
    $('.svg_opcion_1').html(`<svg width="250" height="32" viewBox="0 0 250 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="16" cy="16" r="16" fill="white"/>
                            <circle cx="16" cy="16" r="16" stroke="black"/>
                            <path d="M13.4422 23C13.0989 23 12.7555 22.855 12.4809 22.6011L8.42957 18.5399C7.88024 17.996 7.8459 17.0532 8.39523 16.473C8.91023 15.8928 9.80289 15.8566 10.3522 16.4368L13.4079 19.5189L21.6135 10.4537C22.1284 9.87351 23.0211 9.83725 23.5704 10.4174C24.1198 10.9613 24.1541 11.9041 23.6048 12.4843L14.4378 22.5649C14.1632 22.855 13.8199 23 13.4422 23Z" fill="#2AB7CA"/>
                        </svg>`);
    $('.svg_opcion_2').html(`<svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="16" cy="16" r="15" stroke="white" stroke-width="2"/>
                                <text x="12" y="20" fill="white">2</text>
                            </svg>`);
});


$('body').on('click', '.form_2', function (e) {

    var error                       = "";
    var elegir_medio                = $("#elegir_medio").val();
    var departamento                = $("[name=\"Registro[departamento]\"]").val();
    var provincia                   = $("[name=\"Registro[provincia]\"]").val();
    var distrito                    = $("[name=\"Registro[distrito]\"]").val();
    var institucion_educativa_id    = $("[name=\"Registro[institucion_educativa_id]\"]").val();
    var tipo_institucion_educativa  = $("[name=\"Registro[tipo_institucion_educativa]\"]").val();

    if(!elegir_medio){
        $('.error_elegir_medio').addClass('alert-danger');
        $("#elegir_medio").focus();
        error = error + "error 1";
    }else{
        $('.error_elegir_medio').removeClass('alert-danger');
    }

    if(!departamento){
        $('.error_departamento').addClass('alert-danger');
        $("#departamento").focus();
        error = error + "error 1";
    }else{
        $('.error_departamento').removeClass('alert-danger');
    }

    if(!provincia){
        $('.error_provincia').addClass('alert-danger');
        $("#provincia").focus();
        error = error + "error 1";
    }else{
        $('.error_provincia').removeClass('alert-danger');
    }

    if(!distrito){
        $('.error_distrito').addClass('alert-danger');
        $("#distrito").focus();
        error = error + "error 1";
    }else{
        $('.error_distrito').removeClass('alert-danger');
    }

    if(!institucion_educativa_id){
        $('.error_institucion_educativa_id').addClass('alert-danger');
        $("#institucion_educativa_id").focus();
        error = error + "error 1";
    }else{
        $('.error_institucion_educativa_id').removeClass('alert-danger');
    }

    if(!tipo_institucion_educativa){
        $('.error_tipo_institucion_educativa').addClass('alert-danger');
        $("#tipo_institucion_educativa").focus();
        error = error + "error 1";
    }else{
        $('.error_tipo_institucion_educativa').removeClass('alert-danger');
    }

    if(error != ""){
        console.log(error);
        return false;
    }

    

    $('.formulario_3').show();
    $('.formulario_2').hide();

    $('.svg_opcion_2').html(`<svg width="250" height="32" viewBox="0 0 250 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="16" cy="16" r="16" fill="white"/>
                            <circle cx="16" cy="16" r="16" stroke="black"/>
                            <path d="M13.4422 23C13.0989 23 12.7555 22.855 12.4809 22.6011L8.42957 18.5399C7.88024 17.996 7.8459 17.0532 8.39523 16.473C8.91023 15.8928 9.80289 15.8566 10.3522 16.4368L13.4079 19.5189L21.6135 10.4537C22.1284 9.87351 23.0211 9.83725 23.5704 10.4174C24.1198 10.9613 24.1541 11.9041 23.6048 12.4843L14.4378 22.5649C14.1632 22.855 13.8199 23 13.4422 23Z" fill="#2AB7CA"/>
                        </svg>`);
    $('.svg_opcion_3').html(`<svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="16" cy="16" r="15" stroke="white" stroke-width="2"/>
                                <text x="12" y="20" fill="white">3</text>
                            </svg>`);

});

$('body').on('click', '.form_3', function (e) {
    $('.formulario_4').show();
    $('.formulario_3').hide();

    $('.svg_opcion_3').html(`<svg width="250" height="32" viewBox="0 0 250 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="16" cy="16" r="16" fill="white"/>
                            <circle cx="16" cy="16" r="16" stroke="black"/>
                            <path d="M13.4422 23C13.0989 23 12.7555 22.855 12.4809 22.6011L8.42957 18.5399C7.88024 17.996 7.8459 17.0532 8.39523 16.473C8.91023 15.8928 9.80289 15.8566 10.3522 16.4368L13.4079 19.5189L21.6135 10.4537C22.1284 9.87351 23.0211 9.83725 23.5704 10.4174C24.1198 10.9613 24.1541 11.9041 23.6048 12.4843L14.4378 22.5649C14.1632 22.855 13.8199 23 13.4422 23Z" fill="#2AB7CA"/>
                        </svg>`);
    $('.svg_opcion_4').html(`<svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="16" cy="16" r="15" stroke="white" stroke-width="2"/>
                                <text x="12" y="20" fill="white">4</text>
                            </svg>`);
});

$('body').on('change', '#elegir_medio', function (e) {
    $('#codigo_modular').val('');

    $('#departamento').val('');
    $('#provincia').html('<option value>Selecciona una opción</option>');
    $('#distrito').html('<option value>Selecciona una opción</option>');
    $('#institucion_educativa_id').html('<option value>Selecciona una opción</option>');

    $('#codigo_modular').prop( "disabled", true );
    $('.consultar_codigo').prop( "disabled", true );
    $('#departamento').prop( "disabled", true );
    $('#provincia').prop( "disabled", true );
    $('#distrito').prop( "disabled", true );
    $('#institucion_educativa_id').prop( "disabled", true );

    if($(this).val()=="1"){
        $('#codigo_modular').prop( "disabled", false );
        $('.consultar_codigo').prop( "disabled", false );
    }

    if($(this).val()=="2"){
        
        $('#departamento').prop( "disabled", false );
        $('#provincia').prop( "disabled", false );
        $('#distrito').prop( "disabled", false );
        $('#institucion_educativa_id').prop( "disabled", false );
        
    }
    
});



$('body').on('click', '.btn_agregar_seccion_4', function (e) {
    var descripcion = $('#otros_seccion_4').val();
    var selector_id = $("[name=\"Registro[seccion_4]\"]").length;
    if($.trim(descripcion)==""){
        alert('No se puede agregar información vacia.');
        return false;
    }
    $('.seccion_4_contenido').append(`<label class="btn btn-primario active mt-3"><input type="checkbox" id="seccion_4_${selector_id}" value="${descripcion}" name="Registro[seccion_4]" checked> ${descripcion}</label>&nbsp&nbsp`);
    $('#otros_seccion_4').val('');
});

$('body').on('click', '.btn_agregar_seccion_5', function (e) {
    var descripcion = $('#otros_seccion_5').val();
    var selector_id = $("[name=\"Registro[seccion_5]\"]").length;
    if($.trim(descripcion)==""){
        alert('No se puede agregar información vacia.');
        return false;
    }
    $('.seccion_5_contenido').append(`<label class="btn btn-primario active mt-3"><input type="checkbox" id="seccion_5_${selector_id}" value="${descripcion}" name="Registro[seccion_5][]" checked> ${descripcion}</label>&nbsp&nbsp`);
    $('#otros_seccion_5').val('');
});

$('body').on('click', '.btn_agregar_seccion_6', function (e) {
    var descripcion = $('#otros_seccion_6').val();
    var selector_id = $("[name=\"Registro[seccion_6]\"]").length;
    if($.trim(descripcion)==""){
        alert('No se puede agregar información vacia.');
        return false;
    }
    $('.seccion_6_contenido').append(`<label class="btn btn-primario active mt-3"><input type="checkbox" id="seccion_6_${selector_id}" value="${descripcion}" name="Registro[seccion_6][]" checked> ${descripcion}</label>&nbsp&nbsp`);
    $('#otros_seccion_6').val('');
});


$('body').on('click', '.btn_agregar_seccion_demo', function (e) {
    var seccion_4   = $("[name=\"Registro[seccion_4]\"]");
    var Array_seccion_4 = [];
    if(seccion_4.length>=10){
        $.each(seccion_4, function( index, value ) {
            if(value.checked){
                Array_seccion_4.push(value.value)
            }
        });
        console.log(Array_seccion_4, Array_seccion_4.toString());
    }

});

</script>