<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Foto */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="account-pages my-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 col-lg-12 col-xl-12 col-xxl-12">
                <div class="card overflow-hidden">
                    
                    <div class="bg-primary">
                        <div class="row">
                            <div class="col-9">
                                <div class="text-white p-4">
                                    <h5 class="text-white">ConectaIdeas - Perú</h5>
                                    <p>Registrate</p>
                                </div>
                            </div>
                            <div class="col-3 align-self-end">
                                <img src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/images/profile-img.png" alt="" class="img-fluid">
                            </div>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <div>
                            <a href="https://conectaideasperu.com/">
                                <div class="avatar-md profile-user-wid mb-4">
                                    <span class="avatar-title rounded-circle bg-light">
                                        <img src="<?= \Yii::$app->request->BaseUrl ?>/img/conectaideas_login.png" alt="" class="rounded-circle" height="54">
                                    </span>
                                </div>
                            </a>
                        </div>
                        <div class="p-2">
                            <?php $form = ActiveForm::begin(['options' => ['id' => 'formRegistro','class' => 'form-horizontal','autocomplete'=>'off']]); ?>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>DATOS PERSONALES</h4>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <label for="dni">Número de DNI:</label>
                                            <input type="text" class="form-control numerico error_dni" id="dni" name="Registro[dni]" placeholder="Ingrese DNI" maxlength="8">
                                        </div>
                                    </div>
                                </div>
                                
                                
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <label for="apellido_paterno">Apellido paterno</label>
                                            <input type="text" class="form-control texto error_apellido_paterno" id="apellido_paterno" name="Registro[apellido_paterno]" placeholder="Ingrese Apellido paterno" maxlength="30">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <label for="apellido_materno">Apellido materno</label>
                                            <input type="text" class="form-control texto error_apellido_materno" id="apellido_materno" name="Registro[apellido_materno]" placeholder="Ingrese apellido materno" maxlength="30">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <label for="nombres">Nombres</label>
                                            <input type="text" class="form-control texto error_nombres" id="nombres" name="Registro[nombres]" placeholder="Ingrese nombres" maxlength="30">
                                        </div>
                                    </div>
                                </div>
                                <!-- <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>DATOS DE CONTACTO</h4>
                                    </div>
                                </div> -->
                                
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <label for="correo_electronico">Correo electrónico</label>
                                            <input type="email" class="form-control error_correo_electronico" id="correo_electronico" name="Registro[correo_electronico]" placeholder="Ingrese correo electrónico">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <label for="celular">Celular</label>
                                            <input type="text" class="form-control numerico error_celular" id="celular" name="Registro[celular]" placeholder="Ingrese celular" maxlength="9">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <label for="fecha_nacimiento">Fecha de nacimiento</label>
                                            <input type="date" class="form-control error_fecha_nacimiento" id="fecha_nacimiento" name="Registro[fecha_nacimiento]" placeholder="Ingrese fecha nacimiento" maxlength="10">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <label for="clave">Contraseña</label>
                                            <input type="password" class="form-control error_clave" id="clave" name="Registro[clave]" placeholder="Ingrese su contraseña" maxlength="20">
                                            <p class="error_clave_text"></p>
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-4">
                                        <div class="form-group ">
                                            <label for="codigo_verificador">Código verificador</label>
                                            <input type="text" class="form-control error_codigo_verificador" id="codigo_verificador" name="Registro[codigo_verificador]" placeholder="Ingrese codigo verificador">
                                        </div>
                                    </div> -->
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>DATOS DE INSTITUCIÓN EDUCATIVA</h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group ">
                                            <label for="tipo_institucion_educativa">Tipo de Institución Educativa</label>
                                            <select class="form-control error_tipo_institucion_educativa" id="tipo_institucion_educativa" name="Registro[tipo_institucion_educativa]">
                                                <option value>Seleccione el tipo de institución educativa</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group ">
                                            <label for="codigo_modular">Codigo modular de la I.E.</label>
                                            <select class="form-control error_codigo_modular" id="codigo_modular" name="Registro[codigo_modular]">
                                                <option value>Seleccione el código modular</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <label for="departamento">Departamento</label>
                                            <select class="form-control error_departamento" id="departamento" name="Registro[departamento]">
                                                <option value>Seleccione departamento</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <label for="provincia">Provincia</label>
                                            <select class="form-control error_provincia" id="provincia" name="Registro[provincia]">
                                                <option value>Seleccione provincia</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <label for="distrito">Distrito</label>
                                            <select class="form-control error_distrito" id="distrito" name="Registro[distrito]">
                                                <option value>Seleccione distrito</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <label for="institucion_educativa_id">Institución educativa</label>
                                            <select class="form-control error_institucion_educativa_id" id="institucion_educativa_id" name="Registro[institucion_educativa_id]">
                                                <option value>Seleccione institución educativa</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>DATOS DEL AÑO LECTIVO 2021</h4>
                                    </div>
                                </div>

                                <div class="row cargo_id">
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <label for="cargo_id">Cargo</label>
                                            <select class="form-control error_cargo_id" id="cargo_id" name="Registro[cargo_id]">
                                                <option value>Seleccione cargo</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="grado">Grado</label>
                                            <select class="form-control" id="grado" name="grado">
                                                <option value>Seleccione los grados</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="seccion">Seccion</label>
                                            <select class="form-control" id="seccion" name="seccion">
                                                <option value>Seleccione las secciones</option>
                                            </select>
                                        </div>
                                    </div> -->
                                </div>

                                <div class="row grado" style="display:none">
                                    <div class="col-md-12">
                                        <div class="form-group ">
                                            <label for="grado">Grado</label>
                                            <select class="form-control error_grado" id="grado" name="Registro[grado]">
                                                <option value>Seleccione grado</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>


                                <div class="row seccion_4" style="display:none">
                                    <div class="col-md-12 ">
                                        <div class="form-group ">
                                            <label for="seccion_4">Sección de 4° de primaria</label>
                                            <select class="form-control error_seccion_4" id="seccion_4" name="Registro[seccion_4]">
                                                <option value>Seleccione o agregue la sección de 4°</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row seccion_5" style="display:none">
                                    <div class="col-md-12">
                                        <div class="form-group ">
                                            <label for="seccion_5">Sección de 5° de primaria</label>
                                            <select class="form-control error_seccion_5" id="seccion_5" name="Registro[seccion_5]">
                                                <option value>Seleccione o agregue la sección de 5°</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>


                                <div class="row seccion_6" style="display:none">
                                    <div class="col-md-12">
                                        <div class="form-group ">
                                            <label for="seccion_6">Sección de 6° de primaria</label>
                                            <select class="form-control error_seccion_6" id="seccion_6" name="Registro[seccion_6]">
                                                <option value>Seleccione o agregue la sección de 6°</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="mt-3">
                                    <button class="btn btn-primary btn-block waves-effect waves-light btn-grabar-registro" type="button">Registrate</button>
                                </div>
                            <?php ActiveForm::end(); ?>
                        </div>

                    </div>
                </div>
                <div class="mt-5 text-center">
                    <div>
                        <p>© 2021 - CONECTAIDEAS | PERÚ. </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
	
<!-- /.content -->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog text-center" role="document">
        <i class="fa fa-refresh fa-spin"></i>
        Cargando
    </div>
</div>

<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =  $('#staticBackdrop');
// //loading.modal("show");
// loading.modal('hide');
const delay = ms => new Promise(res => setTimeout(res, ms));


$("#codigo_modular").select2({
    ajax: {
        url: '<?= \Yii::$app->request->BaseUrl ?>/institucion-educativa/get-lista-instituciones-educativas',
        dataType: "json",
        delay: 250,
        data: function (params) {
            return {
                q: params.term, // search term
            };
        },
        processResults: function (data, params) {
            // parse the results into the format expected by Select2
            // since we are using custom formatting functions we do not need to
            // alter the remote JSON data, except to indicate that infinite
            // scrolling can be used
            //params.page = params.page || 1;

            return {
                results: data.results,
            };
        },
        cache: true,
    },
    placeholder: "Buscar código modular",
});

$('body').on('change', '#codigo_modular', function (e,data) {
    InstitucionEducativa($(this).val());
});


// $('#codigo_modular').on('select2:select', function (e) {
//     var data = e.params.data;

//     departamento = $('#departamento').val(data.departamento);
//     Provincias(departamento);
//     // provincia = $('#provincia').val(data.provincia);
//     // Distritos(provincia);
//     // distrito = $('#distrito').val(data.distrito);


//     console.log(data);
// });


TipoInstitucionEducativa()
async function TipoInstitucionEducativa(){
    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/tipo-institucion-educativa/get-tipos-instituciones-educativas',
        method: 'POST',
        data:{"_csrf":csrf},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {
            if(results.success){
                var tipo_institucion_educativa_options = "<option value>Seleccione el tipo de institución educativa</option>";
                $.each(results.data, function( index, value ) {
                    tipo_institucion_educativa_options +=  "<option value='" + value.id + "'>" + value.descripcion + "</option>";
                });
                $('#tipo_institucion_educativa').html(tipo_institucion_educativa_options);
            }
        },
        error:function(){
            alert('No hay conectividad con el sistema');
        }
    });
}



Regiones()
async function Regiones(){
    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-regiones',
        method: 'POST',
        data:{"_csrf":csrf},
        dataType:'Json',
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success:function(results)
        {
            if(results.success){
                var regiones_options = "<option value>Seleccione departamento</option>";
                $.each(results.data, function( index, value ) {
                    regiones_options +=  "<option value='" + value.departamento + "'>" + value.departamento + "</option>";
                });
                $('#departamento').html(regiones_options);
                loading.modal('hide');
            }
        },
        error:function(){
            alert('No hay conectividad con el sistema');
        }
    });
}


async function Provincias(departamento,provincia){
    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-provincias',
        method: 'POST',
        data:{"_csrf":csrf,departamento:departamento},
        dataType:'Json',
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success:function(results)
        {
            if(results.success){
                var provincias_options = "<option value>Seleccione provincia</option>";
                $.each(results.data, function( index, value ) {
                    provincias_options +=  "<option value='" + value.provincia + "'>" + value.provincia + "</option>";
                });
                $('#provincia').html(provincias_options);
                if(provincia){
                    $('#provincia').val(provincia);
                }
                loading.modal('hide');
            }
        },
        error:function(){
            alert('No hay conectividad con el sistema');
        }
    });
}


async function Distritos(provincia,distrito){
    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-distritos',
        method: 'POST',
        data:{"_csrf":csrf,provincia:provincia},
        dataType:'Json',
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success:function(results)
        {
            if(results.success){
                var distritos_options = "<option value>Seleccione distrito</option>";
                $.each(results.data, function( index, value ) {
                    distritos_options +=  "<option value='" + value.distrito + "'>" + value.distrito + "</option>";
                });
                $('#distrito').html(distritos_options);
                if(distrito){
                    $('#distrito').val(distrito);
                }
                loading.modal('hide');
            }
        },
        error:function(){
            alert('No hay conectividad con el sistema');
        }
    });
}

function wait(timeout) {
    return new Promise(resolve => {
        setTimeout(resolve, timeout);
    });
}


function InstitucionEducativa(id){
    $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/institucion-educativa/get-institucion-educativa',
        method: 'POST',
        data:{"_csrf":csrf,id},
        dataType:'Json',
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success:function(results)
        {
            if(results.success){

                // async () => {
                //     await delay(5000);
                //     console.log("Waited 5s");

                //     await delay(5000);
                //     console.log("Waited an additional 5s");
                // };
                    departamento = results.data[0].departamento;
                    provincia = results.data[0].provincia;
                    distrito = results.data[0].distrito;
                    institucion_educativa = results.data[0].id;
                    $('#departamento').val(departamento);
                    Provincias(departamento,provincia);
                    Distritos(provincia,distrito);
                    InstitucionesEducativas(departamento,provincia,distrito,institucion_educativa);
                loading.modal('hide');
            }
        },
        error:function(){
            alert('No hay conectividad con el sistema');
        }
    });
}


async function InstitucionesEducativas(departamento,provincia,distrito,institucion_educativa){
    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/institucion-educativa/get-instituciones-educativas',
        method: 'POST',
        data:{"_csrf":csrf,departamento:departamento,provincia:provincia,distrito:distrito},
        dataType:'Json',
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success:function(results)
        {
            if(results.success){
                var instituciones_educativas_options = "";
                $.each(results.data, function( index, value ) {
                    instituciones_educativas_options +=  "<option value='" + value.id + "' data-cod_modular='" + value.cod_mod + "'>" + value.cod_mod + " - " + value.nombre_ie + "</option>";
                });
                $('#institucion_educativa_id').html(instituciones_educativas_options);
                $('#institucion_educativa_id').select2({multiple:false,placeholder: "Seleccione institución educativa"});
                $('#institucion_educativa_id').val(null).trigger('change');

                if(institucion_educativa){
                    $('#institucion_educativa_id').val(institucion_educativa).trigger('change');
                }

                loading.modal('hide');
            }
        },
        error:function(){
            alert('No hay conectividad con el sistema');
        }
    });
}

async function Cargos(){
    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/cargo/get-cargos',
        method: 'POST',
        data:{"_csrf":csrf},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {
            if(results.success){
                var cargos_options = "<option value>Seleccione cargo</option>";
                $.each(results.data, function( index, value ) {
                    cargos_options +=  "<option value='" + value.id + "'>" + value.descripcion + "</option>";
                });
                $('#cargo_id').html(cargos_options);
            }
        },
        error:function(){
            alert('No hay conectividad con el sistema');
        }
    });
}

Cargos();

var tipo;

async function Grados(){
    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/grado/get-grados',
        method: 'POST',
        data:{"_csrf":csrf},
        dataType:'Json',
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success:function(results)
        {
            //$('#grado').select2('destroy');
            if(results.success){
                var grados_options="";
                // if(tipo==2){
                //     grados_options = "<option value>Seleccione grado</option>";
                // }
                // if(tipo==1){
                //     grados_options = "<option disabled></option>";
                // }
                
                $.each(results.data, function( index, value ) {
                    grados_options +=  "<option value='" + value.grado + "'>" + value.descripcion + "</option>";
                });
                $('#grado').html(grados_options);

                if(tipo==1){
                    $('#grado').select2({multiple:false,placeholder: "Seleccione grado"});
                    $('#grado').val(null).trigger('change');
                }

                if(tipo==2){
                    $('#grado').select2({multiple:true,placeholder: "Seleccione grado"});
                    $('#grado').val(null).trigger('change');
                }

                if(tipo==3){
                    $('#grado').select2({multiple:true,placeholder: "Seleccione grado"});
                    $('#grado').val(null).trigger('change');
                }
                // if(tipo==2){
                //     $('#grado').select2('destroy');
                //     $('#grado').select2({multiple:false});
                // }
                // if(tipo==3){
                //     $('#grado').select2({multiple:true,placeholder: "Seleccione grado"});
                // }
                loading.modal('hide');
            }
        },
        error:function(){
            alert('No hay conectividad con el sistema');
        }
    });
}

$('body').on('change', '#departamento', function (e) {
    departamento = $(this).val();
    Provincias(departamento);
});


$('body').on('change', '#provincia', function (e) {
    provincia = $(this).val();
    Distritos(provincia);
});

$('body').on('change', '#distrito', function (e) {
    departamento = $('#departamento').val();
    provincia = $('#provincia').val();
    distrito = $('#distrito').val();

    InstitucionesEducativas(departamento,provincia,distrito);
});






$('body').on('change', '#tipo_institucion_educativa', function (e) {
    
    $('.cargo_id').show();

    if($('#tipo_institucion_educativa').val()=='1' && ($('#cargo_id').val()=='1' || $('#cargo_id').val()=='2' || $('#cargo_id').val()=='5')){
        $('.grado').hide();
        $('.seccion_4').hide();
        $('.seccion_5').hide();
        $('.seccion_6').hide();

        $('#grado').val(null).trigger('change');
        $('#seccion_4').val(null).trigger('change');
        $('#seccion_5').val(null).trigger('change');
        $('#seccion_6').val(null).trigger('change');
    }

    if($('#tipo_institucion_educativa').val()=='1' && $('#cargo_id').val()=='3'){
        $('.grado').show();
        $('.seccion_4').hide();
        $('.seccion_5').hide();
        $('.seccion_6').hide();
        $('#seccion_4').val(null).trigger('change');
        $('#seccion_5').val(null).trigger('change');
        $('#seccion_6').val(null).trigger('change');
        tipo=1;
        Grados();
    }


    if($('#tipo_institucion_educativa').val()=='1' && $('#cargo_id').val()=='4'){
        $('.grado').show();
        $('.seccion_4').hide();
        $('.seccion_5').hide();
        $('.seccion_6').hide();
        $('#seccion_4').val(null).trigger('change');
        $('#seccion_5').val(null).trigger('change');
        $('#seccion_6').val(null).trigger('change');
        tipo=2;
        Grados();
    }


    if($('#tipo_institucion_educativa').val()=='2' && ($('#cargo_id').val()=='1' || $('#cargo_id').val()=='2' || $('#cargo_id').val()=='4' || $('#cargo_id').val()=='5')){
        $('.grado').hide();
        $('.seccion_4').hide();
        $('.seccion_5').hide();
        $('.seccion_6').hide();
        
        $('#grado').val(null).trigger('change');
        $('#seccion_4').val(null).trigger('change');
        $('#seccion_5').val(null).trigger('change');
        $('#seccion_6').val(null).trigger('change');
    }

    if($('#tipo_institucion_educativa').val()=='2' && $('#cargo_id').val()=='3'){
        $('.grado').show();
        $('.seccion_4').hide();
        $('.seccion_5').hide();
        $('.seccion_6').hide();
        
        $('#seccion_4').val(null).trigger('change');
        $('#seccion_5').val(null).trigger('change');
        $('#seccion_6').val(null).trigger('change');

        tipo=3;
        Grados();
    }


    if($('#tipo_institucion_educativa').val()=='3'){
        $('.cargo_id').hide();
        $('.grado').hide();
        $('.seccion_4').hide();
        $('.seccion_5').hide();
        $('.seccion_6').hide();

        $('#cargo_id').val('');
        $('#grado').val(null).trigger('change');
        $('#seccion_4').val(null).trigger('change');
        $('#seccion_5').val(null).trigger('change');
        $('#seccion_6').val(null).trigger('change');
    }

});

$('body').on('change', '#cargo_id', function (e) {
    
    if($('#tipo_institucion_educativa').val()=='1' && ($('#cargo_id').val()=='1' || $('#cargo_id').val()=='2' || $('#cargo_id').val()=='5')){
        $('.grado').hide();
        $('.seccion_4').hide();
        $('.seccion_5').hide();
        $('.seccion_6').hide();

        $('#grado').val(null).trigger('change');
        $('#seccion_4').val(null).trigger('change');
        $('#seccion_5').val(null).trigger('change');
        $('#seccion_6').val(null).trigger('change');
    }

    if($('#tipo_institucion_educativa').val()=='1' && $('#cargo_id').val()=='3'){
        $('.grado').show();
        $('.seccion_4').hide();
        $('.seccion_5').hide();
        $('.seccion_6').hide();
        $('#seccion_4').val(null).trigger('change');
        $('#seccion_5').val(null).trigger('change');
        $('#seccion_6').val(null).trigger('change');
        tipo=1;
        Grados();
    }


    if($('#tipo_institucion_educativa').val()=='1' && $('#cargo_id').val()=='4'){
        $('.grado').show();
        $('.seccion_4').hide();
        $('.seccion_5').hide();
        $('.seccion_6').hide();
        $('#seccion_4').val(null).trigger('change');
        $('#seccion_5').val(null).trigger('change');
        $('#seccion_6').val(null).trigger('change');
        tipo=2;
        Grados();
    }

    if($('#tipo_institucion_educativa').val()=='2' && ($('#cargo_id').val()=='1' || $('#cargo_id').val()=='2' || $('#cargo_id').val()=='4' || $('#cargo_id').val()=='5')){
        $('.grado').hide();
        $('.seccion_4').hide();
        $('.seccion_5').hide();
        $('.seccion_6').hide();
        
        $('#grado').val(null).trigger('change');
        $('#seccion_4').val(null).trigger('change');
        $('#seccion_5').val(null).trigger('change');
        $('#seccion_6').val(null).trigger('change');
    }

    if($('#tipo_institucion_educativa').val()=='2' && $('#cargo_id').val()=='3'){
        $('.grado').show();
        $('.seccion_4').hide();
        $('.seccion_5').hide();
        $('.seccion_6').hide();
        
        $('#seccion_4').val(null).trigger('change');
        $('#seccion_5').val(null).trigger('change');
        $('#seccion_6').val(null).trigger('change');

        tipo=3;
        Grados();
    }
});










$('body').on('change', '#grado', function (e) {
    if($(this).val()=="4" && $('#tipo_institucion_educativa').val()=="1" && $('#cargo_id').val()=="3"){
        $('.seccion_4').show();
        $('.seccion_5').hide();
        $('.seccion_6').hide();
        $('#seccion_4').select2({tags:true});
        $('#seccion_5').val(null).trigger('change');
        $('#seccion_6').val(null).trigger('change');
    }else if($(this).val()=="5" && $('#tipo_institucion_educativa').val()=="1" && $('#cargo_id').val()=="3"){
        $('.seccion_4').hide();
        $('.seccion_5').show();
        $('.seccion_6').hide();
        $('#seccion_4').val(null).trigger('change');
        $('#seccion_5').select2({tags:true});
        $('#seccion_6').val(null).trigger('change');
    }else if($(this).val()=="6" && $('#tipo_institucion_educativa').val()=="1" && $('#cargo_id').val()=="3"){
        $('.seccion_4').hide();
        $('.seccion_5').hide();
        $('.seccion_6').show();
        $('#seccion_4').val(null).trigger('change');
        $('#seccion_5').val(null).trigger('change');
        $('#seccion_6').select2({tags:true});
    }

    if($('#tipo_institucion_educativa').val()=="1" && $('#cargo_id').val()=="4"){
        $('.seccion_4').hide();
        $('.seccion_5').hide();
        $('.seccion_6').hide();

        $.each($(this).val(), function( index, value ) {
            if(value=="4"){
                $('.seccion_4').show();
                $('#seccion_4').select2({tags:true,multiple:true});
                $('#seccion_4').val(null).trigger('change');
            }

            if(value=="5"){
                $('.seccion_5').show();
                $('#seccion_5').select2({tags:true,multiple:true});
                $('#seccion_5').val(null).trigger('change');
            }

            if(value=="6"){
                $('.seccion_6').show();
                $('#seccion_6').select2({tags:true,multiple:true});
                $('#seccion_6').val(null).trigger('change');
            }
        });
    }
    


    
});

$('body').on('click', '.btn-grabar-registro', function (e) {
    //e.preventDefault();
    var form                        = $('#formRegistro');
    var error                       = "";
    var dni                         = $("[name=\"Registro[dni]\"]").val();
    var apellido_paterno            = $("[name=\"Registro[apellido_paterno]\"]").val();
    var apellido_materno            = $("[name=\"Registro[apellido_materno]\"]").val();
    var nombres                     = $("[name=\"Registro[nombres]\"]").val();
    var correo_electronico          = $("[name=\"Registro[correo_electronico]\"]").val();
    var celular                     = $("[name=\"Registro[celular]\"]").val();
    var fecha_nacimiento            = $("[name=\"Registro[fecha_nacimiento]\"]").val();
    var clave                       = $("[name=\"Registro[clave]\"]").val();
    //var codigo_verificador          = $("[name=\"Registro[codigo_verificador]\"]").val();
    var tipo_institucion_educativa  = $("[name=\"Registro[tipo_institucion_educativa]\"]").val();
    var departamento                = $("[name=\"Registro[departamento]\"]").val();
    var provincia                   = $("[name=\"Registro[provincia]\"]").val();
    var distrito                    = $("[name=\"Registro[distrito]\"]").val();
    var institucion_educativa_id    = $("[name=\"Registro[institucion_educativa_id]\"]").val();
    var cargo_id                    = $("[name=\"Registro[cargo_id]\"]").val();
    var cod_mod                     = $( "#institucion_educativa_id option:selected" ).attr('data-cod_modular');
    var grado                       = $("[name=\"Registro[grado]\"]").val();
    var seccion_4                   = $("[name=\"Registro[seccion_4]\"]").val();
    var seccion_5                   = $("[name=\"Registro[seccion_5]\"]").val();
    var seccion_6                   = $("[name=\"Registro[seccion_6]\"]").val();

    
    //formData.push({name: 'username', value: 'this is username'});
    
    if(!dni){
        $('.error_dni').addClass('alert-danger');
        $("#dni").focus();
        error = error + "error 1";
    }else{
        $('.error_dni').removeClass('alert-danger');
    }

    if(!apellido_paterno){
        $('.error_apellido_paterno').addClass('alert-danger');
        $("#apellido_paterno").focus();
        error = error + "error 1";
    }else{
        $('.error_apellido_paterno').removeClass('alert-danger');
    }

    // if(!apellido_materno){
    //     $('.error_apellido_materno').addClass('alert-danger');
    //     $("#apellido_paterno").focus();
    //     error = error + "error 1";
    // }else{
    //     $('.error_apellido_materno').removeClass('alert-danger');
    // }

    if(!nombres){
        $('.error_nombres').addClass('alert-danger');
        $("#nombres").focus();
        error = error + "error 1";
    }else{
        $('.error_nombres').removeClass('alert-danger');
    }

    if(!correo_electronico){
        $('.error_correo_electronico').addClass('alert-danger');
        $("#correo_electronico").focus();
        error = error + "error 1";
    }else{
        $('.error_correo_electronico').removeClass('alert-danger');
    }

    filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!filter.test(correo_electronico)) {
        $('.error_correo_electronico').addClass('alert-danger');
        $("#correo_electronico").focus();
        error = error + "error 1";
    }else{
        $('.error_correo_electronico').removeClass('alert-danger');
    }


    if(!celular){
        $('.error_celular').addClass('alert-danger');
        $("#celular").focus();
        error = error + "error 1";
    }else{
        $('.error_celular').removeClass('alert-danger');
    }

    if(!fecha_nacimiento){
        $('.error_fecha_nacimiento').addClass('alert-danger');
        $("#fecha_nacimiento").focus();
        error = error + "error 1";
    }else{
        $('.error_fecha_nacimiento').removeClass('alert-danger');
    }

    // if(!codigo_verificador){
    //     $('.error_codigo_verificador').addClass('alert-danger');
    //     $("#codigo_verificador").focus();
    //     error = error + "error 1";
    // }else{
    //     $('.error_codigo_verificador').removeClass('alert-danger');
    // }

    if(!clave){
        $('.error_clave').addClass('alert-danger');
        $("#clave").focus();
        error = error + "error 1";
    }else{
        $('.error_clave').removeClass('alert-danger');
    }

    if(clave.length<6 || clave.length>20){
        $('.error_clave_text').html('La contraseña no debe ser menor de 6 y no mayor de 20 caracteres');
        $('.error_clave').addClass('alert-danger');
        $("#clave").focus();
        error = error + "error 1";
    }else{
        $('.error_clave').removeClass('alert-danger');
    }

    


    if(!tipo_institucion_educativa){
        $('.error_tipo_institucion_educativa').addClass('alert-danger');
        $("#tipo_institucion_educativa").focus();
        error = error + "error 1";
    }else{
        $('.error_tipo_institucion_educativa').removeClass('alert-danger');
    }

    if(!departamento){
        $('.error_departamento').addClass('alert-danger');
        $("#departamento").focus();
        error = error + "error 1";
    }else{
        $('.error_departamento').removeClass('alert-danger');
    }

    if(!provincia){
        $('.error_provincia').addClass('alert-danger');
        $("#provincia").focus();
        error = error + "error 1";
    }else{
        $('.error_provincia').removeClass('alert-danger');
    }

    if(!distrito){
        $('.error_distrito').addClass('alert-danger');
        $("#distrito").focus();
        error = error + "error 1";
    }else{
        $('.error_distrito').removeClass('alert-danger');
    }

    if(!institucion_educativa_id){
        $('.error_institucion_educativa_id').addClass('alert-danger');
        $("#institucion_educativa_id").focus();
        error = error + "error 1";
    }else{
        $('.error_institucion_educativa_id').removeClass('alert-danger');
    }

    if(!cargo_id && (tipo_institucion_educativa=="1" || tipo_institucion_educativa=="2")){
        $('.error_cargo_id').addClass('alert-danger');
        $("#cargo_id").focus();
        error = error + "error 1";
    }else{
        $('.error_cargo_id').removeClass('alert-danger');
    }

    //Criterios de validación
    if(tipo_institucion_educativa == '3'){
        //$('.grado').hide();
    }

    
    if(error != ""){
        console.log(error);
        return false;
    }
    //$(this).prop('disabled', true);
    
    var form_data = new FormData(); // $('#formEncuesta01').serializeArray();
    form_data.append("_csrf", csrf);
    form_data.append("Registro[dni]", dni);
    form_data.append("Registro[apellido_paterno]", apellido_paterno);
    form_data.append("Registro[apellido_materno]", apellido_materno);
    form_data.append("Registro[nombres]", nombres);
    form_data.append("Registro[correo_electronico]", correo_electronico);
    form_data.append("Registro[celular]", celular);
    form_data.append("Registro[fecha_nacimiento]", fecha_nacimiento);
    form_data.append("Registro[clave]", clave);
    //form_data.append("Registro[codigo_verificador]", codigo_verificador);
    form_data.append("Registro[tipo_institucion_educativa]", tipo_institucion_educativa);
    form_data.append("Registro[departamento]", departamento);
    form_data.append("Registro[provincia]", provincia);
    form_data.append("Registro[distrito]", distrito);
    form_data.append("Registro[institucion_educativa_id]", institucion_educativa_id);
    form_data.append("Registro[cargo_id]", cargo_id);
    form_data.append("Registro[cod_mod]", cod_mod);
    form_data.append("Registro[grado]", grado);
    form_data.append("Registro[seccion_4]", seccion_4);
    form_data.append("Registro[seccion_5]", seccion_5);
    form_data.append("Registro[seccion_6]", seccion_6);
    

    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        cache: false,
		contentType: false,
		processData: false,
        data: form_data,
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success: function (results) {
            if(results.success){
                loading.modal('hide');
                // const message = JSON.stringify({
                //     success: true,
                // });
                // window.parent.postMessage(message, '*');

                //window.top.postMessage('hello', '*')
                //results.success;
                location.reload();
            }
        },
    });
});
</script>