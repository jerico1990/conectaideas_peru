<div id="myModal" class="modal d-block" style="background-image:url('<?= \Yii::$app->request->BaseUrl ?>/images/pantallas_conectaideas_2.0-03.png')">
    <!-- <div class="diagonal-bg">
		<svg xmlns='http://www.w3.org/2000/svg' width='100%' height='100%'><line x1='100%' y1='0' x2='1' y2='100%' stroke='#C4C4C4' stroke-width='0.1%'/></svg>
	</div>
	<div class="diagonal-bg">
		<svg xmlns='http://www.w3.org/2000/svg' width='100%' height='100%'><line x1='100%' y1='100%' x2='1' y2='0' stroke='#C4C4C4' stroke-width='0.1%'/></svg>
	</div> -->
    <div id="LandbotOptinContainer" class="n_desktop" style="width: 405px; height: 266px;margin-left:38%"></div>
    <div id="LandbotOptinContainerx" class="n_mobile" style="width: 100%; height: 266px;margin-left:0%"></div> 

    <div class="modal-container" >
        <div class="modal-content" style="margin-top:20px">
            <div class="d-flex justify-content-end invisible">
                <span class="close">&times;</span>
            </div>
            <div class="n_final-box-title n_form-title d-flex justify-content-center mb-3 mt-4"
                style="font-weight: bold;">
                ¡Felicidades!
            </div>
            <div class="d-flex justify-content-center mb-3">
                <div class="n_final-box n_form-subtitle text-center">
                    Has completado exitosamente tu registro al programa Conecta Ideas
                </div>
            </div>
            <div class="container box_res">
                <div class="row d-flex justify-content-center">
                    <div class="col-2 d-flex justify-content-end mobile-p">
                        <div class="container-image-box d-inline-block " style="    margin-left: -11px;">
                            <img src="<?= \Yii::$app->request->BaseUrl ?>/images/person.png" alt="">
                        </div>
                    </div>
                    <div class="col-10">
                        <div class="container-text-box d-inline-block text-justify pb-3">
                            Recibirás tu usuario en un tiempo máximo de 5 días útiles en el correo y el
                            celular que
                            registraste.
                        </div>
                    </div>
                </div>
                <div class="row d-flex justify-content-center mt-3">
                    <div class="col-2 d-flex justify-content-end mobile-p">
                        <div class="container-image-box d-inline-block" style="margin-right: 10px;">
                            <img src="<?= \Yii::$app->request->BaseUrl ?>/images/Vector.png" alt="">
                        </div>
                    </div>
                    <div class="col-10">
                        <div class="container-text-box d-inline-block text-justify pb-3">
                            Con este usuario, podrás ingresar al Portal Docente y a la aplicación de
                            Conecta Ideas Perú.
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-center pt-4">
                <div>
                    <img src="<?= \Yii::$app->request->BaseUrl ?>/images/Group 221.png" alt="">
                </div>
                <div class="n_box3 ml-3">
                    No olvides descargar el app de Conecta ideas en tu celular:
                </div>
            </div>
            <div class="d-flex justify-content-center my-3">
                <div class="ml-3 d-flex">
                    <div>
                        <a target="_blank" href="https://play.google.com/store/apps/details?id=com.conectaideas"><img src="<?= \Yii::$app->request->BaseUrl ?>/images/playstore.png" alt=""></a> 
                    </div>
                    <div>
                        <a target="_blank" href="https://apps.apple.com/pe/app/conectaideas/id1516647278"><img src="<?= \Yii::$app->request->BaseUrl ?>/images/applestore.png" alt=""></a> 
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-center mb-5 mt-4">
                <a href="https://www.conectaideasperu.com/" 
                    class="btn btn-dark n_form-button" >Volver al Home <span class=""><img src="<?= \Yii::$app->request->BaseUrl ?>/images/arrow-forward.png" alt=""></span>
                </a>
            </div>
        </div>
    </div>
    
</div>

<script>
    // $(window).scroll(function () {

    //     let pog = window.innerHeight;
    //     console.log("aadasd", pog);
    //     delta = 10;
    //     if ($(window).scrollTop() > pog + 9 * delta) {
    //         $('#scroller').addClass('stuck');
    //         document.getElementById("cont1").style.paddingTop = "90px";

    //         document.getElementById("scroller").style.visibility = "visible";
    //     } else {
    //         document.getElementById("cont1").style.paddingTop = "0px";
    //         document.getElementById("scroller").style.visibility = "hidden";
    //         $('#scroller').removeClass('stuck');
    //     }

    // });

    // var modal = document.getElementById("myModal");
    // var btn = document.getElementById("myBtn");
    // var span = document.getElementsByClassName("close")[0];
    // btn.onclick = function () {
    //     modal.style.display = "block";
    // }
    // span.onclick = function () {
    //     modal.style.display = "none";
    // }
    // window.onclick = function (event) {
    //     if (event.target == modal) {
    //         modal.style.display = "none";
    //     }
    // }
</script>

<script src="https://static.landbot.io/whatsapp-optin/whatsapp-optin.js"></script>
<script>
var landbotOptin = new Landbot.Optin({
"url": "https://messages.landbot.io/wa/W-488-HJRHJDJT19OHXFU8/opt_in",
"label": "Escribe 51 y tu número de celular para recibir más novedades de Conecta Ideas",
"customParams": {},
"container": "#LandbotOptinContainer"
});

var landbotOptinx = new Landbot.Optin({
"url": "https://messages.landbot.io/wa/W-488-HJRHJDJT19OHXFU8/opt_in",
"label": "Escribe 51 y tu número de celular para recibir más novedades de Conecta Ideas",
"customParams": {},
"container": "#LandbotOptinContainerx"
});
</script>