<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Foto */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['options' => ['id' => 'formRegistro', 'class' => 'form-horizontal', 'autocomplete' => 'off']]); ?>
<div class="row n_100vh">
    <div id="bg-primary" class="col-12 col-lg-4 col-xl-4 n_bg-primary movetop">
        <div class="container n_desktop">
            <div class="row">
                <div class="n_continer-header">
                    <div class="d-flex">
                        <div class="container-image">
                            <img src="<?= \Yii::$app->request->BaseUrl ?>/images/Frame.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="w-100 d-flex justify-content-center">
                    <div class="n_container-image-banner">
                        <div class="container-image">
                            <img src="<?= \Yii::$app->request->BaseUrl ?>/images/icono_formulario.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-center w-100">
                    <div class="flex-column">
                        <div class="n_title" style="font-weight: Bold;">
                            Formulario de Registro para Docentes y Directores
                        </div>
                        <div class="n_description">
                            Dirigido a docentes y directores de 4to, 5to y 6to de primaria de instituciones educativas públicas. Completa las siguientes secciones:
                            <!-- Completa las secciones del formulario para inscribirte al programa Conecta Ideas. -->
                        </div>
                        <div class="n_checklist">
                            <ul class="list-unstyled">
                                <li class="n_space-line-checks">
                                    <div class="n_combo-check svg_opcion_1">
                                        <div class="n_elipse">
                                            <div class="container-image">
                                                <img src="<?= \Yii::$app->request->BaseUrl ?>/images/elipse-limpia.png" alt="">
                                            </div>
                                        </div>
                                        <div class="n_check">
                                            <div class="container-text">
                                                1
                                            </div>
                                        </div>
                                    </div>
                                    <div class="n_combo-text">
                                        <div class="d-flex w-100 h-100 align-items-center">
                                            DATOS PERSONALES
                                        </div>
                                    </div>
                                </li>
                                <li class="n_space-line-checks">
                                    <div class="n_combo-check svg_opcion_2">
                                        <div class="n_elipse">
                                            <div class="container-image">
                                                <img src="<?= \Yii::$app->request->BaseUrl ?>/images/circlefull.png" alt="">
                                            </div>
                                        </div>
                                        <div class="n_check">
                                            <div class="container-text">
                                                2
                                            </div>
                                        </div>
                                    </div>
                                    <div class="n_combo-text">
                                        <div class="d-flex w-100 h-100 align-items-center">
                                            DATOS DE TU I.E.
                                        </div>
                                    </div>
                                </li>
                                <li class="n_space-line-checks">
                                    <div class="n_combo-check svg_opcion_3">
                                        <div class="n_elipse">
                                            <div class="container-image">
                                                <img src="<?= \Yii::$app->request->BaseUrl ?>/images/circlefull.png" alt="">
                                            </div>
                                        </div>
                                        <div class="n_check">
                                            <div class="container-text">
                                                3
                                            </div>
                                        </div>
                                    </div>
                                    <div class="n_combo-text">
                                        <div class="d-flex w-100 h-100 align-items-center">
                                            DATOS DE TU CARGO
                                        </div>
                                    </div>
                                </li>
                                <li class="n_space-line-checks">
                                    <div class="n_combo-check svg_opcion_4">
                                        <div class="n_elipse">
                                            <div class="container-image">
                                                <img src="<?= \Yii::$app->request->BaseUrl ?>/images/circlefull.png" alt="">
                                            </div>
                                        </div>
                                        <div class="n_check">
                                            <div class="container-text">
                                                4
                                            </div>
                                        </div>
                                    </div>
                                    <div class="n_combo-text">
                                        <div class="d-flex w-100 h-100 align-items-center">
                                            CONTRASEÑA
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div id="n_mobile" class="container n_mobile">
            <div class="row">
                <div class="n_continer-header">
                    <div class="d-flex">
                        <div class="container-image">
                            <img src="<?= \Yii::$app->request->BaseUrl ?>/images/Frame.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="w-100 d-flex justify-content-center">
                    <div class="n_container-image-banner">
                        <div class="container-image">
                            <img src="<?= \Yii::$app->request->BaseUrl ?>/images/icono_formulario.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-center w-100">
                    <div class="flex-column">
                        <div class="n_title" style="font-weight: Bold;">
                            Formulario de registro
                        </div>
                        <div class="n_description">
                            Completa las secciones del formulario para inscribirte al programa Conecta Ideas.
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-center w-100">
                    <a ng-click="deleteStart()" href="#cont1">
                        <button type="button" class="btn btn-dark n_form-button btn-ocultar">Empezar</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="d-block d-md-none d-lg-none d-xl-none">
        <div class="navbar_mobile">
            <div class="container">

            </div>
        </div>
    </div>
    <div id="d-mob" class="col-12 col-lg-8 col-xl-8 bg-white n_mobile_inic">
        <div class="container-scroller">
            <nav id="scroller" class="n_navbar navbar navbar-light">
                <div class="d-flex justify-content-between w-100">
                    <!--div class="my-auto">
                        <span>
                            <img class="n_left_arrow" src="<?= \Yii::$app->request->BaseUrl ?>/images/chevron-left-solid.svg" alt="">
                        </span>
                    </div-->
                    <div class="n_combo-check--mobile svg_opcion_1">
                        <div class="n_elipse">
                            <div class="container-image">
                                <img src="<?= \Yii::$app->request->BaseUrl ?>/images/elipse-limpia.png" alt="">
                            </div>
                        </div>
                        <div class="n_check">
                            <div class="container-text">
                                1
                            </div>
                        </div>
                    </div>
                    <div class="n_combo-check--mobile svg_opcion_2">
                        <div class="n_elipse">
                            <div class="container-image">
                                <img src="<?= \Yii::$app->request->BaseUrl ?>/images/circlefull.png" alt="">
                            </div>
                        </div>
                        <div class="n_check">
                            <div class="container-text">
                                2
                            </div>
                        </div>
                    </div>
                    <div class="n_combo-check--mobile svg_opcion_3">
                        <div class="n_elipse">
                            <div class="container-image">
                                <img src="<?= \Yii::$app->request->BaseUrl ?>/images/circlefull.png" alt="">
                            </div>
                        </div>
                        <div class="n_check">
                            <div class="container-text">
                                3
                            </div>
                        </div>
                    </div>
                    <div class="n_combo-check--mobile svg_opcion_4">
                        <div class="n_elipse">
                            <div class="container-image">
                                <img src="<?= \Yii::$app->request->BaseUrl ?>/images/circlefull.png" alt="">
                            </div>
                        </div>
                        <div class="n_check">
                            <div class="container-text">
                                4
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
        <div id="cont1" class="n_form_text">
            <div class="n_form-title" style="font-weight: bold;">
                Datos Personales 
            </div>
            <div class="n_form-subtitle">
                Empecemos llenando la información sobre ti
            </div>
            <div class="n_form-textbox-1">
                <div class="form-group">
                    <label for="dni" class="n_label_size">Número de DNI</label>
                    <input type="number" pattern="[0-9]" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" class="form-control n_inputs_height numerico error_dni" id="dni" name="Registro[dni]" placeholder="Ingresa tu número de DNI" minlength="8" maxlength="8">
                    <div class="error_dni_text"></div>
                </div>
                <div class="form-group">
                    <label for="nombre" class="n_label_size">Nombre</label>
                    <input type="text" class="form-control n_inputs_height texto error_nombres" id="nombres" name="Registro[nombres]" placeholder="Escribe tu nombre" maxlength="30">
                    <div class="error_nombres_text"></div>
                </div>
                <div class="form-group">
                    <label for="apepaterno" class="n_label_size">Apellido paterno</label>
                    <input type="text" class="form-control n_inputs_height texto error_apellido_paterno" id="apellido_paterno" aria-describedby="emailHelp" name="Registro[apellido_paterno]" placeholder="Escribe tu apellido paterno" maxlength="30">
                    <div class="error_apellido_paterno_text"></div>
                </div>
                <div class="form-group">
                    <label for="apematerno" class="n_label_size">Apellido materno</label>
                    <input type="text" class="form-control n_inputs_height texto error_apellido_materno" id="apellido_materno" aria-describedby="emailHelp" name="Registro[apellido_materno]" placeholder="Escribe tu apellido materno" maxlength="30">
                    <div class="error_apellido_materno_text"></div>
                </div>
                <div class="form-group">
                    <label for="fechanac" class="n_label_size">Fecha de nacimiento</label>
                    <!-- <input type="text" class="form-control n_inputs_height error_fecha_nacimiento" id="fecha_nacimiento" name="Registro[fecha_nacimiento]" placeholder="Ingrese fecha nacimiento" maxlength="10" data-date-format="dd-mm-yyyy">
                     -->
                    <div class="d-flex ">
                        <div class="n_date">
                            <div class="input-group">
                                <select class="custom-select custom-select--fixed n_selector error_fecha_nacimiento" id="dia">
                                    <option selected>Día</option>
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                </select>
                            </div>
                        </div>
                        <div class="n_date">
                            <div class="input-group">
                                <select class="custom-select custom-select--fixed n_selector error_fecha_nacimiento" id="mes">
                                    <option selected>Mes</option>
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                </select>
                            </div>
                        </div>
                        <div class="n_date2">
                            <div class="input-group">
                                <select class="custom-select custom-select--fixed n_selector error_fecha_nacimiento" id="anio">
                                    <option selected>Año</option>
                                    <option value="1950">1950</option>
                                    <option value="1951">1951</option>
                                    <option value="1952">1952</option>
                                    <option value="1953">1953</option>
                                    <option value="1954">1954</option>
                                    <option value="1955">1955</option>
                                    <option value="1956">1956</option>
                                    <option value="1957">1957</option>
                                    <option value="1958">1958</option>
                                    <option value="1959">1959</option>
                                    <option value="1960">1960</option>
                                    <option value="1961">1961</option>
                                    <option value="1962">1962</option>
                                    <option value="1963">1963</option>
                                    <option value="1964">1964</option>
                                    <option value="1965">1965</option>
                                    <option value="1966">1966</option>
                                    <option value="1967">1967</option>
                                    <option value="1968">1968</option>
                                    <option value="1969">1969</option>
                                    <option value="1970">1970</option>
                                    <option value="1971">1971</option>
                                    <option value="1972">1972</option>
                                    <option value="1973">1973</option>
                                    <option value="1974">1974</option>
                                    <option value="1975">1975</option>
                                    <option value="1976">1976</option>
                                    <option value="1977">1977</option>
                                    <option value="1978">1978</option>
                                    <option value="1979">1979</option>
                                    <option value="1980">1980</option>
                                    <option value="1981">1981</option>
                                    <option value="1982">1982</option>
                                    <option value="1983">1983</option>
                                    <option value="1984">1984</option>
                                    <option value="1985">1985</option>
                                    <option value="1986">1986</option>
                                    <option value="1987">1987</option>
                                    <option value="1988">1988</option>
                                    <option value="1989">1989</option>
                                    <option value="1990">1990</option>
                                    <option value="1991">1991</option>
                                    <option value="1992">1992</option>
                                    <option value="1993">1993</option>
                                    <option value="1994">1994</option>
                                    <option value="1995">1995</option>
                                    <option value="1996">1996</option>
                                    <option value="1997">1997</option>
                                    <option value="1998">1998</option>
                                    <option value="1999">1999</option>
                                    <option value="2000">2000</option>
                                    <option value="2001">2001</option>
                                    <option value="2002">2002</option>
                                    <option value="2003">2003</option>
                                    <option value="2004">2004</option>
                                    <option value="2005">2005</option>
                                    <option value="2006">2006</option>
                                    <option value="2007">2007</option>
                                    <option value="2008">2008</option>
                                    <option value="2009">2009</option>
                                    <option value="2010">2010</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="error_fecha_nacimiento_text"></div>
                </div>
            </div>
            <div class="n_form-textbox-2">
                <div class="form-group">
                    <label for="Email1" class="n_label_size">Correo electrónico</label>
                    <input type="email" class="form-control n_inputs_height error_correo_electronico" id="correo_electronico" aria-describedby="emailHelp" name="Registro[correo_electronico]" placeholder="Escribe tu correo electrónico">
                    <div class="error_correo_electronico_text"></div>
                </div>
                <div class="form-group">
                    <label for="phone" class="n_label_size">Número de celular</label>
                    <input type="number" pattern="[0-9]" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" class="form-control n_inputs_height numerico error_celular" id="celular" name="Registro[celular]" placeholder="Ingresa tu número de celular" maxlength="9">
                    <div class="error_celular_text"></div>
                </div>
            </div>
            <div class="d-flex justify-content-center justify-content-md-end justify-content-lg-end justify-content-xl-end">
                <button type="button" class="btn btn-dark n_form-button form_1"> Siguiente <span class=""><img src="<?= \Yii::$app->request->BaseUrl ?>/images/arrow-forward.png" alt=""></span></button>
            </div>
        </div>


        <div id="cont2" class="n_form_text" style="display: none;">
            <div class="n_form-title" style="font-weight: bold;">
                Datos de tu institución educativa
            </div>
            <div class="n_form-subtitle">
                Cuéntanos sobre la institución educativa en donde trabajas
            </div>
            <div class="n_form-textbox-1 pb-2">
                <div class="form-group">
                    <label for="instedu" class="n_label_size">Elige la forma en que deseas completar la
                        información</label>
                    <div class="input-group mb-3">
                        <select id="elegir_medio" class="form-control custom-select custom-select--fixed n_selector error_elegir_medio">
                            <option value>Selecciona una opción</option>
                            <option value="1">Por código modular</option>
                            <option value="2">Por ubicación</option>
                        </select>
                        <div class="error_elegir_medio_text"></div>
                    </div>
                </div>
                <div id="codieMaster" class="form-group justify-content-between align-items-end" style="display: none">
                    <div class="boxcodie">
                        <label for="codie" class="n_label_size">Ingresa el codigo modular de tu I.E. :</label>
                        <input type="number" pattern="[0-9]" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" class="form-control n_inputs_height numerico error_codigo_modular" id="codigo_modular" name="Registro[codigo_modular]" placeholder="Ingresa el código" maxlength="7">
                    </div>
                    <div class="d-md-inline-block d-lg-inline-block d-xl-inline-block  d-flex justify-content-center justify-content-md-end justify-content-lg-end justify-content-xl-end">
                        <button type="button" class="ml-lg-3 btn btn-dark n_form-button n_button-consult consultar_codigo">Consultar <span class=""><img src="<?= \Yii::$app->request->BaseUrl ?>/images/arrow-forward.png" alt=""></span></button>
                    </div>

                </div>
            </div>
            <div id="paso4" style="display:none">
                <div class="d-flex mb-3">
                    <div id="alert_verify_sign" class="admiration_sign inline-block por_modulo" style="display: none;">
                        !
                    </div>
                    <div id="alert_verify" class="inline-block n_label_size por_modulo" style="display: none;">
                        Verifica que esta información sea correcta:
                    </div>
                    <div id="alert_empty" class="inline-block n_label_size por_ubicacion" style="display: none;">
                        Completa la información sobre tu institución educativa:
                    </div>
                </div>
                <div class="n_form-textbox-1 pb-3">
                    <div class="form-group">
                        <label for="depa" class="n_label_size">Departamento</label>
                        <div class="input-group mb-3">
                            <select class="form-control custom-select custom-select--fixed n_selector error_departamento" id="departamento" name="Registro[departamento]">
                                <option value>Selecciona una opción</option>
                            </select>
                        </div>
                        <div class="error_departamento_text"></div>
                    </div>
                    <div class="form-group">
                        <label for="provi" class="n_label_size">Provincia</label>
                        <div class="input-group mb-3">
                            <select class="form-control custom-select custom-select--fixed n_selector error_provincia" id="provincia" name="Registro[provincia]">
                                <option value>Selecciona una opción</option>
                            </select>
                        </div>
                        <div class="error_provincia_text"></div>
                    </div>
                    <div class="form-group">
                        <label for="distr" class="n_label_size">Distrito</label>
                        <div class="input-group mb-3">
                            <select class="form-control custom-select custom-select--fixed n_selector error_distrito" id="distrito" name="Registro[distrito]">
                                <option value>Selecciona una opción</option>
                            </select>
                        </div>
                        <div class="error_distrito_text"></div>
                    </div>
                    <div class="form-group">
                        <label for="nameie" class="n_label_size">Elige tu Institución Educativa (I.E.)</label>
                        <div class="input-group mb-3">
                            <select class="form-control custom-select custom-select--fixed n_selector error_institucion_educativa_id" id="institucion_educativa_id" name="Registro[institucion_educativa_id]">
                                <option value>Selecciona una opción</option>
                            </select>
                        </div>
                        <div class="error_institucion_educativa_id_text"></div>
                    </div>
                    <div class="text-center n_label_size por_modulo" style="display:none">
                        ¿No es tu colegio? <br class="d-block d-md-none d-lg-none d-xl-none d-md-none"> <a href="#" class="n_link modificar_informacion">Modifica la información</a>
                    </div>
                </div>
                <div class="n_form-textbox-2">
                    <label for="typeie" class="n_label_size">Tipo de institución Educativa:</label>
                    <div class="input-group mb-3">
                        <select class="form-control custom-select custom-select--fixed n_selector error_tipo_institucion_educativa" disabled id="tipo_institucion_educativa" name="Registro[tipo_institucion_educativa]">
                            <option value>Selecciona una institución educativa</option>
                        </select>
                    </div>
                    <div class="error_tipo_institucion_educativa_text"></div>
                </div>
                <!-- <div class="d-flex justify-content-center justify-content-md-start justify-content-lg-start justify-content-xl-start">
                    <button type="button" class="btn n_form-button form_2_ant" style="background: white;border: 1px solid #858C94;color: #858C94;"> 
                    <span class=""><img src="<?= \Yii::$app->request->BaseUrl ?>/images/arrow-left.svg" alt=""></span>  Anterior </button>
                </div>
                <div class="d-flex justify-content-center justify-content-md-end justify-content-lg-end justify-content-xl-end">
                    <button type="button" class="btn btn-dark n_form-button form_2"> Siguiente <span class=""><img src="<?= \Yii::$app->request->BaseUrl ?>/images/arrow-forward.png" alt=""></span></button>
                </div> -->
            </div>
            <div class="form-group">
                <button type="button" class="btn n_form-button form_2_ant" style="background: white;border: 1px solid #858C94;color: #858C94;"> 
                    <span class=""><img src="<?= \Yii::$app->request->BaseUrl ?>/images/arrow-left.svg" alt=""></span>  Anterior 
                </button>
                <button type="button" class="btn btn-dark n_form-button form_2 pull-right" style="display:none"> 
                    Siguiente <span class=""><img src="<?= \Yii::$app->request->BaseUrl ?>/images/arrow-forward.png" alt=""></span>
                </button>
            </div>
        </div>
        <div id="cont3" class="n_form_text" style="display: none;">
            <div class="container">
                <div class="n_form-title" style="font-weight: bold;">
                    Datos de tu cargo
                </div>
                <div class="n_form-subtitle">
                    Seleccione el cargo que desempeña en la Institución Educativa en la que labora actualmente.
                </div>
                <div class="n_form-textbox-1 pb-2 w-100">
                    <div class="form-group">
                        <label for="instedu2" class="n_label_size">Selecciona tu cargo</label>
                        <div class="input-group mb-3">
                            <select class="form-control custom-select custom-select--fixed n_selector error_cargo_id" id="cargo_id" name="Registro[cargo_id]">
                                <option value>Selecciona una opción</option>
                            </select>
                        </div>
                        <div class="error_cargo_id_text"></div>
                    </div>
                </div>
                <div id="paso5" style="display: none;">
                    <div class="n_form-textbox-1 pb-0 mb-0 w-100">
                        <div class="form-group">
                            <label for="gradeGroup" class="n_label_size">Selecciona los grados que deseas inscribir al programa</label>
                            <div class="input-group mb-3 paso5_contenido error_paso5_contenido">
                                
                            </div>
                            <div class="error_paso5_contenido_text"></div>
                        </div>
                    </div>
                </div>

                <div id="paso6" style="display: none;">
                    <div class="n_form-subtitle-2">
                        Selecciona el aula que deseas inscribir al programa:
                    </div>
                    <div class="n_form-textbox-1 pb-2">
                        <div class="container">
                            <div class="row paso6_contenido">
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div id="paso7" style="display: none;">
                    <div class="n_form-textbox-1 pb-2 mt-3">
                        <div class="pb-3 n_font_600">
                            Selecciona las secciones que deseas inscribir al programa:
                        </div>
                        <div class="n_linea">

                        </div>
                        <div class="form-group paso7_contenido">
                            
                        </div>
                    </div>
                </div>

                <div class="n_form-subtitle">
                    <p>
                        * Recuerde que si enseña a un aula en específico y se desempeña como tutor y docente del área de matemática en dicha aula, deberá seleccionar "TUTOR". 
                    </p>
                    <p>
                        * Si solo es tutor en dicha aula y no enseña matemática, deberá seleccionar “TUTOR”. 
                    </p>
                    <p>
                        * Si enseña matemática a diferentes grados y/o secciones, pero no es tutor de alguna en específico, debe seleccionar "DOCENTE DEL ÁREA DE MATEMÁTICA".
                    </p>
                    
                </div>

                <div id="paso5555">
                    <!-- <div class="d-flex justify-content-center justify-content-md-start justify-content-lg-start justify-content-xl-start">
                        <button type="button" class="btn n_form-button form_3_ant" style="background: white;border: 1px solid #858C94;color: #858C94;">
                            <span class=""><img src="<?= \Yii::$app->request->BaseUrl ?>/images/arrow-left.svg" alt=""></span>
                            Anterior 
                        </button>
                    </div>
                    <div class="d-flex justify-content-center justify-content-md-end justify-content-lg-end justify-content-xl-end">
                        <button type="button" class="btn btn-dark n_form-button form_3">
                            Siguiente <span class=""><img src="<?= \Yii::$app->request->BaseUrl ?>/images/arrow-forward.png" alt=""></span>
                        </button>
                    </div> -->

                    <div class="form-group">
                        <button type="button" class="btn n_form-button form_3_ant" style="background: white;border: 1px solid #858C94;color: #858C94;">
                            <span class=""><img src="<?= \Yii::$app->request->BaseUrl ?>/images/arrow-left.svg" alt=""></span>
                            Anterior 
                        </button>
                        <button type="button" class="btn btn-dark n_form-button form_3 pull-right">
                            Siguiente <span class=""><img src="<?= \Yii::$app->request->BaseUrl ?>/images/arrow-forward.png" alt=""></span>
                        </button>
                    </div>
                </div>


            </div>
        </div>

        <div id="cont4" class="n_form_text" style="display: none;">
            <div id="paso8" class="d-block">
                <div class="n_form-title" style="font-weight: bold;">
                    Ya casi has terminado!
                </div>
                <div class="n_form-subtitle">
                    Como paso final, crea la contraseña que te servirá para ingresar al programa, asegúrate de
                    recordarla.
                </div>
                <div class="n_form-textbox-1 pb-2">
                    <div class="form-group justify-content-between align-items-end">
                        <div class="boxcodie w-100">
                            <label for="password1">Crea tu contraseña</label>
                            <div class="input-group">
                                <input type="text" class="form-control n_inputs_height error_clave" id="clave" name="Registro[clave]" placeholder="Escribe tu contraseña" maxlength="20">
                                <span class=" btn input-group-text btn-password"> <i class="bx bx-show"></i> </span>
                            </div>
                            <div class="error_clave_text"></div>
                        </div>
                    </div>
                </div>

                <div class="n_form-textbox-1 pb-2">
                    <div class="form-group justify-content-between align-items-end">
                        <div class="boxcodie w-100">
                            <label for="password2">Confirma tu contraseña</label>
                            <div class="input-group">
                                <input type="text" class="form-control n_inputs_height error_clave_confirma" id="clave_confirma" placeholder="Escribe tu contraseña" maxlength="20">
                                <span class="btn input-group-text btn-repassword"> <i class="bx bx-show"></i> </span>
                            </div>
                            <div class="error_clave_confirma_text"></div>
                        </div>
                    </div>
                </div>
                <div class="n_form-subtitle">
                    <div class="form-group justify-content-between align-items-end">
                        <div class="boxcodie w-100">
                            <div class="form-check ">
                                <input type="checkbox" id="aceptar_condicion" class="form-check-input error_aceptar_condicion" name="Registro[aceptar_condicion]">
                                <label class="label_aceptar_condicion" for="aceptar_condicion">Al hacer clic en "Finalizar", confirmo que estoy de acuerdo con los <u><b> <a target="_blank" href="http://conecta.monki.pe/terminos-y-condiciones/"> Términos y Condiciones </a> </b></u>  y la <u><b> <a target="_blank" href="http://conecta.monki.pe/politica-privacidad/"> Política de privacidad del Programa</a> </b></u> </label>
                            </div>
                            <div class="error_aceptar_condicion_text"></div>
                        </div>
                    </div>
                </div>

                <!-- <div class="d-flex justify-content-center justify-content-md-start justify-content-lg-start justify-content-xl-start">
                    <button data-toggle="modal" type="button" class="btn n_form-button n_button-consult form_4_ant" style="background: white;border: 1px solid #858C94;color: #858C94;">
                    <span class=""><img src="<?= \Yii::$app->request->BaseUrl ?>/images/arrow-left.svg" alt=""></span>
                        Anterior 
                    </button>
                </div>

                <div class="d-flex justify-content-center justify-content-md-end justify-content-lg-end justify-content-xl-end">
                    <button data-toggle="modal" type="button" class="btn btn-dark n_form-button n_button-consult btn-grabar-registro">Finalizar <span class=""><img src="<?= \Yii::$app->request->BaseUrl ?>/images/arrow-forward.png" alt=""></span>
                    </button>
                </div> -->

                <div class="form-group">
                    <button data-toggle="modal" type="button" class="btn n_form-button n_button-consult form_4_ant" style="background: white;border: 1px solid #858C94;color: #858C94;">
                        <span class=""><img src="<?= \Yii::$app->request->BaseUrl ?>/images/arrow-left.svg" alt=""></span>
                        Anterior 
                    </button>
                    <button data-toggle="modal" type="button" class="btn btn-dark n_form-button n_button-consult btn-grabar-registro pull-right">
                        Finalizar <span class=""><img src="<?= \Yii::$app->request->BaseUrl ?>/images/arrow-forward.png" alt=""></span>
                    </button>
                </div>

            </div>
        </div>
        <div id="myModal" class="modal">
            <div class="modal-content">
                <div class="d-flex justify-content-end invisible">
                    <span class="close">&times;</span>
                </div>
                <div class="n_final-box-title n_form-title d-flex justify-content-center mb-3" style="font-weight: bold;">
                    ¡Felicidades!
                </div>
                <div class="d-flex justify-content-center mb-5">
                    <div class="n_final-box n_form-subtitle">
                        Has completado exitosamente tu registro al programa Conecta Ideas
                    </div>
                </div>
                <div class="container box_res">
                    <div class="row d-flex justify-content-center">
                        <div class="col-2 d-flex justify-content-end mobile-p">
                            <div class="container-image-box d-inline-block " style="    margin-left: -11px;">
                                <img src="<?= \Yii::$app->request->BaseUrl ?>/images/person.png" alt="">
                            </div>
                        </div>
                        <div class="col-10">
                            <div class="container-text-box d-inline-block text-justify pb-3">
                                Recibirás tu usuario en un tiempo máximo de 5 días útiles en el correo y el
                                celular que
                                registraste.
                            </div>
                        </div>
                    </div>
                    <div class="row d-flex justify-content-center mt-3">
                        <div class="col-2 d-flex justify-content-end mobile-p">
                            <div class="container-image-box d-inline-block" style="margin-right: 10px;">
                                <img src="<?= \Yii::$app->request->BaseUrl ?>/images/Vector.png" alt="">
                            </div>
                        </div>
                        <div class="col-10">
                            <div class="container-text-box d-inline-block text-justify pb-3">
                                Con este usuario, podrás ingresar al Intranet para Docentes y a la aplicación de
                                Conecta Ideas Perú.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-center pt-3">
                    <div>
                        <img src="<?= \Yii::$app->request->BaseUrl ?>/images/Group 221.png" alt="">
                    </div>
                    <div class="n_box3 ml-3">
                        No olvides descargar el app de Conecta ideas en tu celular:
                    </div>
                </div>
                <div class="d-flex justify-content-center my-3">
                    <div class="ml-3 d-flex">
                        <div>
                            <img src="<?= \Yii::$app->request->BaseUrl ?>/images/playstore.png" alt="">
                        </div>
                        <div>
                            <img src="<?= \Yii::$app->request->BaseUrl ?>/images/applestore.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-center mb-4">
                    <button id="myBtn" data-toggle="modal" data-target="#exampleModal" type="button" class="btn btn-dark n_form-button" onClick="location.reload();">Volver al Home >
                    </button>
                </div>

            </div>

        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
<div class="modal" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog text-center" role="document">
        <i class="fa fa-refresh fa-spin"></i>
        Cargando
    </div>
</div>

<script>
    var csrf = "<?= Yii::$app->request->getCsrfToken() ?>";
    var loading = $('#staticBackdrop');
    // $("#fecha_nacimiento").datepicker({
    //     language: 'es',
    //     closeText: 'Cerrar',
    //     prevText: '<Ant',
    //     nextText: 'Sig>',
    //     currentText: 'Hoy',
    //     monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    //     monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    //     dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
    //     dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
    //     dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
    //     weekHeader: 'Sm',
    //     dateFormat: 'dd-mm-yyyy',
    //     firstDay: 1,
    //     isRTL: false,
    //     showMonthAfterYear: false,
    //     yearSuffix: ''
    // });


    $('body').on('change', '#elegir_medio', function(e) {
        e.preventDefault();
        $('#codigo_modular').val('');

        $('#departamento').val('');
        $('#provincia').html('<option value>Selecciona una opción</option>');
        $('#distrito').html('<option value>Selecciona una opción</option>');
        $('#institucion_educativa_id').html('<option value>Selecciona una opción</option>');
        $('#tipo_institucion_educativa').val('');
        

        $('.por_ubicacion').hide();
        $('.por_modulo').hide();
        $('.form_2').hide();

        // $('#codigo_modular').prop( "disabled", true );
        // $('.consultar_codigo').prop( "disabled", true );
        // $('#departamento').prop( "disabled", true );
        // $('#provincia').prop( "disabled", true );
        // $('#distrito').prop( "disabled", true );
        // $('#institucion_educativa_id').prop( "disabled", true );

        if ($(this).val() == "1") {
            // $('#codigo_modular').prop( "disabled", false );
            // $('.consultar_codigo').prop( "disabled", false );
            document.getElementById("paso4").style.display = "none";
            document.getElementById("codieMaster").style.display = "block";
            $('#departamento').prop("disabled", true);
            $('#provincia').prop("disabled", true);
            $('#distrito').prop("disabled", true);
            $('#institucion_educativa_id').prop("disabled", true);
            $('.por_modulo').show();
        }

        if ($(this).val() == "2") {
            document.getElementById("codieMaster").style.display = "none";
            document.getElementById("paso4").style.display = "block";
            $('#departamento').prop("disabled", false);
            $('#provincia').prop("disabled", false);
            $('#distrito').prop("disabled", false);
            $('#institucion_educativa_id').prop("disabled", false);
            $('.por_ubicacion').show();
            $('.form_2').show();
        }

    });

    $('body').on('click', '.modificar_informacion', function(e) {
        e.preventDefault();
        $('#codigo_modular').val('');
        $('#departamento').val('');
        $('#elegir_medio').val('');
        $('#provincia').html('<option value>Selecciona una opción</option>');
        $('#distrito').html('<option value>Selecciona una opción</option>');
        $('#institucion_educativa_id').html('<option value>Selecciona una opción</option>');


        $('.por_ubicacion').hide();
        $('.por_modulo').hide();
        document.getElementById("codieMaster").style.display = "none";
        document.getElementById("paso4").style.display = "none";
    });


    $('body').on('click', '.consultar_codigo', function(e, data) {
        e.preventDefault();
        document.getElementById("paso4").style.display = "block";
        InstitucionEducativa($('#codigo_modular').val());
        $('.form_2').show();
    });

    TipoInstitucionEducativa()
    async function TipoInstitucionEducativa() {
        await $.ajax({
            url: '<?= \Yii::$app->request->BaseUrl ?>/tipo-institucion-educativa/get-tipos-instituciones-educativas',
            method: 'POST',
            data: {
                "_csrf": csrf
            },
            dataType: 'Json',
            beforeSend: function() {

            },
            success: function(results) {
                if (results.success) {
                    var tipo_institucion_educativa_options = "<option value>Selecciona un tipo de institución educativa</option>";
                    $.each(results.data, function(index, value) {
                        tipo_institucion_educativa_options = tipo_institucion_educativa_options + "<option value='" + value.id + "'>" + value.descripcion + "</option>";
                    });
                    $('#tipo_institucion_educativa').html(tipo_institucion_educativa_options);
                }
            },
            error: function() {
                alert('No hay conectividad con el sistema');
            }
        });
    }



    Regiones()
    async function Regiones() {
        await $.ajax({
            url: '<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-regiones',
            method: 'POST',
            data: {
                "_csrf": csrf
            },
            dataType: 'Json',
            beforeSend: function() {
                //loading.modal("show");
            },
            success: function(results) {
                if (results.success) {
                    var regiones_options = "<option value>Selecciona una opción</option>";
                    $.each(results.data, function(index, value) {
                        regiones_options += "<option value='" + value.departamento + "'>" + value.departamento + "</option>";
                    });
                    $('#departamento').html(regiones_options);
                    //loading.modal('hide');
                }
            },
            error: function() {
                alert('No hay conectividad con el sistema');
            }
        });
    }


    async function Provincias(departamento, provincia) {
        await $.ajax({
            url: '<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-provincias',
            method: 'POST',
            data: {
                "_csrf": csrf,
                departamento: departamento
            },
            dataType: 'Json',
            beforeSend: function() {

                //loading.modal("show");
            },
            success: function(results) {
                if (results.success) {
                    var provincias_options = "<option value>Selecciona una opción</option>";
                    $.each(results.data, function(index, value) {
                        provincias_options += "<option value='" + value.provincia + "'>" + value.provincia + "</option>";
                    });
                    $('#provincia').html(provincias_options);
                    if (provincia) {
                        $('#provincia').val(provincia);
                    }
                    //loading.modal('hide');
                }
            },
            error: function() {
                alert('No hay conectividad con el sistema');
            }
        });
    }


    async function Distritos(provincia, distrito) {
        await $.ajax({
            url: '<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-distritos',
            method: 'POST',
            data: {
                "_csrf": csrf,
                provincia: provincia
            },
            dataType: 'Json',
            beforeSend: function() {
                //loading.modal("show");
            },
            success: function(results) {
                if (results.success) {
                    var distritos_options = "<option value>Selecciona una opción</option>";
                    $.each(results.data, function(index, value) {
                        distritos_options += "<option value='" + value.distrito + "'>" + value.distrito + "</option>";
                    });
                    $('#distrito').html(distritos_options);
                    if (distrito) {
                        $('#distrito').val(distrito);
                    }
                    //loading.modal('hide');
                }
            },
            error: function() {
                alert('No hay conectividad con el sistema');
            }
        });
    }

    function wait(timeout) {
        return new Promise(resolve => {
            setTimeout(resolve, timeout);
        });
    }


    function InstitucionEducativa(codigo_modular) {
        codigo_modular = ("0000000" + codigo_modular).substr(-7, 7)

        $.ajax({
            url: '<?= \Yii::$app->request->BaseUrl ?>/institucion-educativa/get-institucion-educativa-codigo-modular',
            method: 'POST',
            data: {
                "_csrf": csrf,
                codigo_modular: codigo_modular
            },
            dataType: 'Json',
            beforeSend: function() {
                //loading.modal("show");
            },
            success: function(results) {
                if (results.success) {

                    if (results.data.length > 0) {
                        departamento = results.data[0].departamento;
                        provincia = results.data[0].provincia;
                        distrito = results.data[0].distrito;
                        institucion_educativa = results.data[0].id;
                        $('#departamento').val(departamento);
                        Provincias(departamento, provincia);
                        Distritos(provincia, distrito);
                        InstitucionesEducativas(departamento, provincia, distrito, institucion_educativa);
                        $('#tipo_institucion_educativa').val(results.data[0].tipo_ie_id);
                    }

                    if (results.data.length == 0) {
                        alert("El código modular no se encuentra.")
                    }
                    //loading.modal('hide');
                }
            },
            error: function() {
                alert('No hay conectividad con el sistema');
            }
        });
    }


    async function InstitucionesEducativas(departamento, provincia, distrito, institucion_educativa) {
        await $.ajax({
            url: '<?= \Yii::$app->request->BaseUrl ?>/institucion-educativa/get-instituciones-educativas',
            method: 'POST',
            data: {
                "_csrf": csrf,
                departamento: departamento,
                provincia: provincia,
                distrito: distrito
            },
            dataType: 'Json',
            beforeSend: function() {
                //loading.modal("show");
            },
            success: function(results) {
                if (results.success) {
                    var instituciones_educativas_options = "<option value>Selecciona una opción</option>";
                    $.each(results.data, function(index, value) {
                        instituciones_educativas_options += "<option value='" + value.id + "' data-cod_modular='" + value.cod_mod + "' data-tipo_ie_id='" + value.tipo_ie_id + "' > " + value.nombre_ie + "</option>";
                    });
                    $('#institucion_educativa_id').html(instituciones_educativas_options);
                    if (institucion_educativa) {
                        $('#institucion_educativa_id').val(institucion_educativa).trigger('change');
                    }

                    //loading.modal('hide');
                }
            },
            error: function() {
                alert('No hay conectividad con el sistema');
            }
        });
    }

    async function Cargos(tipo_institucion_id) {
        await $.ajax({
            url: '<?= \Yii::$app->request->BaseUrl ?>/cargo/get-cargos-tipo-institucion',
            method: 'POST',
            data: {
                "_csrf": csrf,
                tipo_institucion_id: tipo_institucion_id
            },
            dataType: 'Json',
            beforeSend: function() {

            },
            success: function(results) {
                if (results.success) {
                    var cargos_options = "<option value>Selecciona una opción</option>";
                    $.each(results.data, function(index, value) {
                        cargos_options += "<option value='" + value.id + "'>" + value.descripcion + "</option>";
                    });
                    $('#cargo_id').html(cargos_options);
                }
            },
            error: function() {
                alert('No hay conectividad con el sistema');
            }
        });
    }



    var tipo;

    async function Grados() {
        await $.ajax({
            url: '<?= \Yii::$app->request->BaseUrl ?>/grado/get-grados',
            method: 'POST',
            data: {
                "_csrf": csrf
            },
            dataType: 'Json',
            beforeSend: function() {
                //loading.modal("show");
            },
            success: function(results) {
                //$('#grado').select2('destroy');
                if (results.success) {
                    var grados_options = "";



                    if (tipo == 1) {
                        //$(".contenedor_derecho_8").css("height", "100px");

                        grados_options += ` <div class="col-12 col-lg-6">
                                                <div class="form-group">
                                                    <label for="gradeGroup" style="font-weight: 600;"></label>
                                                    <div class="form-group">
                                                        <label for="">Selecciona el grado:</label>
                                                        <div class="input-group mb-3">`
                                        grados_options += ` <select class="form-control custom-select custom-select--fixed n_selector error_grado" id="grado" name="Registro[grado]">`;
                                        grados_options += `<option value>Seleccione una opción</option>`;
                                        $.each(results.data, function(index, value) {
                                            var descripcion = value.descripcion;
                                            var grado = value.grado;
                                            grados_options += `<option value="${grado}">${descripcion} </option>`;
                                        });
                                        grados_options += ` </select>
                                                        </div>
                                                        <div class="error_grado_text"></div>
                                                    </div>
                                                </div>
                                            </div>`;

                        grados_options += ` <div class="col-12 col-lg-6">
                                                <div class="form-group">
                                                    <label for="gradeGroup" style="font-weight: 600;"></label>
                                                    <div class="form-group">
                                                        <label for="">Selecciona el aula:</label>
                                                        <div class="input-group mb-3">`;
                                        grados_options += ` <select class="form-control custom-select custom-select--fixed n_selector error_seccion" id="seccion" name="Registro[seccion]">`;
                                            grados_options += `<option value>Seleccione una opción</option>`;
                                            grados_options += `<option value="A">Sección A</option>`;
                                            grados_options += `<option value="B">Sección B</option>`;
                                            grados_options += `<option value="C">Sección C</option>`;
                                            grados_options += `<option value="D">Sección D</option>`;
                                            grados_options += `<option value="E">Sección E</option>`;
                                            grados_options += `<option value="F">Sección F</option>`;
                                            grados_options += `<option value="G">Sección G</option>`;
                                            grados_options += `<option value="H">Sección H</option>`;
                                            grados_options += `<option value="I">Sección I</option>`;
                                            grados_options += `<option value="J">Sección J</option>`;
                                            grados_options += `<option value="otros">OTRO</option>`;
                                        grados_options += ` </select>
                                                        </div>
                                                        <div class="error_seccion_text"></div>
                                                    </div>
                                                </div>
                                            </div>`;
                        grados_options += ` <div class="col-12 col-lg-12 tutor_aula_otros" style="display:none">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <input type="text" class="form-control n_inputs_height error_otros texto2" id="otros" placeholder="Escribe el nombre de la sección" maxlength="30">
                                                    </div>
                                                    <div class="error_otros_text"></div>
                                                </div>
                                            </div>`;
                        $('.paso6_contenido').html(grados_options);
                        $('.texto2').keypress(function(tecla) {
                            var reg = /^[a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ'_\s]+$/;
                            if(!reg.test(String.fromCharCode(tecla.which))){
                                return false;
                            }
                            return true;
                        });
                        
                    }

                    if (tipo == 2) {
                        Array_secciones = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];




                        grados_options = grados_options + ` <div class="n_title_grade n_font_500_16">
                                                                4to de Primaria
                                                            </div>`;
                        grados_options = grados_options + ` <div class="seccion_4_contenido error_seccion_4_contenido"> `;
                        $.each(Array_secciones, function(index, value) {
                            grados_options = grados_options + ` <div class="n_section_box mx-1 d-inline-block">
                                                                    <div class="form-check form-check-inline n_form-button--options my-3 p-0">
                                                                        <input class="invisible form-check-input form-check-input-switcher d-none" type="checkbox" id="seccion_4_${index}" value="${value}" name="Registro[seccion_4]">
                                                                        <label  class="n_hijo n_label_size align-items-center form-check-label d-flex d-sm-flex d-md-flex d-lg-flex d-xl-flex" for="inlineCheckbox-4">
                                                                            <span class="n_hijo n_check_box mr-2" style="display: none;"><img src="<?= \Yii::$app->request->BaseUrl ?>/images/check-solid.svg" alt=""></span>
                                                                        ${value}</label>
                                                                    </div>
                                                                </div>`;
                        });
                        grados_options = grados_options + ` </div> `;
                        grados_options = grados_options + ` <div class="error_seccion_4_contenido_text"></div> `;
                        grados_options = grados_options + ` <div class="input-group mb-3 n_180 mt-4 ">
                                                                <input type="text" class="form-control n_inputs_height bg-transparent n_input pl-1 texto2" style="border-radius: .25rem;" id="otros_seccion_4" aria-label="Default" placeholder="Otro nombre">
                                                                <div class="d-flex align-items-center">
                                                                    <a title="agregar" class="btn_agregar_seccion_4"><img src="<?= \Yii::$app->request->BaseUrl ?>/images/iconplus.png" alt=""></a>
                                                                </div>
                                                            </div>`;

                        grados_options = grados_options + ` <div class="n_title_grade n_font_500_16">
                                                                5to de Primaria
                                                            </div>`;
                        grados_options = grados_options + ` <div  class="seccion_5_contenido error_seccion_5_contenido"> `;
                        $.each(Array_secciones, function(index, value) {
                            grados_options = grados_options + ` <div class="n_section_box mx-1 d-inline-block">
                                                                    <div class="form-check form-check-inline n_form-button--options my-3 p-0">
                                                                        <input class="invisible form-check-input form-check-input-switcher d-none" type="checkbox" id="seccion_5_${index}" value="${value}" name="Registro[seccion_5]">
                                                                        <label  class="n_hijo n_label_size align-items-center form-check-label d-flex d-sm-flex d-md-flex d-lg-flex d-xl-flex" for="inlineCheckbox-5">
                                                                            <span class="n_hijo n_check_box mr-2" style="display: none;"><img src="<?= \Yii::$app->request->BaseUrl ?>/images/check-solid.svg" alt=""></span>
                                                                        ${value}</label>
                                                                    </div>
                                                                </div>`;
                        });
                        grados_options = grados_options + ` </div> `;
                        grados_options = grados_options + ` <div class="error_seccion_5_contenido_text"></div> `;
                        grados_options = grados_options + ` <div class="input-group mb-3 n_180 mt-4">
                                                                <input type="text" class="form-control n_inputs_height bg-transparent n_input pl-1 texto2" style="border-radius: .25rem;" id="otros_seccion_5" aria-label="Default" placeholder="Otro nombre">
                                                                <div class="d-flex align-items-center">
                                                                    <a title="agregar" class="btn_agregar_seccion_5"><img src="<?= \Yii::$app->request->BaseUrl ?>/images/iconplus.png" alt=""></a>
                                                                </div>
                                                            </div>`;


                        grados_options = grados_options + ` <div class="n_title_grade n_font_500_16">
                                                                6to de Primaria
                                                            </div>`;
                        grados_options = grados_options + ` <div  class="seccion_6_contenido error_seccion_6_contenido"> `;
                        $.each(Array_secciones, function(index, value) {
                            grados_options = grados_options + ` <div class="n_section_box mx-1 d-inline-block">
                                                                    <div class="form-check form-check-inline n_form-button--options my-3 p-0">
                                                                        <input class="invisible form-check-input d-none form-check-input-switcher" type="checkbox" id="seccion_6_${index}" value="${value}" name="Registro[seccion_6]">
                                                                        <label  class="n_hijo n_label_size align-items-center form-check-label d-flex d-sm-flex d-md-flex d-lg-flex d-xl-flex" for="inlineCheckbox-6">
                                                                            <span class="n_hijo n_check_box mr-2" style="display: none;"><img src="<?= \Yii::$app->request->BaseUrl ?>/images/check-solid.svg" alt=""></span>
                                                                        ${value}</label>
                                                                    </div>
                                                                </div>`;
                        });
                        grados_options = grados_options + ` </div> `;
                        grados_options = grados_options + ` <div class="error_seccion_6_contenido_text"></div> `;
                        grados_options = grados_options + ` <div class="input-group mb-3 n_180 mt-4">
                                                                <input type="text" class="form-control n_inputs_height bg-transparent n_input pl-1 texto2" style="border-radius: .25rem;" id="otros_seccion_6" aria-label="Default" placeholder="Otro nombre">
                                                                <div class="d-flex align-items-center">
                                                                    <a title="agregar" class="btn_agregar_seccion_6"><img src="<?= \Yii::$app->request->BaseUrl ?>/images/iconplus.png" alt=""></a>
                                                                </div>
                                                            </div>`;

                    $('.paso7_contenido').html(grados_options);
                    $('.texto2').keypress(function(tecla) {
                        var reg = /^[a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ'_\s]+$/;
                        if(!reg.test(String.fromCharCode(tecla.which))){
                            return false;
                        }
                        return true;
                    });
                }

                if(tipo==3){
                    
                                                
                    contador = 1;
                    $.each(results.data, function( index, value ) {
                        var descripcion = value.descripcion;
                        var grado = value.grado;
                        var descripcion_corta = value.nombre_corto;
                        grados_options +=  `<div class="form-check form-check-inline-grados n_form-button--options my-3 p-0" >
                                                <input class="invisible form-check-input  form-check-input-switcher d-none" type="checkbox" id="inlineCheckbox${contador}" value="${grado}" name="Registro[grado]">
                                                <label class="n_hijo n_label_size align-items-center form-check-label d-none d-sm-none d-md-flex d-lg-flex d-xl-flex" for="inlineCheckbox-${grado}">
                                                    <span class="n_hijo n_check_box mr-2" style="display: none;"><img src="<?= \Yii::$app->request->BaseUrl ?>/images/check-solid.svg" alt=""></span>${descripcion}
                                                </label>
                                                <label class="n_hijo form-check-label align-items-center d-flex d-md-none d-lg-none d-xl-none" for="inlineCheckbox-${grado}">
                                                <span class="n_hijo n_check_box mr-2" style="display: none;"><img src="<?= \Yii::$app->request->BaseUrl ?>/images/check-solid.svg" alt=""></span>${descripcion_corta}</label>
                                            </div>`;
                            contador++;
                        });
                        $('.paso5_contenido').html(grados_options);
                    }



                    //loading.modal('hide');
                }
            },
            error: function() {
                alert('No hay conectividad con el sistema');
            }
        });
    }

    
    function validarDNI(dni) {
        var banderaDNI = false;
        $.ajax({
            async: false,
            url: '<?= \Yii::$app->request->BaseUrl ?>/registrar/get-validar-dni',
            method: 'POST',
            data: {
                "_csrf": csrf,
                dni: dni
            },
            dataType: 'Json',
            beforeSend: function() {

            },
            success: function(results) {
                console.log("2");
                if (results.success) {
                    console.log(results);
                    if(results.status=="0"){
                        banderaDNI = true;    
                    }
                }
            },
            error: function() {
                alert('No hay conectividad con el sistema');
            }
        });
        return banderaDNI;
    }


    function validarCorreo(correo_electronico) {
        var banderaCorreo = false;
        $.ajax({
            async: false,
            url: '<?= \Yii::$app->request->BaseUrl ?>/registrar/get-validar-correo',
            method: 'POST',
            data: {
                "_csrf": csrf,
                correo_electronico: correo_electronico
            },
            dataType: 'Json',
            beforeSend: function() {

            },
            success: function(results) {
                if (results.success) {
                    if(results.status=="0"){
                        banderaCorreo = true;    
                    }
                }
            },
            error: function() {
                alert('No hay conectividad con el sistema');
            }
        });
        return banderaCorreo;
    }

    $('body').on('change', '#departamento', function(e) {
        departamento = $(this).val();
        Provincias(departamento);
    });


    $('body').on('change', '#provincia', function(e) {
        provincia = $(this).val();
        Distritos(provincia);
    });

    $('body').on('change', '#distrito', function(e) {
        departamento = $('#departamento').val();
        provincia = $('#provincia').val();
        distrito = $('#distrito').val();

        InstitucionesEducativas(departamento, provincia, distrito);
    });

    $('body').on('change', '#institucion_educativa_id', function(e) {
 
        tipo_ie_id = $("#institucion_educativa_id option:selected").attr('data-tipo_ie_id');

        //InstitucionEducativa(codigo_modular);

        $('#paso5').hide();
        $('#paso6').hide();
        $('#paso7').hide();
        $('.paso5_contenido').html('');
        $('.paso6_contenido').html('');
        $('.paso7_contenido').html('');
        Cargos(tipo_ie_id);
        $('#tipo_institucion_educativa').val(tipo_ie_id);
    });
    


    // $('body').on('change', '#tipo_institucion_educativa', function(e) {
    //     $('#paso5').hide();
    //     $('#paso6').hide();
    //     $('#paso7').hide();
    //     $('.paso5_contenido').html('');
    //     $('.paso6_contenido').html('');
    //     $('.paso7_contenido').html('');
    //     Cargos($(this).val());
    // });

    $('body').on('change', '#cargo_id', function(e) {

        $('#paso5').hide();
        $('#paso6').hide();
        $('#paso7').hide();
        $('.paso5_contenido').html('');
        $('.paso6_contenido').html('');
        $('.paso7_contenido').html('');
        if ($('#tipo_institucion_educativa').val() == '1' && $('#cargo_id').val() == '3') {
            $('#paso6').show();
            tipo = 1;
            Grados();
        }


        if ($('#tipo_institucion_educativa').val() == '1' && $('#cargo_id').val() == '4') {
            $('#paso7').show();
            tipo = 2;
            Grados();
        }

        if ($('#tipo_institucion_educativa').val() == '2' && $('#cargo_id').val() == '8') {
            $('#paso5').show();
            tipo = 3;
            Grados();
        }
    });


    $('body').on('click', '.btn-grabar-registro', function(e) {
        e.preventDefault();
        var form = $('#formRegistro');
        var error = "";
        var dni = $("[name=\"Registro[dni]\"]").val();
        var apellido_paterno = $("[name=\"Registro[apellido_paterno]\"]").val();
        var apellido_materno = $("[name=\"Registro[apellido_materno]\"]").val();
        var nombres = $("[name=\"Registro[nombres]\"]").val();
        var correo_electronico = $("[name=\"Registro[correo_electronico]\"]").val();
        var celular = $("[name=\"Registro[celular]\"]").val();
        // var fecha_nacimiento = $("[name=\"Registro[fecha_nacimiento]\"]").val();
        var clave = $("[name=\"Registro[clave]\"]").val();
        var tipo_institucion_educativa = $("[name=\"Registro[tipo_institucion_educativa]\"]").val();
        var departamento = $("[name=\"Registro[departamento]\"]").val();
        var provincia = $("[name=\"Registro[provincia]\"]").val();
        var distrito = $("[name=\"Registro[distrito]\"]").val();
        var institucion_educativa_id = $("[name=\"Registro[institucion_educativa_id]\"]").val();
        var cargo_id = $("[name=\"Registro[cargo_id]\"]").val();
        var cod_mod = $("#institucion_educativa_id option:selected").attr('data-cod_modular');
        var grado = $("[name=\"Registro[grado]\"]");
        var Array_grado = [];
        var seccion = $("[name=\"Registro[seccion]\"]").val();
        var seccion_4 = $("[name=\"Registro[seccion_4]\"]");
        var seccion_5 = $("[name=\"Registro[seccion_5]\"]");
        var seccion_6 = $("[name=\"Registro[seccion_6]\"]");
        var Array_seccion_4 = [];
        var Array_seccion_5 = [];
        var Array_seccion_6 = [];

        var clave_confirma = $('#clave_confirma').val();
        var aceptar_condicion = $("[name=\"Registro[aceptar_condicion]\"]")[0].checked;



        if (tipo_institucion_educativa == "2" && cargo_id == "8") {
            $.each(grado, function(index, value) {
                if (value.checked) {
                    Array_grado.push(value.value)
                }
            });
        }

        if (tipo_institucion_educativa == "1" && cargo_id == "4") {
            console.log(seccion_4);
            if (seccion_4.length >= 10) {
                $.each(seccion_4, function(index, value) {
                    if (value.checked) {
                        Array_seccion_4.push(value.value)
                    }
                });
                if (Array_seccion_4.length > 0) {
                    Array_grado.push("4");
                }
            }

            if (seccion_5.length >= 10) {
                $.each(seccion_5, function(index, value) {
                    if (value.checked) {
                        Array_seccion_5.push(value.value)
                    }
                });
                if (Array_seccion_5.length > 0) {
                    Array_grado.push("5");
                }
            }

            if (seccion_6.length >= 10) {
                $.each(seccion_6, function(index, value) {
                    if (value.checked) {
                        Array_seccion_6.push(value.value)
                    }
                });
                if (Array_seccion_6.length > 0) {
                    Array_grado.push("6");
                }
            }
        }



        var dia = $('#dia').val();
        var mes = $('#mes').val();
        var anio = $('#anio').val();
        var fecha_nacimiento_validar = moment(`${anio}-${mes}-${dia}`);
        var fecha_nacimiento = `${anio}-${mes}-${dia}`;
        
        var banderaDNI = validarDNI(dni);
        var banderaCorreo = validarCorreo(correo_electronico);
        
        

        if (!dni) {
            $('.error_dni').addClass('alert-danger');
            $('.error_dni').css('border', '1px solid #DA1414');
            $('.error_dni_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Ingrese un número de DNI válido</div>`);
            $("#dni").focus();
            error = error + "error 1";
        }else if (dni && dni.length<8) {
            $('.error_dni').addClass('alert-danger');
            $('.error_dni').css('border', '1px solid #DA1414');
            $('.error_dni_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Ingrese un número de DNI válido</div>`);
            $("#dni").focus();
            error = error + "error 1";
        }else if (dni && !banderaDNI) {
            $('.error_dni').addClass('alert-danger');
            $('.error_dni').css('border', '1px solid #DA1414');
            $('.error_dni_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> El usuario ya se encuentra registrado</div>`);
            $("#dni").focus();
            error = error + "error 1";
        }
        else {
            $('.error_dni').removeClass('alert-danger');
            $('.error_dni').css('border', '');
            $('.error_dni_text').html(``);
        }

        

        if (!apellido_paterno) {
            $('.error_apellido_paterno').addClass('alert-danger');
            $('.error_apellido_paterno').css('border', '1px solid #DA1414');
            $('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            $("#apellido_paterno").focus();
            error = error + "error 1";
        } else {
            $('.error_apellido_paterno').removeClass('alert-danger');
            $('.error_apellido_paterno').css('border', '');
            $('.error_apellido_paterno_text').html(``);
        }

        if (!nombres) {
            $('.error_nombres').addClass('alert-danger');
            $('.error_nombres').css('border', '1px solid #DA1414');
            $('.error_nombres_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            $("#nombres").focus();
            error = error + "error 1";
        } else {
            $('.error_nombres').removeClass('alert-danger');
            $('.error_nombres').css('border', '');
            $('.error_nombres_text').html(``);
        }

        filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!correo_electronico) {
            $('.error_correo_electronico').addClass('alert-danger');
            $('.error_correo_electronico').css('border', '1px solid #DA1414');
            $('.error_correo_electronico_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Verifica tu correo electrónico</div>`);
            $("#correo_electronico").focus();
            error = error + "error 1";
        }else if (correo_electronico && !filter.test(correo_electronico)) {
            $('.error_correo_electronico').addClass('alert-danger');
            $('.error_correo_electronico').css('border', '1px solid #DA1414');
            $('.error_correo_electronico_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Verifica tu correo electrónico</div>`);
            $("#correo_electronico").focus();
            error = error + "error 1";
        }else if (correo_electronico && !banderaCorreo) {
            $('.error_correo_electronico').addClass('alert-danger');
            $('.error_correo_electronico').css('border', '1px solid #DA1414');
            $('.error_correo_electronico_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este correo ya se encuentra registrado</div>`);
            $("#correo_electronico").focus();
            error = error + "error 1";
        } else {
            $('.error_correo_electronico').removeClass('alert-danger');
            $('.error_correo_electronico').css('border', '');
            $('.error_correo_electronico_text').html(``);
        }

        if (!celular) {
            $('.error_celular').addClass('alert-danger');
            $('.error_celular').css('border', '1px solid #DA1414');
            $('.error_celular_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            $("#celular").focus();
            error = error + "error 1";
        } else if (celular && celular.length<8) {
            $('.error_celular').addClass('alert-danger');
            $('.error_celular').css('border', '1px solid #DA1414');
            $('.error_celular_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Número incorrecto, verifica que tenga 9 dígitos</div>`);
            $("#celular").focus();
            error = error + "error 1";
        } else if (celular && celular.substring(0, 1)!='9') {
            $('.error_celular').addClass('alert-danger');
            $('.error_celular').css('border', '1px solid #DA1414');
            $('.error_celular_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Número incorrecto, verifica que empiece con 9.</div>`);
            $("#celular").focus();
            error = error + "error 1";
        } else {
            $('.error_celular').removeClass('alert-danger');
            $('.error_celular_text').html(``);
            $('.error_celular').css('border', '');
        }

        if (!fecha_nacimiento_validar.isValid()) {
            $('.error_fecha_nacimiento').addClass('alert-danger');
            $('.error_fecha_nacimiento').css('border', '1px solid #DA1414');
            $('.error_fecha_nacimiento_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> La fecha es incorrecta, inténtalo de nuevo</div>`);
            $("#fecha_nacimiento").focus();
            error = error + "error 1";
        } else {
            $('.error_fecha_nacimiento').removeClass('alert-danger');
            $('.error_fecha_nacimiento_text').html(``);
            $('.error_fecha_nacimiento').css('border', '');
        }


        if (!elegir_medio) {
            $('.error_elegir_medio').addClass('alert-danger');
            $('.error_elegir_medio').css('border', '1px solid #DA1414');
            $('.error_elegir_medio_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            $("#elegir_medio").focus();
            error = error + "error 1";
        } else {
            $('.error_elegir_medio').removeClass('alert-danger');
            $('.error_elegir_medio_text').html(``);
            $('.error_elegir_medio').css('border', '');
        }

        if (!departamento) {
            $('.error_departamento').addClass('alert-danger');
            $('.error_departamento').css('border', '1px solid #DA1414');
            $('.error_departamento_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            $("#departamento").focus();
            error = error + "error 1";
        } else {
            $('.error_departamento').removeClass('alert-danger');
            $('.error_departamento_text').html(``);
            $('.error_departamento').css('border', '');
        }

        if (!provincia) {
            $('.error_provincia').addClass('alert-danger');
            $('.error_provincia').css('border', '1px solid #DA1414');
            $('.error_provincia_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            $("#provincia").focus();
            error = error + "error 1";
        } else {
            $('.error_provincia').removeClass('alert-danger');
            $('.error_provincia_text').html(``);
            $('.error_provincia').css('border', '');
        }

        if (!distrito) {
            $('.error_distrito').addClass('alert-danger');
            $('.error_distrito').css('border', '1px solid #DA1414');
            $('.error_distrito_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            $("#distrito").focus();
            error = error + "error 1";
        } else {
            $('.error_distrito').removeClass('alert-danger');
            $('.error_distrito_text').html(``);
            $('.error_distrito').css('border', '');
        }

        if (!institucion_educativa_id) {
            $('.error_institucion_educativa_id').addClass('alert-danger');
            $('.error_institucion_educativa_id').css('border', '1px solid #DA1414');
            $('.error_institucion_educativa_id_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            $("#institucion_educativa_id").focus();
            error = error + "error 1";
        } else {
            $('.error_institucion_educativa_id').removeClass('alert-danger');
            $('.error_institucion_educativa_id_text').html(``);
            $('.error_institucion_educativa_id').css('border', '');
        }

        if (!tipo_institucion_educativa) {
            $('.error_tipo_institucion_educativa').addClass('alert-danger');
            $('.error_tipo_institucion_educativa').css('border', '1px solid #DA1414');
            $('.error_tipo_institucion_educativa_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            $("#tipo_institucion_educativa").focus();
            error = error + "error 1";
        } else {
            $('.error_tipo_institucion_educativa').removeClass('alert-danger');
            $('.error_tipo_institucion_educativa_text').html(``);
            $('.error_tipo_institucion_educativa').css('border', '');
        }

        if (!cargo_id && (tipo_institucion_educativa == "1" || tipo_institucion_educativa == "2")) {
            $('.error_cargo_id').addClass('alert-danger');
            $('.error_cargo_id').css('border', '1px solid #DA1414');
            $('.error_cargo_id_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);

            $("#cargo_id").focus();
            error = error + "error 1";
        } else {
            $('.error_cargo_id').removeClass('alert-danger');
            $('.error_cargo_id').css('border', '');
            $('.error_cargo_id_text').html(``);
        }



        if (!clave) {
            $('.error_clave').addClass('alert-danger');
            $('.error_clave').css('border', '1px solid #DA1414');
            $('.error_clave_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            $("#clave").focus();
            error = error + "error error_clave";
        } else if (clave && (clave.length < 6 || clave.length > 20)) {
            $('.error_clave').addClass('alert-danger');
            $('.error_clave').css('border', '1px solid #DA1414');
            $('.error_clave_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Mínimo 6 caracteres y máximo 20</div>`);

            $("#clave").focus();
            error = error + "error error_clave_text";
        } else {
            $('.error_clave').removeClass('alert-danger');
            $('.error_clave').css('border', '');
            $('.error_clave_text').html(``);
        }

        if (!clave_confirma) {
            $('.error_clave_confirma').addClass('alert-danger');
            $('.error_clave_confirma').css('border', '1px solid #DA1414');
            $('.error_clave_confirma_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            $("#clave").focus();
            error = error + "error error_clave_confirma";
        } else if (clave != clave_confirma) {
            $('.error_clave_confirma').addClass('alert-danger');
            $('.error_clave_confirma').css('border', '1px solid #DA1414');
            $('.error_clave_confirma_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Las contraseñas no coinciden</div>`);
            $("#clave").focus();
            error = error + "error error_clave";
        } else {
            $('.error_clave_confirma').removeClass('alert-danger');
            $('.error_clave_confirma').css('border', '');
            $('.error_clave_confirma_text').html(``);
        }


        if (!aceptar_condicion) {
            $('.error_aceptar_condicion').addClass('alert-danger');
            $('.error_aceptar_condicion').css('border', '1px solid #DA1414');
            $('.error_aceptar_condicion_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            $("#aceptar_condicion").focus();
            error = error + "error 1";
        } else {
            $('.error_aceptar_condicion').removeClass('alert-danger');
            $('.error_aceptar_condicion_text').html(``);
            $('.error_aceptar_condicion').css('border', '');
        }

        console.log(error);
        if (error != "") {
            console.log(error);
            return false;
        }

        if (tipo_institucion_educativa && tipo_institucion_educativa == "1" && cargo_id && cargo_id == "3") {
            grado = grado.val();
            if(seccion=='otros'){
                seccion = $('#otros').val();
            }

            if (grado == "4") {
                seccion_4 = seccion;
            }
            if (grado == "5") {
                seccion_5 = seccion;
            }
            if (grado == "6") {
                seccion_6 = seccion;
            }

            
        }

        if (tipo_institucion_educativa && tipo_institucion_educativa == "1" && cargo_id && cargo_id == "4") {
            seccion_4 = Array_seccion_4.toString();
            seccion_5 = Array_seccion_5.toString();
            seccion_6 = Array_seccion_6.toString();
            grado = Array_grado.toString();
        }

        if (tipo_institucion_educativa && tipo_institucion_educativa == "2" && cargo_id && cargo_id == "8") {
            grado = Array_grado.toString();
        }

        // $('.formulario_4').hide();

        // $('.svg_opcion_4').html(`<svg width="250" height="32" viewBox="0 0 250 32" fill="none" xmlns="http://www.w3.org/2000/svg">
        //                         <circle cx="16" cy="16" r="16" fill="white"/>
        //                         <circle cx="16" cy="16" r="16" stroke="black"/>
        //                         <path d="M13.4422 23C13.0989 23 12.7555 22.855 12.4809 22.6011L8.42957 18.5399C7.88024 17.996 7.8459 17.0532 8.39523 16.473C8.91023 15.8928 9.80289 15.8566 10.3522 16.4368L13.4079 19.5189L21.6135 10.4537C22.1284 9.87351 23.0211 9.83725 23.5704 10.4174C24.1198 10.9613 24.1541 11.9041 23.6048 12.4843L14.4378 22.5649C14.1632 22.855 13.8199 23 13.4422 23Z" fill="#2AB7CA"/>
        //                     </svg>`);
        
        var form_data = new FormData(); // $('#formEncuesta01').serializeArray();
        form_data.append("_csrf", csrf);
        form_data.append("Registro[dni]", dni);
        form_data.append("Registro[apellido_paterno]", apellido_paterno);
        form_data.append("Registro[apellido_materno]", apellido_materno);
        form_data.append("Registro[nombres]", nombres);
        form_data.append("Registro[correo_electronico]", correo_electronico);
        form_data.append("Registro[celular]", celular);
        form_data.append("Registro[fecha_nacimiento]", fecha_nacimiento);
        form_data.append("Registro[clave]", clave);
        form_data.append("Registro[tipo_institucion_educativa]", tipo_institucion_educativa);
        form_data.append("Registro[departamento]", departamento);
        form_data.append("Registro[provincia]", provincia);
        form_data.append("Registro[distrito]", distrito);
        form_data.append("Registro[institucion_educativa_id]", institucion_educativa_id);
        form_data.append("Registro[cargo_id]", cargo_id);
        form_data.append("Registro[cod_mod]", cod_mod);
        form_data.append("Registro[grado]", grado);
        form_data.append("Registro[seccion_4]", seccion_4);
        form_data.append("Registro[seccion_5]", seccion_5);
        form_data.append("Registro[seccion_6]", seccion_6);
        form_data.append("Registro[aceptar_condicion]", aceptar_condicion);


        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            dataType: 'json',
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
                //$('.sidebar-mini').LoadingOverlay("hide", true);
            },
            beforeSend: function() {
                //loading.modal("show");
            },
            success: function(results) {
                if (results.success) {
                    window.location.replace("<?= \Yii::$app->request->BaseUrl ?>/registrar/final");
                }
            },
        });
    });



    $('body').on('click', '.btn_agregar_seccion_4', function(e) {
        var descripcion = $('#otros_seccion_4').val();
        var selector_id = $("[name=\"Registro[seccion_4]\"]").length;
        var reg = /^[a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ'_\s]+$/;

        if ($.trim(descripcion) == "") {
            alert('No se puede agregar información vacia.');
            return false;
        }

        if(!reg.test(descripcion)){
            alert('Solo  se puede ingresar letras.');
            return false;
        }

        $('.seccion_4_contenido').append(` <div class="n_section_box mx-1 d-inline-block">
            <div class="form-check form-check-inline n_form-button--options active">
                <input class="invisible form-check-input form-check-input-switcher d-none active" checked type="checkbox" id="seccion_4_${selector_id}" value="${descripcion}" name="Registro[seccion_4]">
                <label class="n_hijo n_label_size align-items-center form-check-label d-flex d-sm-flex d-md-flex d-lg-flex d-xl-flex" for="inlineCheckbox-4">
                    <span class="n_hijo n_check_box mr-2" ><img src="<?= \Yii::$app->request->BaseUrl ?>/images/check-solid.svg" alt=""></span>
                ${descripcion}</label>
            </div>
        </div>`);

        $('#otros_seccion_4').val('');
    });

    $('body').on('click', '.btn_agregar_seccion_5', function(e) {
        var descripcion = $('#otros_seccion_5').val();
        var selector_id = $("[name=\"Registro[seccion_5]\"]").length;
        var reg = /^[a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ'_\s]+$/;

        if ($.trim(descripcion) == "") {
            alert('No se puede agregar información vacia.');
            return false;
        }
        if(!reg.test(descripcion)){
            alert('Solo  se puede ingresar letras.');
            return false;
        }

        $('.seccion_5_contenido').append(` <div class="n_section_box mx-1 d-inline-block">
            <div class="form-check form-check-inline n_form-button--options active">
                <input class="invisible form-check-input form-check-input-switcher d-none active" checked type="checkbox" id="seccion_5_${selector_id}" value="${descripcion}" name="Registro[seccion_5]">
                <label  class="n_hijo n_label_size align-items-center form-check-label d-flex d-sm-flex d-md-flex d-lg-flex d-xl-flex" for="inlineCheckbox-5">
                    <span class="n_hijo n_check_box mr-2" style="display: none;"><img src="<?= \Yii::$app->request->BaseUrl ?>/images/check-solid.svg" alt=""></span>
                ${descripcion}</label>
            </div>
        </div>`);
        $('#otros_seccion_5').val('');
    });

    $('body').on('click', '.btn_agregar_seccion_6', function(e) {
        var descripcion = $('#otros_seccion_6').val();
        var selector_id = $("[name=\"Registro[seccion_6]\"]").length;
        var reg = /^[a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ'_\s]+$/;
        if ($.trim(descripcion) == "") {
            alert('No se puede agregar información vacia.');
            return false;
        }
        if(!reg.test(descripcion)){
            alert('Solo  se puede ingresar letras.');
            return false;
        }
        $('.seccion_6_contenido').append(` <div class="n_section_box mx-1 d-inline-block">
            <div class="form-check form-check-inline n_form-button--options active">
                <input class="invisible form-check-input form-check-input-switcher d-none active" checked type="checkbox" id="seccion_6_${selector_id}" value="${descripcion}" name="Registro[seccion_6]">
                <label  class="n_hijo n_label_size align-items-center form-check-label d-flex d-sm-flex d-md-flex d-lg-flex d-xl-flex" for="inlineCheckbox-6">
                    <span class="n_hijo n_check_box mr-2" style="display: none;"><img src="<?= \Yii::$app->request->BaseUrl ?>/images/check-solid.svg" alt=""></span>
                ${descripcion}</label>
            </div>
        </div>`);
        $('#otros_seccion_6').val('');
    });

    
    

    $('body').on('click', '.form_1', function(e) {
       
        e.preventDefault();
        //$('#staticBackdrop').modal('show')
        //loading.modal('show');
        var error = "";
        var dni = $("[name=\"Registro[dni]\"]").val();
        var apellido_paterno = $("[name=\"Registro[apellido_paterno]\"]").val();
        var apellido_materno = $("[name=\"Registro[apellido_materno]\"]").val();
        var nombres = $("[name=\"Registro[nombres]\"]").val();
        var correo_electronico = $("[name=\"Registro[correo_electronico]\"]").val();
        var celular = $("[name=\"Registro[celular]\"]").val();
        // var fecha_nacimiento = $("[name=\"Registro[fecha_nacimiento]\"]").val();
        
        var dia = $('#dia').val();
        var mes = $('#mes').val();
        var anio = $('#anio').val();

        //isDate(`${anio}-${mes}-${dia}`); //
        var fecha_nacimiento_validar =new moment(`${anio}-${mes}-${dia}`) ;
        var fecha_nacimiento = `${anio}-${mes}-${dia}`;
        var banderaDNI = validarDNI(dni);
        var banderaCorreo = validarCorreo(correo_electronico);
        
        if (!dni) {
            $('.error_dni').addClass('alert-danger');
            $('.error_dni').css('border', '1px solid #DA1414');
            $('.error_dni_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Ingrese un número de DNI válido</div>`);
            $("#dni").focus();
            error = error + "error 1";
        }else if (dni && dni.length<8) {
            $('.error_dni').addClass('alert-danger');
            $('.error_dni').css('border', '1px solid #DA1414');
            $('.error_dni_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Ingrese un número de DNI válido</div>`);
            $("#dni").focus();
            error = error + "error 1";
        }else if (dni && !banderaDNI) {
            $('.error_dni').addClass('alert-danger');
            $('.error_dni').css('border', '1px solid #DA1414');
            $('.error_dni_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> El usuario ya se encuentra registrado</div>`);
            $("#dni").focus();
            error = error + "error 1";
        }
        else {
            $('.error_dni').removeClass('alert-danger');
            $('.error_dni').css('border', '');
            $('.error_dni_text').html(``);
        }


        if (!apellido_paterno) {
            $('.error_apellido_paterno').addClass('alert-danger');
            $('.error_apellido_paterno').css('border', '1px solid #DA1414');
            $('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            $("#apellido_paterno").focus();
            error = error + "error 1";
        } else {
            $('.error_apellido_paterno').removeClass('alert-danger');
            $('.error_apellido_paterno').css('border', '');
            $('.error_apellido_paterno_text').html(``);
        }

        if (!nombres) {
            $('.error_nombres').addClass('alert-danger');
            $('.error_nombres').css('border', '1px solid #DA1414');
            $('.error_nombres_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            $("#nombres").focus();
            error = error + "error 1";
        } else {
            $('.error_nombres').removeClass('alert-danger');
            $('.error_nombres').css('border', '');
            $('.error_nombres_text').html(``);
        }

        filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!correo_electronico) {
            $('.error_correo_electronico').addClass('alert-danger');
            $('.error_correo_electronico').css('border', '1px solid #DA1414');
            $('.error_correo_electronico_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Verifica tu correo electrónico</div>`);
            $("#correo_electronico").focus();
            error = error + "error 1";
        }else if (correo_electronico && !filter.test(correo_electronico)) {
            $('.error_correo_electronico').addClass('alert-danger');
            $('.error_correo_electronico').css('border', '1px solid #DA1414');
            $('.error_correo_electronico_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Verifica tu correo electrónico</div>`);
            $("#correo_electronico").focus();
            error = error + "error 1";
        }else if (correo_electronico && !banderaCorreo) {
            $('.error_correo_electronico').addClass('alert-danger');
            $('.error_correo_electronico').css('border', '1px solid #DA1414');
            $('.error_correo_electronico_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este correo ya se encuentra registrado</div>`);
            $("#correo_electronico").focus();
            error = error + "error 1";
        } else {
            $('.error_correo_electronico').removeClass('alert-danger');
            $('.error_correo_electronico').css('border', '');
            $('.error_correo_electronico_text').html(``);
        }

        if (!celular) {
            $('.error_celular').addClass('alert-danger');
            $('.error_celular').css('border', '1px solid #DA1414');
            $('.error_celular_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            $("#celular").focus();
            error = error + "error 1";
        } else if (celular && celular.length<8) {
            $('.error_celular').addClass('alert-danger');
            $('.error_celular').css('border', '1px solid #DA1414');
            $('.error_celular_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Número incorrecto, verifica que tenga 9 dígitos</div>`);
            $("#celular").focus();
            error = error + "error 1";
        } else if (celular && celular.substring(0, 1)!='9') {
            $('.error_celular').addClass('alert-danger');
            $('.error_celular').css('border', '1px solid #DA1414');
            $('.error_celular_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Número incorrecto, verifica que empiece con 9.</div>`);
            $("#celular").focus();
            error = error + "error 1";
        } else {
            $('.error_celular').removeClass('alert-danger');
            $('.error_celular_text').html(``);
            $('.error_celular').css('border', '');
        }

        if (!fecha_nacimiento_validar.isValid()) {
            $('.error_fecha_nacimiento').addClass('alert-danger');
            $('.error_fecha_nacimiento').css('border', '1px solid #DA1414');
            $('.error_fecha_nacimiento_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> La fecha es incorrecta, inténtalo de nuevo</div>`);
            $("#fecha_nacimiento").focus();
            error = error + "error 1";
        } else {
            $('.error_fecha_nacimiento').removeClass('alert-danger');
            $('.error_fecha_nacimiento_text').html(``);
            $('.error_fecha_nacimiento').css('border', '');
        }

        if (error != "") {
            console.log(error);
            //loading.modal('hide');
            return false;
        }

        $('.svg_opcion_1').html(`<div class="n_elipse">
                                <div class="container-image">
                                    <img src="<?= \Yii::$app->request->BaseUrl ?>/images/elipse.png" alt="">
                                </div>
                            </div>
                            <div class="n_check">
                                <div class="container-image">
                                    <img src="<?= \Yii::$app->request->BaseUrl ?>/images/check.png" alt="">
                                </div>
                            </div>`);
        $('.svg_opcion_2').html(`<div class="n_elipse">
                                <div class="container-image">
                                    <img src="<?= \Yii::$app->request->BaseUrl ?>/images/elipse-limpia.png" alt="">
                                </div>
                            </div>
                            <div class="n_check">
                                <div class="container-text">
                                    2
                                </div>
                            </div>`);

        document.getElementById("cont1").style.display = "none";
        document.getElementById("cont2").style.display = "block";
        //loading.modal('hide');
    });

    

    $('body').on('click', '.form_2', function(e) {
        var error = "";
        var elegir_medio = $("#elegir_medio").val();
        var departamento = $("[name=\"Registro[departamento]\"]").val();
        var provincia = $("[name=\"Registro[provincia]\"]").val();
        var distrito = $("[name=\"Registro[distrito]\"]").val();
        var institucion_educativa_id = $("[name=\"Registro[institucion_educativa_id]\"]").val();
        var tipo_institucion_educativa = $("[name=\"Registro[tipo_institucion_educativa]\"]").val();

        if (!elegir_medio) {
            $('.error_elegir_medio').addClass('alert-danger');
            $('.error_elegir_medio').css('border', '1px solid #DA1414');
            $('.error_elegir_medio_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            $("#elegir_medio").focus();
            error = error + "error 1";
        } else {
            $('.error_elegir_medio').removeClass('alert-danger');
            $('.error_elegir_medio_text').html(``);
            $('.error_elegir_medio').css('border', '');
        }

        if (!departamento) {
            $('.error_departamento').addClass('alert-danger');
            $('.error_departamento').css('border', '1px solid #DA1414');
            $('.error_departamento_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            $("#departamento").focus();
            error = error + "error 1";
        } else {
            $('.error_departamento').removeClass('alert-danger');
            $('.error_departamento_text').html(``);
            $('.error_departamento').css('border', '');
        }

        if (!provincia) {
            $('.error_provincia').addClass('alert-danger');
            $('.error_provincia').css('border', '1px solid #DA1414');
            $('.error_provincia_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            $("#provincia").focus();
            error = error + "error 1";
        } else {
            $('.error_provincia').removeClass('alert-danger');
            $('.error_provincia_text').html(``);
            $('.error_provincia').css('border', '');
        }

        if (!distrito) {
            $('.error_distrito').addClass('alert-danger');
            $('.error_distrito').css('border', '1px solid #DA1414');
            $('.error_distrito_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            $("#distrito").focus();
            error = error + "error 1";
        } else {
            $('.error_distrito').removeClass('alert-danger');
            $('.error_distrito_text').html(``);
            $('.error_distrito').css('border', '');
        }

        if (!institucion_educativa_id) {
            $('.error_institucion_educativa_id').addClass('alert-danger');
            $('.error_institucion_educativa_id').css('border', '1px solid #DA1414');
            $('.error_institucion_educativa_id_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            $("#institucion_educativa_id").focus();
            error = error + "error 1";
        } else {
            $('.error_institucion_educativa_id').removeClass('alert-danger');
            $('.error_institucion_educativa_id_text').html(``);
            $('.error_institucion_educativa_id').css('border', '');
        }

        if (!tipo_institucion_educativa) {
            $('.error_tipo_institucion_educativa').addClass('alert-danger');
            $('.error_tipo_institucion_educativa').css('border', '1px solid #DA1414');
            $('.error_tipo_institucion_educativa_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            $("#tipo_institucion_educativa").focus();
            error = error + "error 1";
        } else {
            $('.error_tipo_institucion_educativa').removeClass('alert-danger');
            $('.error_tipo_institucion_educativa_text').html(``);
            $('.error_tipo_institucion_educativa').css('border', '');
        }

        if (error != "") {
            return false;
        }

        if($('#tipo_institucion_educativa').val()=="3"){
            $('.svg_opcion_2').html(`<div class="n_elipse">
                                    <div class="container-image">
                                        <img src="<?= \Yii::$app->request->BaseUrl ?>/images/elipse.png" alt="">
                                    </div>
                                </div>
                                <div class="n_check">
                                    <div class="container-image">
                                        <img src="<?= \Yii::$app->request->BaseUrl ?>/images/check.png" alt="">
                                    </div>
                                </div>`);
            $('.svg_opcion_3').html(`<div class="n_elipse">
                                    <div class="container-image">
                                        <img src="<?= \Yii::$app->request->BaseUrl ?>/images/elipse.png" alt="">
                                    </div>
                                </div>
                                <div class="n_check">
                                    <div class="container-image">
                                        <img src="<?= \Yii::$app->request->BaseUrl ?>/images/check.png" alt="">
                                    </div>
                                </div>`);
            $('.svg_opcion_4').html(`<div class="n_elipse">
                                    <div class="container-image">
                                        <img src="<?= \Yii::$app->request->BaseUrl ?>/images/elipse-limpia.png" alt="">
                                    </div>
                                </div>
                                <div class="n_check">
                                    <div class="container-text">
                                        4
                                    </div>
                                </div>`);
            document.getElementById("cont2").style.display = "none";
            document.getElementById("cont3").style.display = "none";
            document.getElementById("cont4").style.display = "block";
        }else{
            $('.svg_opcion_2').html(`<div class="n_elipse">
                                    <div class="container-image">
                                        <img src="<?= \Yii::$app->request->BaseUrl ?>/images/elipse.png" alt="">
                                    </div>
                                </div>
                                <div class="n_check">
                                    <div class="container-image">
                                        <img src="<?= \Yii::$app->request->BaseUrl ?>/images/check.png" alt="">
                                    </div>
                                </div>`);
            $('.svg_opcion_3').html(`<div class="n_elipse">
                                    <div class="container-image">
                                        <img src="<?= \Yii::$app->request->BaseUrl ?>/images/elipse-limpia.png" alt="">
                                    </div>
                                </div>
                                <div class="n_check">
                                    <div class="container-text">
                                        3
                                    </div>
                                </div>`);
            document.getElementById("cont2").style.display = "none";
            document.getElementById("cont3").style.display = "block";
        }

        
    });

    $('body').on('click', '.form_2_ant', function(e) {
        $('.svg_opcion_1').html(`<div class="n_elipse">
                                    <div class="container-image">
                                        <img src="<?= \Yii::$app->request->BaseUrl ?>/images/elipse-limpia.png" alt="">
                                    </div>
                                </div>
                                <div class="n_check">
                                    <div class="container-text">
                                        1
                                    </div>
                                </div>`);
        $('.svg_opcion_2').html(`<div class="n_elipse">
                                        <div class="container-image">
                                            <img src="<?= \Yii::$app->request->BaseUrl ?>/images/circlefull.png" alt="">
                                        </div>
                                    </div>
                                    <div class="n_check">
                                        <div class="container-text">
                                            2
                                        </div>
                                    </div>`);

        document.getElementById("cont1").style.display = "block";
        document.getElementById("cont2").style.display = "none";
    });


    $('body').on('click', '.form_3', function(e) {
        var error = "";
        var cargo_id = $("#cargo_id").val();



        if (!cargo_id) {
            $('.error_cargo_id').addClass('alert-danger');
            $('.error_cargo_id').css('border', '1px solid #DA1414');
            $('.error_cargo_id_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);

            $("#cargo_id").focus();
            error = error + "error 1";
        } else {
            $('.error_cargo_id').removeClass('alert-danger');
            $('.error_cargo_id').css('border', '');
            $('.error_cargo_id_text').html(``);
        }

        if(cargo_id && cargo_id=="3"){
            var grado = $("#grado").val();
            var seccion = $("#seccion").val();
            var filter = /^[a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ'_\s]+$/;
            if (!grado) {
                $('.error_grado').addClass('alert-danger');
                $('.error_grado').css('border', '1px solid #DA1414');
                $('.error_grado_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);

                $("#cargo_id").focus();
                error = error + "error 1";
            } else {
                $('.error_grado').removeClass('alert-danger');
                $('.error_grado').css('border', '');
                $('.error_grado_text').html(``);
            }

            if (!seccion) {
                $('.error_seccion').addClass('alert-danger');
                $('.error_seccion').css('border', '1px solid #DA1414');
                $('.error_seccion_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);

                $("#cargo_id").focus();
                error = error + "error 1";
            } else {
                $('.error_seccion').removeClass('alert-danger');
                $('.error_seccion').css('border', '');
                $('.error_seccion_text').html(``);
            }

            if(seccion=='otros' && $.trim($('#otros').val())==''){
                $('.error_otros').addClass('alert-danger');
                $('.error_otros').css('border', '1px solid #DA1414');
                $('.error_otros_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);

                $("#otros").focus();
                error = error + "error 1";
            }else if(seccion=='otros' && !filter.test($('#otros').val())){
                $('.error_otros').addClass('alert-danger');
                $('.error_otros').css('border', '1px solid #DA1414');
                $('.error_otros_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Solo  se puede ingresar letras</div>`);

                $("#otros").focus();
                error = error + "error 1";
            }else{
                $('.error_otros').removeClass('alert-danger');
                $('.error_otros').css('border', '');
                $('.error_otros_text').html(``);
            }
        }else if(cargo_id && cargo_id=="4"){
            
            var seccion_4 = $("[name=\"Registro[seccion_4]\"]");
            var seccion_5 = $("[name=\"Registro[seccion_5]\"]");
            var seccion_6 = $("[name=\"Registro[seccion_6]\"]");
            var Array_grado = [];
            var Array_seccion_4 = [];
            var Array_seccion_5 = [];
            var Array_seccion_6 = [];

            if (seccion_4 && seccion_4.length >= 10) {
                $.each(seccion_4, function(index, value) {
                    if (value.checked) {
                        Array_seccion_4.push(value.value)
                    }
                });
                if (Array_seccion_4.length > 0) {
                    Array_grado.push("4");
                }
            }

            if (seccion_5 && seccion_5.length >= 10) {
                $.each(seccion_5, function(index, value) {
                    if (value.checked) {
                        Array_seccion_5.push(value.value)
                    }
                });
                if (Array_seccion_5.length > 0) {
                    Array_grado.push("5");
                }
            }

            if (seccion_6 && seccion_6.length >= 10) {
                $.each(seccion_6, function(index, value) {
                    if (value.checked) {
                        Array_seccion_6.push(value.value)
                    }
                });
                if (Array_seccion_6.length > 0) {
                    Array_grado.push("6");
                }
            }

            if (Array_seccion_4.length==0 && Array_seccion_5.length==0 && Array_seccion_6.length==0) {
                $('.error_seccion_4_contenido').addClass('alert-danger');
                $('.error_seccion_4_contenido').css('border', '1px solid #DA1414');
                $('.error_seccion_4_contenido_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);

                $('.error_seccion_5_contenido').addClass('alert-danger');
                $('.error_seccion_5_contenido').css('border', '1px solid #DA1414');
                $('.error_seccion_5_contenido_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);

                $('.error_seccion_6_contenido').addClass('alert-danger');
                $('.error_seccion_6_contenido').css('border', '1px solid #DA1414');
                $('.error_seccion_6_contenido_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);

                error = error + "error 1";
            } else {
                $('.error_seccion_4_contenido').removeClass('alert-danger');
                $('.error_seccion_4_contenido').css('border', '');
                $('.error_seccion_4_contenido_text').html(``);

                $('.error_seccion_5_contenido').removeClass('alert-danger');
                $('.error_seccion_5_contenido').css('border', '');
                $('.error_seccion_5_contenido_text').html(``);

                $('.error_seccion_6_contenido').removeClass('alert-danger');
                $('.error_seccion_6_contenido').css('border', '');
                $('.error_seccion_6_contenido_text').html(``);
            }

            
        }else if(cargo_id && cargo_id=="8"){
            var grado = $("[name=\"Registro[grado]\"]");
            var Array_grado = [];

            if (grado.length >= 0) {
                $.each(grado, function(index, value) {
                    if (value.checked) {
                        Array_grado.push(value.value)
                    }
                });
            }
            console.log(Array_grado);
            if (Array_grado.length==0) {
                $('.error_paso5_contenido').addClass('alert-danger');
                $('.error_paso5_contenido').css('border', '1px solid #DA1414');
                $('.error_paso5_contenido_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
                error = error + "error 1";
            } else {
                $('.error_paso5_contenido').removeClass('alert-danger');
                $('.error_paso5_contenido').css('border', '');
                $('.error_paso5_contenido_text').html(``);
            }
        }


        if (error != "") {
            console.log(error);
            return false;
        }

        $('.svg_opcion_3').html(`<div class="n_elipse">
                                <div class="container-image">
                                    <img src="<?= \Yii::$app->request->BaseUrl ?>/images/elipse.png" alt="">
                                </div>
                            </div>
                            <div class="n_check">
                                <div class="container-image">
                                    <img src="<?= \Yii::$app->request->BaseUrl ?>/images/check.png" alt="">
                                </div>
                            </div>`);
        $('.svg_opcion_4').html(`<div class="n_elipse">
                                <div class="container-image">
                                    <img src="<?= \Yii::$app->request->BaseUrl ?>/images/elipse-limpia.png" alt="">
                                </div>
                            </div>
                            <div class="n_check">
                                <div class="container-text">
                                    4
                                </div>
                            </div>`);
        document.getElementById("cont3").style.display = "none";
        document.getElementById("cont4").style.display = "block";
    });

    $('body').on('click', '.form_3_ant', function(e) {
        $('.svg_opcion_2').html(`<div class="n_elipse">
                                    <div class="container-image">
                                        <img src="<?= \Yii::$app->request->BaseUrl ?>/images/elipse-limpia.png" alt="">
                                    </div>
                                </div>
                                <div class="n_check">
                                    <div class="container-text">
                                        2
                                    </div>
                                </div>`);
        $('.svg_opcion_3').html(`<div class="n_elipse">
                                        <div class="container-image">
                                            <img src="<?= \Yii::$app->request->BaseUrl ?>/images/circlefull.png" alt="">
                                        </div>
                                    </div>
                                    <div class="n_check">
                                        <div class="container-text">
                                            3
                                        </div>
                                    </div>`);

        document.getElementById("cont2").style.display = "block";
        document.getElementById("cont3").style.display = "none";
    });

    $('body').on('click', '.form_4_ant', function(e) {

        if($('#tipo_institucion_educativa').val()=="3"){
            $('.svg_opcion_2').html(`<div class="n_elipse">
                                    <div class="container-image">
                                        <img src="<?= \Yii::$app->request->BaseUrl ?>/images/elipse-limpia.png" alt="">
                                    </div>
                                </div>
                                <div class="n_check">
                                    <div class="container-text">
                                        2
                                    </div>
                                </div>`);

            $('.svg_opcion_3').html(`<div class="n_elipse">
                                            <div class="container-image">
                                                <img src="<?= \Yii::$app->request->BaseUrl ?>/images/circlefull.png" alt="">
                                            </div>
                                        </div>
                                        <div class="n_check">
                                            <div class="container-text">
                                                3
                                            </div>
                                        </div>`);
            $('.svg_opcion_4').html(`<div class="n_elipse">
                                            <div class="container-image">
                                                <img src="<?= \Yii::$app->request->BaseUrl ?>/images/circlefull.png" alt="">
                                            </div>
                                        </div>
                                        <div class="n_check">
                                            <div class="container-text">
                                                4
                                            </div>
                                        </div>`);
            document.getElementById("cont2").style.display = "block";
            document.getElementById("cont3").style.display = "none";
            document.getElementById("cont4").style.display = "none";
        }else{
            $('.svg_opcion_3').html(`<div class="n_elipse">
                                        <div class="container-image">
                                            <img src="<?= \Yii::$app->request->BaseUrl ?>/images/elipse-limpia.png" alt="">
                                        </div>
                                    </div>
                                    <div class="n_check">
                                        <div class="container-text">
                                            3
                                        </div>
                                    </div>`);
            $('.svg_opcion_4').html(`<div class="n_elipse">
                                            <div class="container-image">
                                                <img src="<?= \Yii::$app->request->BaseUrl ?>/images/circlefull.png" alt="">
                                            </div>
                                        </div>
                                        <div class="n_check">
                                            <div class="container-text">
                                                4
                                            </div>
                                        </div>`);
            document.getElementById("cont3").style.display = "block";
            document.getElementById("cont4").style.display = "none";
        }


    });
    

    var psw  = 0;
    var rpsw = 0;
    $('body').on('click', '.btn-password', function(e) {
        
        if(psw==0){
            $(this).html('<i class="bx bx-hide"></i>');
            $('#clave').attr('type', 'password'); 
            psw = 1 ;
        }else if(psw==1){
            $(this).html('<i class="bx bx-show"></i>');
            $('#clave').attr('type', 'text'); 
            psw = 0 ;
        }
    });

    $('body').on('click', '.btn-repassword', function(e) {
        if(rpsw==0){
            $(this).html('<i class="bx bx-hide"></i>');
            $('#clave_confirma').attr('type', 'password');
            rpsw = 1 ;
        }else if(rpsw==1){
            $(this).html('<i class="bx bx-show"></i>');
            $('#clave_confirma').attr('type', 'text'); 
            rpsw = 0 ;
        }
    });

    $('body').on('click', '.btn-ocultar', function(e) {
        e.preventDefault();
        
        document.getElementById("n_mobile").style.display = "none";
        document.getElementById("d-mob").id = "";
        
    });
    $('body').on('change', '#seccion', function(e) {
        $('.tutor_aula_otros').hide();
        $('#otros').val('')
        if($(this).val()=='otros'){
            $('.tutor_aula_otros').show();
        }       
    });

    
    
    function isDate(txtDate) {
        var currVal = txtDate;
        console.log(currVal);
        if (currVal == '')
            return false;
        var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; 
        var dtArray = currVal.match(rxDatePattern);
        
        if (dtArray == null)
            return false;
        
        dtMonth = dtArray[1];
        dtDay = dtArray[3];
        dtYear = dtArray[5];

        if (dtMonth < 1 || dtMonth > 12)
            return false;
        else if (dtDay < 1 || dtDay > 31)
            return false;
        else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
            return false;
        else if (dtMonth == 2) {
            var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
            if (dtDay > 29 || (dtDay == 29 && !isleap))
                return false;
        }
        return true;
    }
</script>