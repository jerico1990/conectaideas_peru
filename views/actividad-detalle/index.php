
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0 font-size-18">Recursos</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Recursos</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-3">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <table id="lista-experiencias" class="table table-bordered dt-responsive ">
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-9">
        <div class="card">
            <div class="card-body lista-actividades">
                
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<div id="modal" class="fade modal" role="dialog"  tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>



<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('#staticBackdrop');


async function Actividades(experiencia_id,experiencia_correlativo){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/actividad/get-experiencia-actividades-admin',
                method: 'POST',
                data:{_csrf:csrf,experiencia_id:experiencia_id},
                dataType:'Json',
                beforeSend:function(){
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        
                        var i = 0;
                        var actividades ='';
                        actividades = actividades + '<h3> Experiencia ' + experiencia_correlativo + '</h3>'
                        actividades = actividades + '<ul class="nav nav-tabs" role="tablist">'
                        $.each(results.actividades, function( index, value ) {
                            correlativo = value.correlativo;
                            active = '';
                            if(i==0){
                                active = 'active';
                            }
                            actividades = actividades + `   <li class="nav-item">
                                                                <a class="nav-link ${active}" data-toggle="tab" href="#actividad_${correlativo}" role="tab">
                                                                    <span class="d-none d-sm-block">Actividad ${correlativo}</span>
                                                                </a>
                                                            </li>`;
                            i++;

                        });
                        actividades = actividades + '</ul>';

                        i = 0;
                        actividades = actividades + '<div class="tab-content p-3 text-muted">'
                        $.each(results.actividades, function( index, value ) {
                            correlativo = value.correlativo;
                            active = '';
                            if(i==0){
                                active = 'active';
                            }
                            actividades = actividades + '<div class="tab-pane ' + active + '" id="actividad_' + correlativo + '" role="tabpanel">';

                            $.each(value.detalle, function( index2, value2 ) {
                                id = value2.id;
                                grado = value2.nombre_corto;
                                video = (value2.url_video)?value2.url_video:'';
                                descripcion = (value2.descripcion)?value2.descripcion:'';
                                actividad_chile_id = (value2.actividad_chile_id)?value2.actividad_chile_id:'';
                                if(value2.matriz){
                                    matriz = "<br> <a target='_blank' href='<?= \Yii::$app->request->BaseUrl ?>/recursos/matriz/" + value2.matriz + "'>Ver</a> | <a href='#' data-id='" + value2.id + "' data-experiencia_id='" + experiencia_id + "' data-experiencia_correlativo='" + experiencia_correlativo + "' class='btn-eliminar-matriz'>Borrar</a> ";
                                }else{
                                    matriz = "<input type='file'  id='archivo_matriz_" + id + "' class='form-control'>";
                                }

                                if(value2.solucionario){
                                    solucionario = "<br> <a target='_blank' href='<?= \Yii::$app->request->BaseUrl ?>/recursos/solucionario/" + value2.solucionario + "'>Ver</a> | <a href='#' data-id='" + value2.id + "' data-experiencia_id='" + experiencia_id + "' data-experiencia_correlativo='" + experiencia_correlativo + "' class='btn-eliminar-solucionario'>Borrar</a>";
                                }else{
                                    solucionario = "<input type='file'  id='archivo_solucionario_" + id + "' class='form-control'>";
                                }

                                actividades = actividades + `
                                                            <div class="row">
                                                                <div class="col-3">
                                                                    <div class="card">
                                                                        <div class="card-body">
                                                                            <h4 class="card-title mb-4"></h4>
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    ${grado}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-9">
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="form-group">
                                                                                <label >Nombre de actividad</label>
                                                                                <input type="text"  id="descripcion_${id}" class="form-control" value="${descripcion}">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <label >Video</label>
                                                                                <input type="text"  id="url_video_${id}" class="form-control" value="${video}">
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <label >ID actividad APP Chile</label>
                                                                                <input type="text"  id="actividad_chile_id_${id}" class="form-control" value="${actividad_chile_id}">
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <label >Solucionario</label>
                                                                                ${solucionario}
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <label >Matriz</label>
                                                                                ${matriz}
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <label >&nbsp</label><br>
                                                                                <button data-id="${id}" data-experiencia_id="${experiencia_id}" data-experiencia_correlativo="${experiencia_correlativo}" data-actividad_correlativo="${correlativo}" data-grado="${grado}" class="btn btn-block btn-primary btn-grabar-recurso">Grabar</button>
                                                                            </div>
                                                                        </div>


                                                                    </div>
                                                                </div>
                                                            </div>
                                `;
                            });
                            actividades = actividades + '</div>'
                            i++;

                        });
                        actividades = actividades + '</div>'


                        $('.lista-actividades').html(actividades);
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}

Experiencias();
async function Experiencias(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/experiencia/get-lista-exp',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var experiencias ="";
                       
                        $.each(results.experiencias, function( index, value ) {
                            experiencias = experiencias + "<tr data-experiencia_id='" + value.id + "' data-experiencia_correlativo='" + value.correlativo + "' class='btn-seleccionar-experiencia'>";
                                experiencias = experiencias + "<td> Experiencia " + value.correlativo + "</td>";
                            experiencias = experiencias + "</tr>";
                        });
                        
                        $('#lista-experiencias tbody').html(experiencias);
                        Actividades(results.experiencias[0].id,results.experiencias[0].correlativo)
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}


$('body').on('click', '.btn-seleccionar-experiencia', function (e) {
    e.preventDefault();
    var experiencia_id = $(this).attr('data-experiencia_id');
    var experiencia_correlativo = $(this).attr('data-experiencia_correlativo');
    Actividades(experiencia_id,experiencia_correlativo);
});


$('body').on('click', '.btn-eliminar-matriz', function (e) {
    e.preventDefault();
    var actividad_detalle_id = $(this).attr('data-id');
    var experiencia_id = $(this).attr('data-experiencia_id');
    var experiencia_correlativo = $(this).attr('data-experiencia_correlativo');
    $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/actividad-detalle/eliminar-matriz',
        method: 'POST',
        data:{_csrf:csrf,actividad_id:actividad_detalle_id},
        dataType:'Json',
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success:function(results)
        {   
            if(results && results.success){
                Actividades(experiencia_id,experiencia_correlativo);
            }
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });
});

$('body').on('click', '.btn-eliminar-solucionario', function (e) {
    e.preventDefault();
    var actividad_detalle_id = $(this).attr('data-id');
    var experiencia_id = $(this).attr('data-experiencia_id');
    var experiencia_correlativo = $(this).attr('data-experiencia_correlativo');
    $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/actividad-detalle/eliminar-solucionario',
        method: 'POST',
        data:{_csrf:csrf,actividad_id:actividad_detalle_id},
        dataType:'Json',
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success:function(results)
        {   
            if(results && results.success){
                Actividades(experiencia_id,experiencia_correlativo);
            }
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });
});

$('body').on('click', '.btn-grabar-recurso', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    var experiencia_id = $(this).attr('data-experiencia_id');
    var experiencia_correlativo = $(this).attr('data-experiencia_correlativo');
    var actividad_correlativo = $(this).attr('data-actividad_correlativo');
    var grado_descripcion = $(this).attr('data-grado');
    

    var url_video = $('#url_video_'+id).val()
    var form_data = new FormData();
    form_data.append("_csrf", csrf);

    form_data.append("ActividadDetalle[experiencia_correlativo]", experiencia_correlativo);
    form_data.append("ActividadDetalle[actividad_correlativo]", actividad_correlativo);
    form_data.append("ActividadDetalle[grado_descripcion]", grado_descripcion);


    form_data.append("ActividadDetalle[recurso_id]", id);
    form_data.append("ActividadDetalle[descripcion]", $('#descripcion_'+id).val());
    form_data.append("ActividadDetalle[actividad_chile_id]", $('#actividad_chile_id_'+id).val());
    form_data.append("ActividadDetalle[url_video]", url_video.replaceAll('"', "'"));
    if(document.getElementById('archivo_solucionario_'+id) && document.getElementById('archivo_solucionario_'+id).files[0]){
        form_data.append("ActividadDetalle[archivo_solucionario]", document.getElementById('archivo_solucionario_'+id).files[0]);
    }
    
    if(document.getElementById('archivo_matriz_'+id) && document.getElementById('archivo_matriz_'+id).files[0]){
        form_data.append("ActividadDetalle[archivo_matriz]", document.getElementById('archivo_matriz_'+id).files[0]);
    }
    
    
    form_data.append("ActividadDetalle[temp]",'1');

    $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/actividad-detalle/update-recurso',
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        dataType:'Json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert('No hay conectividad con el sistema');
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        success: function (results) {
            if(results.success){
                //$('#modal').modal('toggle');
                Actividades(experiencia_id,experiencia_correlativo);
            }
        },
    });
});


</script>
