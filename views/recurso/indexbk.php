
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0 font-size-18">Lista de recursos</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Recurso</a></li>
                    <li class="breadcrumb-item active">Lista de recursos</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="card-title">Default Datatable</h4> <button class="btn btn-success btn-agregar-recurso">Agregar</button>
                <p class="card-title-desc">DataTables has most features enabled by
                    default, so all you need to do to use it with your own tables is to call
                    the construction function: <code>$().DataTable();</code>.
                </p>

                <table id="lista-recursos" class="table table-bordered dt-responsive ">
                    <thead>
                        <tr>
                            <th>Semana</th>
                            <th>Grado</th>
                            <th>Solucionario</th>
                            <th>Matriz</th>
                            <th>Video</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>


                    <tbody>
                    </tbody>
                </table>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<div id="modal" class="fade modal" role="dialog"  tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>



<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('#staticBackdrop');
////loading.modal("show");


$('#lista-recursos').DataTable();

Recursos();
async function Recursos(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/recurso/get-lista-recursos',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    ////loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var recursos ="";
                        $('#lista-recursos').DataTable().destroy();
                        $.each(results.recursos, function( index, value ) {
                            recursos = recursos + "<tr>";
                                recursos = recursos + "<td>" + value.semana_descripcion + "</td>";
                                recursos = recursos + "<td>" + value.grado_descripcion + "</td>";
                                recursos = recursos + "<td>" + ((value.solucionario)?`<a href='${value.solucionario}' target='_blank'>Solucionario</a>`:"") + "</td>";
                                recursos = recursos + "<td>" + ((value.matriz)?`<a href='${value.matriz}' target='_blank'>Matriz</a>`:"") + "</td>";
                                recursos = recursos + "<td>" + ((value.video)?`<a href='${value.video}' target='_blank'>Video</a>`:"") + "</td>";
                                recursos = recursos + "<td>";
                                    recursos = recursos + '<a href="#" data-id="' + value.id + '" class="btn btn-primary btn-sm btn-modificar-recurso" > <i class="fas fa-pen-square"></i> </a> ';
                                    recursos = recursos + '<a href="#" data-id="' + value.id + '" class="btn btn-danger btn-sm btn-eliminar-recurso" ><i class="fas fa-trash"></i></a>';
                                recursos = recursos +"</td>";
                            recursos = recursos + "</tr>";
                        });
                        
                        $('#lista-recursos tbody').html(recursos);
                        $('#lista-recursos').DataTable({
                            "paging": true,
                            "lengthChange": true,
                            "searching": true,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 10,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });

                        loading.modal('hide');
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}


async function Recurso(recurso_id){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/recurso/get-recurso',
                method: 'POST',
                data:{_csrf:csrf,recurso_id:recurso_id},
                dataType:'Json',
                beforeSend:function(xhr, settings)
                {
                    ////loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var form = $('#formRecurso');
                        form.loadJSON(results.recurso);
                        //loading.modal('hide');
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}

async function Grados(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/grado/get-lista-grados',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(xhr, settings)
                {
                    ////loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var options_grados = "<option value>Seleccionar</option>";
                        $.each(results.grados, function( index, value ) {
                            options_grados = options_grados + "<option value='" + value.id + "'>" + value.descripcion + "</option>"
                        });
                        $('#grado_id').html(options_grados);
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}

async function Semanas(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/semana/get-lista-semanas',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(xhr, settings)
                {
                    ////loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var options_semanas = "<option value>Seleccionar</option>";
                        $.each(results.semanas, function( index, value ) {
                            options_semanas = options_semanas + "<option value='" + value.id + "'>" + value.semana + "</option>"
                        });
                        $('#semana_id').html(options_semanas);
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}


//agregar

$('body').on('click', '.btn-agregar-recurso', function (e) {
    e.preventDefault();
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/recurso/create',function(){
        Grados();
        Semanas();
    });
    $('#modal').modal('show');
});

//modificar
$('body').on('click', '.btn-modificar-recurso', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/recurso/update?id='+id,function(){
        Grados();
        Semanas();
        Recurso(id);
    });
    $('#modal').modal('show');
});

//eliminar
$('body').on('click', '.btn-eliminar-recurso', function (e) {
        e.preventDefault();
        var recurso_id = $(this).attr('data-id');
        var r = confirm("¿Esta seguro de eliminar al recurso ?");
        if (r == true) {
            $.ajax({
                url:'<?= \Yii::$app->request->BaseUrl ?>/recurso/eliminar-recurso',
                method: 'POST',
                data:{_csrf:csrf,recurso_id:recurso_id},
                dataType:'Json',
                beforeSend:function()
                {
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        Recursos();
                        loading.modal('hide');
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
        }
        
    });


//grabar

$('body').on('click', '.btn-grabar-recurso', function (e) {

    e.preventDefault();
    $('#modal').modal('toggle');

    var form = $('#formRecurso');
    var formData = $('#formRecurso').serializeArray();
    if (form.find('.has-error').length) {
        return false;
    }
    
    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success: function (results) {
            if(results.success){
                Recursos();
                loading.modal('hide');
            }
        },
    });
});
</script>
