
<div class="col-md-3">
    <div class="card ">
        <div class="card-body">
            <div class="text-center">
                <nav aria-label="Page navigation example">
                    <ul id="pagination-demo" class="pagination justify-content-center"></ul>
                </nav>
            </div>

            <div class="mt-4 contenido_recurso">
                
            </div>
        </div>
    </div>
</div>

<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('#staticBackdrop');
$('#pagination-demo').twbsPagination({
    totalPages: 53,
    startPage: 1,
    visiblePages: 4,
    prev: '<span aria-hidden="true">&laquo;</span>',
    next: '<span aria-hidden="true">&raquo;</span>',
    onPageClick: function (event, page) {
        Recurso(page);
        //$('#page-content').text('Page ' + page);
    }
});


async function Recurso(semana){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/semana/get-semana',
                method: 'POST',
                data:{_csrf:csrf,semana:semana},
                dataType:'Json',
                beforeSend:function(xhr, settings)
                {
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    var contenido_recurso = ``;
                    if(results && results.success){
                        solucionario = results.recurso.solucionario;
                        matriz = results.recurso.matriz;
                        video = results.recurso.video;

                        if(!results.recurso){
                            contenido_recurso = `No hay contenido para esta semana`;
                        }else{
                            contenido_recurso = `
                            <div class="card border shadow-none mb-2">
                                <a href="${solucionario}" target="_blank" class="text-body">
                                    <div class="p-2">
                                        <div class="d-flex">
                                            <div class="avatar-xs align-self-center mr-2">
                                                <div class="avatar-title rounded bg-transparent text-primary font-size-20">
                                                    <i class="mdi mdi-file-document"></i>
                                                </div>
                                            </div>

                                            <div class="overflow-hidden mr-auto">
                                                <h5 class="font-size-13 text-truncate mb-1">Solucionario</h5>
                                                <p class="text-muted text-truncate mb-0">1 Archivo</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="card border shadow-none mb-2">
                                <a href="${matriz}" class="text-body">
                                    <div class="p-2">
                                        <div class="d-flex">
                                            <div class="avatar-xs align-self-center mr-2">
                                                <div class="avatar-title rounded bg-transparent text-primary font-size-20">
                                                    <i class="mdi mdi-file-document"></i>
                                                </div>
                                            </div>

                                            <div class="overflow-hidden mr-auto">
                                                <h5 class="font-size-13 text-truncate mb-1">Matrices</h5>
                                                <p class="text-muted text-truncate mb-0">1 Archivo</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="card border shadow-none mb-2">
                                <a href="${video}" class="text-body">
                                    <div class="p-2">
                                        <div class="d-flex">
                                            <div class="avatar-xs align-self-center mr-2">
                                                <div class="avatar-title rounded bg-transparent text-danger font-size-20">
                                                    <i class="mdi mdi-play-circle-outline"></i>
                                                </div>
                                            </div>

                                            <div class="overflow-hidden mr-auto align-middle">
                                                <h5 class="font-size-13 text-truncate">Video</h5>
                                                <p class="text-muted text-truncate mb-0">1 Archivo</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        
                        `;
                        }
                        $('.contenido_recurso').html(contenido_recurso);
                    }
                    loading.modal('hide');
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}
</script>