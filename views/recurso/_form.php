<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['options' => ['id' => 'formRecurso']]); ?>

    <div class="modal-header">
        <h4 class="modal-title"><?= $model->titulo ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label >Semana</label>
                    <select id="semana_id" name="Recurso[semana_id]" class="form-control">
                        <option value>Seleccionar</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label >Grado</label>
                    <select id="grado_id" name="Recurso[grado_id]" class="form-control">
                        <option value>Seleccionar</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label >Solucionario</label>
                    <input type="text" id="solucionario" name="Recurso[solucionario]" class="form-control">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label >Matriz</label>
                    <input type="text" id="matriz" name="Recurso[matriz]" class="form-control">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label >Video</label>
                    <input type="text" id="video" name="Recurso[video]" class="form-control">
                </div>
            </div>
            
        </div>
        
    </div>
    <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary btn-grabar-recurso">Grabar</button>
    </div>

<?php ActiveForm::end(); ?>