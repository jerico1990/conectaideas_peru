
<div class="m-banner m-banner--profile m-banner--intranet">
    <div class="m-banner__bg"></div>
    <div class="m-container">
        <div class="m-breadcrumbs">
            <ul>
                <li><a href="<?= \Yii::$app->request->BaseUrl ?>/panel">Inicio</a></li>
                <li><a href="intranet_panel.html">4to A</a></li>
                <li class="css--active"><a href="intranet_resources.html">Mis recursos</a></li>
            </ul>
        </div>
        <div class="m-banner__content">
            <p class="m-banner__title m-h1">Mis recursos</p>
        </div>
    </div>
</div>
<div class="m-container m-section m-section--medium">
    <p class="m-intranet__subtitle">Selecciona un grado para ver los recursos disponibles:</p>
    <div class="m-intranet__card__grid ">
        <div class="m-intranet__card__box">
            <a class="m-intranet__card" href="#">
                <picture class="m-lazy__img">
                    <source srcset="<?= \Yii::$app->request->BaseUrl ?>/personalizado/img/card_img.png  1x, <?= \Yii::$app->request->BaseUrl ?>/personalizado/img/card_img@2x.png  2x" media="(min-width: 768px)" type="image/png">
                    <source srcset="<?= \Yii::$app->request->BaseUrl ?>/personalizado/img/card_img_mobile.png  1x, <?= \Yii::$app->request->BaseUrl ?>/personalizado/img/card_img_mobile@2x.png  2x" type="image/png">
                    <img  src="<?= \Yii::$app->request->BaseUrl ?>/personalizado/img/card_img_mobile.png">
                </picture>
                <div class="m-intranet__card__content">
                    <p class="m-h2">Recursos 4to A</p>
                    <div class="m-intranet__card__registered">
                        <p>Videos, solucionarios y matrices</p>
                    </div>
                    <div class="m-button m-button--secondary m-button--icon">
                        <span>Ingresar</span>
                        <span class="m-icon-svg m-icon-svg--secondary">
                            <svg width="6" height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M3.44833 5.72832L0.195 8.98166C-0.065 9.24166 -0.065 9.66832 0.195 9.92832C0.455 10.1883 0.875 10.1883 1.135 9.92832L5.52833 5.53499C5.78833 5.27499 5.78833 4.85499 5.52833 4.59499L1.14167 0.19499C1.01711 0.0701553 0.848012 0 0.671667 0C0.495322 0 0.326221 0.0701553 0.201667 0.19499C-0.0583333 0.45499 -0.0583333 0.87499 0.201667 1.13499L3.44833 4.39499C3.52917 4.56165 3.73496 4.69498 3.73496 5.06165C3.73496 5.42831 3.52917 5.56165 3.44833 5.72832Z" fill="white" />
                            </svg>
                        </span>
                    </div>


                </div>
            </a>
        </div>

        <div class="m-intranet__card__box">
            <a class="m-intranet__card" href="#">
                <picture class="m-lazy__img">
                    <source srcset="<?= \Yii::$app->request->BaseUrl ?>/personalizado/img/card_img.png  1x, <?= \Yii::$app->request->BaseUrl ?>/personalizado/img/card_img@2x.png  2x" media="(min-width: 768px)" type="image/png">
                    <source srcset="<?= \Yii::$app->request->BaseUrl ?>/personalizado/img/card_img_mobile.png  1x, <?= \Yii::$app->request->BaseUrl ?>/personalizado/img/card_img_mobile@2x.png  2x" type="image/png">
                    <img  src="<?= \Yii::$app->request->BaseUrl ?>/personalizado/img/card_img_mobile.png">
                </picture>
                <div class="m-intranet__card__content">
                    <p class="m-h2">Recursos 5to A</p>
                    <div class="m-intranet__card__registered">
                        <p>Videos, solucionarios y matrices</p>
                    </div>
                    <div class="m-button m-button--secondary m-button--icon">
                        <span>Ingresar</span>
                        <span class="m-icon-svg m-icon-svg--secondary">
                            <svg width="6" height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M3.44833 5.72832L0.195 8.98166C-0.065 9.24166 -0.065 9.66832 0.195 9.92832C0.455 10.1883 0.875 10.1883 1.135 9.92832L5.52833 5.53499C5.78833 5.27499 5.78833 4.85499 5.52833 4.59499L1.14167 0.19499C1.01711 0.0701553 0.848012 0 0.671667 0C0.495322 0 0.326221 0.0701553 0.201667 0.19499C-0.0583333 0.45499 -0.0583333 0.87499 0.201667 1.13499L3.44833 4.39499C3.52917 4.56165 3.73496 4.69498 3.73496 5.06165C3.73496 5.42831 3.52917 5.56165 3.44833 5.72832Z" fill="white" />
                            </svg>
                        </span>
                    </div>
                </div>
            </a>
        </div>

        <div class="m-intranet__card__box">
            <a class="m-intranet__card" href="#">
                <picture class="m-lazy__img">
                    <source srcset="<?= \Yii::$app->request->BaseUrl ?>/personalizado/img/card_img.png  1x, <?= \Yii::$app->request->BaseUrl ?>/personalizado/img/card_img@2x.png  2x" media="(min-width: 768px)" type="image/png">
                    <source srcset="<?= \Yii::$app->request->BaseUrl ?>/personalizado/img/card_img_mobile.png  1x, <?= \Yii::$app->request->BaseUrl ?>/personalizado/img/card_img_mobile@2x.png  2x" type="image/png">
                    <img src="<?= \Yii::$app->request->BaseUrl ?>/personalizado/img/card_img_mobile.png">
                </picture>
                <div class="m-intranet__card__content">
                    <p class="m-h2">Recursos 6to A</p>
                    <div class="m-intranet__card__registered">
                        <p>Videos, solucionarios y matrices</p>
                    </div>
                    <div class="m-button m-button--secondary m-button--icon">
                        <span>Ingresar</span>
                        <span class="m-icon-svg m-icon-svg--secondary">
                            <svg width="6" height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M3.44833 5.72832L0.195 8.98166C-0.065 9.24166 -0.065 9.66832 0.195 9.92832C0.455 10.1883 0.875 10.1883 1.135 9.92832L5.52833 5.53499C5.78833 5.27499 5.78833 4.85499 5.52833 4.59499L1.14167 0.19499C1.01711 0.0701553 0.848012 0 0.671667 0C0.495322 0 0.326221 0.0701553 0.201667 0.19499C-0.0583333 0.45499 -0.0583333 0.87499 0.201667 1.13499L3.44833 4.39499C3.52917 4.56165 3.73496 4.69498 3.73496 5.06165C3.73496 5.42831 3.52917 5.56165 3.44833 5.72832Z" fill="white" />
                            </svg>
                        </span>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
