<html lang="en" ng-app="includeExample">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Conecta Ideas Peru</title>
    <!-- Icons Css -->
    <link rel="shortcut icon" href="<?= \Yii::$app->request->BaseUrl ?>//img/conectaideas_login.png">
	<link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/css/icons.min.css" type="text/css">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,400;0,500;0,600;0,700;1,400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/css/bootstrap-grid.min.css.map">
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/css/bootstrap-reboot.min.css.map">
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/css/bootstrap.min.css.map">
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/css/style.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" ></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/js/moment.js"></script>
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
    <script src="https://use.fontawesome.com/e084dc0bf8.js"></script>
<!-- 
    <script src="//code.angularjs.org/snapshot/angular.min.js"></script> -->
    <!-- <script src="<?= \Yii::$app->request->BaseUrl ?>/js/app-main.js"></script> -->
    
    
    <!-- <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
	<script src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script> -->
    
    
    <script>
    var bootstrapButton = $.fn.button.noConflict() // return $.fn.button to previously assigned value
    $.fn.bootstrapBtn = bootstrapButton // give $().bootstrapBtn the Bootstrap functionality
        // (function($){
        //     $.fn.datepicker.dates['es'] = {
        //         days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        //         daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
        //         daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
        //         months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        //         monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        //         today: "Hoy",
        //         monthsTitle: "Meses",
        //         clear: "Borrar",
        //         weekStart: 1,
        //         format: "dd/mm/yyyy"
        //     };
        // }(jQuery));

        
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-166928785-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-166928785-1');
</script>

    <style>
    .datepicker table tr td.active,.datepicker table tr td.active:hover{
        background:#2ab7ca !important;
    }

    
    </style>

</head>

<body >

    <div id="sizeScreen" class="container">
        <?= $content ?>
    </div>
    <script>
            $(window).scroll(function () {

                let pog = window.innerHeight;
                delta = 10;
                if ($(window).scrollTop() > pog + 2 * delta) {
                    $('#scroller').addClass('stuck');
                    //document.getElementById("cont1").style.paddingTop = "90px";

                    document.getElementById("scroller").style.visibility = "visible";
                } else {
                    //document.getElementById("cont1").style.paddingTop = "0px";
                    document.getElementById("scroller").style.visibility = "visible";
                    $('#scroller').removeClass('stuck');
                }

            });

            var modal = document.getElementById("myModal");
            var span = document.getElementsByClassName("close")[0];

            window.onclick = function (event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
            }
            $(document).on('click', function (event) {
                $target = $(event.target);
                if ($target.hasClass('n_hijo')) {
                    if ($target.parent().hasClass('active')) {
                        $target.parent().removeClass('active');
                        allListElements = $target.parent().find("span");
                        allListElements.css("display", "none");
                        inputCh = $target.parent().find("input.form-check-input-switcher");
                        inputCh.prop('checked',false);
                    } else {
                        $target.parent().addClass('active');
                        allListElements = $target.parent().find("span");
                        allListElements.css("display", "block");
                        inputCh = $target.parent().find("input.form-check-input-switcher");
                        //inputCh.trigger('click');
                        inputCh.prop('checked',true);
                    }
                }

            });
        </script>


    <script>
		$('.numerico').keypress(function (tecla) {
			var reg = /^[0-9.\s]+$/;
			if(!reg.test(String.fromCharCode(tecla.which))){
				return false;
			}
			return true;
		});		
		$('.texto').keypress(function(tecla) {
			var reg = /^[a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ'_\s]+$/;
			if(!reg.test(String.fromCharCode(tecla.which))){
				return false;
			}
			return true;
		});

        
		</script>
        
    
</body>

</html>