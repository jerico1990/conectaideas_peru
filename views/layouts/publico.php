<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="bootstrap admin template">
        <meta name="author" content="">
        
        <title> ConectaIdeas | Perú. </title>
        <link rel="shortcut icon" href="<?= \Yii::$app->request->BaseUrl ?>//img/conectaideas_login.png">
		<!-- Bootstrap Css -->
		<link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/css/bootstrap.min.css" type="text/css">
		<!-- Icons Css -->
		<link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/css/icons.min.css" type="text/css">
		<!-- App Css-->
		<link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/css/app.min.css" type="text/css">
		

		<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/gh/kevindb/jquery-load-json@1.3.4/dist/jquery.loadJSON.min.js" integrity="sha384-ivtX4sn4dcdfHiO4e0/956wIQSerxsy2QZ6EHzdCVLlyGYYjSb8bqdxKY8IsfDGh" crossorigin="anonymous"></script>
		
		<link rel="preconnect" href="https://fonts.gstatic.com">
    	<link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
<!-- 
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta/dist/css/bootstrap-select.min.css">

		<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta/dist/js/bootstrap-select.min.js"></script> -->
		<script>
			/*$.fn.serializeObject = function() {
				var o = {};
				var a = this.serializeArray();
				$.each(a, function() {
					if (o[this.name]) {
						if (!o[this.name].push) {
							o[this.name] = [o[this.name]];
						}
						o[this.name].push(this.value || '');
					} else {
						o[this.name] = this.value || '';
					}
				});
				return o;
			};*/
		</script>

		<!-- JAVASCRIPT -->
		<script src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/libs/jquery/jquery.min.js"></script>
		<script src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
		<script src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/libs/metismenu/metisMenu.min.js"></script>
		<script src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/libs/simplebar/simplebar.min.js"></script>
		<script src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/libs/node-waves/waves.min.js"></script>
		
		<link href="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/libs/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
		<script src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/libs/select2/js/select2.min.js"></script>

		<link href="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
		<script src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

<!-- 		
		<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
		<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> -->
			<script>
			(function($){
				$.fn.datepicker.dates['es'] = {
					days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
					daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
					daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
					months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
					monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
					today: "Hoy",
					monthsTitle: "Meses",
					clear: "Borrar",
					weekStart: 1,
					format: "dd/mm/yyyy"
				};
			}(jQuery));
			</script>
		<style>
		
		.modal {
			display: none; 
			position: fixed; 
			z-index: 1; 
			padding-top: 50px; 
			left: 0;
			top: 0;
			width: 100%;
			height: 100%;
			overflow: auto;
			background-color: rgb(0, 0, 0);
			background-color: rgba(0, 0, 0, 0.4);
		}

		.modal-content {
			background-color: #f4f9fb;
			margin: auto;
			padding: 20px;
			border: 1px solid #888;
			width: 80%;
		}
		.n_final-box {
			max-width: 305px;
		}

		.container-text-box {
			max-width: 445px;
			border-bottom: 1px solid #c4c4c4;
		}
		.n_box3 {
			max-width: 257px;
		}

		</style>
    </head>
    <body >
		<?= $content ?>
		
		<!-- App js -->
		<script src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/js/app.js"></script>
		
		<script>
		$('.numerico').keypress(function (tecla) {
			var reg = /^[0-9.\s]+$/;
			if(!reg.test(String.fromCharCode(tecla.which))){
				return false;
			}
			return true;
		});		
		$('.texto').keypress(function(tecla) {
			var reg = /^[a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ'_\s]+$/;
			if(!reg.test(String.fromCharCode(tecla.which))){
				return false;
			}
			return true;
		});
		</script>
        
    </body>
</html>
