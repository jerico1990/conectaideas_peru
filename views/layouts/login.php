<!DOCTYPE html>
<html lang="es-PE">

<head>
    <title>Login intranet docentes - Conecta Ideas Perú</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta name="title" content="">
    <meta name="description" content="">
    <link rel="canonical" href="">
    <link
        href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,400;0,500;0,600;0,700;1,400;1,500;1,600;1,700&amp;display=swap"
        rel="stylesheet">
		
    <link href="<?= \Yii::$app->request->BaseUrl ?>/personalizado/monki.css?8c1cbab4a4c1785ed4ec" rel="stylesheet">
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/libs/jquery/jquery.min.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-166928785-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-166928785-1');
</script>
</head>

<body class="m-intranet m-login">
    <div class="m-banner m-banner--intranet">
        <div class="m-banner__bg"></div>
    </div>
    <div class="m-container m-section">
		<?= $content ?>
        
    </div>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/personalizado/js/monki.js?8c1cbab4a4c1785ed4ec"></script>
</body>

</html>


