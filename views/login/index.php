<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<div class="m-modal__scene js--active" id="m-modal__scene__one">
    <div class="m-login__form">
        <h2>¡Bienvenido/a al Portal Docente!</h2>
        <?php $form = ActiveForm::begin(['options' => ['id' => 'formLogin']]); ?>
            <div class="m-input">
                <input id="usuario" type="text" placeholder="Ingresa tu DNI" name="LoginForm[username]">
                <label class="m-input__label error_usuario"></label>
            </div>
            <div class="m-input">
                <input id="password" type="password" placeholder="Ingresa tu contraseña"
                    name="LoginForm[password]">
                <label class="m-input__label error_clave"></label>
                <div class="m-input__password-show">
                    <span class="m-icon-svg m-icon-svg--secondary">
                        <svg width="17" height="12" viewBox="0 0 17 12" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M16.2594 3.68133C11.825 -1.22711 4.64063 -1.22711 0.20625 3.68133C-0.06875 3.98573 -0.06875 4.48037 0.20625 4.74672C0.48125 5.05112 0.928125 5.05112 1.16875 4.74672C5.0875 0.447086 11.3781 0.447086 15.2969 4.74672C15.5719 5.05112 15.9844 5.05112 16.2594 4.74672C16.5344 4.44233 16.5344 3.98573 16.2594 3.68133C16.2938 3.68133 16.2938 3.68133 16.2594 3.68133Z"
                                fill="#808793" />
                            <path
                                d="M16.2938 8.32345C16.5688 8.01905 16.5688 7.56245 16.2938 7.25805C16.0188 6.95365 15.6062 6.95365 15.3312 7.25805C11.4125 11.5577 5.0875 11.5577 1.16875 7.22C0.89375 6.9156 0.446875 6.9156 0.20625 7.22C-0.06875 7.5244 -0.06875 8.01905 0.20625 8.2854C4.64062 13.2319 11.825 13.2319 16.2938 8.32345Z"
                                fill="#808793" />
                            <path
                                d="M8.25 8.78C9.7625 8.78 11 7.4102 11 5.73601C11 4.06181 9.7625 2.69202 8.25 2.69202C6.7375 2.69202 5.5 4.06181 5.5 5.73601C5.5 7.4102 6.7375 8.78 8.25 8.78ZM8.25 4.21401C9.00625 4.21401 9.625 4.89891 9.625 5.73601C9.625 6.57311 9.00625 7.258 8.25 7.258C7.49375 7.258 6.875 6.57311 6.875 5.73601C6.875 4.89891 7.49375 4.21401 8.25 4.21401Z"
                                fill="#808793" />
                        </svg>
                    </span>
                    <span class="m-icon-svg m-icon-svg--secondary">
                        <svg width="17" height="9" viewBox="0 0 17 9" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M16.8004 0.214192C16.5675 -0.0652837 16.1018 -0.0652837 15.8356 0.179258C13.3738 2.58974 10.9119 3.81245 8.48337 3.81245C6.05479 3.81245 3.62622 2.58974 1.16438 0.179258C0.898239 -0.0652837 0.432485 -0.0652837 0.199609 0.214192C0.0665362 0.35393 0 0.563537 0 0.73821C0 0.947817 0.0998043 1.12249 0.232877 1.22729C0.931507 1.92598 1.63014 2.51987 2.32877 3.04389L1.16438 4.65087C1.06458 4.79061 0.998043 5.00022 1.03131 5.17489C1.06458 5.34956 1.16438 5.52424 1.29746 5.66397C1.59687 5.90852 2.02935 5.83865 2.26223 5.52424L3.49315 3.81245C3.9589 4.09192 4.45793 4.33646 4.95695 4.54607L4.22505 6.46747C4.15851 6.64214 4.15851 6.85175 4.22505 7.02642C4.29159 7.20109 4.42466 7.34083 4.591 7.4107C4.92368 7.55044 5.35616 7.37577 5.48924 7.02642L6.2544 5.00022C6.75342 5.13996 7.28571 5.20983 7.818 5.2797V7.3059C7.818 7.69018 8.11742 8.03952 8.51663 8.03952C8.91585 8.03952 9.21526 7.72511 9.21526 7.3059V5.24476C9.71429 5.20983 10.2466 5.10502 10.7789 4.96528L11.544 6.99149C11.6771 7.34083 12.0763 7.55044 12.4423 7.4107C12.6086 7.34083 12.7417 7.20109 12.8082 7.02642C12.8748 6.85175 12.8748 6.64214 12.8082 6.46747L12.0431 4.54607C12.5088 4.33646 13.0078 4.09192 13.5068 3.81245L14.7378 5.52424C14.9706 5.83865 15.4031 5.90852 15.7025 5.66397C16.002 5.41943 16.0685 4.96528 15.8356 4.65087L14.6712 3.04389C15.3699 2.51987 16.1018 1.92598 16.7671 1.22729C17.0333 0.982751 17.0665 0.528603 16.8004 0.214192Z"
                                fill="#808793" />
                        </svg>
                    </span>
                </div>
            </div>
            <button class="m-button m-button--secondary m-button--full btn-iniciar-sesion" type="button">Ingresar</button>

            <p>¿Olvidaste tu contraseña?<br>
                <a href="<?= \Yii::$app->request->BaseUrl ?>/temporal/resetear-clave"><span class="m-modal__scene--js" >Haz click aquí</span></a>
            </p>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<script>
var csrf = "<?= Yii::$app->request->getCsrfToken() ?>";
$('body').on('click', '.btn-iniciar-sesion', function (e) {
    e.preventDefault();
    var usuario = $('#usuario').val();
    var clave = $('#password').val();
    var error = "";

    var validarUsuario = 0;
    var validarClave = 0;

    if (!usuario) {
        $('.error_usuario').html(`Campo obligatorio`);
        $('.error_usuario').addClass('m-input__error');
        $('.error_usuario').addClass('js--active');
        $("#usuario").focus();
        error = error + "error 1";
    }else if ((usuario && (usuario.length<8 || usuario.length>8)) && usuario!='administrador' ) {
        $('.error_usuario').html(`Numero DNI no válido, verifícalo`);
        $('.error_usuario').addClass('m-input__error');
        $('.error_usuario').addClass('js--active');
        $("#usuario").focus();
        error = error + "error 1";
    } else {
        $('.error_usuario').removeClass('m-input__error');
        $('.error_usuario').removeClass('js--active');
        $('.error_usuario').html(``);
        validarUsuario = 1;
    }



    if (!clave) {
        $('.error_clave').html(`Campo obligatorio`);
        $('.error_clave').addClass('m-input__error');
        $('.error_clave').addClass('js--active');
        $("#password").focus();
        error = error + "error error_clave";
    } else {
        $('.error_clave').removeClass('m-input__error');
        $('.error_clave').removeClass('js--active');
        $('.error_clave').html(``);
        validarClave = 1;
    }

    msg = 1;
    if(validarUsuario == 1 && validarClave == 1){
        msg = validarUsuarioClave(usuario,clave);
    }
    

    if(msg == 0 ){
        $('.error_clave').html(`Tu contraseña es incorrecta`);
        $('.error_clave').addClass('m-input__error');
        $('.error_clave').addClass('js--active');
        $("#password").focus();

        $('.error_usuario').html(`Usuario no registrado`);
        $('.error_usuario').addClass('m-input__error');
        $('.error_usuario').addClass('js--active');
        $("#usuario").focus();
        error = error + "error error_clave";
    }else if(msg == 2){
        $('.error_clave').html(`Tu contraseña es incorrecta`);
        $('.error_clave').addClass('m-input__error');
        $('.error_clave').addClass('js--active');
        $("#password").focus();
        error = error + "error error_clave";
    }else if(msg == 3){
        $('.error_usuario').html(`Usuario no registrado`);
        $('.error_usuario').addClass('m-input__error');
        $('.error_usuario').addClass('js--active');
        $("#usuario").focus();
        error = error + "error error_clave";
    }

    if (error != "") {
        return false;
    }

    $( "#formLogin" ).submit();
});


function validarUsuarioClave(usuario,clave) {
    var bandera = 0;
    $.ajax({
        async: false,
        url: '<?= \Yii::$app->request->BaseUrl ?>/login/get-validar-usuario-clave',
        method: 'POST',
        data: {
            "_csrf": csrf,
            usuario: usuario,
            clave: clave
        },
        dataType: 'Json',
        beforeSend: function() {

        },
        success: function(results) {
            
            if (results.success) {
                bandera = results.msg;
            }
        },
        error: function() {
            alert('No hay conectividad con el sistema');
        }
    });
    return bandera;
}
</script>