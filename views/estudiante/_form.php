<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['options' => ['id' => 'formEstudiante']]); ?>

    <div class="modal-header">
        <h4 class="modal-title"><?= $model->titulo ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label >D.N.I</label>
                    <input type="text" id="dni" name="Estudiante[dni]" class="form-control numerico" maxlength="8">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label >Nombres</label>
                    <input type="text" id="nombres" name="Estudiante[nombres]" class="form-control texto">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label >Apellido paterno</label>
                    <input type="text" id="apellido_paterno" name="Estudiante[apellido_paterno]" class="form-control texto">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label >Apellido materno</label>
                    <input type="text" id="apellido_materno" name="Estudiante[apellido_materno]" class="form-control texto">
                </div>
            </div>
            
        </div>
        
    </div>
    <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary btn-grabar-estudiante">Grabar</button>
    </div>

<?php ActiveForm::end(); ?>