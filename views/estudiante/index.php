
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0 font-size-18">Estudiantes</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Estudiante</a></li>
                    <li class="breadcrumb-item active">Estudiantes</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Departamento</label>
                            <select class="form-control" id="departamento">
                                <option value>Selecciona una opción</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Provincia</label>
                            <select class="form-control" id="provincia">
                                <option value>Selecciona una opción</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Distrito</label>
                            <select class="form-control" id="distrito">
                                <option value>Selecciona una opción</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">IIEE</label>
                            <select class="form-control" id="institucion_educativa_id">
                                <option value>Selecciona una opción</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Usuario</label>
                            <input type="text" class="form-control" id="usuario" placeholder="Usuario">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">&nbsp&nbsp</label> <br>
                            <button class="btn btn-success btn-buscar">Buscar</button>
                        </div>
                    </div>
                </div>

                
                
                

                <table id="lista-estudiantes" class="table table-bordered dt-responsive ">
                    <thead>
                        <tr>
                            <th>Región</th>
                            <th>Provincia</th>
                            <th>Distrito</th>
                            <th>Institución Educativa</th>
                            <th>Nombre</th>
                            <th>Usuario</th>
                            <th>Clave</th>
                            <th>Estado</th>
                            <th>Fecha de registro</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>


                    <tbody>
                    </tbody>
                </table>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <!-- <h4 class="card-title">Default Datatable</h4> <button class="btn btn-success btn-agregar-estudiante btn-habilitar-masivamente-estudiante">Agregar</button> -->
                            <a href="<?= \Yii::$app->request->BaseUrl ?>/estudiante/habilitar-masivamente" class="btn btn-success ">Habilitar Masivamente</a>
                        </div>
                    </div>
                </div>

                <!-- <a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/reporte-registros-estudiantes" class="btn btn-success btn-block">Descargar lista de estudiantes</a> -->
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<div id="modal" class="fade modal" role="dialog"  tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>



<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('#staticBackdrop');
//loading.modal("show");
var departamento;
var provincia;
var distrito;
var iiee;
var usuario;

$('#lista-estudiantes').DataTable();

Estudiantes();
async function Estudiantes(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/estudiante/get-lista-estudiantes',
                method: 'POST',
                data:{_csrf:csrf,departamento:departamento,provincia:provincia,distrito:distrito,iiee:iiee,usuario:$('#usuario').val()},
                dataType:'Json',
                beforeSend:function(){
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var estudiantes ="";
                        $('#lista-estudiantes').DataTable().destroy();
                        $.each(results.estudiantes, function( index, value ) {
                            nomb = (value.nombres)?value.nombres:"";
                            appat = (value.apellido_paterno)?value.apellido_paterno:"";
                            apmat = (value.apellido_materno)?value.apellido_materno:"";
                            nombres_completos = `${nomb} ${appat} ${apmat}`;
                            
                            estado_registro = value.estado_registro;
                            estado_descripcion = '';
                            switch (estado_registro) {
                                case '1' : estado_descripcion='Habilitado'; break;
                                case '2' : estado_descripcion='<i class="bx bx-error-circle"></i> En espera'; break;
                                case '0' : estado_descripcion='Eliminado'; break;
                                default: estado_descripcion='';
                            }


                            estudiantes = estudiantes + "<tr>";
                                estudiantes = estudiantes + "<td>" + value.departamento + "</td>";
                                estudiantes = estudiantes + "<td>" + value.provincia + "</td>";
                                estudiantes = estudiantes + "<td>" + value.distrito + "</td>";
                                estudiantes = estudiantes + "<td>" + value.nombre_ie + "</td>";
                                estudiantes = estudiantes + "<td>" + nombres_completos + "</td>";
                                estudiantes = estudiantes + "<td>" + value.usuario + "</td>";
                                estudiantes = estudiantes + "<td>" + value.clave + "</td>";

                                estudiantes = estudiantes + "<td>" + estado_descripcion + "</td>";
                                estudiantes = estudiantes + "<td>" + value.fecha_registro + "</td>";
                                estudiantes = estudiantes + "<td>";
                                    // estudiantes = estudiantes + '<a href="#" data-id="' + value.id + '" class="btn btn-primary btn-sm btn-modificar-estudiante" > <i class="fas fa-pen-square"></i> </a> ';
                                    // estudiantes = estudiantes + '<a href="#" data-id="' + value.id + '" class="btn btn-danger btn-sm btn-eliminar-estudiante" ><i class="fas fa-trash"></i></a> ';
                                    if(estado_registro=='1' ){
                                        estudiantes = estudiantes + '<a title="deshabilitar" href="#" data-matricula_id="' + value.matricula_id + '" class="btn btn-danger btn-sm btn-deshabilitar-estudiante" ><i class="fas fa-trash"></i></a>';
                                    }else if (estado_registro=='2'){
                                        estudiantes = estudiantes + '<a title="habilitar" href="#" data-matricula_id="' + value.matricula_id + '" class="btn btn-success btn-sm btn-habilitar-estudiante" ><i class="fas fa-check"></i></a>';
                                    }
                                    

                                estudiantes = estudiantes +"</td>";
                            estudiantes = estudiantes + "</tr>";
                        });
                        
                        $('#lista-estudiantes tbody').html(estudiantes);
                        $('#lista-estudiantes').DataTable({
                            "paging": true,
                            "lengthChange": true,
                            "searching": false,
                            "ordering": false,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 10,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });

                        loading.modal('hide');
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}


async function Estudiante(estudiante_id){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/estudiante/get-estudiante',
                method: 'POST',
                data:{_csrf:csrf,estudiante_id:estudiante_id},
                dataType:'Json',
                beforeSend:function(xhr, settings)
                {
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var form = $('#formEstudiante');
                        console.log(results.estudiante);
                        form.loadJSON(results.estudiante);
                        //loading.modal('hide');
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}

Regiones()
async function Regiones() {
    await $.ajax({
        url: '<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-regiones',
        method: 'POST',
        data: {
            "_csrf": csrf
        },
        dataType: 'Json',
        beforeSend: function() {
            //loading.modal("show");
        },
        success: function(results) {
            if (results.success) {
                var regiones_options = "<option value>Selecciona una opción</option>";
                $.each(results.data, function(index, value) {
                    regiones_options += "<option value='" + value.departamento + "'>" + value.departamento + "</option>";
                });
                $('#departamento').html(regiones_options);
                //loading.modal('hide');
            }
        },
        error: function() {
            alert('No hay conectividad con el sistema');
        }
    });
}


async function Provincias(departamento, provincia) {
    await $.ajax({
        url: '<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-provincias',
        method: 'POST',
        data: {
            "_csrf": csrf,
            departamento: departamento
        },
        dataType: 'Json',
        beforeSend: function() {

            //loading.modal("show");
        },
        success: function(results) {
            if (results.success) {
                var provincias_options = "<option value>Selecciona una opción</option>";
                $.each(results.data, function(index, value) {
                    provincias_options += "<option value='" + value.provincia + "'>" + value.provincia + "</option>";
                });
                $('#provincia').html(provincias_options);
                if (provincia) {
                    $('#provincia').val(provincia);
                }
                //loading.modal('hide');
            }
        },
        error: function() {
            alert('No hay conectividad con el sistema');
        }
    });
}


async function Distritos(provincia, distrito) {
    await $.ajax({
        url: '<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-distritos',
        method: 'POST',
        data: {
            "_csrf": csrf,
            provincia: provincia
        },
        dataType: 'Json',
        beforeSend: function() {
            //loading.modal("show");
        },
        success: function(results) {
            if (results.success) {
                var distritos_options = "<option value>Selecciona una opción</option>";
                $.each(results.data, function(index, value) {
                    distritos_options += "<option value='" + value.distrito + "'>" + value.distrito + "</option>";
                });
                $('#distrito').html(distritos_options);
                if (distrito) {
                    $('#distrito').val(distrito);
                }
                //loading.modal('hide');
            }
        },
        error: function() {
            alert('No hay conectividad con el sistema');
        }
    });
}


async function InstitucionesEducativas(departamento, provincia, distrito, institucion_educativa) {
    await $.ajax({
        url: '<?= \Yii::$app->request->BaseUrl ?>/institucion-educativa/get-instituciones-educativas',
        method: 'POST',
        data: {
            "_csrf": csrf,
            departamento: departamento,
            provincia: provincia,
            distrito: distrito
        },
        dataType: 'Json',
        beforeSend: function() {
            //loading.modal("show");
        },
        success: function(results) {
            if (results.success) {
                var instituciones_educativas_options = "<option value>Selecciona una opción</option>";
                $.each(results.data, function(index, value) {
                    instituciones_educativas_options += "<option value='" + value.id + "' data-cod_modular='" + value.cod_mod + "'> " + value.nombre_ie + "</option>";
                });
                $('#institucion_educativa_id').html(instituciones_educativas_options);
                if (institucion_educativa) {
                    $('#institucion_educativa_id').val(institucion_educativa).trigger('change');
                }

                //loading.modal('hide');
            }
        },
        error: function() {
            alert('No hay conectividad con el sistema');
        }
    });
}

$('body').on('click', '.btn-buscar', function (e) {
    Estudiantes();
});

//masivamente

$('body').on('click', '.btn-habilitar-masivamente-estudiante', function (e) {
    e.preventDefault();
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/estudiante/masivamente');
    $('#modal').modal('show');
});


//agregar

$('body').on('click', '.btn-agregar-estudiante', function (e) {
    e.preventDefault();
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/estudiante/create');
    $('#modal').modal('show');
});

//modificar
$('body').on('click', '.btn-modificar-estudiante', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/estudiante/update?id='+id,function(){
        Estudiante(id);
    });
    $('#modal').modal('show');
});

//eliminar
// $('body').on('click', '.btn-eliminar-estudiante', function (e) {
//     e.preventDefault();
//     var estudiante_id = $(this).attr('data-id');
//     var r = confirm("¿Esta seguro de eliminar al estudiante ?");
//     if (r == true) {
//         $.ajax({
//             url:'<?= \Yii::$app->request->BaseUrl ?>/estudiante/eliminar-estudiante',
//             method: 'POST',
//             data:{_csrf:csrf,estudiante_id:estudiante_id},
//             dataType:'Json',
//             beforeSend:function()
//             {
//                 //loading.modal("show");
//             },
//             success:function(results)
//             {   
//                 if(results && results.success){
//                     Estudiantes();
//                     loading.modal('hide');
//                 }
//             },
//             error:function(){
//                 alert('Error al realizar el proceso.');
//             }
//         });
//     }
    
// });



$('body').on('click', '.btn-habilitar-estudiante', function (e) {
    e.preventDefault();
    var matricula_id = $(this).attr('data-matricula_id');
    var r = confirm("¿Esta seguro de habilitar al estudiante ?");
    if (r == true) {
        $.ajax({
            url:'<?= \Yii::$app->request->BaseUrl ?>/estudiante/habilitar-estudiante',
            method: 'POST',
            data:{_csrf:csrf,matricula_id:matricula_id},
            dataType:'Json',
            beforeSend:function()
            {
                //loading.modal("show");
            },
            success:function(results)
            {   
                if(results && results.success){
                    Estudiantes();
                }
            },
            error:function(){
                alert('Error al realizar el proceso.');
            }
        });
    }
    
});


$('body').on('click', '.btn-deshabilitar-estudiante', function (e) {
    e.preventDefault();
    var matricula_id = $(this).attr('data-matricula_id');
    var r = confirm("¿Esta seguro de deshabilitar al estudiante ?");
    if (r == true) {
        $.ajax({
            url:'<?= \Yii::$app->request->BaseUrl ?>/estudiante/deshabilitar-estudiante',
            method: 'POST',
            data:{_csrf:csrf,matricula_id:matricula_id},
            dataType:'Json',
            beforeSend:function()
            {
                //loading.modal("show");
            },
            success:function(results)
            {   
                if(results && results.success){
                    Estudiantes();
                }
            },
            error:function(){
                alert('Error al realizar el proceso.');
            }
        });
    }
    
});


$('body').on('change', '#departamento', function(e) {
    departamento = $(this).val();
    Provincias(departamento);
});


$('body').on('change', '#provincia', function(e) {
    provincia = $(this).val();
    Distritos(provincia);
});

$('body').on('change', '#distrito', function(e) {
    departamento = $('#departamento').val();
    provincia = $('#provincia').val();
    distrito = $('#distrito').val();

    InstitucionesEducativas(departamento, provincia, distrito);
});

$('body').on('change', '#institucion_educativa_id', function(e) {
    iiee = $(this).val();
});

//grabar

$('body').on('click', '.btn-grabar-estudiante', function (e) {

    e.preventDefault();
    

    var form = $('#formEstudiante');
    var formData = $('#formEstudiante').serializeArray();
    if (form.find('.has-error').length) {
        return false;
    }
    
    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success: function (results) {
            if(results.success){
                $('#modal').modal('toggle');
                Estudiantes();
                //loading.modal('hide');
            }
        },
    });
});




</script>
