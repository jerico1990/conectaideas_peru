<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0 font-size-18">Habilitación de estudiantes</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Estudiante</a></li>
                    <li class="breadcrumb-item active">Habilitación de estudiantes</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->



<?php $form = ActiveForm::begin(['action'=>'masivamente-archivo','options' => ['id' => 'formEstudianteArchivo']]); ?>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label >Mediante archivo</label>
            <input type="file" id="archivo" name="Estudiante[archivo]" class="form-control" >
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label >&nbsp&nbsp&nbsp</label> <br>
            <button type="button" class="btn btn-primary btn-procesar-estudiante-archivo">Procesar</button>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>


<?php $form = ActiveForm::begin(['action'=>'masivamente','options' => ['id' => 'formEstudiante']]); ?>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label >Usuarios</label>
            <textarea id="masivamente" name="Estudiante[masivamente]" cols="30" rows="10" class="form-control"></textarea>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label >&nbsp&nbsp&nbsp</label> <br>
            <button type="button" class="btn btn-primary btn-procesar-estudiante">Procesar</button>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>


<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
$('body').on('click', '.btn-procesar-estudiante', function (e) {

    e.preventDefault();


    var form = $('#formEstudiante');
    var formData = $('#formEstudiante').serializeArray();
    if (form.find('.has-error').length) {
        return false;
    }

    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success: function (results) {
            if(results.success){
                $('#masivamente').val('');
                toastr.success('Proceso exitoso');
            }
        },
    });
});


$('body').on('click', '.btn-procesar-estudiante-archivo', function (e) {
    e.preventDefault();
    var form = $('#formEstudianteArchivo');
    
	var form_data = new FormData();
	form_data.append("_csrf", csrf);
    form_data.append("Estudiante[archivo]", document.getElementById('archivo').files[0]);
    form_data.append("Estudiante[temp]", "1");

    $.ajax({
        url:form.attr("action"),
		type: 'POST',
		cache: false,
		contentType: false,
		processData: false,
		data: form_data,
		dataType:'Json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert('No hay conectividad con el sistema');
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        success: function (results) {
            if(results.success){
                toastr.success('Proceso exitoso');
            }
        },
    });
});
</script>