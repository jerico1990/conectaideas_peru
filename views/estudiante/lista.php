<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="m-banner m-banner--profile m-banner--intranet">
    <div class="m-banner__bg"></div>
    <div class="m-container">
        <div class="m-breadcrumbs">
            <ul>
                <li><a href="<?= \Yii::$app->request->BaseUrl ?>/panel">Inicio</a></li>
                <li><a href="<?= \Yii::$app->request->BaseUrl ?>/panel/opciones?docente_seccion_id=<?= $docente_seccion_id?>&grado=<?= $grado->grado ?>&seccion=<?= $seccion?>"><?= $grado->nombre_corto ?> <?= $seccion?></a></li>
                <li class="css--active"><a href="#">Mis estudiantes</a></li>
            </ul>
        </div>
        <div class="m-banner__content">
            <p class="m-banner__title m-h1">Mis estudiantes</p>
        </div>
    </div>
</div>
<div class="m-container m-section m-intranet--student__grid">
    <div class="m-student-add">
        <?php $form = ActiveForm::begin(['options' => ['id' => 'formEstudiante']]); ?>
            <p class="m-h2">Agregar estudiante</p>
            <p>Ingresa los datos del estudiante para inscribirlo/a en el programa</p>
            <input type="hidden" id="docente_seccion_id" name="Estudiante[docente_seccion_id]" value="<?= $docente_seccion_id ?>">

            <div class="m-input">
                <input class="texto error_nombres" id="nombres" type="text" placeholder="Nombre del estudiante" name="Estudiante[nombres]" >
                <div class="error_nombres_text"></div>
            </div>
            <div class="m-input">
                <input class="error_apellido_paterno_letra texto" id="apellido_paterno_letra" type="text" placeholder="Primera letra del apellido" maxlength="1" name="Estudiante[apellido_paterno_letra]">
                <div class="error_apellido_paterno_letra_text"></div>
            </div>
            <button class="m-button m-button--secondary m-button--full m-button--icon m-button--icon--auto btn-grabar-estudiante" type="button" id="m-button-student-add">
                <span>
                    <span class="m-icon-svg m-icon-svg--secondary">
                        <svg width="21" height="19" viewBox="0 0 21 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M8.37406 9.09286C8.06393 9.09286 7.79811 9.04762 7.53229 8.95714C6.33609 8.73095 5.3171 8.00714 4.69685 7.0119C4.03229 6.0619 3.81077 4.84048 4.03229 3.66429C4.25381 2.44286 4.91836 1.44762 5.93735 0.769048C6.64621 0.271429 7.48798 0 8.37406 0C8.63988 0 8.95001 0 9.26013 0.0904762C10.412 0.316667 11.431 0.995238 12.0513 1.99048C12.7158 3.03095 12.9373 4.20714 12.7158 5.42857C12.3614 7.2381 11.0323 8.59524 9.26013 8.95714C8.99431 9.04762 8.68419 9.09286 8.37406 9.09286ZM8.37406 1.58333C7.75381 1.58333 7.26646 1.71905 6.82343 2.08095C6.11457 2.53333 5.71583 3.16667 5.53862 3.93571C5.40571 4.75 5.53862 5.51905 5.98165 6.15238C6.42469 6.83095 7.08925 7.28333 7.84241 7.41905H7.93102C8.24115 7.50952 8.59558 7.50952 8.95001 7.41905C10.1462 7.14762 10.988 6.2881 11.2538 5.11191C11.431 4.34286 11.2538 3.52857 10.8108 2.89524C10.3677 2.2619 9.70317 1.80952 8.99431 1.62857H8.95001C8.77279 1.58333 8.59558 1.58333 8.37406 1.58333Z" fill="white" />
                            <path d="M12.1392 15.4261H1.72785C2.3481 12.2594 5.13924 9.90706 8.37342 9.90706C10.1899 9.90706 11.8291 10.6309 13.0696 11.8071C13.3797 11.3999 13.7785 10.9928 14.1772 10.7214C12.6709 9.22849 10.6329 8.32373 8.37342 8.32373C4.03165 8.32373 0.398734 11.7618 0.0443038 16.1499L0 17.0094H12.6266C12.3608 16.5118 12.2278 16.0142 12.1392 15.4261Z" fill="white" />
                            <path d="M16.7028 10.3144C16.2597 10.3596 15.861 10.812 15.861 11.3096V13.7072C15.0635 13.7072 14.2218 13.7072 13.4243 13.7072C12.937 13.7525 12.4939 14.2049 12.5382 14.7025C12.5825 15.2001 13.0256 15.6525 13.5129 15.6072H15.861V18.0049C15.861 18.5025 16.304 18.9549 16.7914 18.9549C17.2787 18.9549 17.7218 18.5025 17.7218 18.0049V15.6072H20.0699C20.5572 15.6072 21.0002 15.1549 21.0002 14.6572C21.0002 14.1596 20.5572 13.7072 20.0699 13.7072H17.7218V11.3096C17.7218 10.7668 17.1901 10.2691 16.7028 10.3144Z" fill="white" />
                        </svg> 
                    </span>
                </span>
                <span>Agregar</span>
            </button>
        <?php ActiveForm::end(); ?>
    </div>
    <div class="m-intranet--student__grid__right">
        <div class="m-student-panel">

            <div class="m-student-panel__button">
                <!-- <button onclick="location.href='<?= \Yii::$app->request->BaseUrl ?>/reporte/reporte-lista-estudiantes?docente_seccion_id=<?= $docente_seccion_id ?>'" class="m-button m-button--full m-button--icon m-button--icon--auto" type="button"> -->

                <button onclick="window.open('<?= \Yii::$app->request->BaseUrl ?>/reporte/reporte-lista-estudiantes-pdf?docente_seccion_id=<?= $docente_seccion_id ?>','_blank');" class="m-button m-button--full m-button--icon m-button--icon--auto" type="button">
                    <span>
                        <span class="m-icon-svg m-icon-svg--secondary">
                            <svg width="18" height="16" viewBox="0 0 18 16" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path d="M18 13.7259V8.4467C18 8.04061 17.6757 7.67513 17.2297 7.67513C16.7838 7.67513 16.4595 8 16.4595 8.4467V13.7259C16.4595 14.132 16.1351 14.4975 15.6892 14.4975H2.27027C1.86486 14.4975 1.5 14.1726 1.5 13.7259V8.4467C1.5 8.04061 1.17568 7.67513 0.72973 7.67513C0.283784 7.67513 0 8.04061 0 8.48731V13.7665C0 15.0254 1.01351 16.0406 2.27027 16.0406H15.7703C16.9865 16 18 14.9848 18 13.7259ZM13.2162 8.56853L9.44595 11.5736C9.16216 11.7766 8.7973 11.7766 8.51351 11.5736L4.74324 8.56853C4.45946 8.28426 4.41892 7.79695 4.7027 7.51269C4.94595 7.22842 5.35135 7.18782 5.67568 7.39086L8.22973 9.42132V0.771573C8.22973 0.365482 8.55405 0 9 0C9.40541 0 9.77027 0.324873 9.77027 0.771573V9.46193L12.3243 7.43147C12.6081 7.14721 13.0946 7.14721 13.3784 7.47208C13.6622 7.75634 13.6622 8.24365 13.3378 8.52792C13.2973 8.52792 13.2568 8.52792 13.2162 8.56853Z" fill="white" />
                            </svg>
                        </span>
                    </span>
                    <span>Descargar lista
                        <span class="m-none m-inline-block-ml">de estudiantes</span>
                    </span>
                </button>
            </div>

            <p class="m-h2">Lista de estudiantes</p>
            <p>Mira aquí a los estudiantes que vas agregando y el estado de su solicitud de inscripción.</p>
            <div class="m-student-panel__status">
                <p><strong class="cantidad_estudiantes">Agregados (0)</strong></p>
                <p><strong>Estado <span class="m-none m-inline-block-ts">actual</span>:</strong></p>
            </div>
            <div class="m-student-panel__search">
                <div class="m-input m-input--medium m-input--light"><input id="search" type="text"
                        placeholder="Buscar nombre ..." name="search"></div>
                <div class="m-input m-input--select m-input--medium m-input--light m-input--select--js">
                    <select id="order" name="order">
                        <option value="">Ordenar</option>
                        <option value="Alfabeticamente">Alfabeticamente</option>
                        <option value="Habilitados">Habilitados</option>
                        <option value="En espera">En espera</option>
                    </select>
                </div>
            </div>
            <table id="lista-estudiantes" class="m-student-panel__cards m-student-panel__cards--table">
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="m-alert m-alert--success" id="m-alert-succesx">
    <span class="m-icon-svg m-icon-svg--secondary">
        <svg width="23" height="16" viewBox="0 0 23 16" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M8.15058 15.9782C7.698 15.9782 7.24541 15.8 6.88334 15.488L1.54283 10.4963C0.818687 9.82782 0.773429 8.66904 1.49757 7.95595C2.17645 7.24286 3.35317 7.19829 4.07731 7.91138L8.10533 11.6997L18.9221 0.557625C19.601 -0.155467 20.7777 -0.200035 21.5019 0.513057C22.226 1.18158 22.2713 2.34035 21.5471 3.05345L9.46308 15.4434C9.10102 15.8 8.64843 15.9782 8.15058 15.9782Z" fill="white" />
        </svg>
    </span>
    <div>
        <p class="m-h3">Estudiante agregado</p>
        <p>Revisa su estado en la Lista de Estudiantes</p>
    </div>
    <div class="m-alert__close m-alert__close--js">
        <span></span>
        <span></span>
    </div>
</div>

<!-- <div class="m-modal m-modal--session modal-delete">
    <div class="m-modal__content m-modal__animation" id="m-student-delete">
        <div>
            <div class="m-modal__container">
                <picture><img src="<?= \Yii::$app->request->BaseUrl ?>/personalizado/img/profile.png"></picture>
                <p class="m-h3 m-h3-2">Eliminar estudiante</p>
                <p>¿Está seguro que desea eliminar al siguiente estudiante?</p>
                <p class="m-h3-2 m-profile__name"></p>
                <hr><button class="m-button m-button--secondary m-button--full btn-grabar-eliminar-estudiante" type="button"><span>Eliminar</span></button><span
                    class="m-button m-button--transparent-disable m-button--full m-modal__close"><span>Cancelar</span></span>
                <div class="m-modal__close "></div>
            </div>
        </div>
    </div>
</div> -->

<div class="m-modal m-modal--session modal-delete">
    <div class="m-modal__content m-modal__animation" id="m-student-delete">
        <div>
            <div class="m-modal__container">
                <div class="m-modal__picture">
                    <span class="m-icon-svg m-icon-svg--secondary">
                        <svg width="96" height="96" viewBox="0 0 96 96" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M48 96C74.5097 96 96 74.5097 96 48C96 21.4903 74.5097 0 48 0C21.4903 0 0 21.4903 0 48C0 74.5097 21.4903 96 48 96Z" fill="white" />
                            <path opacity="0.8" d="M48 96C74.5097 96 96 74.5097 96 48C96 21.4903 74.5097 0 48 0C21.4903 0 0 21.4903 0 48C0 74.5097 21.4903 96 48 96Z" fill="#67CEDC" />
                            <path d="M38.7995 37.1C36.9995 34.6 36.4995 31.5 36.9995 28.3C37.4995 25.1 39.2995 22.7 42.0995 20.9C43.9995 19.5 46.1995 19 48.4995 19C49.2995 19 50.0995 19 50.7995 19.2C56.9995 20.4 61.2995 26.6 59.8995 33C58.8995 37.7 55.3995 41.2 50.7995 42.1C49.1995 42.5 47.6995 42.5 46.2995 42.1C43.2995 41.6 40.4995 39.9 38.7995 37.1Z" fill="white" fill-opacity="0.75" />
                            <path d="M72 68.4H25V64.4C25 54.8 32.8 47 42.4 47H54.5C64.1 47 71.9 54.8 71.9 64.4V68.4H72Z" fill="white" fill-opacity="0.75" />
                            <path d="M67.7992 77.3001C74.4819 77.3001 79.8992 71.8827 79.8992 65.2001C79.8992 58.5175 74.4819 53.1001 67.7992 53.1001C61.1166 53.1001 55.6992 58.5175 55.6992 65.2001C55.6992 71.8827 61.1166 77.3001 67.7992 77.3001Z" fill="#EE4266" />
                            <line x1="63.5" y1="65.5" x2="72.5" y2="65.5" stroke="white" stroke-width="3" stroke-linecap="round" />
                        </svg>
                    </span>
                </div>
                <p class="m-h3 m-h3-2">Eliminar estudiante</p>
                <p>¿Está seguro que desea eliminar al siguiente estudiante?</p>
                <p class="m-h3-2 m-profile__name">luisf15</p>
                <hr>
                <button class="m-button m-button--secondary m-button--full btn-grabar-eliminar-estudiante" type="button">
                    <span>Eliminar</span>
                </button>
                <span class="m-button m-button--transparent-disable m-button--full m-modal__close--js2">
                    <span>Cancelar</span>
                </span>
                <div class="m-modal__close m-modal__close--js2"></div>
            </div>
        </div>
    </div>
</div>

<div class="m-modal m-modal--share modal-share">
    <div class="m-modal__content m-modal__animation" id="m-student-share">
        <div>
            <div class="m-modal__container">
                <p class="m-h3 m-h3-2">Compartir usuario</p>
                <p>Seleccione cómo desea compartir este usuario:</p>
                <p class="m-h3-2 m-profile__name"></p>
                <hr>
                <div class="m-modal__buttons">
                    <div class="m-modal__buttons__box">
                        <a href="#" target="_blank">
                            <span class="m-icon-svg m-icon-svg--secondary">
                                <svg width="88" height="88" viewBox="0 0 88 88" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M44 88C68.3005 88 88 68.3005 88 44C88 19.6995 68.3005 0 44 0C19.6995 0 0 19.6995 0 44C0 68.3005 19.6995 88 44 88Z" fill="#E9EBED" />
                                    <path d="M43.9996 78.2223C62.9 78.2223 78.2218 62.9005 78.2218 44.0001C78.2218 25.0996 62.9 9.77783 43.9996 9.77783C25.0992 9.77783 9.77734 25.0996 9.77734 44.0001C9.77734 62.9005 25.0992 78.2223 43.9996 78.2223Z" fill="#B4BAC4" />
                                    <path d="M24.4453 35.8792C24.6763 36.9185 24.6763 37.9578 25.0227 38.9972C25.8311 42.2306 27.7943 44.8867 29.7574 47.5427C34.1457 53.4322 39.3423 58.2824 46.5021 60.5921C49.0427 61.4004 51.4678 62.4397 54.2393 62.3243C58.974 62.2088 62.323 59.0908 62.6694 54.5871C62.6694 53.8942 62.7849 53.2013 61.9765 52.7394C59.436 51.4691 56.7799 50.3143 54.2393 49.044C53.3155 48.5821 52.7381 48.9285 52.0452 49.7369C51.2368 50.8917 50.1975 51.931 49.3891 53.0858C48.8117 53.7787 48.2343 53.8942 47.3105 53.5477C41.7674 51.3536 37.6101 47.6582 34.4921 42.5771C34.0302 41.7687 34.1457 41.0758 34.7231 40.3829C35.5315 39.4591 36.3398 38.4198 36.9172 37.3804C37.1482 37.034 37.1482 36.3411 37.0327 35.8792C36.1089 33.4541 35.0695 30.9135 34.0302 28.4884C33.7993 28.0265 33.3373 27.4491 32.8754 27.2181C31.6051 26.5252 28.8336 26.9872 27.7943 28.0265C25.8311 30.2206 24.5608 32.7612 24.4453 35.8792Z" fill="#E9EBED" />
                                </svg>
                            </span>
                        </a>
                        <p class="m-h4">Por whatsapp</p>
                    </div>
                    <div class="m-modal__buttons__box">
                        <button class="m-button-share-email--js" type="button" data-modal="m-student-share">
                            <span class="m-icon-svg m-icon-svg--secondary">
                                <svg width="88" height="88" viewBox="0 0 88 88" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M44 88C68.3005 88 88 68.3005 88 44C88 19.6995 68.3005 0 44 0C19.6995 0 0 19.6995 0 44C0 68.3005 19.6995 88 44 88Z" fill="#E9EBED" />
                                    <path d="M43.9996 78.2223C62.9 78.2223 78.2218 62.9005 78.2218 44.0001C78.2218 25.0996 62.9 9.77783 43.9996 9.77783C25.0992 9.77783 9.77734 25.0996 9.77734 44.0001C9.77734 62.9005 25.0992 78.2223 43.9996 78.2223Z" fill="#B4BAC4" />
                                    <path opacity="0.7" d="M46.6403 46.9786C45.508 47.8593 43.8724 47.8593 42.74 46.9786L25 32.5098V56.6664H64.3803V32.5098L46.6403 46.9786Z" fill="white" />
                                    <path d="M25 31L42.9917 45.7204C43.8724 46.4753 45.3822 46.4753 46.2629 45.7204L64.2545 31H25Z" fill="white" fill-opacity="0.9" />
                                </svg>
                            </span>
                        </button>
                        <p class="m-h4">Por correo</p>
                    </div>
                </div>
                <form>
                    <div class="m-input">
                        <input id="email" type="email" placeholder="Ingresa el correo" name="email">
                    </div>
                    <button class="m-button m-button--secondary m-button--full btn-enviar-correo" type="button"><span>Enviar</span></button>
                </form>
                <div class="m-modal__close"></div>
            </div>
        </div>
    </div>
</div>


<script>
$('body').addClass('m-intranet--student')


var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var docente_seccion_id = "<?= $docente_seccion_id ?>";
var lista_estudiantes;

Estudiantes();
async function Estudiantes(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/estudiante/get-lista-estudiantes-docente',
                method: 'POST',
                data:{_csrf:csrf,docente_seccion_id:docente_seccion_id},
                dataType:'Json',
                beforeSend:function(){
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        
                        ListaEstudiantes(results.estudiantes)
                        lista_estudiantes = results.estudiantes
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}

$('body').on('click', '.btn-ordenar-estados', function (e) {
    //console.log($('.btn-ordenar-estados').is('.sorting_asc'));

    if($('.btn-ordenar-estados').is('.sorting_asc')){
        $('.btn-ordenar-estados').addClass('sorting_desc');
        $('.btn-ordenar-estados').removeClass('sorting_asc');
        $('#lista-estudiantes').DataTable().order([1, 'asc']).draw();
    }else if($('.btn-ordenar-estados').is('.sorting_desc')){
        $('.btn-ordenar-estados').addClass('sorting_asc');
        $('.btn-ordenar-estados').removeClass('sorting_desc');
        $('#lista-estudiantes').DataTable().order([1, 'desc']).draw();
    }else{
        $('.btn-ordenar-estados').addClass('sorting_desc');
        $('#lista-estudiantes').DataTable().order([1, 'asc']).draw();
    }

    
    //$('#lista-estudiantes').DataTable().column(1).order( 'desc' ).draw()
    //$('.perfiles').DataTable().column(0).search($('#perfil-descripcion').val()).draw();
});

$('body').on('keyup', '.btn-buscar-nombres', function (e) {
    console.log($(this).val());
    $('#lista-estudiantes').DataTable().column(2).search($('.btn-buscar-nombres').val()).draw();
    //$('#lista-estudiantes').DataTable().column(1).order( 'desc' ).draw()
    //$('.perfiles').DataTable().column(0).search($('#perfil-descripcion').val()).draw();
});


//agregar

$('body').on('click', '.btn-agregar-estudiante', function (e) {
    e.preventDefault();
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/estudiante/create');
    $('#modal').modal('show');
});


$('body').on('click', '.m-modal__close,.m-modal__close--js2', function (e) {
    e.preventDefault();
    $('.modal-delete').removeClass('js--open');
    $('.modal-share').removeClass('js--open');
});


//eliminar


$('body').on('click', '.btn-eliminar-estudiante', function (e) {
    e.preventDefault();
    var matricula_id = $(this).attr('data-matricula_id');
    var nombres = $(this).attr('data-nombres');
    $( ".btn-grabar-eliminar-estudiante" ).attr( "data-matricula_id", matricula_id );


    $('.modal-delete .m-profile__name').html(nombres);
    $('.modal-delete').addClass('js--open');
});

//compartir

$('body').on('click', '.btn-compartir-estudiante', function (e) {
    e.preventDefault();
    var matricula_id = $(this).attr('data-matricula_id');
    var nombres = $(this).attr('data-nombres');
    var usuario = $(this).attr('data-usuario');
    var clave = $(this).attr('data-clave');
    $('.modal-share .m-profile__name').html(nombres);
    $('.modal-share .btn-enviar-correo').attr('data-usuario',`${usuario}`);
    $('.modal-share .btn-enviar-correo').attr('data-nombres',`${nombres}`);
    $('.modal-share .btn-enviar-correo').attr('data-clave',`${clave}`);

    $('.modal-share a').attr('href',`https://api.whatsapp.com/send?text=Hola ${nombres} estos son tus datos para usar el app de Conecta Ideas. %0AUsuario: ${usuario} %0AContraseña: ${clave}`);
    $('.modal-share').addClass('js--open');
});

$('body').on('click', '.btn-enviar-correo', function (e) {
    e.preventDefault();
    var correo = $('#email').val();
    var nombres = $(this).attr('data-nombres');
    var usuario = $(this).attr('data-usuario');
    var clave = $(this).attr('data-clave')
    
    //window.location = `mailto:${correo}?body=Hola ${nombres}, tu usuario para acceder a la plataforma es ${usuario}.`;

    $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/estudiante/enviar-correo',
        method: 'POST',
        data:{_csrf:csrf,correo:correo,nombres:nombres,usuario:usuario,clave:clave},
        dataType:'Json',
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success:function(results)
        {   
            if(results && results.success){
                $('.modal-share').removeClass('js--open');
            }
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });
});


$('body').on('click', '.btn-grabar-eliminar-estudiante', function (e) {
    e.preventDefault();
    var matricula_id = $(this).attr('data-matricula_id');
    $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/estudiante/eliminar-estudiante',
        method: 'POST',
        data:{_csrf:csrf,matricula_id:matricula_id},
        dataType:'Json',
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success:function(results)
        {   
            if(results && results.success){
                //$('#modal').modal('toggle');
                $('.modal-delete').removeClass('js--open');
                Estudiantes();
            }
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });
});
//grabar

$('body').on('click', '.btn-grabar-estudiante', function (e) {

    e.preventDefault();
    //$('#modal').modal('toggle');
    var error = "";
    var nombres = $("[name=\"Estudiante[nombres]\"]").val();
    var apellido_paterno_letra = $("[name=\"Estudiante[apellido_paterno_letra]\"]").val();
    var filter = /^[a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ\s]+$/;

    var nombres_ = $.trim(nombres);

    if(!nombres){
        $('.error_nombres').addClass('alert-danger');
        $('.error_nombres').css('border', '1px solid #DA1414');
        $('.error_nombres_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Campo obligatorio</div>`);
        $("#nombres").focus();
        error = error + "error 1";
    }else if(nombres.length != nombres_.length){
        $('.error_nombres').addClass('alert-danger');
        $('.error_nombres').css('border', '1px solid #DA1414');
        $('.error_nombres_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Solo  se puede ingresar letras</div>`);
        $("#nombres").focus();
        error = error + "error 1";
    }else if(!filter.test(nombres)){
        $('.error_nombres').addClass('alert-danger');
        $('.error_nombres').css('border', '1px solid #DA1414');
        $('.error_nombres_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Solo  se puede ingresar letras</div>`);
        $("#nombres").focus();
        error = error + "error 1";
    } else {
        $('.error_nombres').removeClass('alert-danger');
        $('.error_nombres_text').html(``);
        $('.error_nombres').css('border', '');
    }

    if(!apellido_paterno_letra){
        $('.error_apellido_paterno_letra').addClass('alert-danger');
        $('.error_apellido_paterno_letra').css('border', '1px solid #DA1414');
        $('.error_apellido_paterno_letra_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Campo obligatorio</div>`);
        $("#apellido_paterno_letra").focus();
        error = error + "error 1";
    }else if(apellido_paterno_letra.includes(' ')){
        $('.error_apellido_paterno_letra').addClass('alert-danger');
        $('.error_apellido_paterno_letra').css('border', '1px solid #DA1414');
        $('.error_apellido_paterno_letra_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Solo  se puede ingresar letras</div>`);
        $("#apellido_paterno_letra").focus();
        error = error + "error 1";
    }else if(!filter.test(apellido_paterno_letra)){
        $('.error_apellido_paterno_letra').addClass('alert-danger');
        $('.error_apellido_paterno_letra').css('border', '1px solid #DA1414');
        $('.error_apellido_paterno_letra_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Solo  se puede ingresar letras</div>`);
        $("#apellido_paterno_letra").focus();
        error = error + "error 1";
    } else {
        $('.error_apellido_paterno_letra').removeClass('alert-danger');
        $('.error_apellido_paterno_letra_text').html(``);
        $('.error_apellido_paterno_letra').css('border', '');
    }
    
    
    // if(){
    //     return false;
    // }

    if (error != "") {
        console.log(error);
        return false;
    }


    var form = $('#formEstudiante');
    var formData = $('#formEstudiante').serializeArray();
    if (form.find('.has-error').length) {
        return false;
    }
    
    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success: function (results) {
            if(results.success){
                //toastr.success('Revisa su estado en la Lista de estudiantes', 'Estudiante agregado')
                $('#nombres').val('');
                $('#apellido_paterno_letra').val('');
                $('#m-alert-succesx').addClass('js--active');

                setTimeout(function(){ $('#m-alert-succesx').removeClass('js--active'); }, 2000);

                Estudiantes();
            }
        },
    });
});

$("#search").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#lista-estudiantes tr").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
});


$("#order").on("change", function() {
    
    if($(this).val()=='Alfabeticamente'){
        sortResults('nombres', true);
    }else if($(this).val()=='Habilitados'){
        sortResults('estado_registro', true);
    }else if($(this).val()=='En espera'){
        sortResults('estado_registro', false);
    }

});

function sortResults(prop, asc) {
    lista_estudiantes.sort(function(a, b) {
        if (asc) {
            return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
        } else {
            return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
        }
    });
    ListaEstudiantes(lista_estudiantes)
}


function ListaEstudiantes(list_estudiantes){

    var estudiantes ="";
    var cantidad = 0 ;
    $.each(list_estudiantes, function( index, value ) {
        nomb = (value.nombres)?value.nombres:"";
        appat = (value.apellido_paterno_letra)?value.apellido_paterno_letra:"";
        nombres_completos = `${nomb} ${appat}`;
        usuario = value.usuario;
        clave = value.clave;
        matricula_id = value.matricula_id;
        estado_registro = value.estado_registro;
        estado_descripcion = '';
        
        switch (estado_registro) {
            case '1' : estado_descripcion='Habilitado'; break;
            case '2' : estado_descripcion='En espera'; break;
            default: estado_descripcion='';
        }

        if(value.estado_registro=="1"){
            estudiantes = estudiantes + `
                                        <tr class="m-student-panel__card m-student-panel__card--active">
                                            <td class="m-student-panel__card__name">${nombres_completos}</td>
                                            <td>
                                                <span class="pts">${estado_descripcion}</span>
                                                <div class="m-student-panel__card__status">
                                                    <span class="m-icon-svg m-icon-svg--secondary">
                                                        <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M7 0C3.11057 0 0 3.11057 0 7C0 10.8894 3.11057 14 7 14C10.8894 14 14 10.8894 14 7C14 3.11057 10.8845 0 7 0ZM10.6543 5.21204L6.37789 9.87544C6.22113 10.0322 5.9909 10.1106 5.83415 10.1106C5.6774 10.1106 5.44717 10.0322 5.29041 9.87544L3.3457 7.77397C3.03709 7.46536 3.11057 6.9951 3.42407 6.68649C3.73268 6.37789 4.20294 6.45136 4.51155 6.76487L5.83415 8.24423L9.56683 4.20294C9.87544 3.89433 10.3457 3.89433 10.6543 4.12456C10.9629 4.35479 10.9629 4.89853 10.6543 5.21204Z" fill="#4F9C2E" />
                                                        </svg> 
                                                    </span>
                                                    <span>${estado_descripcion}</span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="m-student-panel__card__body">
                                                    <p>Usuario: ${usuario}</p>
                                                    <p>Contraseña: ${clave}</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="m-student-panel__card__footer">
                                                    <button data-matricula_id="${matricula_id}" data-nombres="${nombres_completos}" class="m-button m-button--transparent-disable m-button--delete m-modal__button btn-eliminar-estudiante" data-modal="m-student-delete">
                                                        <span class="m-icon-svg m-icon-svg--secondary">
                                                            <svg width="9" height="11" viewBox="0 0 9 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                <path d="M8.33837 2.09759H5.97183C5.78694 1.21681 4.89949 0.659586 4.01203 0.839336C3.34644 0.965162 2.84725 1.46846 2.71783 2.09759H0.332795C0.147909 2.09759 0 2.24139 0 2.42114C0 2.60089 0.147909 2.74469 0.332795 2.74469H0.66559V9.25166C0.66559 9.95268 1.25723 10.5279 1.97828 10.5279H6.67439C7.39545 10.5279 7.98709 9.95268 7.98709 9.25166V2.74469H8.31988C8.50477 2.74469 8.65268 2.60089 8.65268 2.42114C8.65268 2.24139 8.52326 2.09759 8.33837 2.09759ZM4.34483 1.45049C4.77006 1.45049 5.13984 1.72011 5.28775 2.09759H3.40191C3.53133 1.72011 3.91959 1.45049 4.34483 1.45049ZM7.33998 9.25166C7.33998 9.61116 7.04417 9.88078 6.69288 9.88078H1.99677C1.627 9.88078 1.34967 9.59318 1.34967 9.25166V2.74469H7.35847V9.25166H7.33998Z" fill="#0A0B29" fill-opacity="0.5" />
                                                                <path d="M3.34647 4.38037C3.16158 4.38037 3.01367 4.52417 3.01367 4.70392V7.95741C3.01367 8.13716 3.16158 8.28096 3.34647 8.28096C3.53135 8.28096 3.67926 8.13716 3.67926 7.95741V4.70392C3.67926 4.52417 3.51286 4.38037 3.34647 4.38037Z" fill="#0A0B29" fill-opacity="0.5" />
                                                                <path d="M5.34256 4.38037C5.15767 4.38037 5.00977 4.52417 5.00977 4.70392V7.95741C5.00977 8.13716 5.15767 8.28096 5.34256 8.28096C5.52745 8.28096 5.67536 8.13716 5.67536 7.95741V4.70392C5.67536 4.52417 5.52745 4.38037 5.34256 4.38037Z" fill="#0A0B29" fill-opacity="0.5" />
                                                            </svg>
                                                        </span>
                                                        <span>Eliminar</span>
                                                    </button>
                                                    <button data-matricula_id="${matricula_id}" data-nombres="${nombres_completos}" data-usuario="${usuario}" data-clave="${clave}" class="m-button m-button--transparent-disable m-button--share m-modal__button btn-compartir-estudiante" data-modal="m-student-share">
                                                        <span class="m-icon-svg m-icon-svg--secondary">
                                                            <svg width="11" height="11" viewBox="0 0 11 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                <path d="M8.0044 0.833496C6.9079 0.833496 6.00878 1.70764 6.00878 2.77368C6.00878 2.81632 6.00878 2.83764 6.00878 2.88028L3.42106 4.33008C3.04825 3.96763 2.54386 3.75443 1.99562 3.75443C0.899124 3.75443 0 4.62857 0 5.69461C0 6.76064 0.899124 7.65611 1.99562 7.65611C2.54386 7.65611 3.04825 7.42158 3.42106 7.08045L6.03071 8.50893C6.03071 8.55158 6.03071 8.5729 6.03071 8.61554C6.03071 9.68157 6.92983 10.5557 8.02633 10.5557C9.12282 10.5557 10.0219 9.68157 10.0219 8.61554C10.0219 7.5495 9.12282 6.67536 8.02633 6.67536C7.32457 6.67536 6.71053 7.03781 6.33773 7.57083L3.92544 6.22762C3.9693 6.05706 4.01316 5.88649 4.01316 5.71593C4.01316 5.54536 3.9693 5.3748 3.92544 5.20423L6.33773 3.86103C6.6886 4.39405 7.32457 4.7565 8.02633 4.7565C9.12282 4.7565 10.0219 3.88235 10.0219 2.81632C10.0219 1.75028 9.12282 0.833496 8.0044 0.833496ZM8.0044 1.81425C8.55264 1.81425 9.01317 2.24066 9.01317 2.795C9.01317 3.34933 8.57457 3.77575 8.0044 3.77575C7.45615 3.77575 6.99562 3.34933 6.99562 2.795C6.99562 2.24066 7.45615 1.81425 8.0044 1.81425ZM1.99562 4.73518C2.54386 4.73518 3.00439 5.16159 3.00439 5.71593C3.00439 6.27026 2.56579 6.69668 1.99562 6.69668C1.42544 6.69668 0.986843 6.27026 0.986843 5.71593C0.986843 5.16159 1.44737 4.73518 1.99562 4.73518ZM8.0044 7.65611C8.55264 7.65611 9.01317 8.08252 9.01317 8.63686C9.01317 9.16987 8.57457 9.61761 8.0044 9.61761C7.45615 9.61761 6.99562 9.1912 6.99562 8.63686C7.01755 8.08252 7.45615 7.65611 8.0044 7.65611Z" fill="#0A0B29" />
                                                            </svg>
                                                        </span>
                                                        <span>Compartir</span>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>`;
        }

        if(value.estado_registro=="2"){
            estudiantes = estudiantes + `
                                        <tr class="m-student-panel__card">
                                            <td class="m-student-panel__card__name">${nombres_completos}</td>
                                            <td><span>En espera</span>
                                                <div class="m-student-panel__card__status">
                                                    <span class="m-icon-svg m-icon-svg--secondary">
                                                        <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M7 0C10.8527 0 14 3.14729 14 7C14 10.907 10.8527 14.0543 6.94574 14C3.14729 14 0 10.8527 0 7C0 3.14729 3.14729 0 7 0ZM7.97674 5.58915C7.97674 4.99225 7.97674 4.39535 7.97674 3.79845C7.97674 3.20155 7.5969 2.82171 7.05426 2.82171C6.51163 2.82171 6.13178 3.20155 6.13178 3.79845C6.13178 4.99225 6.13178 6.24031 6.13178 7.43411C6.13178 7.97674 6.56589 8.41085 7.10853 8.41085C7.65116 8.41085 8.03101 7.97674 8.03101 7.37985C7.97674 6.78295 7.97674 6.18605 7.97674 5.58915ZM7.97674 10.5271C7.97674 9.9845 7.65116 9.65892 7.10853 9.60465C6.62016 9.60465 6.18605 9.93023 6.18605 10.4186C6.18605 10.9612 6.51163 11.3953 7 11.3953C7.5969 11.4496 7.92248 11.124 7.97674 10.5271Z" fill="#EE4266" />
                                                        </svg>
                                                    </span>
                                                    <span>${estado_descripcion}</span>
                                                </div>
                                            </td>
                                        </tr>`;
        }
        cantidad++;
    });
    $('.cantidad_estudiantes').html(`Agregados (${cantidad})`);
    $('#lista-estudiantes tbody').html(estudiantes);
}

// $('.texto').keypress(function(tecla) {
//     var reg = /^[a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ'_\s]+$/;
//     if(!reg.test(String.fromCharCode(tecla.which))){
//         return false;
//     }
//     return true;
// });


// $('.texto').bind('copy paste cut',function(e) {
//   e.preventDefault();
// });


$('.texto').keypress(function(tecla) {
    var reg = /^[a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ\s]+$/;
    if(!reg.test(String.fromCharCode(tecla.which))){
        return false;
    }
    return true;
});
</script>