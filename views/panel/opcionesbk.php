<div class="row" style="height: 220px;background: #0A0B29;">
    <div class="col-2"></div>
    <div class="col-4 text-white text-left">
        <h3 class="text-white">¡Bienvenido!</h3>
        <small> <?= ($grado==9)?'Unidocente':$grado ?> <?= $seccion ?></small>
    </div>
    <div class="col-4 text-right text-white">
        <a href="<?= \Yii::$app->request->BaseUrl ?>/perfil" class="text-center">
            <img src="<?= \Yii::$app->request->BaseUrl ?>/images/perfil.png" width="50" alt="" > <br>
            <?= Yii::$app->user->identity->nombres ?>
        </a>
    </div>
</div>

<div class="row pt-3">
    <div class="col-md-2"></div>
    <div class="col-md-4">
        <div class="card mini-stats-wid">
            <a href="<?= \Yii::$app->request->BaseUrl ?>/estudiante/lista?docente_seccion_id=<?= $docente_seccion_id ?>&grado=<?= $grado ?>&seccion=<?= $seccion ?>">
                <div class="card-body">
                    <div class="media">
                        <div class="media-body">
                            <p class="text-muted font-weight-medium">Mis estudiantes</p>
                            <h4 class="mb-0">Ver o Agregar</h4>
                        </div>

                        <div class="mini-stat-icon avatar-sm rounded-circle bg-primary align-self-center">
                            <span class="avatar-title">
                                <i class="mdi mdi-arrow-right font-size-24"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <div class="card mini-stats-wid">
            <div class="card-body">
                <div class="media">
                    <div class="media-body">
                        <p class="text-muted font-weight-medium">Mis Recursos</p>
                        <h4 class="mb-0">Ver mis recursos</h4>
                    </div>

                    <div class="mini-stat-icon avatar-sm rounded-circle bg-primary align-self-center">
                        <span class="avatar-title">
                            <i class="mdi mdi-arrow-right font-size-24"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="card mini-stats-wid">
            <div class="card-body">
                <div class="media">
                    <div class="media-body">
                        <p class="text-muted font-weight-medium">Mis reportes</p>
                        <h4 class="mb-0">Revisar reportes</h4>
                    </div>

                    <div class="mini-stat-icon avatar-sm rounded-circle bg-primary align-self-center">
                        <span class="avatar-title">
                            <i class="mdi mdi-arrow-right font-size-24"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>
