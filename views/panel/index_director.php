<div class="m-banner m-banner--profile m-banner--intranet">
    <div class="m-banner__bg"></div>
    <div class="m-container">
        <div class="m-banner__content">
            <p class="m-banner__title m-h1">Bienvenido/a</p>
            <p class="m-banner__subtitle"><?= $prefijo_perfil ?> <?= Yii::$app->user->identity->nombres ?></p>
        </div>
        <div class="m-profile">
        <div class="m-profile__picture m-lazy-js" data-bg="<?= \Yii::$app->request->BaseUrl ?>/fotos/<?= Yii::$app->user->identity->foto ?>"></div>
            <a class="m-h3" href="<?= \Yii::$app->request->BaseUrl ?>/perfil">Ver perfil</a>
        </div>
    </div>
</div>
<div class="m-container m-section">
    <p class="m-intranet__subtitle">¿Qué quieres hacer?</p>
    <div class="m-intranet__panel">
        <a class="m-intranet__panel__card m-intranet__panel__card--red " href="<?= \Yii::$app->request->BaseUrl ?>/recurso">
            <div class="m-intranet__panel__card__img">
                <picture><img src="<?= \Yii::$app->request->BaseUrl ?>/personalizado/img/card_resource.png"></picture>
            </div>
            <div class="m-intranet__panel__card__content">
                <p class="m-h3">Ver mis recursos</p>
                <p>Disponibles</p>
            </div>
            <div class="m-intranet__panel__card__icon"><span class="m-icon-svg m-icon-svg--secondary"><svg width="6"
                        height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M3.44833 5.72832L0.195 8.98166C-0.065 9.24166 -0.065 9.66832 0.195 9.92832C0.455 10.1883 0.875 10.1883 1.135 9.92832L5.52833 5.53499C5.78833 5.27499 5.78833 4.85499 5.52833 4.59499L1.14167 0.19499C1.01711 0.0701553 0.848012 0 0.671667 0C0.495322 0 0.326221 0.0701553 0.201667 0.19499C-0.0583333 0.45499 -0.0583333 0.87499 0.201667 1.13499L3.44833 4.39499C3.52917 4.56165 3.73496 4.69498 3.73496 5.06165C3.73496 5.42831 3.52917 5.56165 3.44833 5.72832Z"
                            fill="white" /></svg></span></div>
        </a>
        <!-- <a class="m-intranet__panel__card m-intranet__panel__card--yellow" href="#">
            <div class="m-intranet__panel__card__img">
                <picture><img src="<?= \Yii::$app->request->BaseUrl ?>/personalizado/img/card_report.png"></picture>
            </div>
            <div class="m-intranet__panel__card__content">
                <p class="m-h3">Mis Reportes</p>
                <p>Revisar reportes</p>
            </div>
            <div class="m-intranet__panel__card__icon"><span class="m-icon-svg m-icon-svg--secondary"><svg width="6"
                        height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M3.44833 5.72832L0.195 8.98166C-0.065 9.24166 -0.065 9.66832 0.195 9.92832C0.455 10.1883 0.875 10.1883 1.135 9.92832L5.52833 5.53499C5.78833 5.27499 5.78833 4.85499 5.52833 4.59499L1.14167 0.19499C1.01711 0.0701553 0.848012 0 0.671667 0C0.495322 0 0.326221 0.0701553 0.201667 0.19499C-0.0583333 0.45499 -0.0583333 0.87499 0.201667 1.13499L3.44833 4.39499C3.52917 4.56165 3.73496 4.69498 3.73496 5.06165C3.73496 5.42831 3.52917 5.56165 3.44833 5.72832Z"
                            fill="white" /></svg></span></div>
        </a> -->
        
        </div>
</div>

<!-- 
<div class="row" style="height: 220px;background: #0A0B29;">
    <div class="col-2"></div>
    <div class="col-4 text-white text-left">
        <h3 class="text-white">¡Bienvenido!</h3>
        <small></small>
    </div>
    <div class="col-4 text-right text-white">
        <a href="<?= \Yii::$app->request->BaseUrl ?>/perfil" class="text-center">
            <img src="<?= \Yii::$app->request->BaseUrl ?>/images/perfil.png" width="50" alt="" > <br>
            <?= Yii::$app->user->identity->nombres ?>
        </a>
    </div>
</div>

<div class="row pt-3">
    <div class="col-md-2"></div>
    <div class="col-md-4">
        

        <div class="card mini-stats-wid">
            <div class="card-body">
                <div class="media">
                    <div class="media-body">
                        <p class="text-muted font-weight-medium">Mis Recursos</p>
                        <h4 class="mb-0">Ver mis recursos</h4>
                    </div>

                    <div class="mini-stat-icon avatar-sm rounded-circle bg-primary align-self-center">
                        <span class="avatar-title">
                            <i class="mdi mdi-arrow-right font-size-24"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="card mini-stats-wid">
            <div class="card-body">
                <div class="media">
                    <div class="media-body">
                        <p class="text-muted font-weight-medium">Mis reportes</p>
                        <h4 class="mb-0">Revisar reportes</h4>
                    </div>

                    <div class="mini-stat-icon avatar-sm rounded-circle bg-primary align-self-center">
                        <span class="avatar-title">
                            <i class="mdi mdi-arrow-right font-size-24"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div> -->
