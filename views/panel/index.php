
<div class="m-banner m-banner--profile m-banner--intranet">
    <div class="m-banner__bg"></div>
    <div class="m-container">
        <div class="m-banner__content">
            <p class="m-banner__title m-h1">Bienvenido/a</p>
            <p class="m-banner__subtitle"><?= $prefijo_perfil ?> <?= Yii::$app->user->identity->nombres ?></p>
        </div>
        <div class="m-profile">
            <div class="m-profile__picture m-lazy-js" data-bg="<?= \Yii::$app->request->BaseUrl ?>/fotos/<?= Yii::$app->user->identity->foto ?>"></div>
            <a class="m-h3" href="<?= \Yii::$app->request->BaseUrl ?>/perfil">Ver perfil</a>
        </div>
    </div>
</div>
<div class="m-container m-section m-section--medium">
    <p class="m-intranet__subtitle">Selecciona un aula:</p>
    <div class="m-intranet__card__grid contenido_grados">
        
    </div>
</div>


<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
async function Secciones(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/panel/get-lista-grados-secciones',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(xhr, settings)
                {
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var cardGradoSeccion = '';

                        $.each(results.gradosSecciones, function( index, value ) {
                            grad = value.grado;
                            grado = value.nombre_corto;
                            seccion = value.seccion_descripcion;
                            cantidad = value.cantidad_estudiante;
                            id = value.id;
                            picture = ``;
                            if( cantidad == 0 ){
                                picture = ` <picture class="m-lazy__img">
                                                <source srcset="<?= \Yii::$app->request->BaseUrl ?>/personalizado/img/card_img_disable.png  1x, <?= \Yii::$app->request->BaseUrl ?>/personalizado/img/card_img_disable@2x.png  2x" media="(min-width: 768px)" type="image/png">
                                                <source srcset="<?= \Yii::$app->request->BaseUrl ?>/personalizado/img/card_img_disable_mobile.png  1x, <?= \Yii::$app->request->BaseUrl ?>/personalizado/img/card_img_disable_mobile@2x.png  2x" type="image/png">
                                                <img  src="<?= \Yii::$app->request->BaseUrl ?>/personalizado/img/card_img_disable_mobile.png">
                                            </picture>`;
                            }else{
                                picture = ` <picture class="m-lazy__img">
                                                <source srcset="<?= \Yii::$app->request->BaseUrl ?>/personalizado/img/card_img.png  1x, <?= \Yii::$app->request->BaseUrl ?>/personalizado/img/card_img@2x.png  2x" media="(min-width: 768px)" type="image/png">
                                                <source srcset="<?= \Yii::$app->request->BaseUrl ?>/personalizado/img/card_img_mobile.png  1x, <?= \Yii::$app->request->BaseUrl ?>/personalizado/img/card_img_mobile@2x.png  2x" type="image/png">
                                                <img  src="<?= \Yii::$app->request->BaseUrl ?>/personalizado/img/card_img_mobile.png">
                                            </picture>`;
                            }

                            cardGradoSeccion = cardGradoSeccion + ` <div class="m-intranet__card__box">
                                                                        <a class="m-intranet__card" href="<?= \Yii::$app->request->BaseUrl ?>/panel/opciones?docente_seccion_id=${id}&grado=${grad}&seccion=${seccion}">
                                                                            ${picture}
                                                                            <div class="m-intranet__card__content">
                                                                                <p class="m-h2">${grado} ${seccion}</p>
                                                                                <div class="m-intranet__card__registered">
                                                                                    <div class="m-intranet__card__registered__icon">
                                                                                        <span class="m-icon-svg m-icon-svg--secondary">
                                                                                            <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                                                <path d="M7.51758 0.612793C3.60437 0.612793 0.392578 3.74999 0.392578 7.57233C0.392578 11.4307 3.60437 14.5319 7.51758 14.5319C11.4308 14.5319 14.6426 11.3947 14.6426 7.57233C14.6426 3.74999 11.4677 0.612793 7.51758 0.612793ZM6.44698 10.4932H8.58817C9.80644 10.4932 10.877 11.2144 11.357 12.2962C10.2864 13.1255 8.92043 13.5943 7.55449 13.5943C6.15165 13.5943 4.82263 13.1255 3.71512 12.2962C4.15812 11.2144 5.22872 10.4932 6.44698 10.4932ZM9.5111 6.81507C9.5111 7.89686 8.62509 8.7623 7.51758 8.7623C6.41006 8.7623 5.52405 7.89686 5.52405 6.81507C5.52405 5.73328 6.41006 4.86784 7.51758 4.86784C8.62509 4.86784 9.5111 5.73328 9.5111 6.81507ZM8.58817 9.55561H8.36667C9.58494 9.19502 10.4709 8.11322 10.4709 6.77901C10.4709 5.19238 9.14193 3.89423 7.51758 3.89423C5.89323 3.89423 4.56421 5.19238 4.56421 6.77901C4.56421 8.07716 5.45022 9.19502 6.66848 9.55561H6.44698C4.9703 9.55561 3.64128 10.3489 2.97677 11.6471C1.9431 10.5292 1.35242 9.08684 1.35242 7.57233C1.35242 4.25483 4.1212 1.55035 7.51758 1.55035C10.914 1.55035 13.6827 4.25483 13.6827 7.57233C13.6827 9.08684 13.0921 10.5292 12.0584 11.6471C11.3939 10.385 10.0649 9.55561 8.58817 9.55561Z" fill="#0A0B29" />
                                                                                            </svg>
                                                                                        </span>
                                                                                    </div>
                                                                                    <span>${cantidad}</span>
                                                                                    <p>estudiantes inscritos</p>
                                                                                </div>
                                                                                <div class="m-button m-button--secondary m-button--icon">
                                                                                    <span>Ingresar</span>
                                                                                    <span class="m-icon-svg m-icon-svg--secondary">
                                                                                        <svg width="6" height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                                            <path d="M3.44833 5.72832L0.195 8.98166C-0.065 9.24166 -0.065 9.66832 0.195 9.92832C0.455 10.1883 0.875 10.1883 1.135 9.92832L5.52833 5.53499C5.78833 5.27499 5.78833 4.85499 5.52833 4.59499L1.14167 0.19499C1.01711 0.0701553 0.848012 0 0.671667 0C0.495322 0 0.326221 0.0701553 0.201667 0.19499C-0.0583333 0.45499 -0.0583333 0.87499 0.201667 1.13499L3.44833 4.39499C3.52917 4.56165 3.73496 4.69498 3.73496 5.06165C3.73496 5.42831 3.52917 5.56165 3.44833 5.72832Z" fill="white" />
                                                                                        </svg>
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </a>
                                                                    </div>
                                                                    `;
                        });
                        $('.contenido_grados').html(cardGradoSeccion);
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}

Secciones();
</script>