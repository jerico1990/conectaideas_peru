<div class="row" style="height: 220px;background: #0A0B29;">
    <div class="col-2"></div>
    <div class="col-4 text-white text-left">
        <h3 class="text-white">¡Bienvenido!</h3>
        <small>Mira aquí tus aulas registradas:</small>
    </div>
    <div class="col-4 text-right text-white">
        <a href="<?= \Yii::$app->request->BaseUrl ?>/perfil" class="text-center"> 
            <img src="<?= \Yii::$app->request->BaseUrl ?>/fotos/<?= Yii::$app->user->identity->foto ?>" width="50" alt="" > <br>
            <?= Yii::$app->user->identity->nombres ?>
        </a>
    </div>
</div>

<div class="row pt-3">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="row">
            <div class="contenido_grados"></div>
            <!-- <div class="col-4">
                <div class="card">
                    <img class="card-img-top img-fluid" src="assets/images/small/img-1.jpg" alt="Card image cap">
                    <div class="card-body">
                        <h4 class="card-title mt-0">Card title</h4>
                        <p class="card-text">Some quick example text to build on the card title and make
                            up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-primary waves-effect waves-light">Button</a>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card">
                    <img class="card-img-top img-fluid" src="assets/images/small/img-1.jpg" alt="Card image cap">
                    <div class="card-body">
                        <h4 class="card-title mt-0">Card title</h4>
                        <p class="card-text">Some quick example text to build on the card title and make
                            up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-primary waves-effect waves-light">Button</a>
                    </div>
                </div>
            </div>

            <div class="col-4">
                <div class="card">
                    <img class="card-img-top img-fluid" src="assets/images/small/img-1.jpg" alt="Card image cap">
                    <div class="card-body">
                        <h4 class="card-title mt-0">Card title</h4>
                        <p class="card-text">Some quick example text to build on the card title and make
                            up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-primary waves-effect waves-light">Button</a>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
    
</div>

<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
async function Secciones(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/panel/get-lista-grados-secciones',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(xhr, settings)
                {
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var cardGradoSeccion = '';

                        $.each(results.gradosSecciones, function( index, value ) {
                            grad = value.grado;
                            grado = value.grado_descripcion;
                            seccion = value.seccion_descripcion;
                            cantidad = value.cantidad_estudiante;
                            id = value.id;
                            cardGradoSeccion = cardGradoSeccion + ` <div class="col-xl-4">
                                                                        <div class="card">
                                                                            <img class="card-img-top img-fluid" src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/images/small/img-1.jpg" alt="Card image cap">
                                                                            <div class="card-body">
                                                                                <h4 class="card-title mt-0">${grado} ${seccion}</h4>
                                                                                <p class="card-text">${cantidad} estudiantes inscritos</p>
                                                                                <a href="<?= \Yii::$app->request->BaseUrl ?>/panel/opciones?docente_seccion_id=${id}&grado=${grad}&seccion=${seccion}" class="btn btn-primary waves-effect waves-light">Ingresar</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>`;
                        });
                        $('.contenido_grados').html(cardGradoSeccion);
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}

Secciones();
</script>