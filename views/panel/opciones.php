<div class="m-banner m-banner--profile m-banner--intranet">
    <div class="m-banner__bg"></div>
    <div class="m-container">
        <div class="m-breadcrumbs">
            <ul>
                <li><a href="<?= \Yii::$app->request->BaseUrl ?>/panel">Inicio</a></li>
                <li class="css--active"><a href="#"><?= ($grado->grado==9)?'Unidocente':$grado->nombre_corto ?> <?= $seccion ?></a></li>
            </ul>
        </div>
        <div class="m-banner__content">
            <p class="m-banner__title m-h1"><?= ($grado->grado==9)?'Unidocente':$grado->nombre_corto ?> <?= $seccion ?></p>
        </div>
    </div>
</div>
<div class="m-container m-section m-section--medium">
    <p class="m-intranet__subtitle">¿Qué quieres hacer?</p>
    <div class="m-intranet__panel">
        <a class="m-intranet__panel__card" href="<?= \Yii::$app->request->BaseUrl ?>/estudiante/lista?docente_seccion_id=<?= $docente_seccion_id ?>&grado=<?= $grado->grado ?>&seccion=<?= $seccion ?>">
            <div class="m-intranet__panel__card__img">
                <picture><img src="<?= \Yii::$app->request->BaseUrl ?>/personalizado/img/card_student.png"></picture>
            </div>
            <div class="m-intranet__panel__card__content">
                <p class="m-h3">Ver o agregar</p>
                <p>Estudiantes</p>
            </div>
            <div class="m-intranet__panel__card__icon">
                <span class="m-icon-svg m-icon-svg--secondary">
                    <svg width="6" height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M3.44833 5.72832L0.195 8.98166C-0.065 9.24166 -0.065 9.66832 0.195 9.92832C0.455 10.1883 0.875 10.1883 1.135 9.92832L5.52833 5.53499C5.78833 5.27499 5.78833 4.85499 5.52833 4.59499L1.14167 0.19499C1.01711 0.0701553 0.848012 0 0.671667 0C0.495322 0 0.326221 0.0701553 0.201667 0.19499C-0.0583333 0.45499 -0.0583333 0.87499 0.201667 1.13499L3.44833 4.39499C3.52917 4.56165 3.73496 4.69498 3.73496 5.06165C3.73496 5.42831 3.52917 5.56165 3.44833 5.72832Z" fill="white" />
                    </svg>
                </span>
            </div>
        </a>
        <a class="m-intranet__panel__card m-intranet__panel__card--red " href="<?= \Yii::$app->request->BaseUrl ?>/actividad?docente_seccion_id=<?= $docente_seccion_id ?>&grado=<?= $grado->grado ?>&seccion=<?= $seccion ?>">
            <div class="m-intranet__panel__card__img">
                <picture><img src="<?= \Yii::$app->request->BaseUrl ?>/personalizado/img/card_resource.png"></picture>
            </div>
            <div class="m-intranet__panel__card__content">
                <p class="m-h3">Ver mis recursos</p>
                <p>Disponibles</p>
            </div>
            <div class="m-intranet__panel__card__icon">
                <span class="m-icon-svg m-icon-svg--secondary">
                    <svg width="6" height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M3.44833 5.72832L0.195 8.98166C-0.065 9.24166 -0.065 9.66832 0.195 9.92832C0.455 10.1883 0.875 10.1883 1.135 9.92832L5.52833 5.53499C5.78833 5.27499 5.78833 4.85499 5.52833 4.59499L1.14167 0.19499C1.01711 0.0701553 0.848012 0 0.671667 0C0.495322 0 0.326221 0.0701553 0.201667 0.19499C-0.0583333 0.45499 -0.0583333 0.87499 0.201667 1.13499L3.44833 4.39499C3.52917 4.56165 3.73496 4.69498 3.73496 5.06165C3.73496 5.42831 3.52917 5.56165 3.44833 5.72832Z" fill="white" />
                    </svg>
                </span>
            </div>
        </a>
        <a class="m-intranet__panel__card m-intranet__panel__card--yellow " href="<?= \Yii::$app->request->BaseUrl ?>/reporte?docente_seccion_id=<?= $docente_seccion_id ?>&grado=<?= $grado->grado ?>&seccion=<?= $seccion ?>">
            <div class="m-intranet__panel__card__img">
                <picture><img src="<?= \Yii::$app->request->BaseUrl ?>/personalizado/img/card_report.png"></picture>
            </div>
            <div class="m-intranet__panel__card__content">
                <p class="m-h3">Ver mis reportes</p>
                <p>Disponibles</p>
            </div>
            <div class="m-intranet__panel__card__icon">
                <span class="m-icon-svg m-icon-svg--secondary">
                    <svg width="6" height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M3.44833 5.72832L0.195 8.98166C-0.065 9.24166 -0.065 9.66832 0.195 9.92832C0.455 10.1883 0.875 10.1883 1.135 9.92832L5.52833 5.53499C5.78833 5.27499 5.78833 4.85499 5.52833 4.59499L1.14167 0.19499C1.01711 0.0701553 0.848012 0 0.671667 0C0.495322 0 0.326221 0.0701553 0.201667 0.19499C-0.0583333 0.45499 -0.0583333 0.87499 0.201667 1.13499L3.44833 4.39499C3.52917 4.56165 3.73496 4.69498 3.73496 5.06165C3.73496 5.42831 3.52917 5.56165 3.44833 5.72832Z" fill="white" />
                    </svg>
                </span>
            </div>
        </a>
    </div>
</div>
