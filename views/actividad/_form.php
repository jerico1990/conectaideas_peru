<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['options' => ['id' => 'formActividad']]); ?>

    <div class="modal-header">
        <h4 class="modal-title"><?= $model->titulo ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="row">
            <!-- <div class="col-md-12">
                <div class="form-group">
                    <label >Grado</label>
                    <select name="ActividadCabecera[grado_id]" id="grado_id" class="form-control">
                        <option value>Seleccionar grado</option>
                    </select>
                </div>
            </div> -->
            <div class="col-md-12">
                <div class="form-group">
                    <label >Anidar en:</label>
                    <select name="ActividadCabecera[experiencia_id]" id="experiencia_id" class="form-control">
                        <option value>Selecciona la experiencia</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label >Número de actividad</label>
                    <input type="text" name="ActividadCabecera[correlativo]" id="correlativo" class="form-control">
                </div>
            </div>
            <!-- <div class="col-md-12">
                <div class="form-group">
                    <label >Descripción</label>
                    <input type="text" name="ActividadCabecera[descripcion]" id="descripcion" class="form-control">
                </div>
            </div> -->
            <div class="col-md-12">
                <div class="form-group">
                    <label >Fecha de inicio</label>
                    <input type="date" name="ActividadCabecera[fecha_inicio]" id="fecha_inicio" class="form-control">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label >Fecha de fin</label>
                    <input type="date" name="ActividadCabecera[fecha_fin]" id="fecha_fin" class="form-control">
                </div>
            </div>
            <!-- <div class="col-md-12">
                <div class="form-group">
                    <label >id Actividad Aplicativo</label>
                    <input type="text" name="ActividadCabecera[actividad_chile_id]" id="actividad_chile_id" class="form-control">
                </div>
            </div> -->
        </div>
        
    </div>
    <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary btn-grabar-actividad">Grabar</button>
    </div>

<?php ActiveForm::end(); ?>