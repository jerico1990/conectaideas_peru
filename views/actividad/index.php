
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Actividad</h4>
                </div>
                <br><br>
                <div class="col-12">
                    <button class="btn btn-success btn-agregar-actividad">Agregar</button>
                </div>
            </div>
            

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Lista de actividades</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
               
                

                <div class="row">
                    <!-- <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Grado</label>
                            <select class="form-control" id="grado2_id">
                                <option value>Selecciona una opción</option>
                            </select>
                        </div>
                    </div> -->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Experiencias</label>
                            <select class="form-control" id="experiencia2_id">
                                <option value>Selecciona una opción</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Actividades</label>
                            <select class="form-control" id="actividad2_id">
                                <option value>Selecciona una opción</option>
                            </select>
                        </div>
                    </div>
                    
                    
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">&nbsp&nbsp</label> <br>
                            <button class="btn btn-success btn-buscar">Buscar</button>
                        </div>
                    </div>
                </div>

                <table id="lista-actividades" class="table table-bordered dt-responsive ">
                    <thead>
                        <tr>
                            <!-- <th>Grado</th> -->
                            <th>Experiencia</th>
                            <th>Actividad</th>
                            <!-- <th>Descripción</th> -->
                            <th>Fecha de inicio</th>
                            <th>Fecha de fin</th>
                            <th>Estado</th>
                            <!-- <th>Video</th>
                            <th>Matriz</th>
                            <th>Solucionario</th> -->
                            <th>Recursos</th>
                            <th>Reporte semanal</th>
                        </tr>
                    </thead>


                    <tbody>
                    </tbody>
                </table>


                <!-- <a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/reporte-registros-estudiantes" class="btn btn-success btn-block">Descargar lista de estudiantes</a> -->
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<div id="modal" class="fade modal" role="dialog"  tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>



<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('#staticBackdrop');
var experiencia_id;
var actividad_id;
var grado2_id;
$('#lista-actividades').DataTable();

Actividades();
async function Actividades(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/actividad/get-lista-actividades',
                method: 'POST',
                data:{_csrf:csrf,experiencia_id:experiencia_id,actividad_id:actividad_id,grado_id:grado2_id},
                dataType:'Json',
                beforeSend:function(){
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var actividades ="";
                        $('#lista-actividades').DataTable().destroy();
                        $.each(results.actividades, function( index, value ) {
                            
                            actividades = actividades + "<tr>";
                                // actividades = actividades + "<td> " + value.grado + "</td>";
                                actividades = actividades + "<td> " + value.experiencia_correlativo + "</td>";
                                actividades = actividades + "<td> " + value.correlativo + "</td>";
                                // actividades = actividades + "<td> " + ((value.descripcion)?value.descripcion:'') + "</td>";
                                actividades = actividades + "<td> " + ((value.fecha_inicio)?value.fecha_inicio:'') + "</td>";
                                actividades = actividades + "<td> " + ((value.fecha_fin)?value.fecha_fin:'') + "</td>";
                                actividades = actividades + "<td> " + value.estado_descripcion + "</td>";
                                // if(value.url_video){
                                //     actividades = actividades + "<td> <a target='_blank' href='" + value.url_video + "'>Ver</a> | <a href='#' data-id='" + value.id + "' class='btn-eliminar-video'>Borrar</a> </td>";
                                // }else{
                                //     actividades = actividades + "<td> <a href='#' data-id='" + value.id + "' class='btn-modificar-video'>Cargar</a> </td>";
                                // }

                                // if(value.matriz){
                                //     actividades = actividades + "<td> <a target='_blank' href='<?= \Yii::$app->request->BaseUrl ?>/recursos/matriz/" + value.matriz + "'>Ver</a> | <a href='#' data-id='" + value.id + "' class='btn-eliminar-matriz'>Borrar</a> </td>";
                                // }else{
                                //     actividades = actividades + "<td> <a href='#' data-id='" + value.id + "' class='btn-modificar-matriz'>Cargar</a> </td>";
                                // }

                                // if(value.solucionario){
                                //     actividades = actividades + "<td> <a target='_blank' href='<?= \Yii::$app->request->BaseUrl ?>/recursos/solucionario/" + value.solucionario + "'>Ver</a> | <a href='#' data-id='" + value.id + "' class='btn-eliminar-solucionario'>Borrar</a></td>";
                                // }else{
                                //     actividades = actividades + "<td> <a href='#' data-id='" + value.id + "' class='btn-modificar-solucionario'>Cargar</a> </td>";
                                // }

                                actividades = actividades + "<td>";
                                    
                                   if(value.estado=='1' ){
                                        actividades = actividades + '<a title="deshabilitar" href="#" data-id="' + value.id + '" class="btn btn-danger btn-sm btn-deshabilitar-actividad" >Deshabilitar</a> ';
                                    }else if (value.estado=='0'){
                                        actividades = actividades + '<a title="habilitar" href="#" data-id="' + value.id + '" class="btn btn-success btn-sm btn-habilitar-actividad" >Habilitar</a> ';
                                    }
                                    actividades = actividades + '<a href="#" data-id="' + value.id + '" data-grado_id="' + value.grado_id + '" data-experiencia_id="' + value.experiencia_id + '" class="btn btn-primary btn-sm btn-modificar-actividad" > <i class="fas fa-pen-square"></i> </a> ';
                            

                                actividades = actividades +"</td>";

                                actividades = actividades + "<td>";
                                    
                                   if(value.estado_reporte=='1' ){
                                        actividades = actividades + '<a title="deshabilitar" href="#" data-id="' + value.id + '" class="btn btn-danger btn-sm btn-deshabilitar-actividad-reporte" >Deshabilitar</a> ';
                                    }else if (value.estado_reporte=='0'){
                                        actividades = actividades + '<a title="habilitar" href="#" data-id="' + value.id + '" class="btn btn-success btn-sm btn-habilitar-actividad-reporte" >Habilitar</a> ';
                                    }

                                actividades = actividades +"</td>";

                            actividades = actividades + "</tr>";
                        });
                        
                        $('#lista-actividades tbody').html(actividades);
                        $('#lista-actividades').DataTable({
                            "paging": true,
                            "lengthChange": true,
                            "searching": false,
                            "ordering": false,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 10,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });

                        loading.modal('hide');
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}


async function Actividad(id_actividad){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/actividad/get-actividad',
                method: 'POST',
                data:{_csrf:csrf,id_actividad:id_actividad},
                dataType:'Json',
                beforeSend:function(xhr, settings)
                {
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var form = $('#formActividad');
                        form.loadJSON(results.actividad);
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}

async function ActividadesLista(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/actividad/get-lista-actividades',
                method: 'POST',
                data:{_csrf:csrf,experiencia_id:experiencia_id},
                dataType:'Json',
                beforeSend:function(){
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var actividades_options = "<option value>Selecciona actividad</option>";
                        $.each(results.actividades, function( index, value ) {
                            actividades_options += "<option value='" + value.id + "'>Actividad " + value.correlativo + "</option>";
                        });
                        $('#actividad2_id').html(actividades_options);
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}

async function Grados(grado_id){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/grado/get-lista-grados',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(xhr, settings)
                {
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var grados_options = "<option value>Selecciona grado</option>";
                        $.each(results.grados, function( index, value ) {
                            grados_options += "<option value='" + value.id + "'> " + value.nombre_corto + "</option>";
                        });
                        $('#grado_id').html(grados_options);
                        
                        if(grado_id){
                            $('#grado_id').val(grado_id);
                        }
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}

Experiencias();
async function Experiencias(experiencia_id){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/experiencia/get-lista-exp',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(xhr, settings)
                {
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var experiencias_options = "<option value>Selecciona la experiencia</option>";
                        $.each(results.experiencias, function( index, value ) {
                            experiencias_options += "<option value='" + value.id + "'> Experiencia " + value.correlativo + "</option>";
                        });
                        $('#experiencia_id').html(experiencias_options);
                        $('#experiencia2_id').html(experiencias_options);
                        if(experiencia_id){
                            $('#experiencia_id').val(experiencia_id);
                        }
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}



//agregar

$('body').on('click', '.btn-agregar-actividad', function (e) {
    e.preventDefault();
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/actividad/create',function(){
        Grados();
        Experiencias();
    });
    $('#modal').modal('show');
});

//modificar
$('body').on('click', '.btn-modificar-actividad', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    var grado_id = $(this).attr('data-grado_id');
    var experiencia_id = $(this).attr('data-experiencia_id');
    
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/actividad/update?id='+id,function(){
        Grados(grado_id);
        Experiencias(experiencia_id);
        Actividad(id)
    });
    $('#modal').modal('show');
});


//grabar

$('body').on('click', '.btn-grabar-actividad', function (e) {

    e.preventDefault();
    var form = $('#formActividad');
    var formData = $('#formActividad').serializeArray();
    if (form.find('.has-error').length) {
        return false;
    }
    
    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success: function (results) {
            if(results.success){
                $('#modal').modal('toggle');
                Actividades();
            }
        },
    });
});




$('body').on('click', '.btn-deshabilitar-actividad', function (e) {
    e.preventDefault();
    var actividad_id = $(this).attr('data-id');
    $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/actividad/deshabilitar-actividad',
        method: 'POST',
        data:{_csrf:csrf,actividad_id:actividad_id},
        dataType:'Json',
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success:function(results)
        {   
            if(results && results.success){
                Actividades();
            }
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });
});

$('body').on('click', '.btn-habilitar-actividad', function (e) {
    e.preventDefault();
    var actividad_id = $(this).attr('data-id');
    $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/actividad/habilitar-actividad',
        method: 'POST',
        data:{_csrf:csrf,actividad_id:actividad_id},
        dataType:'Json',
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success:function(results)
        {   
            if(results && results.success){
                Actividades();
            }
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });
});

$('body').on('click', '.btn-deshabilitar-actividad-reporte', function (e) {
    e.preventDefault();
    var actividad_id = $(this).attr('data-id');
    $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/actividad/deshabilitar-actividad-reporte',
        method: 'POST',
        data:{_csrf:csrf,actividad_id:actividad_id},
        dataType:'Json',
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success:function(results)
        {   
            if(results && results.success){
                Actividades();
            }
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });
});

$('body').on('click', '.btn-habilitar-actividad-reporte', function (e) {
    e.preventDefault();
    var actividad_id = $(this).attr('data-id');
    $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/actividad/habilitar-actividad-reporte',
        method: 'POST',
        data:{_csrf:csrf,actividad_id:actividad_id},
        dataType:'Json',
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success:function(results)
        {   
            if(results && results.success){
                Actividades();
            }
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });
});

$('body').on('change', '#experiencia2_id', function(e) {
    experiencia_id = $(this).val();
    ActividadesLista();
    //Actividades();
});

$('body').on('change', '#actividad2_id', function(e) {
    actividad_id = $(this).val();
    //Actividades();
});



$('body').on('click', '.btn-buscar', function(e) {
    Actividades();
});
</script>
