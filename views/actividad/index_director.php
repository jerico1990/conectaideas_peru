<div class="m-banner m-banner--profile m-banner--intranet">
    <div class="m-banner__bg"></div>
    <div class="m-container">
        <div class="m-breadcrumbs">
            <ul>
                <li><a href="<?= \Yii::$app->request->BaseUrl ?>/panel">Inicio</a></li>
                <li><a href="<?= \Yii::$app->request->BaseUrl ?>/recurso">Mi recursos</a></li>
                <li class="css--active"><a href="#"><?= $grado->nombre_corto ?></a></li>
            </ul>
        </div>
        <div class="m-banner__content">
            <p class="m-banner__title m-h1">Mi recursos</p>
        </div>
    </div>
</div>


<div class="m-container m-section">
    <div class="m-tab__container m-tab--only" id="tab-aside-resource">
        <div class="m-intranet--report__grid">
            <div class="m-intranet--report__grid__left">
                <div class="m-intranet--report__tab__mobile">
                    <div class="m-intranet--report__tab__button">
                        <p class="experiencia_mobil">Experiencia 1</p>
                        <span class="m-icon-svg m-icon-svg--secondary">
                            <svg width="6" height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M3.44833 5.72832L0.195 8.98166C-0.065 9.24166 -0.065 9.66832 0.195 9.92832C0.455 10.1883 0.875 10.1883 1.135 9.92832L5.52833 5.53499C5.78833 5.27499 5.78833 4.85499 5.52833 4.59499L1.14167 0.19499C1.01711 0.0701553 0.848012 0 0.671667 0C0.495322 0 0.326221 0.0701553 0.201667 0.19499C-0.0583333 0.45499 -0.0583333 0.87499 0.201667 1.13499L3.44833 4.39499C3.52917 4.56165 3.73496 4.69498 3.73496 5.06165C3.73496 5.42831 3.52917 5.56165 3.44833 5.72832Z" fill="white" />
                            </svg>
                        </span>
                    </div>
                    <div id="lista-experiencias" class="m-intranet__aside">
                        <!-- <ul>
                            <li>
                                <a class="m-tab__button--js js--active" href="#tab-aside-resource-1" data-container="tab-aside-resource">
                                    <span class="m-h3">Experiencia 1</span>
                                    <span class="m-icon-svg m-icon-svg--secondary">
                                        <svg width="6" height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M3.44833 5.72832L0.195 8.98166C-0.065 9.24166 -0.065 9.66832 0.195 9.92832C0.455 10.1883 0.875 10.1883 1.135 9.92832L5.52833 5.53499C5.78833 5.27499 5.78833 4.85499 5.52833 4.59499L1.14167 0.19499C1.01711 0.0701553 0.848012 0 0.671667 0C0.495322 0 0.326221 0.0701553 0.201667 0.19499C-0.0583333 0.45499 -0.0583333 0.87499 0.201667 1.13499L3.44833 4.39499C3.52917 4.56165 3.73496 4.69498 3.73496 5.06165C3.73496 5.42831 3.52917 5.56165 3.44833 5.72832Z" fill="white" />
                                        </svg>
                                    </span>
                                </a>
                            </li>

                            <li>
                                <a class="m-tab__button--js" href="#tab-aside-resource-2" data-container="tab-aside-resource">
                                    <span class="m-h3">Experiencia 2</span>
                                    <span class="m-icon-svg m-icon-svg--secondary">
                                        <svg width="6" height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M3.44833 5.72832L0.195 8.98166C-0.065 9.24166 -0.065 9.66832 0.195 9.92832C0.455 10.1883 0.875 10.1883 1.135 9.92832L5.52833 5.53499C5.78833 5.27499 5.78833 4.85499 5.52833 4.59499L1.14167 0.19499C1.01711 0.0701553 0.848012 0 0.671667 0C0.495322 0 0.326221 0.0701553 0.201667 0.19499C-0.0583333 0.45499 -0.0583333 0.87499 0.201667 1.13499L3.44833 4.39499C3.52917 4.56165 3.73496 4.69498 3.73496 5.06165C3.73496 5.42831 3.52917 5.56165 3.44833 5.72832Z" fill="white" />
                                        </svg>
                                    </span>
                                </a>
                            </li>
                            
                        </ul> -->
                    </div>
                </div>
            </div>
            <div class="m-intranet--report__grid__right lista-actividades">
                <!-- <div class="m-tab__content js--active--desktop" id="tab-aside-resource-1" data-container="tab-aside-resource">
                    <h2>Reportes de la Experiencia 1</h2>
                    <p>Mira los recursos para cada actividad de esta experiencia:</p>
                    <div class="m-tab__container m-help-center__content m-tab--only" id="tab-resource-1">
                        
                        <div class="m-box-inline-block m-tab__nav m-tab__help-center__nav">
                            <a  class="m-tab__button m-tab__button--js js--active" href="#tab-resource-1-0" data-container="tab-resource-1" title="Ir a &lt;span class=&quot;m-none m-inline-block-ls&quot;&gt;Actividad&lt;/span&gt; 1">
                                <span class="m-none m-inline-block-ls">Actividad</span> 1
                            </a>
                            <a class="m-tab__button m-tab__button--js" href="#tab-resource-1-1" data-container="tab-resource-1" title="Ir a &lt;span class=&quot;m-none m-inline-block-ls&quot;&gt;Actividad&lt;/span&gt; 2">
                                <span class="m-none m-inline-block-ls">Actividad</span> 2
                            </a>
                            <a class="m-tab__button m-tab__button--js" href="#tab-resource-1-2" data-container="tab-resource-1" title="Ir a &lt;span class=&quot;m-none m-inline-block-ls&quot;&gt;Actividad&lt;/span&gt; 3">
                                <span class="m-none m-inline-block-ls">Actividad</span> 3
                            </a>
                            <a class="m-tab__button m-tab__button--js" href="#tab-resource-1-3" data-container="tab-resource-1" title="Ir a &lt;span class=&quot;m-none m-inline-block-ls&quot;&gt;Actividad&lt;/span&gt; 4">
                                <span class="m-none m-inline-block-ls">Actividad</span> 4
                            </a>
                        </div>

                        <div class="m-tab__content js--active--desktop" id="tab-resource-1-0" data-container="tab-resource-1">
                            <div class="m-intranet--report__accordion">
                                <h3 class="m-h3-2">Resolución de problemas con fracciones</h3>
                                <div class="m-intranet__content__multimedia">
                                    <div class="m-intranet__content__multimedia__video">
                                        <a class="m-video--css m-video__modal--js" href="https://youtu.be/v3jCNGIhsNQ">
                                            <picture class="m-lazy__img">
                                                <img class="m-lazy-js" data-src=" ./img/resource_content_0.png ">
                                            </picture>
                                        </a>
                                    </div>
                                    <div class="m-intranet__content__multimedia__aside">
                                        <p>Recursos para descarga:</p>
                                        <div class="m-intranet__content__multimedia__aside__buttons">
                                            <div>
                                                <a class="m-button m-button--teal m-button--full m-button--icon m-button--icon--auto" href="">
                                                    <span>
                                                        <span class="m-icon-svg m-icon-svg--secondary">
                                                            <svg width="18" height="16" viewBox="0 0 18 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                <path d="M18 13.7259V8.4467C18 8.04061 17.6757 7.67513 17.2297 7.67513C16.7838 7.67513 16.4595 8 16.4595 8.4467V13.7259C16.4595 14.132 16.1351 14.4975 15.6892 14.4975H2.27027C1.86486 14.4975 1.5 14.1726 1.5 13.7259V8.4467C1.5 8.04061 1.17568 7.67513 0.72973 7.67513C0.283784 7.67513 0 8.04061 0 8.48731V13.7665C0 15.0254 1.01351 16.0406 2.27027 16.0406H15.7703C16.9865 16 18 14.9848 18 13.7259ZM13.2162 8.56853L9.44595 11.5736C9.16216 11.7766 8.7973 11.7766 8.51351 11.5736L4.74324 8.56853C4.45946 8.28426 4.41892 7.79695 4.7027 7.51269C4.94595 7.22842 5.35135 7.18782 5.67568 7.39086L8.22973 9.42132V0.771573C8.22973 0.365482 8.55405 0 9 0C9.40541 0 9.77027 0.324873 9.77027 0.771573V9.46193L12.3243 7.43147C12.6081 7.14721 13.0946 7.14721 13.3784 7.47208C13.6622 7.75634 13.6622 8.24365 13.3378 8.52792C13.2973 8.52792 13.2568 8.52792 13.2162 8.56853Z" fill="white" />
                                                            </svg>
                                                        </span>
                                                    </span>
                                                    <span>Solucionario</span>
                                                </a>
                                            </div>
                                            <div>
                                                <a class="m-button m-button--magenta m-button--full m-button--icon m-button--icon--auto" href="">
                                                    <span>
                                                        <span class="m-icon-svg m-icon-svg--secondary">
                                                            <svg width="18" height="16" viewBox="0 0 18 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                <path d="M18 13.7259V8.4467C18 8.04061 17.6757 7.67513 17.2297 7.67513C16.7838 7.67513 16.4595 8 16.4595 8.4467V13.7259C16.4595 14.132 16.1351 14.4975 15.6892 14.4975H2.27027C1.86486 14.4975 1.5 14.1726 1.5 13.7259V8.4467C1.5 8.04061 1.17568 7.67513 0.72973 7.67513C0.283784 7.67513 0 8.04061 0 8.48731V13.7665C0 15.0254 1.01351 16.0406 2.27027 16.0406H15.7703C16.9865 16 18 14.9848 18 13.7259ZM13.2162 8.56853L9.44595 11.5736C9.16216 11.7766 8.7973 11.7766 8.51351 11.5736L4.74324 8.56853C4.45946 8.28426 4.41892 7.79695 4.7027 7.51269C4.94595 7.22842 5.35135 7.18782 5.67568 7.39086L8.22973 9.42132V0.771573C8.22973 0.365482 8.55405 0 9 0C9.40541 0 9.77027 0.324873 9.77027 0.771573V9.46193L12.3243 7.43147C12.6081 7.14721 13.0946 7.14721 13.3784 7.47208C13.6622 7.75634 13.6622 8.24365 13.3378 8.52792C13.2973 8.52792 13.2568 8.52792 13.2162 8.56853Z" fill="white" />
                                                            </svg>
                                                        </span>
                                                    </span>
                                                    <span>Matriz</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="m-tab__content" id="tab-resource-1-1" data-container="tab-resource-1">
                            <div class="m-intranet--report__accordion">
                                <h3 class="m-h3-2">Resolución de problemas con fracciones</h3>
                                <div class="m-intranet__content__multimedia">
                                    <div class="m-intranet__content__multimedia__video"><a
                                            class="m-video--css m-video__modal--js"
                                            href="https://youtu.be/v3jCNGIhsNQ">
                                            <picture class="m-lazy__img"><img class="m-lazy-js"
                                                    data-src=" ./img/resource_content_0.png "></picture>
                                        </a></div>
                                    <div class="m-intranet__content__multimedia__aside">
                                        <p>Recursos para descarga:</p>
                                        <div class="m-intranet__content__multimedia__aside__buttons">
                                            <div><a class="m-button m-button--teal m-button--full m-button--icon m-button--icon--auto"
                                                    href=""><span><span
                                                            class="m-icon-svg m-icon-svg--secondary"><svg width="18"
                                                                height="16" viewBox="0 0 18 16" fill="none"
                                                                xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M18 13.7259V8.4467C18 8.04061 17.6757 7.67513 17.2297 7.67513C16.7838 7.67513 16.4595 8 16.4595 8.4467V13.7259C16.4595 14.132 16.1351 14.4975 15.6892 14.4975H2.27027C1.86486 14.4975 1.5 14.1726 1.5 13.7259V8.4467C1.5 8.04061 1.17568 7.67513 0.72973 7.67513C0.283784 7.67513 0 8.04061 0 8.48731V13.7665C0 15.0254 1.01351 16.0406 2.27027 16.0406H15.7703C16.9865 16 18 14.9848 18 13.7259ZM13.2162 8.56853L9.44595 11.5736C9.16216 11.7766 8.7973 11.7766 8.51351 11.5736L4.74324 8.56853C4.45946 8.28426 4.41892 7.79695 4.7027 7.51269C4.94595 7.22842 5.35135 7.18782 5.67568 7.39086L8.22973 9.42132V0.771573C8.22973 0.365482 8.55405 0 9 0C9.40541 0 9.77027 0.324873 9.77027 0.771573V9.46193L12.3243 7.43147C12.6081 7.14721 13.0946 7.14721 13.3784 7.47208C13.6622 7.75634 13.6622 8.24365 13.3378 8.52792C13.2973 8.52792 13.2568 8.52792 13.2162 8.56853Z"
                                                                    fill="white" /></svg>
                                                        </span></span><span>Solucionario</span></a></div>
                                            <div><a class="m-button m-button--magenta m-button--full m-button--icon m-button--icon--auto"
                                                    href=""><span><span
                                                            class="m-icon-svg m-icon-svg--secondary"><svg width="18"
                                                                height="16" viewBox="0 0 18 16" fill="none"
                                                                xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M18 13.7259V8.4467C18 8.04061 17.6757 7.67513 17.2297 7.67513C16.7838 7.67513 16.4595 8 16.4595 8.4467V13.7259C16.4595 14.132 16.1351 14.4975 15.6892 14.4975H2.27027C1.86486 14.4975 1.5 14.1726 1.5 13.7259V8.4467C1.5 8.04061 1.17568 7.67513 0.72973 7.67513C0.283784 7.67513 0 8.04061 0 8.48731V13.7665C0 15.0254 1.01351 16.0406 2.27027 16.0406H15.7703C16.9865 16 18 14.9848 18 13.7259ZM13.2162 8.56853L9.44595 11.5736C9.16216 11.7766 8.7973 11.7766 8.51351 11.5736L4.74324 8.56853C4.45946 8.28426 4.41892 7.79695 4.7027 7.51269C4.94595 7.22842 5.35135 7.18782 5.67568 7.39086L8.22973 9.42132V0.771573C8.22973 0.365482 8.55405 0 9 0C9.40541 0 9.77027 0.324873 9.77027 0.771573V9.46193L12.3243 7.43147C12.6081 7.14721 13.0946 7.14721 13.3784 7.47208C13.6622 7.75634 13.6622 8.24365 13.3378 8.52792C13.2973 8.52792 13.2568 8.52792 13.2162 8.56853Z"
                                                                    fill="white" /></svg>
                                                        </span></span><span>Matriz</span></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div> -->

<!-- 
                <div class="m-tab__content" id="tab-aside-resource-2" data-container="tab-aside-resource">
                    <h2>Reportes de la Experiencia 2</h2>
                    <p>Mira los recursos para cada actividad de esta experiencia:</p>
                    <div class="m-tab__container m-help-center__content m-tab--only" id="tab-resource-2">
                        <div class="m-box-inline-block m-tab__nav m-tab__help-center__nav"><a
                                class="m-tab__button m-tab__button--js js--active" href="#tab-resource-2-0"
                                data-container="tab-resource-2"
                                title="Ir a &lt;span class=&quot;m-none m-inline-block-ls&quot;&gt;Actividad&lt;/span&gt; 1"><span
                                    class="m-none m-inline-block-ls">Actividad</span> 1</a><a
                                class="m-tab__button m-tab__button--js" href="#tab-resource-2-1"
                                data-container="tab-resource-2"
                                title="Ir a &lt;span class=&quot;m-none m-inline-block-ls&quot;&gt;Actividad&lt;/span&gt; 2"><span
                                    class="m-none m-inline-block-ls">Actividad</span> 2</a><a
                                class="m-tab__button m-tab__button--js" href="#tab-resource-2-2"
                                data-container="tab-resource-2"
                                title="Ir a &lt;span class=&quot;m-none m-inline-block-ls&quot;&gt;Actividad&lt;/span&gt; 3"><span
                                    class="m-none m-inline-block-ls">Actividad</span> 3</a><a
                                class="m-tab__button m-tab__button--js" href="#tab-resource-2-3"
                                data-container="tab-resource-2"
                                title="Ir a &lt;span class=&quot;m-none m-inline-block-ls&quot;&gt;Actividad&lt;/span&gt; 4"><span
                                    class="m-none m-inline-block-ls">Actividad</span> 4</a></div>
                        <div class="m-tab__content js--active--desktop" id="tab-resource-2-0"
                            data-container="tab-resource-2">
                            <div class="m-intranet--report__accordion">
                                <h3 class="m-h3-2">Resolución de problemas con fracciones</h3>
                                <div class="m-intranet__content__multimedia">
                                    <div class="m-intranet__content__multimedia__video"><a
                                            class="m-video--css m-video__modal--js"
                                            href="https://youtu.be/v3jCNGIhsNQ">
                                            <picture class="m-lazy__img"><img class="m-lazy-js"
                                                    data-src=" ./img/resource_content_0.png "></picture>
                                        </a></div>
                                    <div class="m-intranet__content__multimedia__aside">
                                        <p>Recursos para descarga:</p>
                                        <div class="m-intranet__content__multimedia__aside__buttons">
                                            <div><a class="m-button m-button--teal m-button--full m-button--icon m-button--icon--auto"
                                                    href=""><span><span
                                                            class="m-icon-svg m-icon-svg--secondary"><svg width="18"
                                                                height="16" viewBox="0 0 18 16" fill="none"
                                                                xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M18 13.7259V8.4467C18 8.04061 17.6757 7.67513 17.2297 7.67513C16.7838 7.67513 16.4595 8 16.4595 8.4467V13.7259C16.4595 14.132 16.1351 14.4975 15.6892 14.4975H2.27027C1.86486 14.4975 1.5 14.1726 1.5 13.7259V8.4467C1.5 8.04061 1.17568 7.67513 0.72973 7.67513C0.283784 7.67513 0 8.04061 0 8.48731V13.7665C0 15.0254 1.01351 16.0406 2.27027 16.0406H15.7703C16.9865 16 18 14.9848 18 13.7259ZM13.2162 8.56853L9.44595 11.5736C9.16216 11.7766 8.7973 11.7766 8.51351 11.5736L4.74324 8.56853C4.45946 8.28426 4.41892 7.79695 4.7027 7.51269C4.94595 7.22842 5.35135 7.18782 5.67568 7.39086L8.22973 9.42132V0.771573C8.22973 0.365482 8.55405 0 9 0C9.40541 0 9.77027 0.324873 9.77027 0.771573V9.46193L12.3243 7.43147C12.6081 7.14721 13.0946 7.14721 13.3784 7.47208C13.6622 7.75634 13.6622 8.24365 13.3378 8.52792C13.2973 8.52792 13.2568 8.52792 13.2162 8.56853Z"
                                                                    fill="white" /></svg>
                                                        </span></span><span>Solucionario</span></a></div>
                                            <div><a class="m-button m-button--magenta m-button--full m-button--icon m-button--icon--auto"
                                                    href=""><span><span
                                                            class="m-icon-svg m-icon-svg--secondary"><svg width="18"
                                                                height="16" viewBox="0 0 18 16" fill="none"
                                                                xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M18 13.7259V8.4467C18 8.04061 17.6757 7.67513 17.2297 7.67513C16.7838 7.67513 16.4595 8 16.4595 8.4467V13.7259C16.4595 14.132 16.1351 14.4975 15.6892 14.4975H2.27027C1.86486 14.4975 1.5 14.1726 1.5 13.7259V8.4467C1.5 8.04061 1.17568 7.67513 0.72973 7.67513C0.283784 7.67513 0 8.04061 0 8.48731V13.7665C0 15.0254 1.01351 16.0406 2.27027 16.0406H15.7703C16.9865 16 18 14.9848 18 13.7259ZM13.2162 8.56853L9.44595 11.5736C9.16216 11.7766 8.7973 11.7766 8.51351 11.5736L4.74324 8.56853C4.45946 8.28426 4.41892 7.79695 4.7027 7.51269C4.94595 7.22842 5.35135 7.18782 5.67568 7.39086L8.22973 9.42132V0.771573C8.22973 0.365482 8.55405 0 9 0C9.40541 0 9.77027 0.324873 9.77027 0.771573V9.46193L12.3243 7.43147C12.6081 7.14721 13.0946 7.14721 13.3784 7.47208C13.6622 7.75634 13.6622 8.24365 13.3378 8.52792C13.2973 8.52792 13.2568 8.52792 13.2162 8.56853Z"
                                                                    fill="white" /></svg>
                                                        </span></span><span>Matriz</span></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-tab__content" id="tab-resource-2-1" data-container="tab-resource-2">
                            <div class="m-intranet--report__accordion">
                                <h3 class="m-h3-2">Resolución de problemas con fracciones</h3>
                                <div class="m-intranet__content__multimedia">
                                    <div class="m-intranet__content__multimedia__video"><a
                                            class="m-video--css m-video__modal--js"
                                            href="https://youtu.be/v3jCNGIhsNQ">
                                            <picture class="m-lazy__img"><img class="m-lazy-js"
                                                    data-src=" ./img/resource_content_0.png "></picture>
                                        </a></div>
                                    <div class="m-intranet__content__multimedia__aside">
                                        <p>Recursos para descarga:</p>
                                        <div class="m-intranet__content__multimedia__aside__buttons">
                                            <div><a class="m-button m-button--teal m-button--full m-button--icon m-button--icon--auto"
                                                    href=""><span><span
                                                            class="m-icon-svg m-icon-svg--secondary"><svg width="18"
                                                                height="16" viewBox="0 0 18 16" fill="none"
                                                                xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M18 13.7259V8.4467C18 8.04061 17.6757 7.67513 17.2297 7.67513C16.7838 7.67513 16.4595 8 16.4595 8.4467V13.7259C16.4595 14.132 16.1351 14.4975 15.6892 14.4975H2.27027C1.86486 14.4975 1.5 14.1726 1.5 13.7259V8.4467C1.5 8.04061 1.17568 7.67513 0.72973 7.67513C0.283784 7.67513 0 8.04061 0 8.48731V13.7665C0 15.0254 1.01351 16.0406 2.27027 16.0406H15.7703C16.9865 16 18 14.9848 18 13.7259ZM13.2162 8.56853L9.44595 11.5736C9.16216 11.7766 8.7973 11.7766 8.51351 11.5736L4.74324 8.56853C4.45946 8.28426 4.41892 7.79695 4.7027 7.51269C4.94595 7.22842 5.35135 7.18782 5.67568 7.39086L8.22973 9.42132V0.771573C8.22973 0.365482 8.55405 0 9 0C9.40541 0 9.77027 0.324873 9.77027 0.771573V9.46193L12.3243 7.43147C12.6081 7.14721 13.0946 7.14721 13.3784 7.47208C13.6622 7.75634 13.6622 8.24365 13.3378 8.52792C13.2973 8.52792 13.2568 8.52792 13.2162 8.56853Z"
                                                                    fill="white" /></svg>
                                                        </span></span><span>Solucionario</span></a></div>
                                            <div><a class="m-button m-button--magenta m-button--full m-button--icon m-button--icon--auto"
                                                    href=""><span><span
                                                            class="m-icon-svg m-icon-svg--secondary"><svg width="18"
                                                                height="16" viewBox="0 0 18 16" fill="none"
                                                                xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M18 13.7259V8.4467C18 8.04061 17.6757 7.67513 17.2297 7.67513C16.7838 7.67513 16.4595 8 16.4595 8.4467V13.7259C16.4595 14.132 16.1351 14.4975 15.6892 14.4975H2.27027C1.86486 14.4975 1.5 14.1726 1.5 13.7259V8.4467C1.5 8.04061 1.17568 7.67513 0.72973 7.67513C0.283784 7.67513 0 8.04061 0 8.48731V13.7665C0 15.0254 1.01351 16.0406 2.27027 16.0406H15.7703C16.9865 16 18 14.9848 18 13.7259ZM13.2162 8.56853L9.44595 11.5736C9.16216 11.7766 8.7973 11.7766 8.51351 11.5736L4.74324 8.56853C4.45946 8.28426 4.41892 7.79695 4.7027 7.51269C4.94595 7.22842 5.35135 7.18782 5.67568 7.39086L8.22973 9.42132V0.771573C8.22973 0.365482 8.55405 0 9 0C9.40541 0 9.77027 0.324873 9.77027 0.771573V9.46193L12.3243 7.43147C12.6081 7.14721 13.0946 7.14721 13.3784 7.47208C13.6622 7.75634 13.6622 8.24365 13.3378 8.52792C13.2973 8.52792 13.2568 8.52792 13.2162 8.56853Z"
                                                                    fill="white" /></svg>
                                                        </span></span><span>Matriz</span></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-tab__content" id="tab-resource-2-2" data-container="tab-resource-2">
                            <div class="m-intranet--report__accordion">
                                <h3 class="m-h3-2">Resolución de problemas con fracciones</h3>
                                <div class="m-intranet__content__multimedia">
                                    <div class="m-intranet__content__multimedia__video"><a
                                            class="m-video--css m-video__modal--js"
                                            href="https://youtu.be/v3jCNGIhsNQ">
                                            <picture class="m-lazy__img"><img class="m-lazy-js"
                                                    data-src=" ./img/resource_content_0.png "></picture>
                                        </a></div>
                                    <div class="m-intranet__content__multimedia__aside">
                                        <p>Recursos para descarga:</p>
                                        <div class="m-intranet__content__multimedia__aside__buttons">
                                            <div><a class="m-button m-button--teal m-button--full m-button--icon m-button--icon--auto"
                                                    href=""><span><span
                                                            class="m-icon-svg m-icon-svg--secondary"><svg width="18"
                                                                height="16" viewBox="0 0 18 16" fill="none"
                                                                xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M18 13.7259V8.4467C18 8.04061 17.6757 7.67513 17.2297 7.67513C16.7838 7.67513 16.4595 8 16.4595 8.4467V13.7259C16.4595 14.132 16.1351 14.4975 15.6892 14.4975H2.27027C1.86486 14.4975 1.5 14.1726 1.5 13.7259V8.4467C1.5 8.04061 1.17568 7.67513 0.72973 7.67513C0.283784 7.67513 0 8.04061 0 8.48731V13.7665C0 15.0254 1.01351 16.0406 2.27027 16.0406H15.7703C16.9865 16 18 14.9848 18 13.7259ZM13.2162 8.56853L9.44595 11.5736C9.16216 11.7766 8.7973 11.7766 8.51351 11.5736L4.74324 8.56853C4.45946 8.28426 4.41892 7.79695 4.7027 7.51269C4.94595 7.22842 5.35135 7.18782 5.67568 7.39086L8.22973 9.42132V0.771573C8.22973 0.365482 8.55405 0 9 0C9.40541 0 9.77027 0.324873 9.77027 0.771573V9.46193L12.3243 7.43147C12.6081 7.14721 13.0946 7.14721 13.3784 7.47208C13.6622 7.75634 13.6622 8.24365 13.3378 8.52792C13.2973 8.52792 13.2568 8.52792 13.2162 8.56853Z"
                                                                    fill="white" /></svg>
                                                        </span></span><span>Solucionario</span></a></div>
                                            <div><a class="m-button m-button--magenta m-button--full m-button--icon m-button--icon--auto"
                                                    href=""><span><span
                                                            class="m-icon-svg m-icon-svg--secondary"><svg width="18"
                                                                height="16" viewBox="0 0 18 16" fill="none"
                                                                xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M18 13.7259V8.4467C18 8.04061 17.6757 7.67513 17.2297 7.67513C16.7838 7.67513 16.4595 8 16.4595 8.4467V13.7259C16.4595 14.132 16.1351 14.4975 15.6892 14.4975H2.27027C1.86486 14.4975 1.5 14.1726 1.5 13.7259V8.4467C1.5 8.04061 1.17568 7.67513 0.72973 7.67513C0.283784 7.67513 0 8.04061 0 8.48731V13.7665C0 15.0254 1.01351 16.0406 2.27027 16.0406H15.7703C16.9865 16 18 14.9848 18 13.7259ZM13.2162 8.56853L9.44595 11.5736C9.16216 11.7766 8.7973 11.7766 8.51351 11.5736L4.74324 8.56853C4.45946 8.28426 4.41892 7.79695 4.7027 7.51269C4.94595 7.22842 5.35135 7.18782 5.67568 7.39086L8.22973 9.42132V0.771573C8.22973 0.365482 8.55405 0 9 0C9.40541 0 9.77027 0.324873 9.77027 0.771573V9.46193L12.3243 7.43147C12.6081 7.14721 13.0946 7.14721 13.3784 7.47208C13.6622 7.75634 13.6622 8.24365 13.3378 8.52792C13.2973 8.52792 13.2568 8.52792 13.2162 8.56853Z"
                                                                    fill="white" /></svg>
                                                        </span></span><span>Matriz</span></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-tab__content" id="tab-resource-2-3" data-container="tab-resource-2">
                            <div class="m-intranet--report__accordion">
                                <h3 class="m-h3-2">Resolución de problemas con fracciones</h3>
                                <div class="m-intranet__content__multimedia">
                                    <div class="m-intranet__content__multimedia__video"><a
                                            class="m-video--css m-video__modal--js"
                                            href="https://youtu.be/v3jCNGIhsNQ">
                                            <picture class="m-lazy__img"><img class="m-lazy-js"
                                                    data-src=" ./img/resource_content_0.png "></picture>
                                        </a></div>
                                    <div class="m-intranet__content__multimedia__aside">
                                        <p>Recursos para descarga:</p>
                                        <div class="m-intranet__content__multimedia__aside__buttons">
                                            <div><a class="m-button m-button--teal m-button--full m-button--icon m-button--icon--auto"
                                                    href=""><span><span
                                                            class="m-icon-svg m-icon-svg--secondary"><svg width="18"
                                                                height="16" viewBox="0 0 18 16" fill="none"
                                                                xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M18 13.7259V8.4467C18 8.04061 17.6757 7.67513 17.2297 7.67513C16.7838 7.67513 16.4595 8 16.4595 8.4467V13.7259C16.4595 14.132 16.1351 14.4975 15.6892 14.4975H2.27027C1.86486 14.4975 1.5 14.1726 1.5 13.7259V8.4467C1.5 8.04061 1.17568 7.67513 0.72973 7.67513C0.283784 7.67513 0 8.04061 0 8.48731V13.7665C0 15.0254 1.01351 16.0406 2.27027 16.0406H15.7703C16.9865 16 18 14.9848 18 13.7259ZM13.2162 8.56853L9.44595 11.5736C9.16216 11.7766 8.7973 11.7766 8.51351 11.5736L4.74324 8.56853C4.45946 8.28426 4.41892 7.79695 4.7027 7.51269C4.94595 7.22842 5.35135 7.18782 5.67568 7.39086L8.22973 9.42132V0.771573C8.22973 0.365482 8.55405 0 9 0C9.40541 0 9.77027 0.324873 9.77027 0.771573V9.46193L12.3243 7.43147C12.6081 7.14721 13.0946 7.14721 13.3784 7.47208C13.6622 7.75634 13.6622 8.24365 13.3378 8.52792C13.2973 8.52792 13.2568 8.52792 13.2162 8.56853Z"
                                                                    fill="white" /></svg>
                                                        </span></span><span>Solucionario</span></a></div>
                                            <div><a class="m-button m-button--magenta m-button--full m-button--icon m-button--icon--auto"
                                                    href=""><span><span
                                                            class="m-icon-svg m-icon-svg--secondary"><svg width="18"
                                                                height="16" viewBox="0 0 18 16" fill="none"
                                                                xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M18 13.7259V8.4467C18 8.04061 17.6757 7.67513 17.2297 7.67513C16.7838 7.67513 16.4595 8 16.4595 8.4467V13.7259C16.4595 14.132 16.1351 14.4975 15.6892 14.4975H2.27027C1.86486 14.4975 1.5 14.1726 1.5 13.7259V8.4467C1.5 8.04061 1.17568 7.67513 0.72973 7.67513C0.283784 7.67513 0 8.04061 0 8.48731V13.7665C0 15.0254 1.01351 16.0406 2.27027 16.0406H15.7703C16.9865 16 18 14.9848 18 13.7259ZM13.2162 8.56853L9.44595 11.5736C9.16216 11.7766 8.7973 11.7766 8.51351 11.5736L4.74324 8.56853C4.45946 8.28426 4.41892 7.79695 4.7027 7.51269C4.94595 7.22842 5.35135 7.18782 5.67568 7.39086L8.22973 9.42132V0.771573C8.22973 0.365482 8.55405 0 9 0C9.40541 0 9.77027 0.324873 9.77027 0.771573V9.46193L12.3243 7.43147C12.6081 7.14721 13.0946 7.14721 13.3784 7.47208C13.6622 7.75634 13.6622 8.24365 13.3378 8.52792C13.2973 8.52792 13.2568 8.52792 13.2162 8.56853Z"
                                                                    fill="white" /></svg>
                                                        </span></span><span>Matriz</span></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 -->
            </div>
        </div>
    </div>
</div>




<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var grado_id = "<?= $grado->id ?>";
Experiencias();
async function Experiencias(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/experiencia/get-lista-experiencias',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var experiencias ="";
                        var i = 0;
                        experiencias = experiencias + '<ul>';
                        $.each(results.experiencias, function( index, value ) {
                            experiencia_id = value.id;
                            experiencia_correlativo = value.correlativo;
                            active = '';
                            if(i==0){
                                active = 'js--active';
                                $('.experiencia_mobil').html('Experiencia '+ experiencia_correlativo)
                            }
                            experiencias = experiencias + `
                            <li>
                                <a data-id="${experiencia_id}" data-experiencia_id="${experiencia_id}" data-experiencia_correlativo="${experiencia_correlativo}" class="${active} btn-seleccionar-experiencia" href="#tab-aside-resource-${experiencia_id}" data-container="tab-aside-resource">
                                    <span class="m-h3">Experiencia ${experiencia_correlativo}</span>
                                    <span class="m-icon-svg m-icon-svg--secondary">
                                        <svg width="6" height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M3.44833 5.72832L0.195 8.98166C-0.065 9.24166 -0.065 9.66832 0.195 9.92832C0.455 10.1883 0.875 10.1883 1.135 9.92832L5.52833 5.53499C5.78833 5.27499 5.78833 4.85499 5.52833 4.59499L1.14167 0.19499C1.01711 0.0701553 0.848012 0 0.671667 0C0.495322 0 0.326221 0.0701553 0.201667 0.19499C-0.0583333 0.45499 -0.0583333 0.87499 0.201667 1.13499L3.44833 4.39499C3.52917 4.56165 3.73496 4.69498 3.73496 5.06165C3.73496 5.42831 3.52917 5.56165 3.44833 5.72832Z" fill="white" />
                                        </svg>
                                    </span>
                                </a>
                            </li>`;
                            i++;
                        });
                        experiencias = experiencias + '</ul>';
                        
                        $('#lista-experiencias').html(experiencias);
                        Actividades(results.experiencias[0].id,results.experiencias[0].correlativo)
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}


async function Actividades(experiencia_id,experiencia_correlativo){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/actividad/get-experiencia-actividades',
                method: 'POST',
                data:{_csrf:csrf,experiencia_id:experiencia_id,grado_id:grado_id},
                dataType:'Json',
                beforeSend:function(){
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        
                        var i = 0;
                        var actividades ='';
                        
                        actividades = actividades + `
                                                    <div class="m-tab__content js--active--desktop" id="tab-aside-resource-${experiencia_correlativo}" data-container="tab-aside-resource">
                                                        <h2>Recursos de la Experiencia ${experiencia_correlativo}</h2>
                                                        <p>Mira los recursos para cada actividad de esta experiencia:</p>
                                                        <div class="m-tab__container m-help-center__content m-tab--only" id="tab-resource-${experiencia_correlativo}">
                                                            <div class="m-box-inline-block m-tab__nav m-tab__help-center__nav">
                        `;

                        
                        $.each(results.actividades, function( index, value ) {
                            correlativo = value.correlativo;
                            active = '';
                            if(i==0){
                                active = 'js--active';
                            }
                            actividades = actividades + `   <a  class="m-tab__button m-tab__button--js ${active}" href="#tab-resource-${experiencia_correlativo}-${correlativo}" data-container="tab-resource-${experiencia_correlativo}" title="Ir a &lt;span class=&quot;m-none m-inline-block-ls&quot;&gt;Actividad&lt;/span&gt; ${correlativo}">
                                                                <span class="m-none m-inline-block-ls">Actividad</span> ${correlativo}
                                                            </a>`;
                            i++;

                        });

                        actividades = actividades + `</div>`;



                        i = 0;
                        $.each(results.actividades, function( index, value ) {
                            correlativo = value.correlativo;
                            active = '';
                            if(i==0){
                                active = 'js--active--desktop';
                            }
                         
                            $.each(value.detalle, function( index2, value2 ) {
                                id = value2.id;
                                grado = value2.nombre_corto;
                                video = value2.url_video;
                                matriz = value2.matriz;
                                solucionario = value2.solucionario;
                                descripcion = value2.descripcion;
                                // <div class="m-iframe">
                                //                                                 <iframe width="1108" height="623" src="${video}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                //                                                 </div>

                                actividades = actividades + `
                                                            <div class="m-tab__content ${active}" id="tab-resource-${experiencia_correlativo}-${correlativo}" data-container="tab-resource-${experiencia_correlativo}">
                                                                <div class="m-intranet--report__accordion">
                                                                    <h3 class="m-h3-2">${descripcion}</h3>
                                                                    <div class="m-intranet__content__multimedia">
                                                                        <div class="m-intranet__content__multimedia__video">
                                                                            <div class="m-iframe">
                                                                                ${video}
                                                                            </div>
                                                                        </div>
                                                                        <div class="m-intranet__content__multimedia__aside">
                                                                            <p>Recursos para descarga:</p>
                                                                            <div class="m-intranet__content__multimedia__aside__buttons">
                                                                                <div>
                                                                                    <a target="_blank" class="m-button m-button--teal m-button--full m-button--icon m-button--icon--auto" href="<?= \Yii::$app->request->BaseUrl ?>/recursos/solucionario/${solucionario}">
                                                                                        <span>
                                                                                            <span class="m-icon-svg m-icon-svg--secondary">
                                                                                                <svg width="18" height="16" viewBox="0 0 18 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                                                    <path d="M18 13.7259V8.4467C18 8.04061 17.6757 7.67513 17.2297 7.67513C16.7838 7.67513 16.4595 8 16.4595 8.4467V13.7259C16.4595 14.132 16.1351 14.4975 15.6892 14.4975H2.27027C1.86486 14.4975 1.5 14.1726 1.5 13.7259V8.4467C1.5 8.04061 1.17568 7.67513 0.72973 7.67513C0.283784 7.67513 0 8.04061 0 8.48731V13.7665C0 15.0254 1.01351 16.0406 2.27027 16.0406H15.7703C16.9865 16 18 14.9848 18 13.7259ZM13.2162 8.56853L9.44595 11.5736C9.16216 11.7766 8.7973 11.7766 8.51351 11.5736L4.74324 8.56853C4.45946 8.28426 4.41892 7.79695 4.7027 7.51269C4.94595 7.22842 5.35135 7.18782 5.67568 7.39086L8.22973 9.42132V0.771573C8.22973 0.365482 8.55405 0 9 0C9.40541 0 9.77027 0.324873 9.77027 0.771573V9.46193L12.3243 7.43147C12.6081 7.14721 13.0946 7.14721 13.3784 7.47208C13.6622 7.75634 13.6622 8.24365 13.3378 8.52792C13.2973 8.52792 13.2568 8.52792 13.2162 8.56853Z" fill="white" />
                                                                                                </svg>
                                                                                            </span>
                                                                                        </span>
                                                                                        <span>Solucionario</span>
                                                                                    </a>
                                                                                </div>
                                                                                <div>
                                                                                    <a target="_blank" class="m-button m-button--magenta m-button--full m-button--icon m-button--icon--auto" href="<?= \Yii::$app->request->BaseUrl ?>/recursos/matriz/${matriz}">
                                                                                        <span>
                                                                                            <span class="m-icon-svg m-icon-svg--secondary">
                                                                                                <svg width="18" height="16" viewBox="0 0 18 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                                                    <path d="M18 13.7259V8.4467C18 8.04061 17.6757 7.67513 17.2297 7.67513C16.7838 7.67513 16.4595 8 16.4595 8.4467V13.7259C16.4595 14.132 16.1351 14.4975 15.6892 14.4975H2.27027C1.86486 14.4975 1.5 14.1726 1.5 13.7259V8.4467C1.5 8.04061 1.17568 7.67513 0.72973 7.67513C0.283784 7.67513 0 8.04061 0 8.48731V13.7665C0 15.0254 1.01351 16.0406 2.27027 16.0406H15.7703C16.9865 16 18 14.9848 18 13.7259ZM13.2162 8.56853L9.44595 11.5736C9.16216 11.7766 8.7973 11.7766 8.51351 11.5736L4.74324 8.56853C4.45946 8.28426 4.41892 7.79695 4.7027 7.51269C4.94595 7.22842 5.35135 7.18782 5.67568 7.39086L8.22973 9.42132V0.771573C8.22973 0.365482 8.55405 0 9 0C9.40541 0 9.77027 0.324873 9.77027 0.771573V9.46193L12.3243 7.43147C12.6081 7.14721 13.0946 7.14721 13.3784 7.47208C13.6622 7.75634 13.6622 8.24365 13.3378 8.52792C13.2973 8.52792 13.2568 8.52792 13.2162 8.56853Z" fill="white" />
                                                                                                </svg>
                                                                                            </span>
                                                                                        </span>
                                                                                        <span>Matriz</span>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                `;
                            });
                            i++;

                        });


                        $('.lista-actividades').html(actividades);
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}


$('body').on('click', '.btn-seleccionar-experiencia', function (e) {
    e.preventDefault();
    $('.m-intranet--report__tab__button').removeClass('js--active');
    $( ".btn-seleccionar-experiencia" ).each(function( index ) {
        $(this).removeClass('js--active');
    });

    $(this).addClass('js--active');

    var experiencia_id = $(this).attr('data-experiencia_id');
    var experiencia_correlativo = $(this).attr('data-experiencia_correlativo');
    $('.experiencia_mobil').html('Experiencia ' + experiencia_correlativo);
    Actividades(experiencia_id,experiencia_correlativo);
});

</script>