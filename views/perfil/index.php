<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="m-banner m-banner--profile m-banner--profile--tertiary m-banner--intranet">
    <div class="m-banner__bg"></div>
    <div class="m-container">
        <div class="m-breadcrumbs">
            <ul>
                <li><a href="<?= \Yii::$app->request->BaseUrl ?>/panel">Inicio</a></li>
                <li class="css--active"><a href="intranet_profile.html">Mi perfil</a></li>
            </ul>
        </div>
        <div class="m-banner__content">
            <p class="m-banner__title m-h1">Mi perfil</p>
        </div>
        <div class="m-profile__logout">
            <button class="m-button m-button--icon m-button--icon--auto m-modal__button" type="button" data-modal="m-profile-session">
                <span>
                    <span class="m-icon-svg m-icon-svg--secondary">
                        <svg width="19" height="15" viewBox="0 0 19 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M11.2491 11.879C10.8358 11.879 10.4981 12.1605 10.4981 12.5049V13.1308C10.4981 13.4752 10.1604 13.7567 9.74716 13.7567H2.24779C1.83452 13.7567 1.49685 13.4752 1.49685 13.1308V1.87342C1.49685 1.52898 1.83452 1.24755 2.24779 1.24755H9.74716C10.1604 1.24755 10.4981 1.52898 10.4981 1.87342V2.4993C10.4981 2.84374 10.8358 3.12517 11.2491 3.12517C11.6623 3.12517 12 2.84374 12 2.4993V1.87342C12 0.8359 10.992 0 9.7522 0H2.24779C1.00798 0 0 0.840101 0 1.87342V13.1266C0 14.1641 1.00798 15 2.24779 15H9.74716C10.992 15 11.995 14.1599 11.995 13.1266V12.5007C12 12.1563 11.6623 11.879 11.2491 11.879Z" fill="white" />
                            <path d="M17.349 6.30132L15.2263 4.17922C14.9137 3.91207 14.4398 3.94735 14.1675 4.25987C13.9255 4.54215 13.9255 4.95548 14.1675 5.23775L16.0683 7.1431H5.25321C4.83977 7.1431 4.50195 7.48082 4.50195 7.89415C4.50195 8.30748 4.83977 8.6452 5.25321 8.6452H16.0633L14.1675 10.5505C13.8751 10.8429 13.8751 11.3167 14.1675 11.6091C14.4599 11.9014 14.9339 11.9014 15.2263 11.6091L17.349 9.48698C18.2263 8.60487 18.2263 7.18342 17.349 6.30132Z" fill="white" />
                        </svg>
                    </span>
                </span>
                <span>Cerrar sesión</span>
            </button>
        </div>
    </div>
</div>
<div class="m-container m-section">
    <div class="m-profile__detail">

        <div class="m-profile__detail__header m-modal__button" data-modal="m-profile-picture">
            <div class="m-profile__detail__header__picture m-lazy-js" data-bg="<?= \Yii::$app->request->BaseUrl ?>/fotos/<?= Yii::$app->user->identity->foto ?>"></div>
            <div class="m-profile__detail__header__icon">
                <span class="m-icon-svg m-icon-svg--secondary">
                    <svg width="18" height="15" viewBox="0 0 18 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M16.3149 14.2942H1.6098C0.819752 14.2942 0.182617 13.6546 0.182617 12.8616V4.36859C0.182617 3.57556 0.819752 2.93603 1.6098 2.93603H4.64256L4.76999 1.75929C4.79547 1.58022 4.84644 1.40115 4.9229 1.27324L4.99935 1.22208C5.25421 0.889519 5.611 0.710449 5.99328 0.710449H11.9569C12.3137 0.710449 12.6195 0.863937 12.8743 1.11975C13.0018 1.29882 13.1037 1.47789 13.1547 1.7337V1.78487L13.2821 2.93603H16.3149C17.1049 2.93603 17.742 3.57556 17.742 4.36859V12.8872C17.742 13.6546 17.1049 14.2942 16.3149 14.2942ZM1.6098 3.98486C1.38043 3.98486 1.20203 4.16393 1.20203 4.39417V12.8872C1.20203 13.1174 1.38043 13.2965 1.6098 13.2965H16.3403C16.5697 13.2965 16.7481 13.1174 16.7481 12.8872V4.36859C16.7481 4.13835 16.5697 3.95928 16.3403 3.95928H13.1037C12.6705 3.95928 12.3391 3.67789 12.2882 3.24301L12.1353 1.93836C12.1098 1.83603 12.0843 1.81045 12.0843 1.81045C12.0333 1.75929 12.0078 1.7337 11.9314 1.7337H5.9678C5.94231 1.7337 5.89134 1.7337 5.81489 1.81045C5.81489 1.83603 5.7894 1.88719 5.7894 1.93836L5.63649 3.26859C5.58552 3.67789 5.22872 3.98486 4.82096 3.98486H1.6098Z"
                            fill="white" />
                        <path d="M8.94888 12.0176C6.83359 12.0176 5.10059 10.278 5.10059 8.15478C5.10059 6.03153 6.83359 4.29199 8.94888 4.29199C11.0642 4.29199 12.7972 6.03153 12.7972 8.15478C12.7972 10.278 11.0642 12.0176 8.94888 12.0176ZM8.94888 5.31525C7.39427 5.31525 6.12 6.59432 6.12 8.15478C6.12 9.71524 7.39427 10.9943 8.94888 10.9943C10.5035 10.9943 11.7778 9.71524 11.7778 8.15478C11.7778 6.59432 10.5035 5.31525 8.94888 5.31525Z"
                            fill="white" />
                    </svg>
                </span>
            </div>
        </div>
        <hr>
        <div class="m-profile__detail__list">
            <p class="m-h3">Nombre:</p>
            <p class="nombres"></p>
            <button class="m-modal__button" type="button" data-modal="m-profile-edit-name">
                <span class="m-h4">modificar</span>
                <div class="m-profile__detail__icon">
                    <span class="m-icon-svg m-icon-svg--secondary">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M13.9055 2.4883L11.9117 0.494502C11.2524 -0.164834 10.1824 -0.164834 9.52309 0.494502L0.951727 9.0856C0.876713 9.16062 0.821439 9.27511 0.801699 9.38566L0.0120753 13.7483C-0.0274059 13.9378 0.031816 14.1234 0.162104 14.2379C0.292392 14.3682 0.481901 14.4274 0.65167 14.3879L5.01434 13.5983C5.12883 13.5786 5.21964 13.5233 5.3144 13.4483L13.9055 4.85717C14.5648 4.21758 14.5648 3.14369 13.9055 2.4883ZM1.25178 13.1482L1.8519 9.83969L4.56031 12.5481L1.25178 13.1482ZM5.38941 11.794L4.61163 11.0162L4.39448 10.7991L3.60486 10.0095L3.33639 9.74099L2.60994 9.01454L8.73741 2.88311L9.44807 3.59377L9.61389 3.75959L9.73629 3.88199L10.8141 4.95982L11.5208 5.66653L5.38941 11.794ZM13.1159 4.06755L12.3263 4.85717L9.52309 2.09349L10.3127 1.30387C10.5378 1.07882 10.897 1.07882 11.1023 1.30387L13.0961 3.29766C13.3251 3.50297 13.3251 3.8583 13.1159 4.06755Z"
                                fill="#0A0B29" />
                        </svg>
                    </span>
                </div>
            </button>
        </div>
        <div class="m-profile__detail__list">
            <p class="m-h3">Apellido:</p>
            <p class="apellidos"></p>
            <button class="m-modal__button" type="button" data-modal="m-profile-edit-lastname">
                <span class="m-h4">modificar</span>
                <div class="m-profile__detail__icon">
                    <span class="m-icon-svg m-icon-svg--secondary">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M13.9055 2.4883L11.9117 0.494502C11.2524 -0.164834 10.1824 -0.164834 9.52309 0.494502L0.951727 9.0856C0.876713 9.16062 0.821439 9.27511 0.801699 9.38566L0.0120753 13.7483C-0.0274059 13.9378 0.031816 14.1234 0.162104 14.2379C0.292392 14.3682 0.481901 14.4274 0.65167 14.3879L5.01434 13.5983C5.12883 13.5786 5.21964 13.5233 5.3144 13.4483L13.9055 4.85717C14.5648 4.21758 14.5648 3.14369 13.9055 2.4883ZM1.25178 13.1482L1.8519 9.83969L4.56031 12.5481L1.25178 13.1482ZM5.38941 11.794L4.61163 11.0162L4.39448 10.7991L3.60486 10.0095L3.33639 9.74099L2.60994 9.01454L8.73741 2.88311L9.44807 3.59377L9.61389 3.75959L9.73629 3.88199L10.8141 4.95982L11.5208 5.66653L5.38941 11.794ZM13.1159 4.06755L12.3263 4.85717L9.52309 2.09349L10.3127 1.30387C10.5378 1.07882 10.897 1.07882 11.1023 1.30387L13.0961 3.29766C13.3251 3.50297 13.3251 3.8583 13.1159 4.06755Z"
                                fill="#0A0B29" />
                        </svg>
                    </span>
                </div>
            </button>
        </div>
        <div class="m-profile__detail__list">
            <p class="m-h3">DNI:</p>
            <p class="dni"></p>
        </div>
        <div class="m-profile__detail__list">
            <p class="m-h3">I.E:</p>
            <p class="ie"></p>
        </div>
        <div class="m-profile__detail__list">
            <p class="m-h3">Correo:</p>
            <p class="correo"></p>
            <button class="m-modal__button" type="button" data-modal="m-profile-edit-email">
                <span class="m-h4">modificar</span>
                <div class="m-profile__detail__icon">
                    <span class="m-icon-svg m-icon-svg--secondary">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M13.9055 2.4883L11.9117 0.494502C11.2524 -0.164834 10.1824 -0.164834 9.52309 0.494502L0.951727 9.0856C0.876713 9.16062 0.821439 9.27511 0.801699 9.38566L0.0120753 13.7483C-0.0274059 13.9378 0.031816 14.1234 0.162104 14.2379C0.292392 14.3682 0.481901 14.4274 0.65167 14.3879L5.01434 13.5983C5.12883 13.5786 5.21964 13.5233 5.3144 13.4483L13.9055 4.85717C14.5648 4.21758 14.5648 3.14369 13.9055 2.4883ZM1.25178 13.1482L1.8519 9.83969L4.56031 12.5481L1.25178 13.1482ZM5.38941 11.794L4.61163 11.0162L4.39448 10.7991L3.60486 10.0095L3.33639 9.74099L2.60994 9.01454L8.73741 2.88311L9.44807 3.59377L9.61389 3.75959L9.73629 3.88199L10.8141 4.95982L11.5208 5.66653L5.38941 11.794ZM13.1159 4.06755L12.3263 4.85717L9.52309 2.09349L10.3127 1.30387C10.5378 1.07882 10.897 1.07882 11.1023 1.30387L13.0961 3.29766C13.3251 3.50297 13.3251 3.8583 13.1159 4.06755Z"
                                fill="#0A0B29" />
                        </svg>
                    </span>
                </div>
            </button>
        </div>
        <div class="m-profile__detail__list">
            <p class="m-h3">Celular:</p>
            <p class="celular"></p>
            <button class="m-modal__button" type="button" data-modal="m-profile-edit-phone">
                <span class="m-h4">modificar</span>
                <div class="m-profile__detail__icon">
                    <span class="m-icon-svg m-icon-svg--secondary">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M13.9055 2.4883L11.9117 0.494502C11.2524 -0.164834 10.1824 -0.164834 9.52309 0.494502L0.951727 9.0856C0.876713 9.16062 0.821439 9.27511 0.801699 9.38566L0.0120753 13.7483C-0.0274059 13.9378 0.031816 14.1234 0.162104 14.2379C0.292392 14.3682 0.481901 14.4274 0.65167 14.3879L5.01434 13.5983C5.12883 13.5786 5.21964 13.5233 5.3144 13.4483L13.9055 4.85717C14.5648 4.21758 14.5648 3.14369 13.9055 2.4883ZM1.25178 13.1482L1.8519 9.83969L4.56031 12.5481L1.25178 13.1482ZM5.38941 11.794L4.61163 11.0162L4.39448 10.7991L3.60486 10.0095L3.33639 9.74099L2.60994 9.01454L8.73741 2.88311L9.44807 3.59377L9.61389 3.75959L9.73629 3.88199L10.8141 4.95982L11.5208 5.66653L5.38941 11.794ZM13.1159 4.06755L12.3263 4.85717L9.52309 2.09349L10.3127 1.30387C10.5378 1.07882 10.897 1.07882 11.1023 1.30387L13.0961 3.29766C13.3251 3.50297 13.3251 3.8583 13.1159 4.06755Z"
                                fill="#0A0B29" />
                        </svg>
                    </span>
                </div>
            </button>
        </div>
    </div>
</div>
<div class="m-modal m-modal--session">
    <div class="m-modal__content m-modal__animation" id="m-profile-edit-name">
        <div>
            <div class="m-modal__container">
                <div class="m-modal__scene js--active" id="m-modal__scene__name_one">
                    <div class="m-modal__picture"><span class="m-icon-svg m-icon-svg--secondary"><svg width="96"
                                height="96" viewBox="0 0 96 96" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="48" cy="48" r="48" fill="#85D8E3" />
                                <path
                                    d="M66.6884 25.5792C69.9239 28.1299 70.8025 32.549 68.5776 35.4487L44.028 67.4364L31.8943 71.7263L32.2908 57.933L56.8403 25.9454C58.9897 23.136 63.453 23.0285 66.6884 25.5792Z"
                                    fill="#E0B17E" />
                                <path
                                    d="M32.4759 57.4737L56.8403 25.9454C58.9897 23.136 63.453 23.0285 66.6884 25.5792C69.9239 28.1299 70.8025 32.549 68.6531 35.3584L44.4058 66.9844L32.4759 57.4737Z"
                                    fill="#FFE100" />
                                <path opacity="0.5"
                                    d="M68.4269 35.6281L44.4062 66.983L40.0004 63.4541L64.0211 32.0992L68.4269 35.6281Z"
                                    fill="#FFAD12" />
                                <path
                                    d="M49.6558 34.9718L61.5615 44.502L67.2738 37.1525L55.3681 27.6223L49.6558 34.9718Z"
                                    fill="#696969" />
                                <path
                                    d="M69.56 34.2733C71.1805 32.0967 69.9242 28.1295 66.6888 25.5788C63.4533 23.028 59.4432 22.5933 57.6301 24.7626L56.4969 26.1183L68.3098 35.5313L69.56 34.2733Z"
                                    fill="#FF7156" />
                                <path
                                    d="M56.5739 26.0283L68.1526 35.2456C68.6208 35.6369 68.8623 36.2993 68.4845 36.7513L68.409 36.8416C68.1068 37.2032 67.4535 37.2713 66.8683 36.7822L55.2896 27.5649C54.8215 27.1736 54.58 26.5112 54.9577 26.0592L55.0332 25.9688C55.3354 25.6073 55.9887 25.5392 56.5739 26.0283Z"
                                    fill="#838383" />
                                <path d="M37.0226 69.8713L31.8942 71.7259L31.9902 65.665L37.0226 69.8713Z"
                                    fill="#494949" /></svg></span></div>
                    <p class="m-h3 m-h3-2">Modificar nombre</p>
                    <?php $form = ActiveForm::begin(['action'=>'perfil/update-nombres','options' => ['id' => 'formNombres']]); ?>
                        <div class="m-input"><input id="nombres" type="text" placeholder="Ingresa tu nombre"
                        name="Docente[nombres]"></div><button
                            class="m-button m-button--secondary m-button--full m-modal__scene--js btn-grabar-nombres" type="button"
                            data-scene-in="m-modal__scene__name_two"
                            data-scene-out="m-modal__scene__name_one"><span>Guardar</span></button>
                    <?php ActiveForm::end(); ?>
                    
                    <span
                        class="m-button m-button--transparent-disable m-button--full m-modal__close--js"><span>Cancelar</span></span>
                </div>
                <div class="m-modal__scene" id="m-modal__scene__name_two">
                    <div class="m-modal__picture"><span class="m-icon-svg m-icon-svg--secondary"><svg width="96"
                                height="96" viewBox="0 0 96 96" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="48" cy="48" r="48" fill="#4F9C2E" />
                                <path
                                    d="M43.9048 63C43.1538 63 42.4027 62.6653 41.8019 62.0795L32.9397 52.7074C31.738 51.4523 31.6629 49.2766 32.8646 47.9377C33.9911 46.5989 35.9438 46.5152 37.1455 47.8541L43.8297 54.9668L61.7794 34.047C62.906 32.7081 64.8587 32.6244 66.0603 33.9633C67.262 35.2185 67.3371 37.3941 66.1354 38.733L46.0828 61.9958C45.482 62.6653 44.7309 63 43.9048 63Z"
                                    fill="white" /></svg></span></div>
                    <p>Tu nombre fue guardado correctamente</p>
                </div>
                <div class="m-modal__close m-modal__close--js"></div>
            </div>
        </div>
    </div>
</div>
<div class="m-modal m-modal--session">
    <div class="m-modal__content m-modal__animation" id="m-profile-edit-lastname">
        <div>
            <div class="m-modal__container">
                <div class="m-modal__scene js--active" id="m-modal__scene__lastname_one">
                    <div class="m-modal__picture">
                        <span class="m-icon-svg m-icon-svg--secondary">
                            <svg width="96" height="96" viewBox="0 0 96 96" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="48" cy="48" r="48" fill="#85D8E3" />
                                <path d="M66.6884 25.5792C69.9239 28.1299 70.8025 32.549 68.5776 35.4487L44.028 67.4364L31.8943 71.7263L32.2908 57.933L56.8403 25.9454C58.9897 23.136 63.453 23.0285 66.6884 25.5792Z"
                                    fill="#E0B17E" />
                                <path d="M32.4759 57.4737L56.8403 25.9454C58.9897 23.136 63.453 23.0285 66.6884 25.5792C69.9239 28.1299 70.8025 32.549 68.6531 35.3584L44.4058 66.9844L32.4759 57.4737Z"
                                    fill="#FFE100" />
                                <path opacity="0.5" d="M68.4269 35.6281L44.4062 66.983L40.0004 63.4541L64.0211 32.0992L68.4269 35.6281Z"
                                    fill="#FFAD12" />
                                <path d="M49.6558 34.9718L61.5615 44.502L67.2738 37.1525L55.3681 27.6223L49.6558 34.9718Z"
                                    fill="#696969" />
                                <path d="M69.56 34.2733C71.1805 32.0967 69.9242 28.1295 66.6888 25.5788C63.4533 23.028 59.4432 22.5933 57.6301 24.7626L56.4969 26.1183L68.3098 35.5313L69.56 34.2733Z"
                                    fill="#FF7156" />
                                <path d="M56.5739 26.0283L68.1526 35.2456C68.6208 35.6369 68.8623 36.2993 68.4845 36.7513L68.409 36.8416C68.1068 37.2032 67.4535 37.2713 66.8683 36.7822L55.2896 27.5649C54.8215 27.1736 54.58 26.5112 54.9577 26.0592L55.0332 25.9688C55.3354 25.6073 55.9887 25.5392 56.5739 26.0283Z"
                                    fill="#838383" />
                                <path d="M37.0226 69.8713L31.8942 71.7259L31.9902 65.665L37.0226 69.8713Z"
                                    fill="#494949" />
                            </svg>
                        </span>
                    </div>
                    <p class="m-h3 m-h3-2">Modificar apellido</p>
                    <?php $form = ActiveForm::begin(['action'=>'perfil/update-apellido','options' => ['id' => 'formApellido']]); ?>
                        <div class="m-input">
                            <input id="apellido_paterno" type="text" placeholder="Ingresa tu apellido" name="Docente[apellido_paterno]">
                        </div>
                        <button class="m-button m-button--secondary m-button--full m-modal__scene--js btn-grabar-apellido" type="button" data-scene-in="m-modal__scene__lastname_two" data-scene-out="m-modal__scene__lastname_one">
                            <span>Guardar</span>
                        </button>
                    <?php ActiveForm::end(); ?>
                    <span class="m-button m-button--transparent-disable m-button--full m-modal__close--js"><span>Cancelar</span></span>
                </div>
                <div class="m-modal__scene" id="m-modal__scene__lastname_two">
                    <div class="m-modal__picture">
                        <span class="m-icon-svg m-icon-svg--secondary">
                            <svg width="96" height="96" viewBox="0 0 96 96" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="48" cy="48" r="48" fill="#4F9C2E" />
                                <path d="M43.9048 63C43.1538 63 42.4027 62.6653 41.8019 62.0795L32.9397 52.7074C31.738 51.4523 31.6629 49.2766 32.8646 47.9377C33.9911 46.5989 35.9438 46.5152 37.1455 47.8541L43.8297 54.9668L61.7794 34.047C62.906 32.7081 64.8587 32.6244 66.0603 33.9633C67.262 35.2185 67.3371 37.3941 66.1354 38.733L46.0828 61.9958C45.482 62.6653 44.7309 63 43.9048 63Z"
                                    fill="white" />
                            </svg>
                        </span>
                    </div>
                    <p>Tu apellido fue guardado correctamente</p>
                </div>
                <div class="m-modal__close m-modal__close--js"></div>
            </div>
        </div>
    </div>
</div>

<div class="m-modal m-modal--session">
    <div class="m-modal__content m-modal__animation" id="m-profile-session">
        <div>
            <div class="m-modal__container">
                <div class="m-modal__picture">
                    <span class="m-icon-svg m-icon-svg--secondary">
                        <svg width="96" height="96" viewBox="0 0 96 96" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M48 96C74.5097 96 96 74.5097 96 48C96 21.4903 74.5097 0 48 0C21.4903 0 0 21.4903 0 48C0 74.5097 21.4903 96 48 96Z" fill="white" />
                            <path opacity="0.8" d="M48 96C74.5097 96 96 74.5097 96 48C96 21.4903 74.5097 0 48 0C21.4903 0 0 21.4903 0 48C0 74.5097 21.4903 96 48 96Z" fill="#67CEDC" />
                            <path d="M64.7992 71.5578H49.0992C48.2992 71.5578 47.6992 70.9578 47.6992 70.1578V26.1578C47.6992 25.3578 48.2992 24.7578 49.0992 24.7578H64.7992C65.5992 24.7578 66.1992 25.3578 66.1992 26.1578V70.1578C66.1992 70.9578 65.5992 71.5578 64.7992 71.5578Z" fill="#0A0B29" />
                            <path d="M73.284 37.6308L84.6193 48.8859C85.1269 49.3615 85.1269 50.0749 84.6193 50.5504L73.284 61.8848C72.4381 62.6774 71 62.1226 71 61.0129V38.5027C71 37.3931 72.4381 36.8382 73.284 37.6308Z" fill="white" />
                            <path d="M71 43.2578H60C59.4477 43.2578 59 43.7055 59 44.2578V55.2578C59 55.8101 59.4477 56.2578 60 56.2578H71C71.5523 56.2578 72 55.8101 72 55.2578V44.2578C72 43.7055 71.5523 43.2578 71 43.2578Z" fill="white" />
                            <path d="M23 24.4579V71.2579L47.8 79.3579C49.1 79.7579 50.3 78.8579 50.3 77.5579V18.9579C50.3 17.6579 49 16.7579 47.8 17.0579L23 24.4579Z" fill="#565777" />
                            <path d="M43.5 47.2578C44.8807 47.2578 46 46.1385 46 44.7578C46 43.3771 44.8807 42.2578 43.5 42.2578C42.1193 42.2578 41 43.3771 41 44.7578C41 46.1385 42.1193 47.2578 43.5 47.2578Z" fill="#0A0B29" />
                        </svg>
                    </span>
                </div>
                <p class="m-h3 m-h3-2">Cerrar sesión</p>
                <p>¿Está seguro que desea cerrar su sesión en el Portal Docente?</p>
                <a class="m-button m-button--secondary m-button--full" href="<?= \Yii::$app->request->BaseUrl ?>/login/logout">
                    <span>Confirmar</span>
                </a>
                <span class="m-button m-button--transparent-disable m-button--full m-modal__close--js">
                    <span>Cancelar</span>
                </span>
                <div class="m-modal__close m-modal__close--js"></div>
            </div>
        </div>
    </div>
</div>



<div class="m-modal m-modal--session">
    <div class="m-modal__content m-modal__animation" id="m-profile-picture">
        <div>
            <div class="m-modal__container">
                <div class="m-modal__scene js--active" id="m-modal__scene__profile_one">
                    <div class="m-modal__picture">
                        <span class="m-icon-svg m-icon-svg--secondary">
                            <svg width="96" height="96" viewBox="0 0 96 96" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M48 96C74.5097 96 96 74.5097 96 48C96 21.4903 74.5097 0 48 0C21.4903 0 0 21.4903 0 48C0 74.5097 21.4903 96 48 96Z" fill="white" />
                                <path opacity="0.8" d="M48 96C74.5097 96 96 74.5097 96 48C96 21.4903 74.5097 0 48 0C21.4903 0 0 21.4903 0 48C0 74.5097 21.4903 96 48 96Z" fill="#67CEDC" />
                                <path d="M70.45 19H25C23.8954 19 23 19.8954 23 21V75.1765C23 76.281 23.8954 77.1765 25 77.1765H70.45C71.5546 77.1765 72.45 76.281 72.45 75.1765V21C72.45 19.8954 71.5546 19 70.45 19Z" fill="white" />
                                <path d="M65.4631 24.1113H29.9277C28.8232 24.1113 27.9277 25.0068 27.9277 26.1113V62.5704C27.9277 63.675 28.8232 64.5704 29.9277 64.5704H65.4631C66.5677 64.5704 67.4631 63.675 67.4631 62.5704V26.1113C67.4631 25.0068 66.5677 24.1113 65.4631 24.1113Z" fill="#E6F4F8" />
                                <g opacity="0.8">
                                    <path d="M40.9821 43.809C39.7505 42.0231 39.3194 39.8062 39.7505 37.5892C40.1816 35.3723 41.4132 33.5864 43.3222 32.3548C44.677 31.3695 46.2166 31 47.8793 31C48.4335 31 48.9878 31 49.542 31.1232C53.9759 31.9237 56.9934 36.3576 56.0081 40.9146C55.3307 44.24 52.8674 46.7033 49.542 47.3807C48.4335 47.627 47.3251 47.627 46.4013 47.3807C44.1844 46.9496 42.2138 45.718 40.9821 43.809Z" fill="#0A0B29" />
                                    <path d="M64.5685 64.5084H31.3145V61.6757C31.3145 54.8401 36.8568 49.3594 43.6308 49.3594H52.1906C59.0262 49.3594 64.5069 54.9017 64.5069 61.6757V64.5084H64.5685Z" fill="#0A0B29" />
                                </g>
                            </svg>
                        </span>
                    </div>
                    <p class="m-h3 m-h3-2">Foto de perfil</p>
                    <p>Sube una foto tuya para personalizar tu perfil</p>
                    <?php $form = ActiveForm::begin(['action'=>'perfil/update-foto','options' => ['id' => 'formFoto']]); ?>
                        <div class="m-input">
                            <div class="m-input--file">
                                <input type="hidden" id="temp" name="Docente[temp]" value="1">
                                <input id="picture" type="file" name="Docente[archivo]" data-modal="m-profile-picture"> 
                                <label for="picture">
                                    <span class="m-icon-svg m-icon-svg--secondary">
                                        <svg width="18" height="16" viewBox="0 0 18 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M17.9962 13.6897V8.42052C17.9962 8.0152 17.6719 7.65041 17.2261 7.65041C16.7802 7.65041 16.456 7.97467 16.456 8.42052V13.6897C16.456 14.095 16.1317 14.4598 15.6859 14.4598H2.26979C1.86447 14.4598 1.49968 14.1355 1.49968 13.6897V8.42052C1.49968 8.0152 1.17543 7.65041 0.729576 7.65041C0.283724 7.65041 0 8.0152 0 8.46105V13.7302C0 14.9867 1.0133 16 2.26979 16H15.7669C16.9829 15.9595 17.9962 14.9462 17.9962 13.6897ZM13.2134 3.15136C13.254 3.19189 13.2945 3.19189 13.2945 3.23243C13.5782 3.51615 13.6187 4.00253 13.335 4.28626C13.0513 4.56998 12.5649 4.61051 12.2812 4.32679L9.72768 2.30019V10.974C9.72768 11.3794 9.40342 11.7441 8.95757 11.7441C8.55225 11.7441 8.18746 11.4199 8.18746 10.974V2.30019L5.63394 4.32679C5.30969 4.52945 4.90437 4.48892 4.66118 4.20519C4.37745 3.92147 4.37745 3.43509 4.70171 3.15136L8.47118 0.151995C8.75491 -0.050665 9.1197 -0.050665 9.40342 0.151995L13.2134 3.15136Z" fill="white" />
                                        </svg>
                                    </span>
                                    <span>Subir foto</span>
                                </label>
                            </div>
                        </div>
                        <button class="m-button m-button--green btn-grabar-fotos" disabled="true" data-scene-in="m-modal__scene__foto_two">Confirmar</button>
                        <!-- <input class="m-button m-button--green" type="submit" value="Confirmar" disabled="true"> -->
                    <?php ActiveForm::end(); ?>
                    <span class="m-button m-button--transparent-disable m-button--full m-modal__close--js">
                        <span>Cancelar</span>
                    </span>
                </div>

                <div class="m-modal__scene" id="m-modal__scene__profile_two">
                    <div class="m-modal__picture">
                        <span class="m-icon-svg m-icon-svg--secondary">
                            <svg width="96" height="96" viewBox="0 0 96 96" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="48" cy="48" r="48" fill="#4F9C2E" />
                                <path d="M43.9048 63C43.1538 63 42.4027 62.6653 41.8019 62.0795L32.9397 52.7074C31.738 51.4523 31.6629 49.2766 32.8646 47.9377C33.9911 46.5989 35.9438 46.5152 37.1455 47.8541L43.8297 54.9668L61.7794 34.047C62.906 32.7081 64.8587 32.6244 66.0603 33.9633C67.262 35.2185 67.3371 37.3941 66.1354 38.733L46.0828 61.9958C45.482 62.6653 44.7309 63 43.9048 63Z" fill="white" />
                            </svg>
                        </span>
                    </div>
                    <p>Tu foto fue subida correctamente</p>
                </div>
                <div class="m-modal__close btn-cerrando-refresh"></div>
            </div>
            

        </div>
    </div>
</div>



<div class="m-modal m-modal--session">
    <div class="m-modal__content m-modal__animation" id="m-profile-edit-email">
        <div>
            <div class="m-modal__container">
                <div class="m-modal__scene js--active" id="m-modal__scene__one">
                    <div class="m-modal__picture"><span class="m-icon-svg m-icon-svg--secondary"><svg width="96"
                                height="96" viewBox="0 0 96 96" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M48 96C74.5097 96 96 74.5097 96 48C96 21.4903 74.5097 0 48 0C21.4903 0 0 21.4903 0 48C0 74.5097 21.4903 96 48 96Z"
                                    fill="white" />
                                <path opacity="0.8"
                                    d="M48 96C74.5097 96 96 74.5097 96 48C96 21.4903 74.5097 0 48 0C21.4903 0 0 21.4903 0 48C0 74.5097 21.4903 96 48 96Z"
                                    fill="#67CEDC" />
                                <path opacity="0.9"
                                    d="M51.0631 52.1666C49.5631 53.3333 47.3964 53.3333 45.8964 52.1666L22.3965 33V64.9999H74.563V33L51.0631 52.1666Z"
                                    fill="white" />
                                <path
                                    d="M22.3965 31L46.2298 50.5C47.3964 51.4999 49.3964 51.4999 50.5631 50.5L74.3964 31H22.3965Z"
                                    fill="white" /></svg></span></div>
                    <p class="m-h3 m-h3-2">Actualizar correo</p>
                    <p>Ingresa en la casilla tu nuevo correo electrónico de contacto:</p>
                    <?php $form = ActiveForm::begin(['action'=>'perfil/update-correo','options' => ['id' => 'formCorreo']]); ?>
                        <div class="m-input">
                            <input id="correo_electronico" name="Docente[correo_electronico]" type="email" placeholder="Nuevo correo" >
                        </div>
                        <button class="m-button m-button--secondary m-button--full m-modal__scene--js btn-grabar-correo" type="button" data-scene-in="m-modal__scene__two" data-scene-out="m-modal__scene__one">
                            <span>Modificar correo</span>
                        </button>
                    <?php ActiveForm::end(); ?>
                    
                    <span
                        class="m-button m-button--transparent-disable m-button--full m-modal__close--js"><span>Cancelar</span></span>
                </div>
                <div class="m-modal__scene" id="m-modal__scene__two">
                    <div class="m-modal__picture"><span class="m-icon-svg m-icon-svg--secondary"><svg width="96"
                                height="96" viewBox="0 0 96 96" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="48" cy="48" r="48" fill="#4F9C2E" />
                                <path
                                    d="M43.9048 63C43.1538 63 42.4027 62.6653 41.8019 62.0795L32.9397 52.7074C31.738 51.4523 31.6629 49.2766 32.8646 47.9377C33.9911 46.5989 35.9438 46.5152 37.1455 47.8541L43.8297 54.9668L61.7794 34.047C62.906 32.7081 64.8587 32.6244 66.0603 33.9633C67.262 35.2185 67.3371 37.3941 66.1354 38.733L46.0828 61.9958C45.482 62.6653 44.7309 63 43.9048 63Z"
                                    fill="white" /></svg></span></div>
                    <p>Tu correo electrónico fue cambiado exitosamente</p>
                </div>
                <div class="m-modal__close m-modal__close--js"></div>
            </div>
        </div>
    </div>
</div>
<div class="m-modal m-modal--session">
    <div class="m-modal__content m-modal__animation" id="m-profile-edit-phone">
        <div>
            <div class="m-modal__container">
                <div class="m-modal__scene js--active" id="m-modal__scene__phone_one">
                    <div class="m-modal__picture">
                        <span class="m-icon-svg m-icon-svg--secondary">
                            <svg width="96" height="96" viewBox="0 0 96 96" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M48 96C74.5097 96 96 74.5097 96 48C96 21.4903 74.5097 0 48 0C21.4903 0 0 21.4903 0 48C0 74.5097 21.4903 96 48 96Z" fill="white" />
                                <path opacity="0.8" d="M48 96C74.5097 96 96 74.5097 96 48C96 21.4903 74.5097 0 48 0C21.4903 0 0 21.4903 0 48C0 74.5097 21.4903 96 48 96Z" fill="#67CEDC" />
                                <path d="M61.5227 77.6667H34.4773C32.5455 77.6667 31 76.0811 31 74.0991V22.5676C31 20.5856 32.5455 19 34.4773 19H61.5227C63.4545 19 65 20.5856 65 22.5676V73.967C65 76.0811 63.4545 77.6667 61.5227 77.6667Z" fill="#002B44" />
                                <path d="M63 26H33V67H63V26Z" fill="#E6F4F8" />
                                <path d="M51.8635 23.203H44.265C43.8787 23.203 43.6211 22.9363 43.6211 22.5363C43.6211 22.1363 43.8787 21.8696 44.265 21.8696H51.8635C52.2499 21.8696 52.5075 22.1363 52.5075 22.5363C52.5075 22.9363 52.1211 23.203 51.8635 23.203Z" fill="#67CEDC" />
                            </svg>
                        </span>
                    </div>
                    <p class="m-h3 m-h3-2">Actualizar celular</p>
                    <p>Ingresa en la casilla tu nuevo número celular de contacto:</p>
                    <?php $form = ActiveForm::begin(['action'=>'perfil/update-celular','options' => ['id' => 'formCelular']]); ?>
                        <div class="m-input">
                            <input id="celular" name="Docente[celular]" type="text" placeholder="Nuevo celular" >
                        </div>
                        <button class="m-button m-button--secondary m-button--full m-modal__scene--js btn-grabar-celular" type="button" data-scene-in="m-modal__scene__phone_two" data-scene-out="m-modal__scene__phone_one">
                            <span>Modificar celular</span>
                        </button>
                    <?php ActiveForm::end(); ?>
                    <span class="m-button m-button--transparent-disable m-button--full m-modal__close--js">
                        <span>Cancelar</span>
                    </span>
                </div>
                <div class="m-modal__scene" id="m-modal__scene__phone_two">
                    <div class="m-modal__picture">
                        <span class="m-icon-svg m-icon-svg--secondary">
                            <svg width="96" height="96" viewBox="0 0 96 96" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="48" cy="48" r="48" fill="#4F9C2E" />
                                <path d="M43.9048 63C43.1538 63 42.4027 62.6653 41.8019 62.0795L32.9397 52.7074C31.738 51.4523 31.6629 49.2766 32.8646 47.9377C33.9911 46.5989 35.9438 46.5152 37.1455 47.8541L43.8297 54.9668L61.7794 34.047C62.906 32.7081 64.8587 32.6244 66.0603 33.9633C67.262 35.2185 67.3371 37.3941 66.1354 38.733L46.0828 61.9958C45.482 62.6653 44.7309 63 43.9048 63Z" fill="white" />
                            </svg>
                        </span>
                    </div>
                    <p>Tu número celular fue cambiado exitosamente</p>
                </div>
                <div class="m-modal__close m-modal__close--js"></div>
            </div>
        </div>
    </div>
</div>


<script>

var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
async function informacionGeneral(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/perfil/get-informacion-general',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(xhr, settings)
                {
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        $('.nombres').html(results.informacionGeneral[0].nombres);
                        $('.apellidos').html(results.informacionGeneral[0].apellido_paterno + ' ' + results.informacionGeneral[0].apellido_materno);
                        $('.dni').html(results.informacionGeneral[0].dni);
                        $('.ie').html(results.informacionGeneral[0].nombre_ie);
                        $('.correo').html(results.informacionGeneral[0].correo_electronico);
                        $('.celular').html(results.informacionGeneral[0].celular);

                        //var form = $('#formDocente');
                        //form.loadJSON(results.informacionGeneral);
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}

informacionGeneral();

//modificar
$('body').on('click', '.btn-modificar-correo', function (e) {
    e.preventDefault();
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/perfil/update-correo',function(){
        //informacionGeneral()
    });
    $('#modal').modal('show');
});

$('body').on('click', '.btn-modificar-celular', function (e) {
    e.preventDefault();
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/perfil/update-celular',function(){
        //informacionGeneral()
    });
    $('#modal').modal('show');
});

$('body').on('click', '.btn-modificar-foto', function (e) {
    e.preventDefault();
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/perfil/update-foto',function(){
        //informacionGeneral()
    });
    $('#modal').modal('show');
});

$('body').on('click', '.btn-grabar-nombres', function (e) {

    e.preventDefault();
    var form = $('#formNombres');
    var formData = $('#formNombres').serializeArray();
    if (form.find('.has-error').length) {
        return false;
    }

    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success: function (results) {
            if(results.success){
                informacionGeneral()
            }
        },
    });
});

$('body').on('click', '.btn-grabar-apellido', function (e) {

    e.preventDefault();
    var form = $('#formApellido');
    var formData = $('#formApellido').serializeArray();
    if (form.find('.has-error').length) {
        return false;
    }
    console.log(formData);
    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success: function (results) {
            if(results.success){
                informacionGeneral()
            }
        },
    });
});


$('body').on('click', '.btn-grabar-correo', function (e) {

    e.preventDefault();
    var form = $('#formCorreo');
    var formData = $('#formCorreo').serializeArray();
    if (form.find('.has-error').length) {
        return false;
    }
    console.log(formData);
    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success: function (results) {
            if(results.success){
                informacionGeneral()
            }
        },
    });
});


$('body').on('click', '.btn-grabar-celular', function (e) {

    e.preventDefault();

    var form = $('#formCelular');
    var formData = $('#formCelular').serializeArray();
    if (form.find('.has-error').length) {
        return false;
    }
    console.log(form);
    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success: function (results) {
            if(results.success){
                informacionGeneral()
            }
        },
    });
});


$('body').on('click', '.btn-grabar-fotos', function (e) {
    e.preventDefault();
    var form = $('#formFoto');
    
    console.log( document.getElementById('picture'));
	var form_data = new FormData();
	form_data.append("_csrf", csrf);
    form_data.append("Docente[archivo]", document.getElementById('picture').files[0]);
    form_data.append("Docente[temp]", $('#temp').val());

    $.ajax({
        url:form.attr("action"),
		type: 'POST',
		cache: false,
		contentType: false,
		processData: false,
		data: form_data,
		dataType:'Json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert('No hay conectividad con el sistema');
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        success: function (results) {
            if(results.success){
                // $('.m-modal--profile').removeClass('js--open');
               
                $('#picture').val('');
                $('.m-input--file label span:eq(1)').html('Subir foto');
                $('.btn-grabar-fotos').attr('disabled',true);
                $('#foto-perfil').attr('src', '<?= \Yii::$app->request->BaseUrl ?>/fotos/' + results.foto);
                $('#m-modal__scene__profile_one').removeClass('js--active');
                $('#m-modal__scene__profile_two').addClass('js--active');

                // location.reload();
                //toastr.success('Foto subida ');
            }
        },
    });
});

$('body').on('click', '.btn-cerrando-refresh', function (e) {
    e.preventDefault();
    location.reload();
});


</script>