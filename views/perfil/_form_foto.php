<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['options' => ['id' => 'formDocente']]); ?>
<!-- 
    <div class="modal-header">
        
        
    </div> -->
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <h4 class="modal-title"><?= $model->titulo ?> </h4> 
                <small>Sube una foto tuya para personalizar tu perfil</small> 
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-12">
                <div class="form-group">
                    <input type="hidden" id="temp" name="Docente[temp]" value="1">
                    <input type="file" id="archivo" name="Docente[archivo]" class="form-control" >
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <button type="button" class="btn btn-block btn-primary btn-grabar-fotos">Confirmar</button>
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-12">
                <button type="button" class="btn btn-block btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
    <!-- <div class="modal-footer justify-content-between">
        
    </div> -->

<?php ActiveForm::end(); ?>