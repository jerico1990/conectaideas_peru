
<style>
.filemanager-sidebar{
    max-width:450px;
}
</style>
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0 font-size-18">Base de datos</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Base de datos</a></li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">
        <div class="card filemanager-sidebar mr-md-8">
            <div class="card-body">

                <div class="d-flex flex-column h-100">
                    <div class="mb-4">
                        <ul class="list-unstyled categories-list">
                            <li>
                                <div class="custom-accordion">
                                    <a class="text-body font-weight-medium py-1 d-flex align-items-center" data-toggle="collapse" href="#categories-collapse" role="button" aria-expanded="false" aria-controls="categories-collapse">
                                        <i class="mdi mdi-folder font-size-16 text-warning mr-2"></i> Docentes
                                        <i class="mdi mdi-chevron-up accor-down-icon ml-auto"></i>
                                    </a>
                                    <div class="collapse show" id="categories-collapse">
                                        <div class="card border-0 shadow-none pl-2 mb-0">
                                            <ul class="list-unstyled mb-0">
                                                <li><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/reporte-registros-docentes-safari" class="d-flex align-items-center"><span class="mr-auto">Docentes registrados</span> <i class="mdi  accor-down-icon"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="custom-accordion">
                                    <a class="text-body font-weight-medium py-1 d-flex align-items-center" data-toggle="collapse" href="#categories-collapse" role="button" aria-expanded="false" aria-controls="categories-collapse">
                                        <i class="mdi mdi-folder font-size-16 text-warning mr-2"></i> Estudiantes
                                        <i class="mdi mdi-chevron-up accor-down-icon ml-auto"></i>
                                    </a>
                                    <div class="collapse show" id="categories-collapse">
                                        <div class="card border-0 shadow-none pl-2 mb-0">
                                            <ul class="list-unstyled mb-0">
                                                <li><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/reporte-registros-estudiantes-agregados" class="d-flex align-items-center"><span class="mr-auto">Estudiantes con registro en espera</span> <i class="mdi  accor-down-icon"></i></a></li>
                                                <li><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/reporte-registros-estudiantes-eliminados" class="d-flex align-items-center"><span class="mr-auto">Estudiantes eliminados</span> <i class="mdi  accor-down-icon"></i></a></li>
                                                <li><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/reporte-registros-estudiantes" class="d-flex align-items-center"><span class="mr-auto">Base de datos completa de estudiantes registrados</span> <i class="mdi  accor-down-icon"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="custom-accordion">
                                    <a class="text-body font-weight-medium py-1 d-flex align-items-center" data-toggle="collapse" href="#categories-collapse" role="button" aria-expanded="false" aria-controls="categories-collapse">
                                        <i class="mdi mdi-folder font-size-16 text-warning mr-2"></i> Recursos
                                        <i class="mdi mdi-chevron-up accor-down-icon ml-auto"></i>
                                    </a>
                                    <div class="collapse show" id="categories-collapse">
                                        <div class="card border-0 shadow-none pl-2 mb-0">
                                            <ul class="list-unstyled mb-0">
                                                <li><a href="#" class="d-flex align-items-center"><span class="mr-auto">Recursos cargados en la plataforma (Proximamente)</span> <i class="mdi  accor-down-icon"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            
                        </ul>
                    </div>

                </div>

            </div>
        </div>
<!-- 
        <div class="card">
            <div class="card-body text-center">
                <h5 class="mb-0 mt-3 text-primary">Registro docentes</h5>
                <div class="row">
                    <div class="col-md-4"></div>

                    <div class="col-md-4">
                        <div class="color-box bg-primary p-4 mt-3 rounded">
                            <a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/reporte-registros-docentes-safari"><h5 class="my-2 text-white"> Descargar </h5></a>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
    </div> <!-- end col -->
</div> <!-- end row -->