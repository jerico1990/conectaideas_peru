
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0 font-size-18">Reporte Semanal</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reporte</a></li>
                    <li class="breadcrumb-item active">Reporte Semanal</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Información cargada</h2>   
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <!-- <h4 class="card-title">Default Datatable</h4> <button class="btn btn-success btn-agregar-estudiante btn-habilitar-masivamente-estudiante">Agregar</button> -->
                            <a href="#" class="btn btn-success btn-procesar disabled">Procesar</a>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <table id="lista-reporte01" class="table table-bordered dt-responsive ">
                            <thead>
                                <th>Reporte</th>
                                <th>Registros totales</th>
                                <th>Nuevos por procesar</th>
                            </thead>
                            <tbody>
                            
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->


<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Resultados</h2>   
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/reporte-resultados-sincronizacion" class="btn btn-success btn-descargar-resultados ">Resultados</a>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <table id="lista-reporte02" class="table table-bordered dt-responsive ">
                            <thead>
                                <th>Reporte</th>
                                <th>Pegistros procesados</th>
                                <th>Registros con error</th>
                            </thead>
                            <tbody>
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->

<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";

ReportePorProcesar();
async function ReportePorProcesar(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/reporte/get-lista-reporte-semanal-por-procesar',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var lista01 ="";
                        var cantidad = 0;
                        $('#lista-reporte01').DataTable().destroy();
                        $.each(results.reportes, function( index, value ) {
                            lista01 = lista01 + "<tr>";
                                lista01 = lista01 + "<td>" + value.reporte + "</td>";
                                lista01 = lista01 + "<td>" + value.cantidad_total + "</td>";
                                lista01 = lista01 + "<td>" + value.cantidad_por_procesar + "</td>";
                            lista01 = lista01 + "</tr>";
                            cantidad = cantidad + parseInt(value.cantidad_por_procesar);
                        });
                        if(cantidad>0){
                            $('.btn-procesar').removeClass('disabled');
                        }else{
                            $('.btn-procesar').addClass('disabled');
                        }
                       
                        $('#lista-reporte01 tbody').html(lista01);
                        /*$('#lista-reporte01').DataTable({
                            "paging": true,
                            "lengthChange": true,
                            "searching": false,
                            "ordering": false,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 10,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });*/

                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}


ReporteResultados();
async function ReporteResultados(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/reporte/get-lista-reporte-semanal-resultado-procesamiento',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var lista02 ="";
                        $('#lista-reporte02').DataTable().destroy();
                        $.each(results.reportes, function( index, value ) {
                            lista02 = lista02 + "<tr>";
                                lista02 = lista02 + "<td>" + value.reporte + "</td>";
                                lista02 = lista02 + "<td>" + value.cantidad_total + "</td>";
                                lista02 = lista02 + "<td>" + value.cantidad_por_procesar + "</td>";
                            lista02 = lista02 + "</tr>";
                        });
                        
                        $('#lista-reporte02 tbody').html(lista02);
                        // $('#lista-reporte02').DataTable({
                        //     "paging": true,
                        //     "lengthChange": true,
                        //     "searching": false,
                        //     "ordering": false,
                        //     "info": true,
                        //     "autoWidth": false,
                        //     "pageLength" : 10,
                        //     "language": {
                        //         "sProcessing":    "Procesando...",
                        //         "sLengthMenu":    "Mostrar _MENU_ registros",
                        //         "sZeroRecords":   "No se encontraron resultados",
                        //         "sEmptyTable":    "Ningun dato disponible en esta lista",
                        //         "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        //         "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                        //         "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                        //         "sInfoPostFix":   "",
                        //         "sSearch":        "Buscar:",
                        //         "sUrl":           "",
                        //         "sInfoThousands":  ",",
                        //         "sLoadingRecords": "Cargando...",
                        //         "oPaginate": {
                        //             "sFirst":    "Primero",
                        //             "sLast":    "Último",
                        //             "sNext":    "Siguiente",
                        //             "sPrevious": "Anterior"
                        //         },
                        //         "oAria": {
                        //             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        //             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        //         }
                        //     },
                        // });

                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}

$('body').on('click', '.btn-procesar', function (e) {
    Procesamiento();
});

async function Procesamiento(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/reporte/procesar-semanal',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(xhr, settings)
                {
                    $('.btn-procesar').html('<i class="bx bx-hourglass bx-spin font-size-16 align-middle mr-2"></i> Procesando');
                    $('.btn-procesar').addClass('disabled');
                },
                success:function(results)
                {   
                    if(results && results.success){
                        $('.btn-procesar').html('Procesar');
                        ReportePorProcesar();
                        alert("Procesamiento finalizado");
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}


</script>