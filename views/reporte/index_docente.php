
<!-- Custom Css-->
<link href="<?= \Yii::$app->request->BaseUrl ?>/modal/magnific-popup.css" rel="stylesheet" type="text/css" />

<script src="<?= \Yii::$app->request->BaseUrl ?>/modal/jquery.magnific-popup.js"></script>

<style>
.m-intranet__content__score__item__header p:last-child{
    background-color:#4F9C2E !important;
}
.m-intranet__content__score__item__header--danger p:last-child{
    background-color:#EE4266 !important;
}
.m-intranet__content__score__item__header--warning p:last-child{
    background-color:#FFAD12 !important;
}

</style>

<div class="m-banner m-banner--profile m-banner--intranet">
    <div class="m-banner__bg"></div>
    <div class="m-container">
        <div class="m-breadcrumbs">
            <ul>
                <li><a href="<?= \Yii::$app->request->BaseUrl ?>/panel">Inicio</a></li>
                <li><a href="<?= \Yii::$app->request->BaseUrl ?>/panel/opciones?docente_seccion_id=<?= $docente_seccion_id?>&grado=<?= $grado->grado ?>&seccion=<?= $seccion?>"><?= $grado->nombre_corto ?> <?= $seccion?></a></li>
                <li class="css--active"><a href="#">Mis reportes</a></li>
            </ul>
        </div>
        <div class="m-banner__content">
            <p class="m-banner__title m-h1">Mis reportes</p>
        </div>
    </div>
</div>


<div class="m-container m-section">
    <div class="m-tab__container m-tab--only" id="tab-aside-report">
        <div class="m-intranet--report__grid">
            <div class="m-intranet--report__grid__left">
                <div class="m-intranet--report__tab__mobile">
                    <div class="m-intranet--report__tab__button">
                        <p class="experiencia_mobil">Experiencia 1</p>
                        <span class="m-icon-svg m-icon-svg--secondary">
                            <svg width="6" height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M3.44833 5.72832L0.195 8.98166C-0.065 9.24166 -0.065 9.66832 0.195 9.92832C0.455 10.1883 0.875 10.1883 1.135 9.92832L5.52833 5.53499C5.78833 5.27499 5.78833 4.85499 5.52833 4.59499L1.14167 0.19499C1.01711 0.0701553 0.848012 0 0.671667 0C0.495322 0 0.326221 0.0701553 0.201667 0.19499C-0.0583333 0.45499 -0.0583333 0.87499 0.201667 1.13499L3.44833 4.39499C3.52917 4.56165 3.73496 4.69498 3.73496 5.06165C3.73496 5.42831 3.52917 5.56165 3.44833 5.72832Z" fill="white" />
                            </svg>
                        </span>
                    </div>
                    <div id="lista-experiencias" class="m-intranet__aside">
                        <!-- <ul>
                            <li>
                                <a class="m-tab__button--js js--active" href="#tab-aside-resource-1" data-container="tab-aside-resource">
                                    <span class="m-h3">Experiencia 1</span>
                                    <span class="m-icon-svg m-icon-svg--secondary">
                                        <svg width="6" height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M3.44833 5.72832L0.195 8.98166C-0.065 9.24166 -0.065 9.66832 0.195 9.92832C0.455 10.1883 0.875 10.1883 1.135 9.92832L5.52833 5.53499C5.78833 5.27499 5.78833 4.85499 5.52833 4.59499L1.14167 0.19499C1.01711 0.0701553 0.848012 0 0.671667 0C0.495322 0 0.326221 0.0701553 0.201667 0.19499C-0.0583333 0.45499 -0.0583333 0.87499 0.201667 1.13499L3.44833 4.39499C3.52917 4.56165 3.73496 4.69498 3.73496 5.06165C3.73496 5.42831 3.52917 5.56165 3.44833 5.72832Z" fill="white" />
                                        </svg>
                                    </span>
                                </a>
                            </li>

                            <li>
                                <a class="m-tab__button--js" href="#tab-aside-resource-2" data-container="tab-aside-resource">
                                    <span class="m-h3">Experiencia 2</span>
                                    <span class="m-icon-svg m-icon-svg--secondary">
                                        <svg width="6" height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M3.44833 5.72832L0.195 8.98166C-0.065 9.24166 -0.065 9.66832 0.195 9.92832C0.455 10.1883 0.875 10.1883 1.135 9.92832L5.52833 5.53499C5.78833 5.27499 5.78833 4.85499 5.52833 4.59499L1.14167 0.19499C1.01711 0.0701553 0.848012 0 0.671667 0C0.495322 0 0.326221 0.0701553 0.201667 0.19499C-0.0583333 0.45499 -0.0583333 0.87499 0.201667 1.13499L3.44833 4.39499C3.52917 4.56165 3.73496 4.69498 3.73496 5.06165C3.73496 5.42831 3.52917 5.56165 3.44833 5.72832Z" fill="white" />
                                        </svg>
                                    </span>
                                </a>
                            </li>
                            
                        </ul> -->
                    </div>
                </div>
            </div>
            <div class="m-intranet--report__grid__right lista-actividades">
                <!-- <div class="m-tab__content js--active--desktop" id="tab-aside-resource-1" data-container="tab-aside-resource">
                    <h2>Reportes de la Experiencia 1</h2>
                    <p>Mira los recursos para cada actividad de esta experiencia:</p>
                    <div class="m-tab__container m-help-center__content m-tab--only" id="tab-resource-1">
                        
                        <div class="m-box-inline-block m-tab__nav m-tab__help-center__nav">
                            <a  class="m-tab__button m-tab__button--js js--active" href="#tab-resource-1-0" data-container="tab-resource-1" title="Ir a &lt;span class=&quot;m-none m-inline-block-ls&quot;&gt;Actividad&lt;/span&gt; 1">
                                <span class="m-none m-inline-block-ls">Actividad</span> 1
                            </a>
                            <a class="m-tab__button m-tab__button--js" href="#tab-resource-1-1" data-container="tab-resource-1" title="Ir a &lt;span class=&quot;m-none m-inline-block-ls&quot;&gt;Actividad&lt;/span&gt; 2">
                                <span class="m-none m-inline-block-ls">Actividad</span> 2
                            </a>
                            <a class="m-tab__button m-tab__button--js" href="#tab-resource-1-2" data-container="tab-resource-1" title="Ir a &lt;span class=&quot;m-none m-inline-block-ls&quot;&gt;Actividad&lt;/span&gt; 3">
                                <span class="m-none m-inline-block-ls">Actividad</span> 3
                            </a>
                            <a class="m-tab__button m-tab__button--js" href="#tab-resource-1-3" data-container="tab-resource-1" title="Ir a &lt;span class=&quot;m-none m-inline-block-ls&quot;&gt;Actividad&lt;/span&gt; 4">
                                <span class="m-none m-inline-block-ls">Actividad</span> 4
                            </a>
                        </div>

                        <div class="m-tab__content js--active--desktop" id="tab-resource-1-0" data-container="tab-resource-1">
                            <div class="m-intranet--report__accordion">
                                <h3 class="m-h3-2">Resolución de problemas con fracciones</h3>
                                <div class="m-intranet__content__multimedia">
                                    <div class="m-intranet__content__multimedia__video">
                                        <a class="m-video--css m-video__modal--jsxxx" href="https://youtu.be/v3jCNGIhsNQ">
                                            <picture class="m-lazy__img">
                                                <img class="m-lazy-js" data-src=" ./img/resource_content_0.png ">
                                            </picture>
                                        </a>
                                    </div>
                                    <div class="m-intranet__content__multimedia__aside">
                                        <p>Recursos para descarga:</p>
                                        <div class="m-intranet__content__multimedia__aside__buttons">
                                            <div>
                                                <a class="m-button m-button--teal m-button--full m-button--icon m-button--icon--auto" href="">
                                                    <span>
                                                        <span class="m-icon-svg m-icon-svg--secondary">
                                                            <svg width="18" height="16" viewBox="0 0 18 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                <path d="M18 13.7259V8.4467C18 8.04061 17.6757 7.67513 17.2297 7.67513C16.7838 7.67513 16.4595 8 16.4595 8.4467V13.7259C16.4595 14.132 16.1351 14.4975 15.6892 14.4975H2.27027C1.86486 14.4975 1.5 14.1726 1.5 13.7259V8.4467C1.5 8.04061 1.17568 7.67513 0.72973 7.67513C0.283784 7.67513 0 8.04061 0 8.48731V13.7665C0 15.0254 1.01351 16.0406 2.27027 16.0406H15.7703C16.9865 16 18 14.9848 18 13.7259ZM13.2162 8.56853L9.44595 11.5736C9.16216 11.7766 8.7973 11.7766 8.51351 11.5736L4.74324 8.56853C4.45946 8.28426 4.41892 7.79695 4.7027 7.51269C4.94595 7.22842 5.35135 7.18782 5.67568 7.39086L8.22973 9.42132V0.771573C8.22973 0.365482 8.55405 0 9 0C9.40541 0 9.77027 0.324873 9.77027 0.771573V9.46193L12.3243 7.43147C12.6081 7.14721 13.0946 7.14721 13.3784 7.47208C13.6622 7.75634 13.6622 8.24365 13.3378 8.52792C13.2973 8.52792 13.2568 8.52792 13.2162 8.56853Z" fill="white" />
                                                            </svg>
                                                        </span>
                                                    </span>
                                                    <span>Solucionario</span>
                                                </a>
                                            </div>
                                            <div>
                                                <a class="m-button m-button--magenta m-button--full m-button--icon m-button--icon--auto" href="">
                                                    <span>
                                                        <span class="m-icon-svg m-icon-svg--secondary">
                                                            <svg width="18" height="16" viewBox="0 0 18 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                <path d="M18 13.7259V8.4467C18 8.04061 17.6757 7.67513 17.2297 7.67513C16.7838 7.67513 16.4595 8 16.4595 8.4467V13.7259C16.4595 14.132 16.1351 14.4975 15.6892 14.4975H2.27027C1.86486 14.4975 1.5 14.1726 1.5 13.7259V8.4467C1.5 8.04061 1.17568 7.67513 0.72973 7.67513C0.283784 7.67513 0 8.04061 0 8.48731V13.7665C0 15.0254 1.01351 16.0406 2.27027 16.0406H15.7703C16.9865 16 18 14.9848 18 13.7259ZM13.2162 8.56853L9.44595 11.5736C9.16216 11.7766 8.7973 11.7766 8.51351 11.5736L4.74324 8.56853C4.45946 8.28426 4.41892 7.79695 4.7027 7.51269C4.94595 7.22842 5.35135 7.18782 5.67568 7.39086L8.22973 9.42132V0.771573C8.22973 0.365482 8.55405 0 9 0C9.40541 0 9.77027 0.324873 9.77027 0.771573V9.46193L12.3243 7.43147C12.6081 7.14721 13.0946 7.14721 13.3784 7.47208C13.6622 7.75634 13.6622 8.24365 13.3378 8.52792C13.2973 8.52792 13.2568 8.52792 13.2162 8.56853Z" fill="white" />
                                                            </svg>
                                                        </span>
                                                    </span>
                                                    <span>Matriz</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="m-tab__content" id="tab-resource-1-1" data-container="tab-resource-1">
                            <div class="m-intranet--report__accordion">
                                <h3 class="m-h3-2">Resolución de problemas con fracciones</h3>
                                <div class="m-intranet__content__multimedia">
                                    <div class="m-intranet__content__multimedia__video"><a
                                            class="m-video--css m-video__modal--js"
                                            href="https://youtu.be/v3jCNGIhsNQ">
                                            <picture class="m-lazy__img"><img class="m-lazy-js"
                                                    data-src=" ./img/resource_content_0.png "></picture>
                                        </a></div>
                                    <div class="m-intranet__content__multimedia__aside">
                                        <p>Recursos para descarga:</p>
                                        <div class="m-intranet__content__multimedia__aside__buttons">
                                            <div><a class="m-button m-button--teal m-button--full m-button--icon m-button--icon--auto"
                                                    href=""><span><span
                                                            class="m-icon-svg m-icon-svg--secondary"><svg width="18"
                                                                height="16" viewBox="0 0 18 16" fill="none"
                                                                xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M18 13.7259V8.4467C18 8.04061 17.6757 7.67513 17.2297 7.67513C16.7838 7.67513 16.4595 8 16.4595 8.4467V13.7259C16.4595 14.132 16.1351 14.4975 15.6892 14.4975H2.27027C1.86486 14.4975 1.5 14.1726 1.5 13.7259V8.4467C1.5 8.04061 1.17568 7.67513 0.72973 7.67513C0.283784 7.67513 0 8.04061 0 8.48731V13.7665C0 15.0254 1.01351 16.0406 2.27027 16.0406H15.7703C16.9865 16 18 14.9848 18 13.7259ZM13.2162 8.56853L9.44595 11.5736C9.16216 11.7766 8.7973 11.7766 8.51351 11.5736L4.74324 8.56853C4.45946 8.28426 4.41892 7.79695 4.7027 7.51269C4.94595 7.22842 5.35135 7.18782 5.67568 7.39086L8.22973 9.42132V0.771573C8.22973 0.365482 8.55405 0 9 0C9.40541 0 9.77027 0.324873 9.77027 0.771573V9.46193L12.3243 7.43147C12.6081 7.14721 13.0946 7.14721 13.3784 7.47208C13.6622 7.75634 13.6622 8.24365 13.3378 8.52792C13.2973 8.52792 13.2568 8.52792 13.2162 8.56853Z"
                                                                    fill="white" /></svg>
                                                        </span></span><span>Solucionario</span></a></div>
                                            <div><a class="m-button m-button--magenta m-button--full m-button--icon m-button--icon--auto"
                                                    href=""><span><span
                                                            class="m-icon-svg m-icon-svg--secondary"><svg width="18"
                                                                height="16" viewBox="0 0 18 16" fill="none"
                                                                xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M18 13.7259V8.4467C18 8.04061 17.6757 7.67513 17.2297 7.67513C16.7838 7.67513 16.4595 8 16.4595 8.4467V13.7259C16.4595 14.132 16.1351 14.4975 15.6892 14.4975H2.27027C1.86486 14.4975 1.5 14.1726 1.5 13.7259V8.4467C1.5 8.04061 1.17568 7.67513 0.72973 7.67513C0.283784 7.67513 0 8.04061 0 8.48731V13.7665C0 15.0254 1.01351 16.0406 2.27027 16.0406H15.7703C16.9865 16 18 14.9848 18 13.7259ZM13.2162 8.56853L9.44595 11.5736C9.16216 11.7766 8.7973 11.7766 8.51351 11.5736L4.74324 8.56853C4.45946 8.28426 4.41892 7.79695 4.7027 7.51269C4.94595 7.22842 5.35135 7.18782 5.67568 7.39086L8.22973 9.42132V0.771573C8.22973 0.365482 8.55405 0 9 0C9.40541 0 9.77027 0.324873 9.77027 0.771573V9.46193L12.3243 7.43147C12.6081 7.14721 13.0946 7.14721 13.3784 7.47208C13.6622 7.75634 13.6622 8.24365 13.3378 8.52792C13.2973 8.52792 13.2568 8.52792 13.2162 8.56853Z"
                                                                    fill="white" /></svg>
                                                        </span></span><span>Matriz</span></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div> -->

<!-- 
                <div class="m-tab__content" id="tab-aside-resource-2" data-container="tab-aside-resource">
                    <h2>Reportes de la Experiencia 2</h2>
                    <p>Mira los recursos para cada actividad de esta experiencia:</p>
                    <div class="m-tab__container m-help-center__content m-tab--only" id="tab-resource-2">
                        <div class="m-box-inline-block m-tab__nav m-tab__help-center__nav"><a
                                class="m-tab__button m-tab__button--js js--active" href="#tab-resource-2-0"
                                data-container="tab-resource-2"
                                title="Ir a &lt;span class=&quot;m-none m-inline-block-ls&quot;&gt;Actividad&lt;/span&gt; 1"><span
                                    class="m-none m-inline-block-ls">Actividad</span> 1</a><a
                                class="m-tab__button m-tab__button--js" href="#tab-resource-2-1"
                                data-container="tab-resource-2"
                                title="Ir a &lt;span class=&quot;m-none m-inline-block-ls&quot;&gt;Actividad&lt;/span&gt; 2"><span
                                    class="m-none m-inline-block-ls">Actividad</span> 2</a><a
                                class="m-tab__button m-tab__button--js" href="#tab-resource-2-2"
                                data-container="tab-resource-2"
                                title="Ir a &lt;span class=&quot;m-none m-inline-block-ls&quot;&gt;Actividad&lt;/span&gt; 3"><span
                                    class="m-none m-inline-block-ls">Actividad</span> 3</a><a
                                class="m-tab__button m-tab__button--js" href="#tab-resource-2-3"
                                data-container="tab-resource-2"
                                title="Ir a &lt;span class=&quot;m-none m-inline-block-ls&quot;&gt;Actividad&lt;/span&gt; 4"><span
                                    class="m-none m-inline-block-ls">Actividad</span> 4</a></div>
                        <div class="m-tab__content js--active--desktop" id="tab-resource-2-0"
                            data-container="tab-resource-2">
                            <div class="m-intranet--report__accordion">
                                <h3 class="m-h3-2">Resolución de problemas con fracciones</h3>
                                <div class="m-intranet__content__multimedia">
                                    <div class="m-intranet__content__multimedia__video"><a
                                            class="m-video--css m-video__modal--js"
                                            href="https://youtu.be/v3jCNGIhsNQ">
                                            <picture class="m-lazy__img"><img class="m-lazy-js"
                                                    data-src=" ./img/resource_content_0.png "></picture>
                                        </a></div>
                                    <div class="m-intranet__content__multimedia__aside">
                                        <p>Recursos para descarga:</p>
                                        <div class="m-intranet__content__multimedia__aside__buttons">
                                            <div><a class="m-button m-button--teal m-button--full m-button--icon m-button--icon--auto"
                                                    href=""><span><span
                                                            class="m-icon-svg m-icon-svg--secondary"><svg width="18"
                                                                height="16" viewBox="0 0 18 16" fill="none"
                                                                xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M18 13.7259V8.4467C18 8.04061 17.6757 7.67513 17.2297 7.67513C16.7838 7.67513 16.4595 8 16.4595 8.4467V13.7259C16.4595 14.132 16.1351 14.4975 15.6892 14.4975H2.27027C1.86486 14.4975 1.5 14.1726 1.5 13.7259V8.4467C1.5 8.04061 1.17568 7.67513 0.72973 7.67513C0.283784 7.67513 0 8.04061 0 8.48731V13.7665C0 15.0254 1.01351 16.0406 2.27027 16.0406H15.7703C16.9865 16 18 14.9848 18 13.7259ZM13.2162 8.56853L9.44595 11.5736C9.16216 11.7766 8.7973 11.7766 8.51351 11.5736L4.74324 8.56853C4.45946 8.28426 4.41892 7.79695 4.7027 7.51269C4.94595 7.22842 5.35135 7.18782 5.67568 7.39086L8.22973 9.42132V0.771573C8.22973 0.365482 8.55405 0 9 0C9.40541 0 9.77027 0.324873 9.77027 0.771573V9.46193L12.3243 7.43147C12.6081 7.14721 13.0946 7.14721 13.3784 7.47208C13.6622 7.75634 13.6622 8.24365 13.3378 8.52792C13.2973 8.52792 13.2568 8.52792 13.2162 8.56853Z"
                                                                    fill="white" /></svg>
                                                        </span></span><span>Solucionario</span></a></div>
                                            <div><a class="m-button m-button--magenta m-button--full m-button--icon m-button--icon--auto"
                                                    href=""><span><span
                                                            class="m-icon-svg m-icon-svg--secondary"><svg width="18"
                                                                height="16" viewBox="0 0 18 16" fill="none"
                                                                xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M18 13.7259V8.4467C18 8.04061 17.6757 7.67513 17.2297 7.67513C16.7838 7.67513 16.4595 8 16.4595 8.4467V13.7259C16.4595 14.132 16.1351 14.4975 15.6892 14.4975H2.27027C1.86486 14.4975 1.5 14.1726 1.5 13.7259V8.4467C1.5 8.04061 1.17568 7.67513 0.72973 7.67513C0.283784 7.67513 0 8.04061 0 8.48731V13.7665C0 15.0254 1.01351 16.0406 2.27027 16.0406H15.7703C16.9865 16 18 14.9848 18 13.7259ZM13.2162 8.56853L9.44595 11.5736C9.16216 11.7766 8.7973 11.7766 8.51351 11.5736L4.74324 8.56853C4.45946 8.28426 4.41892 7.79695 4.7027 7.51269C4.94595 7.22842 5.35135 7.18782 5.67568 7.39086L8.22973 9.42132V0.771573C8.22973 0.365482 8.55405 0 9 0C9.40541 0 9.77027 0.324873 9.77027 0.771573V9.46193L12.3243 7.43147C12.6081 7.14721 13.0946 7.14721 13.3784 7.47208C13.6622 7.75634 13.6622 8.24365 13.3378 8.52792C13.2973 8.52792 13.2568 8.52792 13.2162 8.56853Z"
                                                                    fill="white" /></svg>
                                                        </span></span><span>Matriz</span></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-tab__content" id="tab-resource-2-1" data-container="tab-resource-2">
                            <div class="m-intranet--report__accordion">
                                <h3 class="m-h3-2">Resolución de problemas con fracciones</h3>
                                <div class="m-intranet__content__multimedia">
                                    <div class="m-intranet__content__multimedia__video"><a
                                            class="m-video--css m-video__modal--js"
                                            href="https://youtu.be/v3jCNGIhsNQ">
                                            <picture class="m-lazy__img"><img class="m-lazy-js"
                                                    data-src=" ./img/resource_content_0.png "></picture>
                                        </a></div>
                                    <div class="m-intranet__content__multimedia__aside">
                                        <p>Recursos para descarga:</p>
                                        <div class="m-intranet__content__multimedia__aside__buttons">
                                            <div><a class="m-button m-button--teal m-button--full m-button--icon m-button--icon--auto"
                                                    href=""><span><span
                                                            class="m-icon-svg m-icon-svg--secondary"><svg width="18"
                                                                height="16" viewBox="0 0 18 16" fill="none"
                                                                xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M18 13.7259V8.4467C18 8.04061 17.6757 7.67513 17.2297 7.67513C16.7838 7.67513 16.4595 8 16.4595 8.4467V13.7259C16.4595 14.132 16.1351 14.4975 15.6892 14.4975H2.27027C1.86486 14.4975 1.5 14.1726 1.5 13.7259V8.4467C1.5 8.04061 1.17568 7.67513 0.72973 7.67513C0.283784 7.67513 0 8.04061 0 8.48731V13.7665C0 15.0254 1.01351 16.0406 2.27027 16.0406H15.7703C16.9865 16 18 14.9848 18 13.7259ZM13.2162 8.56853L9.44595 11.5736C9.16216 11.7766 8.7973 11.7766 8.51351 11.5736L4.74324 8.56853C4.45946 8.28426 4.41892 7.79695 4.7027 7.51269C4.94595 7.22842 5.35135 7.18782 5.67568 7.39086L8.22973 9.42132V0.771573C8.22973 0.365482 8.55405 0 9 0C9.40541 0 9.77027 0.324873 9.77027 0.771573V9.46193L12.3243 7.43147C12.6081 7.14721 13.0946 7.14721 13.3784 7.47208C13.6622 7.75634 13.6622 8.24365 13.3378 8.52792C13.2973 8.52792 13.2568 8.52792 13.2162 8.56853Z"
                                                                    fill="white" /></svg>
                                                        </span></span><span>Solucionario</span></a></div>
                                            <div><a class="m-button m-button--magenta m-button--full m-button--icon m-button--icon--auto"
                                                    href=""><span><span
                                                            class="m-icon-svg m-icon-svg--secondary"><svg width="18"
                                                                height="16" viewBox="0 0 18 16" fill="none"
                                                                xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M18 13.7259V8.4467C18 8.04061 17.6757 7.67513 17.2297 7.67513C16.7838 7.67513 16.4595 8 16.4595 8.4467V13.7259C16.4595 14.132 16.1351 14.4975 15.6892 14.4975H2.27027C1.86486 14.4975 1.5 14.1726 1.5 13.7259V8.4467C1.5 8.04061 1.17568 7.67513 0.72973 7.67513C0.283784 7.67513 0 8.04061 0 8.48731V13.7665C0 15.0254 1.01351 16.0406 2.27027 16.0406H15.7703C16.9865 16 18 14.9848 18 13.7259ZM13.2162 8.56853L9.44595 11.5736C9.16216 11.7766 8.7973 11.7766 8.51351 11.5736L4.74324 8.56853C4.45946 8.28426 4.41892 7.79695 4.7027 7.51269C4.94595 7.22842 5.35135 7.18782 5.67568 7.39086L8.22973 9.42132V0.771573C8.22973 0.365482 8.55405 0 9 0C9.40541 0 9.77027 0.324873 9.77027 0.771573V9.46193L12.3243 7.43147C12.6081 7.14721 13.0946 7.14721 13.3784 7.47208C13.6622 7.75634 13.6622 8.24365 13.3378 8.52792C13.2973 8.52792 13.2568 8.52792 13.2162 8.56853Z"
                                                                    fill="white" /></svg>
                                                        </span></span><span>Matriz</span></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-tab__content" id="tab-resource-2-2" data-container="tab-resource-2">
                            <div class="m-intranet--report__accordion">
                                <h3 class="m-h3-2">Resolución de problemas con fracciones</h3>
                                <div class="m-intranet__content__multimedia">
                                    <div class="m-intranet__content__multimedia__video"><a
                                            class="m-video--css m-video__modal--js"
                                            href="https://youtu.be/v3jCNGIhsNQ">
                                            <picture class="m-lazy__img"><img class="m-lazy-js"
                                                    data-src=" ./img/resource_content_0.png "></picture>
                                        </a></div>
                                    <div class="m-intranet__content__multimedia__aside">
                                        <p>Recursos para descarga:</p>
                                        <div class="m-intranet__content__multimedia__aside__buttons">
                                            <div><a class="m-button m-button--teal m-button--full m-button--icon m-button--icon--auto"
                                                    href=""><span><span
                                                            class="m-icon-svg m-icon-svg--secondary"><svg width="18"
                                                                height="16" viewBox="0 0 18 16" fill="none"
                                                                xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M18 13.7259V8.4467C18 8.04061 17.6757 7.67513 17.2297 7.67513C16.7838 7.67513 16.4595 8 16.4595 8.4467V13.7259C16.4595 14.132 16.1351 14.4975 15.6892 14.4975H2.27027C1.86486 14.4975 1.5 14.1726 1.5 13.7259V8.4467C1.5 8.04061 1.17568 7.67513 0.72973 7.67513C0.283784 7.67513 0 8.04061 0 8.48731V13.7665C0 15.0254 1.01351 16.0406 2.27027 16.0406H15.7703C16.9865 16 18 14.9848 18 13.7259ZM13.2162 8.56853L9.44595 11.5736C9.16216 11.7766 8.7973 11.7766 8.51351 11.5736L4.74324 8.56853C4.45946 8.28426 4.41892 7.79695 4.7027 7.51269C4.94595 7.22842 5.35135 7.18782 5.67568 7.39086L8.22973 9.42132V0.771573C8.22973 0.365482 8.55405 0 9 0C9.40541 0 9.77027 0.324873 9.77027 0.771573V9.46193L12.3243 7.43147C12.6081 7.14721 13.0946 7.14721 13.3784 7.47208C13.6622 7.75634 13.6622 8.24365 13.3378 8.52792C13.2973 8.52792 13.2568 8.52792 13.2162 8.56853Z"
                                                                    fill="white" /></svg>
                                                        </span></span><span>Solucionario</span></a></div>
                                            <div><a class="m-button m-button--magenta m-button--full m-button--icon m-button--icon--auto"
                                                    href=""><span><span
                                                            class="m-icon-svg m-icon-svg--secondary"><svg width="18"
                                                                height="16" viewBox="0 0 18 16" fill="none"
                                                                xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M18 13.7259V8.4467C18 8.04061 17.6757 7.67513 17.2297 7.67513C16.7838 7.67513 16.4595 8 16.4595 8.4467V13.7259C16.4595 14.132 16.1351 14.4975 15.6892 14.4975H2.27027C1.86486 14.4975 1.5 14.1726 1.5 13.7259V8.4467C1.5 8.04061 1.17568 7.67513 0.72973 7.67513C0.283784 7.67513 0 8.04061 0 8.48731V13.7665C0 15.0254 1.01351 16.0406 2.27027 16.0406H15.7703C16.9865 16 18 14.9848 18 13.7259ZM13.2162 8.56853L9.44595 11.5736C9.16216 11.7766 8.7973 11.7766 8.51351 11.5736L4.74324 8.56853C4.45946 8.28426 4.41892 7.79695 4.7027 7.51269C4.94595 7.22842 5.35135 7.18782 5.67568 7.39086L8.22973 9.42132V0.771573C8.22973 0.365482 8.55405 0 9 0C9.40541 0 9.77027 0.324873 9.77027 0.771573V9.46193L12.3243 7.43147C12.6081 7.14721 13.0946 7.14721 13.3784 7.47208C13.6622 7.75634 13.6622 8.24365 13.3378 8.52792C13.2973 8.52792 13.2568 8.52792 13.2162 8.56853Z"
                                                                    fill="white" /></svg>
                                                        </span></span><span>Matriz</span></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-tab__content" id="tab-resource-2-3" data-container="tab-resource-2">
                            <div class="m-intranet--report__accordion">
                                <h3 class="m-h3-2">Resolución de problemas con fracciones</h3>
                                <div class="m-intranet__content__multimedia">
                                    <div class="m-intranet__content__multimedia__video"><a
                                            class="m-video--css m-video__modal--js"
                                            href="https://youtu.be/v3jCNGIhsNQ">
                                            <picture class="m-lazy__img"><img class="m-lazy-js"
                                                    data-src=" ./img/resource_content_0.png "></picture>
                                        </a></div>
                                    <div class="m-intranet__content__multimedia__aside">
                                        <p>Recursos para descarga:</p>
                                        <div class="m-intranet__content__multimedia__aside__buttons">
                                            <div><a class="m-button m-button--teal m-button--full m-button--icon m-button--icon--auto"
                                                    href=""><span><span
                                                            class="m-icon-svg m-icon-svg--secondary"><svg width="18"
                                                                height="16" viewBox="0 0 18 16" fill="none"
                                                                xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M18 13.7259V8.4467C18 8.04061 17.6757 7.67513 17.2297 7.67513C16.7838 7.67513 16.4595 8 16.4595 8.4467V13.7259C16.4595 14.132 16.1351 14.4975 15.6892 14.4975H2.27027C1.86486 14.4975 1.5 14.1726 1.5 13.7259V8.4467C1.5 8.04061 1.17568 7.67513 0.72973 7.67513C0.283784 7.67513 0 8.04061 0 8.48731V13.7665C0 15.0254 1.01351 16.0406 2.27027 16.0406H15.7703C16.9865 16 18 14.9848 18 13.7259ZM13.2162 8.56853L9.44595 11.5736C9.16216 11.7766 8.7973 11.7766 8.51351 11.5736L4.74324 8.56853C4.45946 8.28426 4.41892 7.79695 4.7027 7.51269C4.94595 7.22842 5.35135 7.18782 5.67568 7.39086L8.22973 9.42132V0.771573C8.22973 0.365482 8.55405 0 9 0C9.40541 0 9.77027 0.324873 9.77027 0.771573V9.46193L12.3243 7.43147C12.6081 7.14721 13.0946 7.14721 13.3784 7.47208C13.6622 7.75634 13.6622 8.24365 13.3378 8.52792C13.2973 8.52792 13.2568 8.52792 13.2162 8.56853Z"
                                                                    fill="white" /></svg>
                                                        </span></span><span>Solucionario</span></a></div>
                                            <div><a class="m-button m-button--magenta m-button--full m-button--icon m-button--icon--auto"
                                                    href=""><span><span
                                                            class="m-icon-svg m-icon-svg--secondary"><svg width="18"
                                                                height="16" viewBox="0 0 18 16" fill="none"
                                                                xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M18 13.7259V8.4467C18 8.04061 17.6757 7.67513 17.2297 7.67513C16.7838 7.67513 16.4595 8 16.4595 8.4467V13.7259C16.4595 14.132 16.1351 14.4975 15.6892 14.4975H2.27027C1.86486 14.4975 1.5 14.1726 1.5 13.7259V8.4467C1.5 8.04061 1.17568 7.67513 0.72973 7.67513C0.283784 7.67513 0 8.04061 0 8.48731V13.7665C0 15.0254 1.01351 16.0406 2.27027 16.0406H15.7703C16.9865 16 18 14.9848 18 13.7259ZM13.2162 8.56853L9.44595 11.5736C9.16216 11.7766 8.7973 11.7766 8.51351 11.5736L4.74324 8.56853C4.45946 8.28426 4.41892 7.79695 4.7027 7.51269C4.94595 7.22842 5.35135 7.18782 5.67568 7.39086L8.22973 9.42132V0.771573C8.22973 0.365482 8.55405 0 9 0C9.40541 0 9.77027 0.324873 9.77027 0.771573V9.46193L12.3243 7.43147C12.6081 7.14721 13.0946 7.14721 13.3784 7.47208C13.6622 7.75634 13.6622 8.24365 13.3378 8.52792C13.2973 8.52792 13.2568 8.52792 13.2162 8.56853Z"
                                                                    fill="white" /></svg>
                                                        </span></span><span>Matriz</span></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 -->
            </div>
        </div>
    </div>
</div>




<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var grado_id = "<?= $grado->id ?>";
var docente_seccion_id = "<?= $docente_seccion_id ?>";
Experiencias();
async function Experiencias(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/experiencia/get-lista-experiencias-reportes',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var experiencias ="";
                        var experiencias2= "";
                        var i = 0;
                        // $.each(results.experiencias, function( index, value ) {
                        //     experiencia_id = value.id;
                        //     experiencia_correlativo = value.correlativo;
                           
                        //     experiencias2 = experiencias2 + `
                        //     <div class="m-intranet--report__tab__button">
                        //         <p>Experiencia ${experiencia_correlativo}</p>
                        //         <span class="m-icon-svg m-icon-svg--secondary">
                        //             <svg width="6" height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                        //                 <path d="M3.44833 5.72832L0.195 8.98166C-0.065 9.24166 -0.065 9.66832 0.195 9.92832C0.455 10.1883 0.875 10.1883 1.135 9.92832L5.52833 5.53499C5.78833 5.27499 5.78833 4.85499 5.52833 4.59499L1.14167 0.19499C1.01711 0.0701553 0.848012 0 0.671667 0C0.495322 0 0.326221 0.0701553 0.201667 0.19499C-0.0583333 0.45499 -0.0583333 0.87499 0.201667 1.13499L3.44833 4.39499C3.52917 4.56165 3.73496 4.69498 3.73496 5.06165C3.73496 5.42831 3.52917 5.56165 3.44833 5.72832Z" fill="white" />
                        //             </svg>
                        //         </span>
                        //     </div>`;
                        // });

                        // $('#lista-experiencias2').html(experiencias2);

                        experiencias = experiencias + '<ul>';
                        $.each(results.experiencias, function( index, value ) {
                            experiencia_id = value.id;
                            experiencia_correlativo = value.correlativo;
                            active = '';
                            if(i==0){
                                active = 'js--active';
                                $('.experiencia_mobil').html('Experiencia '+ experiencia_correlativo)
                            }
                            experiencias = experiencias + `
                            <li>
                                <a data-id="${experiencia_id}" data-experiencia_id="${experiencia_id}" data-experiencia_correlativo="${experiencia_correlativo}" class="${active} btn-seleccionar-experiencia" href="#tab-aside-resource-${experiencia_id}" data-container="tab-aside-resource">
                                    <span class="m-h3">Experiencia ${experiencia_correlativo}</span>
                                    <span class="m-icon-svg m-icon-svg--secondary">
                                        <svg width="6" height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M3.44833 5.72832L0.195 8.98166C-0.065 9.24166 -0.065 9.66832 0.195 9.92832C0.455 10.1883 0.875 10.1883 1.135 9.92832L5.52833 5.53499C5.78833 5.27499 5.78833 4.85499 5.52833 4.59499L1.14167 0.19499C1.01711 0.0701553 0.848012 0 0.671667 0C0.495322 0 0.326221 0.0701553 0.201667 0.19499C-0.0583333 0.45499 -0.0583333 0.87499 0.201667 1.13499L3.44833 4.39499C3.52917 4.56165 3.73496 4.69498 3.73496 5.06165C3.73496 5.42831 3.52917 5.56165 3.44833 5.72832Z" fill="white" />
                                        </svg>
                                    </span>
                                </a>
                            </li>`;
                            i++;
                        });
                        experiencias = experiencias + '</ul>';
                        
                        $('#lista-experiencias').html(experiencias);

                        

                        Actividades(results.experiencias[0].id,results.experiencias[0].correlativo)
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}


async function Actividades(experiencia_id,experiencia_correlativo){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/actividad/get-experiencia-actividades-reportes',
                method: 'POST',
                data:{_csrf:csrf,experiencia_id:experiencia_id,grado_id:grado_id},
                dataType:'Json',
                beforeSend:function(){
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        listaGraficoReport01 = [];
                        listaGraficoReport03 = [];
                        var i = 0;
                        var actividades ='';

                        var p02_principal = results.p02_texto.texto_principal;
                        var p02_texto_enlace = results.p02_texto.texto_enlace;
                        var p02_tipo = results.p02_texto.tipo;
                        var p02_texto = ``;
                        var p02_archivo = results.p02_texto.archivo;
                        var p02_video = results.p02_texto.video;
                        var p02_enlace = results.p02_texto.enlace;

                        if(p02_tipo==1){
                            p02_texto = `${p02_principal} <a href="<?= \Yii::$app->request->BaseUrl ?>/archivo_texto/${p02_archivo}" target='_blank'>${p02_texto_enlace}</a>`;
                        }else if(p02_tipo==2){
                            p02_texto = `${p02_principal} <a href="${p02_video}" class="m-video__modal--jsxxx">${p02_texto_enlace}</a>`;
                        }else if(p02_tipo==3){
                            p02_texto = `${p02_principal} <a href="${p02_enlace}" target="_blank">${p02_texto_enlace}</a>`;
                        }


                        var p05_principal_01 = results.p05_texto_01.texto_principal;
                        var p05_texto_enlace_01 = results.p05_texto_01.texto_enlace;
                        var p05_tipo_01 = results.p05_texto_01.tipo;
                        var p05_texto_01 = ``;
                        var p05_archivo_01 = results.p05_texto_01.archivo;
                        var p05_video_01 = results.p05_texto_01.video;
                        var p05_enlace_01 = results.p05_texto_01.enlace;

                        var p05_principal_02 = results.p05_texto_02.texto_principal;
                        var p05_texto_enlace_02 = results.p05_texto_02.texto_enlace;
                        var p05_tipo_02 = results.p05_texto_02.tipo;
                        var p05_texto_02 = ``;
                        var p05_archivo_02 = results.p05_texto_02.archivo;
                        var p05_video_02 = results.p05_texto_02.video;
                        var p05_enlace_02 = results.p05_texto_02.enlace;

                        if(p05_tipo_01==1){
                            p05_texto_01 = `${p05_principal_01} <a href="<?= \Yii::$app->request->BaseUrl ?>/archivo_texto/${p05_archivo_01}" target='_blank'>${p05_texto_enlace_01}</a>`;
                        }else if(p05_tipo_01==2){
                            p05_texto_01 = `${p05_principal_01} <a href="${p05_video_01}" class="m-video__modal--jsxxx">${p05_texto_enlace_01}</a>`;
                        }else if(p05_tipo_01==3){
                            p05_texto_01 = `${p05_principal_01} <a href="${p05_enlace_01}" target="_blank">${p05_texto_enlace_01}</a>`;
                        }

                        if(p05_tipo_02==1){
                            p05_texto_02 = `${p05_principal_02} <a href="<?= \Yii::$app->request->BaseUrl ?>/archivo_texto/${p05_archivo_02}" target='_blank'>${p05_texto_enlace_02}</a>`;
                        }else if(p05_tipo_02==2){
                            p05_texto_02 = `${p05_principal_02} <a href="${p05_video_02}" class="m-video__modal--jsxxx">${p05_texto_enlace_02}</a>`;
                        }else if(p05_tipo_02==3){
                            p05_texto_02 = `${p05_principal_02} <a href="${p05_enlace_02}" target="_blank">${p05_texto_enlace_02}</a>`;
                        }



                        
                        actividades = actividades + `
                                                    <div class="m-tab__content js--active--desktop" id="tab-aside-report-${experiencia_correlativo}" data-container="tab-aside-report">
                                                        <h2>Reportes de la Experiencia ${experiencia_correlativo}</h2>
                                                        <p>Selecciona una actividad para ver el reporte correspondiente:</p>
                                                        <div class="m-tab__container m-help-center__content m-tab--only" id="tab-report-${experiencia_correlativo}">
                                                            <div class="m-box-inline-block m-tab__nav m-tab__help-center__nav">
                        `;

                        
                        $.each(results.actividades, function( index, value ) {
                            correlativo = value.correlativo;
                            active = '';
                            if(i==0){
                                active = 'js--active';
                            }
                            actividades = actividades + `   <a  class="m-tab__button m-tab__button--js ${active}" href="#tab-report-${experiencia_correlativo}-${correlativo}" data-container="tab-report-${experiencia_correlativo}" title="Ir a &lt;span class=&quot;m-none m-inline-block-ls&quot;&gt;Actividad&lt;/span&gt; ${correlativo}">
                                                                <span class="m-none m-inline-block-ls">Actividad</span> ${correlativo}
                                                            </a>`;
                            i++;

                        });

                        actividades = actividades + `</div>`;

                        // ningun_estudiante = 0;
                        // $.each(results.actividades, function( index, value ) {
                        //     $.each(value.reporte01, function( index2, value2 ) {
                        //         ningun_estudiante = ningun_estudiante + parseInt(value2.conectados);
                        //     });
                        // });

                        // if(ningun_estudiante==0){
                            
                        //     actividades = actividades + `<div class="m-tab__content js--active--desktop" id="tab-resource-1-0" data-container="tab-resource-1">
                        //                                     <div class="m-intranet--report__accordion m-intranet--report__accordion--404">
                        //                                         <div class="m-content-editor">
                        //                                             <p>Lamentablemente, ninguno de tus estudiantes usó Conecta Ideas esta semana.</p>
                        //                                             <p>¡Podemos hacerlo! Continúa animando a tus estudiantes a ingresar a Conecta Ideas.</p>
                        //                                         </div>
                        //                                         <picture class="m-lazy__img">
                        //                                             <source data-srcset=" ./img/404_video.png  1x, ./img/404_video@2x.png 2x"
                        //                                                 type="image/png"><img class="m-lazy-js" data-src=" ./img/404_video.png ">
                        //                                         </picture>
                        //                                     </div>
                        //                                 </div>`;
                        //     $('.lista-actividades').html(actividades);
                        // } else {

                            i = 0;
                            $.each(results.actividades, function( index, value ) {
                                correlativo = value.correlativo;
                                active = '';
                                if(i==0){
                                    active = 'js--active--desktop';
                                }

                                ningun_estudiante = 0 ;
                                $.each(value.reporte01, function( index2, value2 ) {
                                    ningun_estudiante = ningun_estudiante + parseInt(value2.conectados);
                                });

                                if(ningun_estudiante==0){
                                    actividades = actividades + `<div class="m-tab__content ${active}" id="tab-report-${experiencia_correlativo}-${correlativo}" data-container="tab-report-${experiencia_correlativo}">
                                                            <div class="m-intranet--report__accordion m-intranet--report__accordion--404">
                                                                <div class="m-content-editor">
                                                                    <p>Lamentablemente, ninguno de tus estudiantes usó Conecta Ideas esta semana.</p>
                                                                    <p>¡Podemos hacerlo! Continúa animando a tus estudiantes a ingresar a Conecta Ideas.</p>
                                                                </div>
                                                                <picture class="m-lazy__img">
                                                                    <source srcset="<?= \Yii::$app->request->BaseUrl ?>/personalizado/img/404_video.png  1x, <?= \Yii::$app->request->BaseUrl ?>/personalizado/img/404_video@2x.png 2x"
                                                                        type="image/png">
                                                                        <img  src="<?= \Yii::$app->request->BaseUrl ?>/personalizado/img/404_video.png ">
                                                                </picture>
                                                            </div>
                                                        </div>`;
                                }else{
                                    reporte_02 = '';
                                    $.each(value.reporte02, function( index2, value2 ) {
                                        nombres = value2.nombres;
                                        apellido_paterno_letra = value2.apellido_paterno_letra;
                                        reporte_02 = reporte_02 + ` <li>
                                                                        <p>${nombres} ${apellido_paterno_letra}</p>
                                                                    </li>`;
                                    });

                                    reporte_03 = '';
                                    $.each(value.reporte03, function( index2, value2 ) {
                                        correlativo_reporte = value2.id;
                                        competencia = value2.descripcion;

                                        reporte_03 = reporte_03 + `   <div class="m-intranet__content__card__info m-intranet__content__card__info--blue">
                                                                        <div class="m-intranet__content__card__info__title">
                                                                            <span class="m-icon-svg m-icon-svg--secondary">
                                                                                <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                                    <circle cx="6" cy="6" r="6" fill="#3F7AFE" />
                                                                                    <path
                                                                                        d="M5.04386 9.00004C4.91455 9.00004 4.78524 8.94433 4.68179 8.84683L3.15593 7.28694C2.94903 7.07803 2.9361 6.71591 3.143 6.49307C3.33696 6.27023 3.67317 6.2563 3.88007 6.47914L5.03093 7.66299L8.12145 4.18109C8.31542 3.95825 8.65162 3.94432 8.85852 4.16717C9.06542 4.37608 9.07835 4.7382 8.87145 4.96104L5.41886 8.8329C5.31541 8.94433 5.1861 9.00004 5.04386 9.00004Z"
                                                                                        fill="white" />
                                                                                </svg>
                                                                            </span>
                                                                            <h3>Competencia:</h3>
                                                                        </div>
                                                                        <p>${competencia}</p>
                                                                    </div>

                                                                    <div class="m-intranet__content__grid">
                                                                        <div class="m-intranet__content__grid__left">
                                                                            <div class="m-intranet__content__grid__graphic">
                                                                                <div style="height:350px;width: 320px" id="grafico_03_${correlativo}_${correlativo_reporte}"></div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="m-intranet__content__grid__right">
                                                                            <div class="m-intranet__content__level">
                                                                                <p class="m-intranet__content__level__item m-intranet__content__level__item--high">
                                                                                    <strong>Avanzado:</strong> El estudiante respondió correctamente 21 preguntas o más.
                                                                                </p>
                                                                                <p class="m-intranet__content__level__item m-intranet__content__level__item--medium">
                                                                                    <strong>Intermedio:</strong> El estudiante respondió correctamente entre 11 y 20 preguntas.
                                                                                </p>
                                                                                <p class="m-intranet__content__level__item">
                                                                                    <strong>Inicial:</strong> El estudiante respondió correctamente 10 preguntas o menos.
                                                                                </p>
                                                                                <p class="m-intranet__content__level__item m-intranet__content__level__item--low">
                                                                                    <strong>No usó el app:</strong> El estudiante no usó Conecta Ideas esta semana.
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>`;
                                    });


                                    reporte_04 = '';
                                    reporte_04_competencia = '';
                                    $.each(value.reporte04, function( index2, value2 ) {
                                        reporte_04_competencia = value2.descripcion_capacidad;
                                        numero_estudiantes = value2.numero_estudiantes;
                                        numero_problema = value2.numero_problema;
                                        reporte_04 = reporte_04 + ` <tr>
                                                                        <td>${numero_problema}</td>
                                                                        <td>${numero_estudiantes}</td>
                                                                    </tr>`;
                                    });

                                    reporte_05_left = '';
                                    reporte_05_right= '';
                                    c_reporte05 = 0;
                                    c_reporte05_2 = 1;
                                    cantidad_reporte05 = value.reporte05;
                                    cantidad_sumada_reporte05 = parseFloat((parseFloat(cantidad_reporte05.length)/2).toFixed(0));
                                    //console.log(cantidad_reporte05,cantidad_sumada_reporte05);
                                    $.each(value.reporte05, function( index2, value2 ) {
                                        estudiante_id = value2.id;
                                        puntaje = value2.puntaje;
                                        nombres = value2.nombres;
                                        apellido_paterno_letra = value2.apellido_paterno_letra;
                                        p01 = Puntaje(value2.p01);
                                        p02 = Puntaje(value2.p02);
                                        p03 = Puntaje(value2.p03);
                                        p04 = Puntaje(value2.p04);
                                        p05 = Puntaje(value2.p05);
                                        p06 = Puntaje(value2.p06);
                                        p07 = Puntaje(value2.p07);
                                        p08 = Puntaje(value2.p08);
                                        p09 = Puntaje(value2.p09);
                                        p10 = Puntaje(value2.p10);
                                        p11 = Puntaje(value2.p11);
                                        p12 = Puntaje(value2.p12);
                                        p13 = Puntaje(value2.p13);
                                        p14 = Puntaje(value2.p14);
                                        p15 = Puntaje(value2.p15);
                                        p16 = Puntaje(value2.p16);
                                        p17 = Puntaje(value2.p17);
                                        p18 = Puntaje(value2.p18);
                                        p19 = Puntaje(value2.p19);
                                        p20 = Puntaje(value2.p20);
                                        p21 = Puntaje(value2.p21);
                                        p22 = Puntaje(value2.p22);
                                        p23 = Puntaje(value2.p23);
                                        p24 = Puntaje(value2.p24);
                                        p25 = Puntaje(value2.p25);
                                        p26 = Puntaje(value2.p26);
                                        p27 = Puntaje(value2.p27);
                                        p28 = Puntaje(value2.p28);
                                        p29 = Puntaje(value2.p29);
                                        p30 = Puntaje(value2.p30);

                                        puntaje_color = ''
                                        if(parseInt(puntaje) >= 0 && parseInt(puntaje)	<= 10){
                                            puntaje_color = 'm-intranet__content__score__item__header--danger';
                                        }else if(parseInt(puntaje) >= 11 && parseInt(puntaje) <= 20){
                                            puntaje_color = 'm-intranet__content__score__item__header--warning';
                                        }else if(parseInt(puntaje) >= 21 && parseInt(puntaje) <= 30){
                                            puntaje_color = '';
                                        }

                                        

                                        //if(c_reporte05%2){
                                        if(cantidad_sumada_reporte05<c_reporte05_2){
                                            reporte_05_right = reporte_05_right + `   <div class="m-intranet__content__score__item">
                                                                                        <div class="m-intranet__content__score__item__header m-intranet__content__score__item__header--js ${puntaje_color} btn-score_item_puntaje" id="m-intranet-score-${experiencia_correlativo}-${correlativo}-${estudiante_id}">
                                                                                            <p>${nombres} ${apellido_paterno_letra}</p>
                                                                                            <p>${puntaje}</p>
                                                                                        </div>
                                                                                        <div>
                                                                                            <div class="m-intranet__content__score__item__content">
                                                                                                <div>
                                                                                                    <div class="m-intranet__content__score__item__table">
                                                                                                        <div class="m-intranet__content__score__item__table__header">
                                                                                                            <p>P</p>
                                                                                                            <p>R</p>
                                                                                                        </div>
                                                                                                        <div class="m-intranet__content__score__item__table__data">
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p01}">
                                                                                                                1</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p02}">
                                                                                                                2</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p03}">
                                                                                                                3</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p04}">
                                                                                                                4</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p05}">
                                                                                                                5</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p06}">
                                                                                                                6</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p07}">
                                                                                                                7</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p08}">
                                                                                                                8</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p09}">
                                                                                                                9</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p10}">
                                                                                                                10</p>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div>
                                                                                                    <div class="m-intranet__content__score__item__table">
                                                                                                        <div class="m-intranet__content__score__item__table__header">
                                                                                                            <p>P</p>
                                                                                                            <p>R</p>
                                                                                                        </div>
                                                                                                        <div class="m-intranet__content__score__item__table__data">
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p11}">
                                                                                                                11</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p12}">
                                                                                                                12</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p13}">
                                                                                                                13</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p14}">
                                                                                                                14</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p15}">
                                                                                                                15</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p16}">
                                                                                                                16</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p17}">
                                                                                                                17</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p18}">
                                                                                                                18</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p19}">
                                                                                                                19</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p20}">
                                                                                                                20</p>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div>
                                                                                                    <div class="m-intranet__content__score__item__table">
                                                                                                        <div class="m-intranet__content__score__item__table__header">
                                                                                                            <p>P</p>
                                                                                                            <p>R</p>
                                                                                                        </div>
                                                                                                        <div class="m-intranet__content__score__item__table__data">
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p21}">
                                                                                                                21</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p22}">
                                                                                                                22</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p23}">
                                                                                                                23</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p24}">
                                                                                                                24</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p25}">
                                                                                                                25</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p26}">
                                                                                                                26</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p27}">
                                                                                                                27</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p28}">
                                                                                                                28</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p29}">
                                                                                                                29</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p30}">
                                                                                                                30</p>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>`;
                                        }else{
                                            reporte_05_left = reporte_05_left + ` <div class="m-intranet__content__score__item">
                                                                                        <div class="m-intranet__content__score__item__header m-intranet__content__score__item__header--js ${puntaje_color} btn-score_item_puntaje" id="m-intranet-score-${experiencia_correlativo}-${correlativo}-${estudiante_id}">
                                                                                            <p>${nombres} ${apellido_paterno_letra}</p>
                                                                                            <p>${puntaje}</p>
                                                                                        </div>
                                                                                        <div>
                                                                                            <div class="m-intranet__content__score__item__content">
                                                                                                <div>
                                                                                                    <div class="m-intranet__content__score__item__table">
                                                                                                        <div class="m-intranet__content__score__item__table__header">
                                                                                                            <p>P</p>
                                                                                                            <p>R</p>
                                                                                                        </div>
                                                                                                        <div class="m-intranet__content__score__item__table__data">
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p01}">
                                                                                                                1</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p02}">
                                                                                                                2</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p03}">
                                                                                                                3</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p04}">
                                                                                                                4</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p05}">
                                                                                                                5</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p06}">
                                                                                                                6</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p07}">
                                                                                                                7</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p08}">
                                                                                                                8</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p09}">
                                                                                                                9</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p10}">
                                                                                                                10</p>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div>
                                                                                                    <div class="m-intranet__content__score__item__table">
                                                                                                        <div class="m-intranet__content__score__item__table__header">
                                                                                                            <p>P</p>
                                                                                                            <p>R</p>
                                                                                                        </div>
                                                                                                        <div class="m-intranet__content__score__item__table__data">
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p11}">
                                                                                                                11</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p12}">
                                                                                                                12</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p13}">
                                                                                                                13</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p14}">
                                                                                                                14</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p15}">
                                                                                                                15</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p16}">
                                                                                                                16</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p17}">
                                                                                                                17</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p18}">
                                                                                                                18</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p19}">
                                                                                                                19</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p20}">
                                                                                                                20</p>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div>
                                                                                                    <div class="m-intranet__content__score__item__table">
                                                                                                        <div class="m-intranet__content__score__item__table__header">
                                                                                                            <p>P</p>
                                                                                                            <p>R</p>
                                                                                                        </div>
                                                                                                        <div class="m-intranet__content__score__item__table__data">
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p21}">
                                                                                                                21</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p22}">
                                                                                                                22</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p23}">
                                                                                                                23</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p24}">
                                                                                                                24</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p25}">
                                                                                                                25</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p26}">
                                                                                                                26</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p27}">
                                                                                                                27</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p28}">
                                                                                                                28</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p29}">
                                                                                                                29</p>
                                                                                                            <p class="m-intranet__content__score__item__table__data__reply ${p30}">
                                                                                                                30</p>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>`
                                        }
                                        c_reporte05++;
                                        c_reporte05_2++;
                                    });


                                    $.each(value.detalle, function( index2, value2 ) {
                                        id = value2.id;
                                        grado = value2.nombre_corto;
                                        video = value2.url_video;
                                        matriz = value2.matriz;
                                        solucionario = value2.solucionario;
                                        descripcion = value2.descripcion;
                                        actividad_cabecera_id = value2.actividad_cabecera_id;

                                        actividades = actividades + `
                                                                    <div class="m-tab__content ${active}" id="tab-report-${experiencia_correlativo}-${correlativo}" data-container="tab-report-${experiencia_correlativo}">
                                                                        <div class="m-intranet--report__accordion">
                                                                            <p>Reporte</p>
                                                                            <h3 class="m-h3-2">${descripcion}</h3>
                                                                            
                                                                            <hr>
                                                                            <div class="m-accordion">

                                                                                <div class="m-accordion__box">
                                                                                    <div class="m-accordion__toggle" id="tab-report-accordion-0">
                                                                                        <h3 class="m-accordion__toggle__title">1. ¿Cuántos estudiantes de mi aula usaron Conecta Ideas esta semana?</h3>
                                                                                    </div>
                                                                                    <div class="m-accordion__content m-content-editor">
                                                                                        <div class="m-content-editor">
                                                                                            <div style="height:292px" id="grafico_01_${correlativo}"></div>
                                                                                                
                                                                                        
                                                                                            
                                                                                            <div class="m-intranet__content__card__info">
                                                                                                <div class="m-intranet__content__card__info__title">
                                                                                                    <span class="m-icon-svg m-icon-svg--secondary">
                                                                                                        <svg width="14" height="20" viewBox="0 0 14 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                                                            <path
                                                                                                                d="M6.79644 15.7092C7.71244 15.7092 8.62844 15.7092 9.54858 15.7092C9.7351 15.7092 9.92991 15.5316 9.94234 15.3372C9.95477 15.1427 9.96721 14.9483 9.96721 14.7538C9.97135 14.2381 9.97135 13.7266 9.96721 13.2109C9.96721 13.1136 9.99208 13.0587 10.0708 13.008C10.6262 12.6486 11.1651 12.2724 11.6251 11.7821C12.595 10.7464 13.2333 9.53738 13.4944 8.12548C13.6022 7.53789 13.5732 6.94185 13.5525 6.35003C13.5276 5.56799 13.3203 4.83244 13.0136 4.12649C12.6199 3.21763 12.0479 2.43982 11.3267 1.77614C10.8749 1.36186 10.39 1.00677 9.84701 0.732002C9.18384 0.393822 8.47923 0.182459 7.74975 0.0514136C7.3477 -0.0204498 6.94566 -0.00354096 6.54361 0.0218226C6.15815 0.0471862 5.76854 0.0598681 5.39136 0.140186C4.33858 0.360004 3.38943 0.816548 2.55632 1.50982C1.26315 2.58777 0.430045 3.95317 0.106751 5.6314C-0.0134478 6.25703 -0.0175924 6.89112 0.0238552 7.52098C0.106751 8.81452 0.570968 9.96434 1.3046 11.0127C1.83513 11.7736 2.51073 12.3823 3.28995 12.86C3.54278 13.0122 3.63811 13.1813 3.62153 13.4814C3.59252 13.9887 3.61325 14.5002 3.61325 15.0117C3.61325 15.13 3.62568 15.2484 3.63811 15.3668C3.66298 15.5528 3.85779 15.7176 4.04016 15.7176C4.9603 15.7092 5.8763 15.7092 6.79644 15.7092ZM4.51681 14.7834C4.51681 14.7285 4.51681 14.6777 4.51681 14.627C4.51266 14.0648 4.5251 13.5068 4.50852 12.9446C4.49194 12.4753 4.46293 12.45 4.08161 12.2513C3.42673 11.9089 2.85889 11.4523 2.38224 10.8817C1.56157 9.90516 1.06834 8.7807 0.93571 7.49562C0.873538 6.89957 0.898407 6.30776 1.02275 5.72862C1.23414 4.73099 1.61545 3.81367 2.29935 3.04854C2.67652 2.62581 3.06613 2.20309 3.54693 1.89873C4.69089 1.17164 5.92604 0.808093 7.28553 0.939138C8.03574 1.011 8.74864 1.20968 9.42424 1.54364C9.95477 1.80573 10.419 2.16082 10.8459 2.57931C11.4593 3.17536 11.9401 3.8644 12.251 4.66758C12.6074 5.57644 12.7318 6.51912 12.6406 7.49139C12.5826 8.11703 12.4334 8.72152 12.1847 9.29643C11.6127 10.6111 10.6925 11.6045 9.44911 12.2809C9.36207 12.3274 9.27503 12.3823 9.20042 12.45C9.14654 12.5007 9.10095 12.581 9.09266 12.6486C9.07193 12.8304 9.06364 13.0122 9.06364 13.194C9.0595 13.6801 9.06364 14.162 9.0595 14.6481C9.0595 14.6946 9.0595 14.7369 9.0595 14.7834C7.54251 14.7834 6.0338 14.7834 4.51681 14.7834Z"
                                                                                                                fill="#0A0B29" />
                                                                                                            <path
                                                                                                                d="M6.80505 17.5691C7.71276 17.5691 8.62047 17.5691 9.52818 17.5691C9.84318 17.5691 10.0711 17.1675 9.9095 16.8927C9.81417 16.7363 9.70226 16.6307 9.48259 16.6349C7.67546 16.6476 5.86418 16.6433 4.05705 16.6349C3.83324 16.6349 3.73791 16.7702 3.65501 16.9266C3.56382 17.0957 3.62185 17.2521 3.73376 17.3958C3.83324 17.5226 3.94929 17.5733 4.11508 17.5691C5.01036 17.5649 5.90563 17.5691 6.80505 17.5691Z"
                                                                                                                fill="#0A0B29" />
                                                                                                            <path
                                                                                                                d="M6.79243 19.1982C7.40586 19.1982 8.01929 19.194 8.62857 19.2024C8.8441 19.2067 8.93114 19.0587 9.01404 18.9065C9.10108 18.7459 9.05549 18.5937 8.94358 18.45C8.83996 18.3147 8.71976 18.2725 8.54982 18.2725C7.35612 18.2809 6.16657 18.2809 4.97287 18.2725C4.74905 18.2725 4.65372 18.4035 4.56254 18.5599C4.46721 18.7205 4.52109 18.8812 4.62471 19.0207C4.71589 19.139 4.81951 19.2109 4.98945 19.2067C5.59044 19.1898 6.19144 19.1982 6.79243 19.1982Z"
                                                                                                                fill="#0A0B29" />
                                                                                                        </svg>
                                                                                                    </span>
                                                                                                    <h3>Analice:</h3>
                                                                                                </div>
                                                                                                <ul>
                                                                                                    <li>Es importante comparar el número de estudiantes cada semana para saber si es menor, mayor o igual a las semanas anteriores. Converse con sus estudiantes y padres de familia para seguir fortaleciendo su participación en Conecta Ideas o invitarlos a probarla ya que es gratuita y no consume su plan de datos de internet.</li>
                                                                                                
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="m-accordion__box">
                                                                                    <div class="m-accordion__toggle" id="tab-report-accordion-1">
                                                                                        <h3 class="m-accordion__toggle__title">2. ¿Qué estudiantes NO usaron Conecta Ideas esta semana?</h3>
                                                                                    </div>
                                                                                    <div class="m-accordion__content m-content-editor">
                                                                                        <div class="m-content-editor">
                                                                                            <ul class="m-intranet__content__list">
                                                                                                ${reporte_02}
                                                                                            </ul>
                                                                                            <div class="m-intranet__content__card__info">
                                                                                                <div class="m-intranet__content__card__info__title">
                                                                                                    <span class="m-icon-svg m-icon-svg--secondary">
                                                                                                        <svg width="14" height="20" viewBox="0 0 14 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                                                            <path
                                                                                                                d="M6.79644 15.7092C7.71244 15.7092 8.62844 15.7092 9.54858 15.7092C9.7351 15.7092 9.92991 15.5316 9.94234 15.3372C9.95477 15.1427 9.96721 14.9483 9.96721 14.7538C9.97135 14.2381 9.97135 13.7266 9.96721 13.2109C9.96721 13.1136 9.99208 13.0587 10.0708 13.008C10.6262 12.6486 11.1651 12.2724 11.6251 11.7821C12.595 10.7464 13.2333 9.53738 13.4944 8.12548C13.6022 7.53789 13.5732 6.94185 13.5525 6.35003C13.5276 5.56799 13.3203 4.83244 13.0136 4.12649C12.6199 3.21763 12.0479 2.43982 11.3267 1.77614C10.8749 1.36186 10.39 1.00677 9.84701 0.732002C9.18384 0.393822 8.47923 0.182459 7.74975 0.0514136C7.3477 -0.0204498 6.94566 -0.00354096 6.54361 0.0218226C6.15815 0.0471862 5.76854 0.0598681 5.39136 0.140186C4.33858 0.360004 3.38943 0.816548 2.55632 1.50982C1.26315 2.58777 0.430045 3.95317 0.106751 5.6314C-0.0134478 6.25703 -0.0175924 6.89112 0.0238552 7.52098C0.106751 8.81452 0.570968 9.96434 1.3046 11.0127C1.83513 11.7736 2.51073 12.3823 3.28995 12.86C3.54278 13.0122 3.63811 13.1813 3.62153 13.4814C3.59252 13.9887 3.61325 14.5002 3.61325 15.0117C3.61325 15.13 3.62568 15.2484 3.63811 15.3668C3.66298 15.5528 3.85779 15.7176 4.04016 15.7176C4.9603 15.7092 5.8763 15.7092 6.79644 15.7092ZM4.51681 14.7834C4.51681 14.7285 4.51681 14.6777 4.51681 14.627C4.51266 14.0648 4.5251 13.5068 4.50852 12.9446C4.49194 12.4753 4.46293 12.45 4.08161 12.2513C3.42673 11.9089 2.85889 11.4523 2.38224 10.8817C1.56157 9.90516 1.06834 8.7807 0.93571 7.49562C0.873538 6.89957 0.898407 6.30776 1.02275 5.72862C1.23414 4.73099 1.61545 3.81367 2.29935 3.04854C2.67652 2.62581 3.06613 2.20309 3.54693 1.89873C4.69089 1.17164 5.92604 0.808093 7.28553 0.939138C8.03574 1.011 8.74864 1.20968 9.42424 1.54364C9.95477 1.80573 10.419 2.16082 10.8459 2.57931C11.4593 3.17536 11.9401 3.8644 12.251 4.66758C12.6074 5.57644 12.7318 6.51912 12.6406 7.49139C12.5826 8.11703 12.4334 8.72152 12.1847 9.29643C11.6127 10.6111 10.6925 11.6045 9.44911 12.2809C9.36207 12.3274 9.27503 12.3823 9.20042 12.45C9.14654 12.5007 9.10095 12.581 9.09266 12.6486C9.07193 12.8304 9.06364 13.0122 9.06364 13.194C9.0595 13.6801 9.06364 14.162 9.0595 14.6481C9.0595 14.6946 9.0595 14.7369 9.0595 14.7834C7.54251 14.7834 6.0338 14.7834 4.51681 14.7834Z"
                                                                                                                fill="#0A0B29" />
                                                                                                            <path
                                                                                                                d="M6.80505 17.5691C7.71276 17.5691 8.62047 17.5691 9.52818 17.5691C9.84318 17.5691 10.0711 17.1675 9.9095 16.8927C9.81417 16.7363 9.70226 16.6307 9.48259 16.6349C7.67546 16.6476 5.86418 16.6433 4.05705 16.6349C3.83324 16.6349 3.73791 16.7702 3.65501 16.9266C3.56382 17.0957 3.62185 17.2521 3.73376 17.3958C3.83324 17.5226 3.94929 17.5733 4.11508 17.5691C5.01036 17.5649 5.90563 17.5691 6.80505 17.5691Z"
                                                                                                                fill="#0A0B29" />
                                                                                                            <path
                                                                                                                d="M6.79243 19.1982C7.40586 19.1982 8.01929 19.194 8.62857 19.2024C8.8441 19.2067 8.93114 19.0587 9.01404 18.9065C9.10108 18.7459 9.05549 18.5937 8.94358 18.45C8.83996 18.3147 8.71976 18.2725 8.54982 18.2725C7.35612 18.2809 6.16657 18.2809 4.97287 18.2725C4.74905 18.2725 4.65372 18.4035 4.56254 18.5599C4.46721 18.7205 4.52109 18.8812 4.62471 19.0207C4.71589 19.139 4.81951 19.2109 4.98945 19.2067C5.59044 19.1898 6.19144 19.1982 6.79243 19.1982Z"
                                                                                                                fill="#0A0B29" />
                                                                                                        </svg>
                                                                                                    </span>
                                                                                                    <h3>Analice:</h3>
                                                                                                </div>
                                                                                                <ul>
                                                                                                    <li>¿La mayoría de los estudiantes de su aula ha usado Conecta Ideas esta semana?</li>
                                                                                                    <li>¿Qué estrategias puede usar para que los estudiantes de este listado usen Conecta Ideas?</li>
                                                                                                </ul>
                                                                                                

                                                                                            </div>

                                                                                            <p>${p02_texto}</p>

                                                                                            <!--<div class="m-intranet__content__card__alert">
                                                                                                <span class="m-icon-svg m-icon-svg--secondary">
                                                                                                    <svg width="56" height="56" viewBox="0 0 56 56" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                                                        <circle opacity="0.4" cx="28" cy="28" r="28" fill="#FFAD12" />
                                                                                                        <path d="M28.5 12L31.98 22.7102H43.2414L34.1307 29.3295L37.6107 40.0398L28.5 33.4205L19.3893 40.0398L22.8693 29.3295L13.7586 22.7102H25.02L28.5 12Z" fill="#FFAD12" />
                                                                                                    </svg>
                                                                                                </span>
                                                                                                <p>¿Cómo incrementar la cantidad de estudiantes que usan Conecta Ideas? 
                                                                                                    <a class="m-video__modal--js" href="https://youtu.be/v3jCNGIhsNQ">video</a>
                                                                                                </p>
                                                                                            </div>-->
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="m-accordion__box">
                                                                                    <div class="m-accordion__toggle" id="tab-report-accordion-2">
                                                                                        <h3 class="m-accordion__toggle__title">3. ¿Cuánto están aprendiendo mis estudiantes?</h3>
                                                                                    </div>
                                                                                    <div class="m-accordion__content m-content-editor">
                                                                                        <div class="m-content-editor">
                                                                                            ${reporte_03}
                                                                                            <div class="m-intranet__content__card__info">
                                                                                                <div class="m-intranet__content__card__info__title">
                                                                                                    <span class="m-icon-svg m-icon-svg--secondary">
                                                                                                        <svg width="14" height="20" viewBox="0 0 14 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                                                            <path
                                                                                                                d="M6.79644 15.7092C7.71244 15.7092 8.62844 15.7092 9.54858 15.7092C9.7351 15.7092 9.92991 15.5316 9.94234 15.3372C9.95477 15.1427 9.96721 14.9483 9.96721 14.7538C9.97135 14.2381 9.97135 13.7266 9.96721 13.2109C9.96721 13.1136 9.99208 13.0587 10.0708 13.008C10.6262 12.6486 11.1651 12.2724 11.6251 11.7821C12.595 10.7464 13.2333 9.53738 13.4944 8.12548C13.6022 7.53789 13.5732 6.94185 13.5525 6.35003C13.5276 5.56799 13.3203 4.83244 13.0136 4.12649C12.6199 3.21763 12.0479 2.43982 11.3267 1.77614C10.8749 1.36186 10.39 1.00677 9.84701 0.732002C9.18384 0.393822 8.47923 0.182459 7.74975 0.0514136C7.3477 -0.0204498 6.94566 -0.00354096 6.54361 0.0218226C6.15815 0.0471862 5.76854 0.0598681 5.39136 0.140186C4.33858 0.360004 3.38943 0.816548 2.55632 1.50982C1.26315 2.58777 0.430045 3.95317 0.106751 5.6314C-0.0134478 6.25703 -0.0175924 6.89112 0.0238552 7.52098C0.106751 8.81452 0.570968 9.96434 1.3046 11.0127C1.83513 11.7736 2.51073 12.3823 3.28995 12.86C3.54278 13.0122 3.63811 13.1813 3.62153 13.4814C3.59252 13.9887 3.61325 14.5002 3.61325 15.0117C3.61325 15.13 3.62568 15.2484 3.63811 15.3668C3.66298 15.5528 3.85779 15.7176 4.04016 15.7176C4.9603 15.7092 5.8763 15.7092 6.79644 15.7092ZM4.51681 14.7834C4.51681 14.7285 4.51681 14.6777 4.51681 14.627C4.51266 14.0648 4.5251 13.5068 4.50852 12.9446C4.49194 12.4753 4.46293 12.45 4.08161 12.2513C3.42673 11.9089 2.85889 11.4523 2.38224 10.8817C1.56157 9.90516 1.06834 8.7807 0.93571 7.49562C0.873538 6.89957 0.898407 6.30776 1.02275 5.72862C1.23414 4.73099 1.61545 3.81367 2.29935 3.04854C2.67652 2.62581 3.06613 2.20309 3.54693 1.89873C4.69089 1.17164 5.92604 0.808093 7.28553 0.939138C8.03574 1.011 8.74864 1.20968 9.42424 1.54364C9.95477 1.80573 10.419 2.16082 10.8459 2.57931C11.4593 3.17536 11.9401 3.8644 12.251 4.66758C12.6074 5.57644 12.7318 6.51912 12.6406 7.49139C12.5826 8.11703 12.4334 8.72152 12.1847 9.29643C11.6127 10.6111 10.6925 11.6045 9.44911 12.2809C9.36207 12.3274 9.27503 12.3823 9.20042 12.45C9.14654 12.5007 9.10095 12.581 9.09266 12.6486C9.07193 12.8304 9.06364 13.0122 9.06364 13.194C9.0595 13.6801 9.06364 14.162 9.0595 14.6481C9.0595 14.6946 9.0595 14.7369 9.0595 14.7834C7.54251 14.7834 6.0338 14.7834 4.51681 14.7834Z"
                                                                                                                fill="#0A0B29" />
                                                                                                            <path
                                                                                                                d="M6.80505 17.5691C7.71276 17.5691 8.62047 17.5691 9.52818 17.5691C9.84318 17.5691 10.0711 17.1675 9.9095 16.8927C9.81417 16.7363 9.70226 16.6307 9.48259 16.6349C7.67546 16.6476 5.86418 16.6433 4.05705 16.6349C3.83324 16.6349 3.73791 16.7702 3.65501 16.9266C3.56382 17.0957 3.62185 17.2521 3.73376 17.3958C3.83324 17.5226 3.94929 17.5733 4.11508 17.5691C5.01036 17.5649 5.90563 17.5691 6.80505 17.5691Z"
                                                                                                                fill="#0A0B29" />
                                                                                                            <path
                                                                                                                d="M6.79243 19.1982C7.40586 19.1982 8.01929 19.194 8.62857 19.2024C8.8441 19.2067 8.93114 19.0587 9.01404 18.9065C9.10108 18.7459 9.05549 18.5937 8.94358 18.45C8.83996 18.3147 8.71976 18.2725 8.54982 18.2725C7.35612 18.2809 6.16657 18.2809 4.97287 18.2725C4.74905 18.2725 4.65372 18.4035 4.56254 18.5599C4.46721 18.7205 4.52109 18.8812 4.62471 19.0207C4.71589 19.139 4.81951 19.2109 4.98945 19.2067C5.59044 19.1898 6.19144 19.1982 6.79243 19.1982Z"
                                                                                                                fill="#0A0B29" />
                                                                                                        </svg>
                                                                                                    </span>
                                                                                                    <h3>Analice:</h3>
                                                                                                </div>
                                                                                                <ul>
                                                                                                    <li>¿Cómo ha sido el desempeño de sus estudiantes en esta competencia matemática?</li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="m-accordion__box">
                                                                                    <div class="m-accordion__toggle" id="tab-report-accordion-3">
                                                                                        <h3 class="m-accordion__toggle__title">4. ¿Qué fue lo más difícil para mis estudiantes esta semana?</h3>
                                                                                    </div>
                                                                                    <div class="m-accordion__content m-content-editor">
                                                                                        <div class="m-content-editor">
                                                                                            <p>A continuación, se muestra la capacidad matemática donde sus
                                                                                                estudiantes tuvieron un desempeño más bajo y, además, los
                                                                                                problemas de matemática en los que se equivocaron la mitad de
                                                                                                sus estudiantes o más.</p>
                                                                                            <div class="m-intranet__content__card__info m-intranet__content__card__info--danger">
                                                                                                <div class="m-intranet__content__card__info__title">
                                                                                                    <span class="m-icon-svg m-icon-svg--secondary">
                                                                                                        <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                                                            <path
                                                                                                                d="M5.9997 0C9.30187 0 11.9994 2.69754 11.9994 5.9997C11.9994 9.34838 9.30187 12.0459 5.95319 11.9994C2.69754 11.9994 0 9.30187 0 5.9997C0 2.69754 2.69754 0 5.9997 0ZM6.83687 4.79046C6.83687 4.27886 6.83687 3.76726 6.83687 3.25565C6.83687 2.74405 6.51131 2.41849 6.04621 2.41849C5.58112 2.41849 5.25555 2.74405 5.25555 3.25565C5.25555 4.27886 5.25555 5.34857 5.25555 6.37178C5.25555 6.83687 5.62763 7.20895 6.09272 7.20895C6.55782 7.20895 6.88338 6.83687 6.88338 6.32527C6.83687 5.81367 6.83687 5.30206 6.83687 4.79046ZM6.83687 9.02281C6.83687 8.55772 6.55782 8.27866 6.09272 8.23215C5.67414 8.23215 5.30206 8.51121 5.30206 8.92979C5.30206 9.39489 5.58112 9.76696 5.9997 9.76696C6.51131 9.81347 6.79036 9.53441 6.83687 9.02281Z"
                                                                                                                fill="#EE4266" />
                                                                                                            <path
                                                                                                                d="M6.83779 4.79031C6.83779 5.30191 6.83779 5.81352 6.83779 6.37163C6.83779 6.88323 6.51222 7.2088 6.04713 7.25531C5.58204 7.25531 5.25647 6.88323 5.20996 6.41814C5.20996 5.39493 5.20996 4.32522 5.20996 3.30201C5.20996 2.79041 5.58204 2.46484 6.00062 2.46484C6.46571 2.46484 6.79128 2.79041 6.79128 3.30201C6.83779 3.7671 6.83779 4.27871 6.83779 4.79031Z"
                                                                                                                fill="white" />
                                                                                                        </svg>
                                                                                                    </span>
                                                                                                    <h3>Capacidad con más bajos resultados:</h3>
                                                                                                </div>
                                                                                                <p>${reporte_04_competencia}</p>
                                                                                            </div>
                                                                                            <table>
                                                                                                <thead>
                                                                                                    <tr>
                                                                                                        <td>Nº de problema</td>
                                                                                                        <td>Cantidad de estudiantes que se equivocaron</td>
                                                                                                    </tr>
                                                                                                </thead>
                                                                                                <tbody>
                                                                                                    ${reporte_04}
                                                                                                </tbody>
                                                                                            </table>
                                                                                            <div class="m-intranet__content__card__info">
                                                                                                <div class="m-intranet__content__card__info__title">
                                                                                                    <span class="m-icon-svg m-icon-svg--secondary">
                                                                                                        <svg width="14" height="20" viewBox="0 0 14 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                                                            <path
                                                                                                                d="M6.79644 15.7092C7.71244 15.7092 8.62844 15.7092 9.54858 15.7092C9.7351 15.7092 9.92991 15.5316 9.94234 15.3372C9.95477 15.1427 9.96721 14.9483 9.96721 14.7538C9.97135 14.2381 9.97135 13.7266 9.96721 13.2109C9.96721 13.1136 9.99208 13.0587 10.0708 13.008C10.6262 12.6486 11.1651 12.2724 11.6251 11.7821C12.595 10.7464 13.2333 9.53738 13.4944 8.12548C13.6022 7.53789 13.5732 6.94185 13.5525 6.35003C13.5276 5.56799 13.3203 4.83244 13.0136 4.12649C12.6199 3.21763 12.0479 2.43982 11.3267 1.77614C10.8749 1.36186 10.39 1.00677 9.84701 0.732002C9.18384 0.393822 8.47923 0.182459 7.74975 0.0514136C7.3477 -0.0204498 6.94566 -0.00354096 6.54361 0.0218226C6.15815 0.0471862 5.76854 0.0598681 5.39136 0.140186C4.33858 0.360004 3.38943 0.816548 2.55632 1.50982C1.26315 2.58777 0.430045 3.95317 0.106751 5.6314C-0.0134478 6.25703 -0.0175924 6.89112 0.0238552 7.52098C0.106751 8.81452 0.570968 9.96434 1.3046 11.0127C1.83513 11.7736 2.51073 12.3823 3.28995 12.86C3.54278 13.0122 3.63811 13.1813 3.62153 13.4814C3.59252 13.9887 3.61325 14.5002 3.61325 15.0117C3.61325 15.13 3.62568 15.2484 3.63811 15.3668C3.66298 15.5528 3.85779 15.7176 4.04016 15.7176C4.9603 15.7092 5.8763 15.7092 6.79644 15.7092ZM4.51681 14.7834C4.51681 14.7285 4.51681 14.6777 4.51681 14.627C4.51266 14.0648 4.5251 13.5068 4.50852 12.9446C4.49194 12.4753 4.46293 12.45 4.08161 12.2513C3.42673 11.9089 2.85889 11.4523 2.38224 10.8817C1.56157 9.90516 1.06834 8.7807 0.93571 7.49562C0.873538 6.89957 0.898407 6.30776 1.02275 5.72862C1.23414 4.73099 1.61545 3.81367 2.29935 3.04854C2.67652 2.62581 3.06613 2.20309 3.54693 1.89873C4.69089 1.17164 5.92604 0.808093 7.28553 0.939138C8.03574 1.011 8.74864 1.20968 9.42424 1.54364C9.95477 1.80573 10.419 2.16082 10.8459 2.57931C11.4593 3.17536 11.9401 3.8644 12.251 4.66758C12.6074 5.57644 12.7318 6.51912 12.6406 7.49139C12.5826 8.11703 12.4334 8.72152 12.1847 9.29643C11.6127 10.6111 10.6925 11.6045 9.44911 12.2809C9.36207 12.3274 9.27503 12.3823 9.20042 12.45C9.14654 12.5007 9.10095 12.581 9.09266 12.6486C9.07193 12.8304 9.06364 13.0122 9.06364 13.194C9.0595 13.6801 9.06364 14.162 9.0595 14.6481C9.0595 14.6946 9.0595 14.7369 9.0595 14.7834C7.54251 14.7834 6.0338 14.7834 4.51681 14.7834Z"
                                                                                                                fill="#0A0B29" />
                                                                                                            <path
                                                                                                                d="M6.80505 17.5691C7.71276 17.5691 8.62047 17.5691 9.52818 17.5691C9.84318 17.5691 10.0711 17.1675 9.9095 16.8927C9.81417 16.7363 9.70226 16.6307 9.48259 16.6349C7.67546 16.6476 5.86418 16.6433 4.05705 16.6349C3.83324 16.6349 3.73791 16.7702 3.65501 16.9266C3.56382 17.0957 3.62185 17.2521 3.73376 17.3958C3.83324 17.5226 3.94929 17.5733 4.11508 17.5691C5.01036 17.5649 5.90563 17.5691 6.80505 17.5691Z"
                                                                                                                fill="#0A0B29" />
                                                                                                            <path
                                                                                                                d="M6.79243 19.1982C7.40586 19.1982 8.01929 19.194 8.62857 19.2024C8.8441 19.2067 8.93114 19.0587 9.01404 18.9065C9.10108 18.7459 9.05549 18.5937 8.94358 18.45C8.83996 18.3147 8.71976 18.2725 8.54982 18.2725C7.35612 18.2809 6.16657 18.2809 4.97287 18.2725C4.74905 18.2725 4.65372 18.4035 4.56254 18.5599C4.46721 18.7205 4.52109 18.8812 4.62471 19.0207C4.71589 19.139 4.81951 19.2109 4.98945 19.2067C5.59044 19.1898 6.19144 19.1982 6.79243 19.1982Z"
                                                                                                                fill="#0A0B29" />
                                                                                                        </svg>
                                                                                                    </span>
                                                                                                    <h3>Analice:</h3>
                                                                                                </div>
                                                                                                <ul>
                                                                                                    <li>¿Qué capacidad matemática tiene más bajos resultados en su aula?</li>
                                                                                                    <li>¿A qué desempeños corresponden los problemas matemáticos con más errores? Use la matriz de la actividad para este análisis. La puede encontrar en la sección de Recursos en el Portal Docente (<a href="<?= \Yii::$app->request->BaseUrl ?>/actividad?docente_seccion_id=<?= $docente_seccion_id ?>&grado=<?= $grado->grado ?>&seccion=<?= $seccion ?>" target="_blank">https://portal.conectaideasperu.com/recurso</a>).</li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="m-accordion__box">
                                                                                    <div class="m-accordion__toggle" id="tab-report-accordion-4">
                                                                                        <h3 class="m-accordion__toggle__title">5. ¿Cómo le fue en esta actividad a cada estudiante de mi aula?</h3>
                                                                                    </div>
                                                                                    <div class="m-accordion__content m-content-editor">
                                                                                        <div class="m-content-editor">
                                                                                            <div class="m-intranet__content__score__legend-quiz">
                                                                                                <p>En las tablas que se encuentran a continuación:</p>
                                                                                                <p data-letter="P"><span>= pregunta</span></p>
                                                                                                <p data-letter="R"><span>= respuesta</span></p>
                                                                                            </div>
                                                                                            <div class="m-intranet__content__score__legend">
                                                                                                <p>Respuesta correcta</p>
                                                                                                <p>Respuesta incorrecta</p>
                                                                                                <p>El estudiante no vio la pregunta.</p>
                                                                                            </div>

                                                                                            <div class="m-intranet__content__grid m-intranet__content__grid--same">

                                                                                                <div class="m-intranet__content__grid__left">
                                                                                                    <div class="m-intranet__content__score__header">
                                                                                                        <p>Nombre de estudiante</p>
                                                                                                        <p>Puntaje</p>
                                                                                                    </div>
                                                                                                    <div class="m-intranet__content__score">

                                                                                                        ${reporte_05_left}

                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="m-intranet__content__grid__right">
                                                                                                    <div class="m-intranet__content__score__header">
                                                                                                        <p>Nombre de estudiante</p>
                                                                                                        <p>Puntaje</p>
                                                                                                    </div>
                                                                                                    <div class="m-intranet__content__score">

                                                                                                        ${reporte_05_right}

                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>

                                                                                            
                                                                                            <p>${p05_texto_01} ${p05_texto_02}</p>

                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <button data-actividad_cabecera_id="${actividad_cabecera_id}" data-correlativo_actividad="${correlativo}" class="m-button m-button--full m-button--icon m-button--icon--auto btn-reporte-semanal-pdf" type="button">
                                                                                <span>
                                                                                    <span class="m-icon-svg m-icon-svg--secondary">
                                                                                        <svg width="18" height="16" viewBox="0 0 18 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                                            <path
                                                                                                d="M18 13.7259V8.4467C18 8.04061 17.6757 7.67513 17.2297 7.67513C16.7838 7.67513 16.4595 8 16.4595 8.4467V13.7259C16.4595 14.132 16.1351 14.4975 15.6892 14.4975H2.27027C1.86486 14.4975 1.5 14.1726 1.5 13.7259V8.4467C1.5 8.04061 1.17568 7.67513 0.72973 7.67513C0.283784 7.67513 0 8.04061 0 8.48731V13.7665C0 15.0254 1.01351 16.0406 2.27027 16.0406H15.7703C16.9865 16 18 14.9848 18 13.7259ZM13.2162 8.56853L9.44595 11.5736C9.16216 11.7766 8.7973 11.7766 8.51351 11.5736L4.74324 8.56853C4.45946 8.28426 4.41892 7.79695 4.7027 7.51269C4.94595 7.22842 5.35135 7.18782 5.67568 7.39086L8.22973 9.42132V0.771573C8.22973 0.365482 8.55405 0 9 0C9.40541 0 9.77027 0.324873 9.77027 0.771573V9.46193L12.3243 7.43147C12.6081 7.14721 13.0946 7.14721 13.3784 7.47208C13.6622 7.75634 13.6622 8.24365 13.3378 8.52792C13.2973 8.52792 13.2568 8.52792 13.2162 8.56853Z"
                                                                                                fill="white" />
                                                                                        </svg> 
                                                                                    </span>
                                                                                </span>
                                                                                <span>Descargar reporte</span>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                        `;
                                    });
                                    
                                }
                                i++;

                            });

                            $('.lista-actividades').html(actividades);
                        
                            $.each(results.actividades, function( index, value ) {
                                correlativo = value.correlativo;
                                xAxis = [];
                                listaConectados = [];
                                listaNoConectados = [];
                                ningun_estudiante = 0 ;
                                $.each(value.reporte01, function( index2, value2 ) {
                                    xAxis.push(value2.fecha_fin);
                                    listaConectados.push(parseInt(value2.conectados));
                                    listaNoConectados.push(parseInt(value2.no_conectados));
                                    ningun_estudiante = ningun_estudiante + parseInt(value2.conectados);
                                });

                                if(ningun_estudiante!=0){
                                    data = [ {
                                        name: 'No Conectados',
                                        color : '#808793',
                                        data: listaNoConectados,
                                        dataLabels: {
                                            style:{
                                                fontSize: 16
                                            }
                                        }
                                    },{
                                        name: 'Conectados',
                                        color : '#2AB7CA',
                                        data: listaConectados,
                                        dataLabels: {
                                            style:{
                                                fontSize: 16
                                            }
                                        }
                                    },]
                                    Grafico01(correlativo,xAxis,data);
                                    

                                    $.each(value.reporte03, function( index2, value2 ) {
                                        listaArrayReporte03 = [];

                                        if(parseInt(value2.avanzado)>0){
                                            listaArrayReporte03.push({
                                                name: 'Avanzado',
                                                y: parseInt(value2.avanzado),
                                                color:'#4f9c2e',
                                                dataLabels: {
                                                    style:{
                                                        fontSize: 16
                                                    }
                                                }
                                            })
                                        }

                                        if(parseInt(value2.intermedio)>0){
                                            listaArrayReporte03.push({
                                                name: 'Intermedio',
                                                y: parseInt(value2.intermedio),
                                                color:'#ffad12',
                                                dataLabels: {
                                                    style:{
                                                        fontSize: 16
                                                    }
                                                }
                                            })
                                        }

                                        if(parseInt(value2.inicial)>0){
                                            listaArrayReporte03.push({
                                                name: 'Inicial',
                                                y: parseInt(value2.inicial),
                                                color:'#ee4266',
                                                dataLabels: {
                                                    style:{
                                                        fontSize: 16
                                                    }
                                                }
                                            })
                                        }

                                        if(parseInt(value2.ninguno)>0){
                                            listaArrayReporte03.push({
                                                name: 'No usó el app',
                                                y: parseInt(value2.ninguno),
                                                color:'#808793',
                                                dataLabels: {
                                                    style:{
                                                        fontSize: 16
                                                    }
                                                }
                                            })
                                        }


                                        // data = [{
                                        //     name: 'Avanzado',
                                        //     y: parseInt(value2.avanzado),
                                        //     color:'#4f9c2e'
                                        // }, {
                                        //     name: 'Intermedio',
                                        //     y: parseInt(value2.intermedio),
                                        //     color:'#ffad12'
                                        // }, {
                                        //     name: 'Inicial',
                                        //     y: parseInt(value2.inicial),
                                        //     color:'#ee4266'
                                        // }, {
                                        //     name: 'No usó el app',
                                        //     y: parseInt(value2.ninguno),
                                        //     color:'#808793'
                                        // }]
                                        Grafico03(correlativo,value2.id,listaArrayReporte03);
                                    });
                                }
                                
                            });
                            
                            /* carga del modal video */
                            $('.m-video__modal--jsxxx').magnificPopup({
                                type: "iframe",
                                removalDelay: 300,
                                iframe:
                                    ((s = 0),
                                    {
                                        markup:
                                            '<div class="mfp-iframe-scaler"><div class="mfp-close"></div><iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe></div><div class="mfp-bottom-bar"><div class="mfp-title"></div></div>',
                                        patterns: {
                                            youtube: {
                                                index: "youtu",
                                                id: function (e) {
                                                    var t = e.match(/^.*(?:youtu.be\/|v\/|e\/|u\/\w+\/|embed\/|v=)([^#\&\?]*).*/);
                                                    if (!t || !t[1]) return null;
                                                    if (-1 != e.indexOf("t=")) {
                                                        var n = e.split("t=")[1].replace("h", ":").replace("m", ":").replace("s", "").split(":");
                                                        1 == n.length ? (s = n[0]) : 2 == n.length ? (s = 60 * +n[0] + +n[1]) : 3 == n.length && (s = 60 * +n[0] * 60 + 60 * +n[1] + +n[2]);
                                                    }
                                                    var i = "?autoplay=1";
                                                    return s > 0 && (i = "?start=" + s + "&autoplay=1"), t[1] + i;
                                                },
                                                src: "//www.youtube.com/embed/%id%",
                                            },
                                            vimeo: {
                                                index: "vimeo.com/",
                                                id: function (e) {
                                                    var t = e.match(/(https?:\/\/)?(www.)?(player.)?vimeo.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/);
                                                    return t && t[5] ? t[5] : null;
                                                },
                                                src: "//player.vimeo.com/video/%id%?autoplay=1",
                                            },
                                        },
                                    }),
                                mainClass: "mfp-zoom-in",
                            });
                        
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}


listaGraficoReport01 = [];
function Grafico01(correlativo_actividad,xAxis,data){
    listaGraficoReport01.push({'actividad':correlativo_actividad,'grafico':Highcharts.chart('grafico_01_'+correlativo_actividad, {
        credits: {
            enabled: false
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            title: {
                text: 'Semana'
            },
            categories: xAxis
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Número de estudiantes'
            },
            stackLabels: {
                enabled: false,
                style: {
                    fontWeight: 'bold',
                    color: ( // theme
                        Highcharts.defaultOptions.title.style &&
                        Highcharts.defaultOptions.title.style.color
                    ) || 'gray'
                }
            }
        },
        legend: {
            align: 'right',
            x: -30,
            verticalAlign: 'top',
            y: -10,
            floating: true,
            backgroundColor:
                Highcharts.defaultOptions.legend.backgroundColor || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                grouping: false,
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                }
            }
            
        },
        series: data
    })});
}

listaGraficoReport03 = [];
function Grafico03(correlativo_actividad,identificador,data){
    listaGraficoReport03.push({'actividad':correlativo_actividad,'grafico':Highcharts.chart('grafico_03_' + correlativo_actividad + '_' + identificador, {
        credits: {
            enabled: false
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '<b>{point.percentage:.1f}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    distance: -50,
                    format: '{point.percentage:.1f} %'
                }
            }
        },
        series: [{
            data: data
        }]
    })});
}

function Puntaje(valor){
    if(valor == -1){
        return 'm-intranet__content__score__item__table__data__reply--incorrect'
    }else if(valor == 0){
        return ''
    }else if(valor == 1){
        return 'm-intranet__content__score__item__table__data__reply--correct'
    }
}

$('body').on('click', '.btn-seleccionar-experiencia', function (e) {
    e.preventDefault();
    $('.m-intranet--report__tab__button').removeClass('js--active');
    $( ".btn-seleccionar-experiencia" ).each(function( index ) {
        $(this).removeClass('js--active');
    });

    $(this).addClass('js--active');
    
    var experiencia_id = $(this).attr('data-experiencia_id');
    var experiencia_correlativo = $(this).attr('data-experiencia_correlativo');
    $('.experiencia_mobil').html('Experiencia ' + experiencia_correlativo);
    Actividades(experiencia_id,experiencia_correlativo);
});



$('body').on('click', '.btn-score_item_puntaje', function (e) {
    e.preventDefault();

    if($(this).hasClass('js--active')){
        $(this).removeClass('js--active');
    }else{
        $( ".btn-score_item_puntaje" ).each(function( index ) {
            $(this).removeClass('js--active');
        });

        $(this).addClass('js--active');
    }
});

$('body').on('click', '.btn-reporte-semanal-pdf', function (e) {
    e.preventDefault();
    
    var actividad_cabecera_id = $(this).attr('data-actividad_cabecera_id');
    var correlativo_actividad = $(this).attr('data-correlativo_actividad');
    var grafico_reporte01;
    var grafico_reporte03_1;
    var grafico_reporte03_2;
    $.each(listaGraficoReport01, function (i, v) {
        if (v.actividad == correlativo_actividad) {
            grafico_reporte01 = v.grafico;
        }
    });
    var contador_reporte_03=0;
    $.each(listaGraficoReport03, function (i, v) {
        if (v.actividad == correlativo_actividad) {
            if(contador_reporte_03==0){
                grafico_reporte03_1 = v.grafico;
            }

            if(contador_reporte_03==1){
                grafico_reporte03_2 = v.grafico;
            }
            
            contador_reporte_03++
        }
    });


    
    if(grafico_reporte01){
        Highcharts.exportChartsReporte01([grafico_reporte01]);
    }
    if(grafico_reporte03_1){
        Highcharts.exportChartsReporte03([grafico_reporte03_1]);
    }
    if(grafico_reporte03_2){
        Highcharts.exportChartsReporte03([grafico_reporte03_2]);
    }
    
    //ListaImagenReporte01;
    //console.log(window.btoa());
    //console.log(Highcharts.exportCharts([grafico_reporte01]));
    
    $.ajax({
        url: '<?= \Yii::$app->request->BaseUrl ?>/reporte/reporte-semanal-pdf-demo',
        method: 'POST',
        data:{_csrf:csrf,docente_seccion_id:docente_seccion_id,grado_id:grado_id,actividad_cabecera_id:actividad_cabecera_id,imagen01_svg:ListaImagenReporte01[0],imagen03_1_svg:ListaImagenReporte03[0],imagen03_2_svg:ListaImagenReporte03[1]},
        //dataType:'Json',
        cache: false,
        xhr: function () {
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 2) {
                    if (xhr.status == 200) {
                        xhr.responseType = "blob";
                    } else {
                        xhr.responseType = "text";
                    }
                }
            };
            return xhr;
        },
        success:function(data)
        {   
            var blob=new Blob([data], { type: "application/octetstream" });
            var link=document.createElement('a');
            link.href=window.URL.createObjectURL(blob);
            link.download="ReporteSemanal.pdf";
            link.click();
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });
});

Highcharts.getSVG = function(charts, options, callback) {
    var svgArr = [],
    top = 0,
    width = 0,
    addSVG = function(svgres) {
        // Grab width/height from exported chart
        var svgWidth = +svgres.match(
            /^<svg[^>]*width\s*=\s*\"?(\d+)\"?[^>]*>/
            )[1],
            svgHeight = +svgres.match(
            /^<svg[^>]*height\s*=\s*\"?(\d+)\"?[^>]*>/
            )[1],
            // Offset the position of this chart in the final SVG
            svg;

        if (svgWidth > 500) {
            svg = svgres.replace('<svg', '<g transform="translate(0,' + top + ')" ');
            top += svgHeight;
            width = Math.max(width, svgWidth);
        } else {
            svg = svgres.replace('<svg', '<g transform="translate(' + width + ', 0 )"');
            top = Math.max(top, svgHeight);
            width += svgWidth;
        }

        svg = svg.replace('</svg>', '</g>');
        svgArr.push(svg);
    },
    exportChart = function(i) {
        if (i === charts.length) {
        
            // add SVG image to exported svg
            addSVG(svgImg.outerHTML);
            
            return callback('<svg height="' + top + '" width="' + width +
            '" version="1.1" xmlns="http://www.w3.org/2000/svg">' + svgArr.join('') + '</svg>');
        }
        charts[i].getSVGForLocalExport(options, {}, function(e) {
            console.log("Failed to get SVG");
        }, function(svg) {
            addSVG(svg);
            return exportChart(i + 1); // Export next only when this SVG is received
        });
    };
        
    exportChart(0);
};


Highcharts.setOptions({
    exporting: {
        fallbackToExportServer: false // Ensure the export happens on the client side or not at all
    }
});

ListaImagenReporte01 = [];
Highcharts.exportChartsReporte01 = function(charts, options) {
    options = Highcharts.merge(Highcharts.getOptions().exporting, options);
    // Get SVG asynchronously and then download the resulting SVG
    Highcharts.getSVG(charts, options, function(svg) {
        ListaImagenReporte01.push(svg);
    });
};

ListaImagenReporte03 = [];
Highcharts.exportChartsReporte03 = function(charts, options) {
    options = Highcharts.merge(Highcharts.getOptions().exporting, options);
    // Get SVG asynchronously and then download the resulting SVG
    Highcharts.getSVG(charts, options, function(svg) {
        ListaImagenReporte03.push(svg);
    });
};

var svgImg = document.createElementNS('http://www.w3.org/2000/svg','svg');
svgImg.setAttribute('xmlns:xlink','http://www.w3.org/1999/xlink');
svgImg.setAttribute('height','400');
svgImg.setAttribute('width','600');
svgImg.setAttribute('id','test');

var svgimg = document.createElementNS('http://www.w3.org/2000/svg','image');
svgimg.setAttribute('height','400');
svgimg.setAttribute('width','600');
svgimg.setAttribute('id','testimg');


// $('body').on('click',, function (e) {
//     e.preventDefault();
//     console.log('333')
//     $('.m-video__modal--jsxxx').magnificPopup({
//     type: "iframe",
//     removalDelay: 300,
//     iframe:
//         ((s = 0),
//         {
//             markup:
//                 '<div class="mfp-iframe-scaler"><div class="mfp-close"></div><iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe></div><div class="mfp-bottom-bar"><div class="mfp-title"></div></div>',
//             patterns: {
//                 youtube: {
//                     index: "youtu",
//                     id: function (e) {
//                         var t = e.match(/^.*(?:youtu.be\/|v\/|e\/|u\/\w+\/|embed\/|v=)([^#\&\?]*).*/);
//                         if (!t || !t[1]) return null;
//                         if (-1 != e.indexOf("t=")) {
//                             var n = e.split("t=")[1].replace("h", ":").replace("m", ":").replace("s", "").split(":");
//                             1 == n.length ? (s = n[0]) : 2 == n.length ? (s = 60 * +n[0] + +n[1]) : 3 == n.length && (s = 60 * +n[0] * 60 + 60 * +n[1] + +n[2]);
//                         }
//                         var i = "?autoplay=1";
//                         return s > 0 && (i = "?start=" + s + "&autoplay=1"), t[1] + i;
//                     },
//                     src: "//www.youtube.com/embed/%id%",
//                 },
//                 vimeo: {
//                     index: "vimeo.com/",
//                     id: function (e) {
//                         var t = e.match(/(https?:\/\/)?(www.)?(player.)?vimeo.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/);
//                         return t && t[5] ? t[5] : null;
//                     },
//                     src: "//player.vimeo.com/video/%id%?autoplay=1",
//                 },
//             },
//         }),
//     mainClass: "mfp-zoom-in",
// })

// })
</script>