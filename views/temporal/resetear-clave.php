<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<div >
    <div class="m-login__form">
        <h2>Recupera tu nueva contraseña</h2>
        <?php $form = ActiveForm::begin(['options' => ['id' => 'formResetearClave']]); ?>
            <div class="m-input">
                <input type="number" pattern="[0-9]" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" class="numerico error_dni" id="dni" name="Resetear[dni]" placeholder="Ingresa tu usuario (DNI)" minlength="8" maxlength="8">

                <div class="error_dni_text"></div>
            </div>
            <button class="m-button m-button--secondary m-button--full btn-enviar-resetar_clave" type="submit">Confirmar</button>
        <?php ActiveForm::end(); ?>
    </div>
</div>



<script>
$('body').on('click', '.btn-enviar-resetar_clave', function(e) {
    var form = $('#formResetearClave');
    var dni = $("[name=\"Resetear[dni]\"]").val();
    var error = "";

    if (!dni) {
        $('.error_dni').addClass('alert-danger');
        $('.error_dni').css('border', '1px solid #DA1414');
        $('.error_dni_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Ingrese un número de DNI válido</div>`);
        $("#dni").focus();
        error = error + "error 1";
    }else if (dni && dni.length<8) {
        $('.error_dni').addClass('alert-danger');
        $('.error_dni').css('border', '1px solid #DA1414');
        $('.error_dni_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Ingrese un número de DNI válido</div>`);
        $("#dni").focus();
        error = error + "error 1";
    }else {
        $('.error_dni').removeClass('alert-danger');
        $('.error_dni').css('border', '');
        $('.error_dni_text').html(``);
    }

    if (error != "") {
        return false;
    }

    form.submit();
});
</script>