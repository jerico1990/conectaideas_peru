<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<div class="m-container m-section">
    <div class="m-login__form">
        <h2>Crea tu nueva contraseña</h2>
        <?php $form = ActiveForm::begin(['options' => ['id' => 'formResetearClaveValidar']]); ?>
            <div class="m-input">
                <div class="input-group">
                    <input type="text" class="error_clave" id="clave" name="Resetear[clave]" placeholder="Escribe tu nueva contraseña" maxlength="20">
                    <span class=" btn input-group-text btn-password"> <i class="bx bx-show"></i> </span>
                </div>
                <div class="error_clave_text"></div>
            </div>
            <div class="m-input">
                <div class="input-group">
                    <input type="text" class="error_clave_confirma" id="clave_confirma" name="Resetear[clave_confirma]" placeholder="Confirma tu contraseña" maxlength="20">
                    <span class=" btn input-group-text btn-password2"> <i class="bx bx-show"></i> </span>
                </div>
                <div class="error_clave_confirma_text"></div>
            </div>
            <button class="m-button m-button--secondary m-button--full btn-validar-clave" type="submit">Cambiar</button>
        <?php ActiveForm::end(); ?>
    </div>
</div>


	

<script>
$('body').on('click', '.btn-validar-clave', function(e) {
    var form = $('#formResetearClaveValidar');
    var clave = $("[name=\"Resetear[clave]\"]").val();
    var clave_confirma = $("[name=\"Resetear[clave_confirma]\"]").val();
    var error = "";

    if (!clave) {
        $('.error_clave').addClass('alert-danger');
        $('.error_clave').css('border', '1px solid #DA1414');
        $('.error_clave_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
        $("#clave").focus();
        error = error + "error error_clave";
    } else if (clave && (clave.length < 6 || clave.length > 20)) {
        $('.error_clave').addClass('alert-danger');
        $('.error_clave').css('border', '1px solid #DA1414');
        $('.error_clave_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Mínimo 6 caracteres y máximo 20</div>`);

        $("#clave").focus();
        error = error + "error error_clave_text";
    } else {
        $('.error_clave').removeClass('alert-danger');
        $('.error_clave').css('border', '');
        $('.error_clave_text').html(``);
    }

    if (!clave_confirma) {
        $('.error_clave_confirma').addClass('alert-danger');
        $('.error_clave_confirma').css('border', '1px solid #DA1414');
        $('.error_clave_confirma_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
        $("#clave").focus();
        error = error + "error error_clave_confirma";
    } else if (clave != clave_confirma) {
        $('.error_clave_confirma').addClass('alert-danger');
        $('.error_clave_confirma').css('border', '1px solid #DA1414');
        $('.error_clave_confirma_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Las contraseñas no coinciden</div>`);
        $("#clave").focus();
        error = error + "error error_clave";
    } else {
        $('.error_clave_confirma').removeClass('alert-danger');
        $('.error_clave_confirma').css('border', '');
        $('.error_clave_confirma_text').html(``);
    }

    if (error != "") {
        return false;
    }

    form.submit();
});


var psw  = 0;
var rpsw = 0;
$('body').on('click', '.btn-password', function(e) {
    
    if(psw==0){
        $(this).html('<i class="bx bx-hide"></i>');
        $('#clave').attr('type', 'password'); 
        psw = 1 ;
    }else if(psw==1){
        $(this).html('<i class="bx bx-show"></i>');
        $('#clave').attr('type', 'text'); 
        psw = 0 ;
    }
});

$('body').on('click', '.btn-repassword', function(e) {
    if(rpsw==0){
        $(this).html('<i class="bx bx-hide"></i>');
        $('#clave_confirma').attr('type', 'password');
        rpsw = 1 ;
    }else if(rpsw==1){
        $(this).html('<i class="bx bx-show"></i>');
        $('#clave_confirma').attr('type', 'text'); 
        rpsw = 0 ;
    }
});
</script>