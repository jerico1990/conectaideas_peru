<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div class="account-pages my-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-6 col-xl-5">
                <div class="card overflow-hidden">
                    <div class="bg-primary">
                        <div class="row">
                            <div class="col-9">
                                <div class="text-white p-4">
                                    <h5 class="text-white">Resetear clave</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <div>
                            
                        </div>
                        <div class="p-2 ">
                            Se ha enviado el email al siguiente correo : <?= $correo_electronico ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
	

<script>

</script>