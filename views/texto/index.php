
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0 font-size-18">Textos de Reporte</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Textos de Reporte</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-3">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <table id="lista-experiencias" class="table table-bordered dt-responsive ">
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-9">
        <div class="card">
            <div class="card-body lista-texto">
                
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<div id="modal" class="fade modal" role="dialog"  tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>



<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('#staticBackdrop');


async function Actividades(experiencia_id,experiencia_correlativo){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/actividad/get-experiencia-actividades-admin',
                method: 'POST',
                data:{_csrf:csrf,experiencia_id:experiencia_id},
                dataType:'Json',
                beforeSend:function(){
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        
                        var i = 0;
                        var actividades ='';
                        actividades = actividades + '<h3> Experiencia ' + experiencia_correlativo + '</h3>'
                        actividades = actividades + '<ul class="nav nav-tabs" role="tablist">'
                        $.each(results.grados, function( index, value ) {
                            correlativo = value.correlativo;
                            active = '';
                            if(i==0){
                                active = 'active';
                            }
                            actividades = actividades + `   <li class="nav-item">
                                                                <a class="nav-link ${active}" data-toggle="tab" href="#actividad_${correlativo}" role="tab">
                                                                    <span class="d-none d-sm-block">Actividad ${correlativo}</span>
                                                                </a>
                                                            </li>`;
                            i++;

                        });
                        actividades = actividades + '</ul>';

                        i = 0;
                        actividades = actividades + '<div class="tab-content p-3 text-muted">'
                        $.each(results.actividades, function( index, value ) {
                            correlativo = value.correlativo;
                            active = '';
                            if(i==0){
                                active = 'active';
                            }
                            actividades = actividades + '<div class="tab-pane ' + active + '" id="actividad_' + correlativo + '" role="tabpanel">';

                            $.each(value.detalle, function( index2, value2 ) {
                                id = value2.id;
                                grado = value2.nombre_corto;
                                video = (value2.url_video)?value2.url_video:'';
                                descripcion = (value2.descripcion)?value2.descripcion:'';
                                actividad_chile_id = (value2.actividad_chile_id)?value2.actividad_chile_id:'';
                                if(value2.matriz){
                                    matriz = "<br> <a target='_blank' href='<?= \Yii::$app->request->BaseUrl ?>/recursos/matriz/" + value2.matriz + "'>Ver</a> | <a href='#' data-id='" + value2.id + "' data-experiencia_id='" + experiencia_id + "' data-experiencia_correlativo='" + experiencia_correlativo + "' class='btn-eliminar-matriz'>Borrar</a> ";
                                }else{
                                    matriz = "<input type='file'  id='archivo_matriz_" + id + "' class='form-control'>";
                                }

                                if(value2.solucionario){
                                    solucionario = "<br> <a target='_blank' href='<?= \Yii::$app->request->BaseUrl ?>/recursos/solucionario/" + value2.solucionario + "'>Ver</a> | <a href='#' data-id='" + value2.id + "' data-experiencia_id='" + experiencia_id + "' data-experiencia_correlativo='" + experiencia_correlativo + "' class='btn-eliminar-solucionario'>Borrar</a>";
                                }else{
                                    solucionario = "<input type='file'  id='archivo_solucionario_" + id + "' class='form-control'>";
                                }

                                actividades = actividades + `
                                                            <div class="row">
                                                                <div class="col-3">
                                                                    <div class="card">
                                                                        <div class="card-body">
                                                                            <h4 class="card-title mb-4"></h4>
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    ${grado}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-9">
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="form-group">
                                                                                <label >Nombre de actividad</label>
                                                                                <input type="text"  id="descripcion_${id}" class="form-control" value="${descripcion}">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <label >Video</label>
                                                                                <input type="text"  id="url_video_${id}" class="form-control" value="${video}">
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <label >ID actividad APP Chile</label>
                                                                                <input type="text"  id="actividad_chile_id_${id}" class="form-control" value="${actividad_chile_id}">
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <label >Solucionario</label>
                                                                                ${solucionario}
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <label >Matriz</label>
                                                                                ${matriz}
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <label >&nbsp</label><br>
                                                                                <button data-id="${id}" data-experiencia_id="${experiencia_id}" data-experiencia_correlativo="${experiencia_correlativo}" data-actividad_correlativo="${correlativo}" data-grado="${grado}" class="btn btn-block btn-primary btn-grabar-recurso">Grabar</button>
                                                                            </div>
                                                                        </div>


                                                                    </div>
                                                                </div>
                                                            </div>
                                `;
                            });
                            actividades = actividades + '</div>'
                            i++;

                        });
                        actividades = actividades + '</div>'


                        $('.lista-actividades').html(actividades);
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}



async function ExperienciaGradosTextos(experiencia_id,experiencia_correlativo){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/texto/get-experiencia-grados-textos',
                method: 'POST',
                data:{_csrf:csrf,experiencia_id:experiencia_id},
                dataType:'Json',
                beforeSend:function(){
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        
                        var i = 0;
                        var experiencia ='';
                        var grado ='';

                        experiencia = experiencia + '<h3> Experiencia ' + experiencia_correlativo + '</h3>'
                        experiencia = experiencia + '<ul class="nav nav-tabs" role="tablist">';

                        $.each(results.grados, function( index, value ) {
                            grados = '';
                            correlativo = value.correlativo;
                            active = '';
                            if(i==0){
                                active = 'active';
                            }
                            grados = ` <li class="nav-item">
                                                <a class="nav-link ${active}" data-toggle="tab" href="#grado_${correlativo}" role="tab">
                                                    <span class="d-none d-sm-block">Grado ${correlativo}</span>
                                                </a>
                                            </li>`;
                            experiencia = experiencia + grados;
                            i++;
                        });
                        experiencia = experiencia + '</ul>';






                        i = 0;
                        experiencia = experiencia + '<div class="tab-content p-3 text-muted">'
                        $.each(results.grados, function( index, value ) {
                            grados = '';
                            correlativo = value.correlativo;
                            active = '';
                            if(i==0){
                                active = 'active';
                            }
                            grados = grados + '<div class="tab-pane ' + active + '" id="grado_' + correlativo + '" role="tabpanel">';

                            $.each(value.textos, function( index2, value2 ) {
                                var id = value2.id;
                                var pregunta = value2.pregunta;
                                var grado_id = value2.grado_id;
                                var texto_principal = (value2.texto_principal)?value2.texto_principal:'';
                                var texto_enlace = (value2.texto_enlace)?value2.texto_enlace:'';
                                var tipo = (value2.tipo)?value2.tipo:'';

                                var video = (value2.video)?value2.video:'';
                                var enlace = (value2.enlace)?value2.enlace:'';
                                var archivo = (value2.archivo)?value2.archivo:'';
                                //var descargar='';
                                //if(archivo!=''){
                                //var descargar = `<a class="btn btn-success" target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/archivo_texto/${archivo}" ${archivo==''?`style="display:none"`:``}>Descargar</a>`;
                                //}
                                
                                if(archivo){
                                    archivo_ = "<br> <a target='_blank' href='<?= \Yii::$app->request->BaseUrl ?>/archivo_texto/" + archivo + "'>Ver</a> | <a href='#' data-id='" + value2.id + "' class='btn-eliminar-archivo'>Borrar</a> ";
                                }else{
                                    archivo_ = "<input type='file'  id='carga_archivo_" + id + "' class='form-control'>";
                                }


                                grados = grados + `
                                                            <div class="row">
                                                                <div class="col-3">
                                                                    <div class="card">
                                                                        <div class="card-body">
                                                                            <h4 class="card-title mb-4"></h4>
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    Pregunta ${pregunta}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-9">
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="form-group">
                                                                                <label >Texto</label>
                                                                                <input type="text"  id="texto_principal_${id}" class="form-control" value="${texto_principal}">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <label >Texto para enlace</label>
                                                                                <input type="text"  id="texto_enlace_${id}" class="form-control" value="${texto_enlace}">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label >Tipo</label>
                                                                                <select  id="tipo_${id}" data-id="${id}" class="form-control tipo">
                                                                                    <option value>Seleccionar</option>
                                                                                    <option value="1" ${tipo==1?`selected`:``}>Archivo</option>
                                                                                    <option value="2" ${tipo==2?`selected`:``}>Video</option>
                                                                                    <option value="3" ${tipo==3?`selected`:``}>Enlace</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group visible_carga_archivo_${id}" ${tipo==1?``:`style="display:none"`}>
                                                                                <label for="">Cargar archivo</label>
                                                                                ${archivo_}
                                                                            </div>
                                                                            <div class="form-group visible_enlace_video_${id}" ${tipo==2?``:`style="display:none"`}>
                                                                                <label for="">Enlace</label>
                                                                                <input type="text" id="enlace_video_${id}" class="form-control" value="${video}">
                                                                            </div>
                                                                            <div class="form-group visible_enlace_enlace_${id}" ${tipo==3?``:`style="display:none"`}>
                                                                                <label for="">Enlace</label>
                                                                                <input type="text" id="enlace_enlace_${id}" class="form-control" value="${enlace}">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <label >&nbsp</label><br>
                                                                                <button data-id="${id}" data-experiencia_correlativo="${experiencia_correlativo}" data-experiencia_id="${experiencia_id}" data-grado_id="${grado_id}" class="btn btn-block btn-primary btn-grabar-texto">Grabar</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                `;
                            });
                            experiencia = experiencia + grados + '</div>';
                            i++;

                        });
                        experiencia = experiencia + '</div>'


                        $('.lista-texto').html(experiencia);
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}

Experiencias();
async function Experiencias(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/experiencia/get-lista-exp',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var experiencias ="";
                       
                        $.each(results.experiencias, function( index, value ) {
                            experiencias = experiencias + "<tr data-experiencia_id='" + value.id + "' data-experiencia_correlativo='" + value.correlativo + "' class='btn-seleccionar-experiencia'>";
                                experiencias = experiencias + "<td> Experiencia " + value.correlativo + "</td>";
                            experiencias = experiencias + "</tr>";
                        });
                        
                        $('#lista-experiencias tbody').html(experiencias);
                        ExperienciaGradosTextos(results.experiencias[0].id,results.experiencias[0].correlativo)
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}


$('body').on('click', '.btn-seleccionar-experiencia', function (e) {
    e.preventDefault();
    var experiencia_id = $(this).attr('data-experiencia_id');
    var experiencia_correlativo = $(this).attr('data-experiencia_correlativo');
    ExperienciaGradosTextos(experiencia_id,experiencia_correlativo);
});



$('body').on('click', '.btn-grabar-texto', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    var experiencia_id = $(this).attr('data-experiencia_id');
    var experiencia_correlativo = $(this).attr('data-experiencia_correlativo');
    var grado_id = $(this).attr('data-grado_id');
    

    var texto_principal = $('#texto_principal_'+id).val();
    var texto_enlace = $('#texto_enlace_'+id).val();
    var tipo = $('#tipo_'+id).val();
    var carga_archivo = ($('#carga_archivo_'+id).val())?document.getElementById('carga_archivo_'+id).files[0]:'';
    var enlace_video = ($('#enlace_video_'+id).val())?$('#enlace_video_'+id).val():'';
    var enlace_enlace = ($('#enlace_enlace_'+id).val())?$('#enlace_enlace_'+id).val():'';



    var form_data = new FormData();
    form_data.append("_csrf", csrf);
    form_data.append("Texto[texto_principal]", texto_principal);
    form_data.append("Texto[texto_enlace]", texto_enlace);
    form_data.append("Texto[tipo]", tipo);
    form_data.append("Texto[archivo_documento]", carga_archivo);
    form_data.append("Texto[video]", enlace_video);
    form_data.append("Texto[enlace]", enlace_enlace);

    

    $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/texto/update-texto?id='+id,
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        dataType:'Json',
        beforeSend:function(){
            loading.modal("show");
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert('No hay conectividad con el sistema');
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        success: function (results) {
            if(results.success){
                //ExperienciaGradosTextos(experiencia_id,experiencia_correlativo)

                if(tipo=="1"){
                    archivo = results.archivo;
                    $('.visible_carga_archivo_'+id).html(`
                        <label for="">Cargar archivo</label>
                        <br> <a target='_blank' href='<?= \Yii::$app->request->BaseUrl ?>/archivo_texto/${archivo}'>Ver</a> | <a href='#' data-id='${id}' class='btn-eliminar-archivo'>Borrar</a> 
                    `);
                }

                setTimeout(function(){
                    loading.modal('hide');
                },1000)
            }
        },
    });
});



$('body').on('change', '.tipo', function (e) {
    var id = $(this).attr('data-id');
    $('.visible_enlace_video_'+ id).hide();
    $('.visible_carga_archivo_' + id).hide();
    $('.visible_enlace_enlace_' + id).hide();

    $('#carga_archivo_'+ id).val('');
    $('#enlace_video_'+ id).val('');
    $('#enlace_enlace_'+ id).val('');
    
    if($(this).val()=="1"){
        $('.visible_carga_archivo_' + id).show();
    }else if($(this).val()=="2"){
        $('.visible_enlace_video_'+ id).show();
    }else if($(this).val()=="3"){
        $('.visible_enlace_enlace_'+ id).show();
    }
});


$('body').on('click', '.btn-eliminar-archivo', function (e) {
    e.preventDefault();
    var texto_id = $(this).attr('data-id');
    $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/texto/eliminar-archivo',
        method: 'POST',
        data:{_csrf:csrf,texto_id:texto_id},
        dataType:'Json',
        beforeSend:function()
        {
            loading.modal("show");
        },
        success:function(results)
        {   
            if(results && results.success){
                $('.visible_carga_archivo_'+texto_id).html(`
                    <label for="">Cargar archivo</label>
                    <input type='file'  id='carga_archivo_${texto_id}' class='form-control'>
                `);
                setTimeout(function(){
                    loading.modal('hide');
                },1000)
            }
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });
});

</script>
