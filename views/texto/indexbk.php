<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0 font-size-18">Textos para reportes</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Textos</a></li>
                    <li class="breadcrumb-item active">Textos para reportes</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->
<?php $form = ActiveForm::begin(['options' => ['id' => 'formP02']]); ?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Pregunta 02</h2>   
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">texto</label>
                            <textarea name="Texto[p02_texto_1]" id="p02_texto_1" cols="30" rows="5" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Enlace</label>
                            <input type="text" id="p02_enlace_1" name="Texto[p02_enlace_1]" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Tipo</label>
                            <select name="Texto[p02_tipo_1]" id="p02_tipo_1" class="form-control">
                                <option value>Seleccionar</option>
                                <option value="1">Archivo</option>
                                <option value="2">Enlace</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group p02_carga_archivo_01" style="display:none">
                            <label for="">Cargar archivo</label>
                            <input type="file" name="Texto[p02_carga_archivo_1]" id="p02_carga_archivo_1" class="form-control"> <br>
                            <a class="p02_descargar_01 btn btn-success" style="display:none" target="_blank">Descargar</a>
                        </div>
                        <div class="form-group p02_enlace_video_01" style="display:none">
                            <label for="">Enlace de video</label>
                            <input type="text" name="Texto[p02_enlace_video_1]" id="p02_enlace_video_1" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <button class="btn btn-success btn-grabar-p02">Grabar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<?php ActiveForm::end(); ?>

<?php $form = ActiveForm::begin(['options' => ['id' => 'formP05']]); ?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
            <div class="row">
                    <div class="col-md-12">
                        <h2>Pregunta 05</h2>   
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">texto</label>
                            <textarea name="Texto[p05_texto_1]" id="p05_texto_1" cols="30" rows="5" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Enlace</label>
                            <input type="text" id="p05_enlace_1" name="Texto[p05_enlace_1]" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Tipo</label>
                            <select name="Texto[p05_tipo_1]" id="p05_tipo_1" class="form-control">
                                <option value>Seleccionar</option>
                                <option value="1">Archivo</option>
                                <option value="2">Enlace</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group p05_carga_archivo_01" style="display:none">
                            <label for="">Cargar archivo</label>
                            <input type="file" name="Texto[p05_carga_archivo_1]" id="p05_carga_archivo_1" class="form-control"> <br>
                            <a class="p05_descargar_01 btn btn-success" style="display:none" target="_blank">Descargar</a>
                            
                        </div>
                        <div class="form-group p05_enlace_video_01" style="display:none">
                            <label for="">Enlace de video</label>
                            <input type="text" name="Texto[p05_enlace_video_1]" id="p05_enlace_video_1" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">texto</label>
                            <textarea name="Texto[p05_texto_2]" id="p05_texto_2" cols="30" rows="5" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Enlace</label>
                            <input type="text" id="p05_enlace_2" name="Texto[p05_enlace_2]" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Tipo</label>
                            <select name="Texto[p05_tipo_2]" id="p05_tipo_2" class="form-control">
                                <option value>Seleccionar</option>
                                <option value="1">Archivo</option>
                                <option value="2">Enlace</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group p05_carga_archivo_02" style="display:none">
                            <label for="">Cargar archivo</label>
                            <input type="file" name="Texto[p05_carga_archivo_2]" id="p05_carga_archivo_2" class="form-control"> <br>
                            <a class="p05_descargar_02 btn btn-success" style="display:none" target="_blank">Descargar</a>
                        </div>
                        <div class="form-group p05_enlace_video_02" style="display:none">
                            <label for="">Enlace de video</label>
                            <input type="text" name="Texto[p05_enlace_video_2]" id="p05_enlace_video_2" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <button class="btn btn-success btn-grabar-p05">Grabar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<?php ActiveForm::end(); ?>


<div id="modal" class="fade modal" role="dialog"  tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>

<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('#staticBackdrop');

$('body').on('change', '#p02_tipo_1', function (e) {
    if($(this).val()=="1"){
        $('.p02_carga_archivo_01').show();
        $('.p02_enlace_video_01').hide();
        $('#p02_enlace_video_1').val('');
    }else if($(this).val()=="2"){
        $('.p02_enlace_video_01').show();
        $('.p02_carga_archivo_01').hide();
        $('#p02_carga_archivo_1').val('');
    }
});


$('body').on('change', '#p05_tipo_1', function (e) {
    if($(this).val()=="1"){
        $('.p05_carga_archivo_01').show();
        $('.p05_enlace_video_01').hide();
        $('#p05_enlace_video_1').val('');
    }else if($(this).val()=="2"){
        $('.p05_enlace_video_01').show();
        $('.p05_carga_archivo_01').hide();
        $('#p05_carga_archivo_1').val('');
    }
    
});


$('body').on('change', '#p05_tipo_2', function (e) {
    if($(this).val()=="1"){
        $('.p05_carga_archivo_02').show();
        $('.p05_enlace_video_02').hide();
        $('#p05_enlace_video_2').val('');
    }else if($(this).val()=="2"){
        $('.p05_enlace_video_02').show();
        $('.p05_carga_archivo_02').hide();
        $('#p05_carga_archivo_2').val('');
    }
});



$('body').on('click', '.btn-grabar-p02', function (e) {
    e.preventDefault();
    


    var form_data = new FormData();
    form_data.append("_csrf", csrf);

    form_data.append("Texto[p02_texto_1]", $('#p02_texto_1').val());
    form_data.append("Texto[p02_enlace_1]", $('#p02_enlace_1').val());
    form_data.append("Texto[p02_tipo_1]", $('#p02_tipo_1').val());
    form_data.append("Texto[p02_enlace_video_1]", $('#p02_enlace_video_1').val());
    if(document.getElementById('p02_carga_archivo_1') && document.getElementById('p02_carga_archivo_1').files[0]){
        form_data.append("Texto[p02_carga_archivo_1]", document.getElementById('p02_carga_archivo_1').files[0]);
    }

    $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/texto/update-pregunta02',
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        dataType:'Json',
        beforeSend:function(){
            loading.modal("show");
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert('No hay conectividad con el sistema');
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        success: function (results) {
            if(results.success){
                setTimeout(function(){
                    loading.modal('hide');
                },3000)
            }
        },
    });
});


$('body').on('click', '.btn-grabar-p05', function (e) {
    e.preventDefault();
    


    var form_data = new FormData();
    form_data.append("_csrf", csrf);

    form_data.append("Texto[p05_texto_1]", $('#p05_texto_1').val());
    form_data.append("Texto[p05_enlace_1]", $('#p05_enlace_1').val());
    form_data.append("Texto[p05_tipo_1]", $('#p05_tipo_1').val());
    form_data.append("Texto[p05_enlace_video_1]", $('#p05_enlace_video_1').val());
    if(document.getElementById('p05_carga_archivo_1') && document.getElementById('p05_carga_archivo_1').files[0]){
        form_data.append("Texto[p05_carga_archivo_1]", document.getElementById('p05_carga_archivo_1').files[0]);
    }


    form_data.append("Texto[p05_texto_2]", $('#p05_texto_2').val());
    form_data.append("Texto[p05_enlace_2]", $('#p05_enlace_2').val());
    form_data.append("Texto[p05_tipo_2]", $('#p05_tipo_2').val());
    form_data.append("Texto[p05_enlace_video_2]", $('#p05_enlace_video_2').val());
    if(document.getElementById('p05_carga_archivo_2') && document.getElementById('p05_carga_archivo_2').files[0]){
        form_data.append("Texto[p05_carga_archivo_2]", document.getElementById('p05_carga_archivo_2').files[0]);
    }

    $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/texto/update-pregunta05',
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        dataType:'Json',
        beforeSend:function(){
            loading.modal("show");
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert('No hay conectividad con el sistema');
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        success: function (results) {
            if(results.success){
                setTimeout(function(){
                    loading.modal('hide');
                },3000)
            }
        },
    });
});

Texto(1);
Texto(2);
Texto(3);
async function Texto(id){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/texto/get-texto',
                method: 'POST',
                data:{_csrf:csrf,id:id},
                dataType:'Json',
                beforeSend:function(){
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        if(id==1){
                            $('#p02_texto_1').val(results.texto.texto_principal);
                            $('#p02_enlace_1').val(results.texto.enlace);
                            $('#p02_tipo_1').val(results.texto.tipo);

                            if(results.texto.tipo==1){
                                $('.p02_carga_archivo_01').show();
                                //$('#p02_carga_archivo_1').val(results.texto.archivo);
                                $('.p02_descargar_01').show();
                                $('.p02_descargar_01').attr("href", "<?= \Yii::$app->request->BaseUrl ?>/archivo_texto/"+results.texto.archivo);
                                
                            }else if(results.texto.tipo==2){
                                $('.p02_enlace_video_01').show();
                                $('#p02_enlace_video_1').val(results.texto.video);
                            }
                        }

                        if(id==2){
                            $('#p05_texto_1').val(results.texto.texto_principal);
                            $('#p05_enlace_1').val(results.texto.enlace);
                            $('#p05_tipo_1').val(results.texto.tipo);

                            if(results.texto.tipo==1){
                                $('.p05_carga_archivo_01').show();
                                //$('#p05_carga_archivo_1').val(results.texto.archivo);

                                $('.p05_descargar_01').show();
                                $('.p05_descargar_01').attr("href", "<?= \Yii::$app->request->BaseUrl ?>/archivo_texto/"+results.texto.archivo);

                            }else if(results.texto.tipo==2){
                                $('.p05_enlace_video_01').show();
                                $('#p05_enlace_video_1').val(results.texto.video);
                            }
                        }

                        if(id==3){
                            $('#p05_texto_2').val(results.texto.texto_principal);
                            $('#p05_enlace_2').val(results.texto.enlace);
                            $('#p05_tipo_2').val(results.texto.tipo);

                            if(results.texto.tipo==1){
                                $('.p05_carga_archivo_02').show();
                                //$('#p05_carga_archivo_2').val(results.texto.archivo);

                                $('.p05_descargar_02').show();
                                $('.p05_descargar_02').attr("href", "<?= \Yii::$app->request->BaseUrl ?>/archivo_texto/"+results.texto.archivo);
                            }else if(results.texto.tipo==2){
                                $('.p05_enlace_video_02').show();
                                $('#p05_enlace_video_2').val(results.texto.video);
                            }
                        }
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}
</script>