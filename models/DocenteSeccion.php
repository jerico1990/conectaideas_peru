<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "docente_seccion".
 *
 * @property int $id
 * @property int|null $docente_id
 * @property int|null $seccion_id
 * @property string|null $fecha_registro
 * @property int|null $estado
 *
 * @property Docente $docente
 * @property Seccion $seccion
 * @property Matricula[] $matriculas
 */
class DocenteSeccion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'docente_seccion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['docente_id', 'seccion_id', 'estado'], 'integer'],
            [['fecha_registro'], 'safe'],
            [['docente_id'], 'exist', 'skipOnError' => true, 'targetClass' => Docente::className(), 'targetAttribute' => ['docente_id' => 'id']],
            [['seccion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Seccion::className(), 'targetAttribute' => ['seccion_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'docente_id' => 'Docente ID',
            'seccion_id' => 'Seccion ID',
            'fecha_registro' => 'Fecha Registro',
            'estado' => 'Estado',
        ];
    }

    /**
     * Gets query for [[Docente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocente()
    {
        return $this->hasOne(Docente::className(), ['id' => 'docente_id']);
    }

    /**
     * Gets query for [[Seccion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSeccion()
    {
        return $this->hasOne(Seccion::className(), ['id' => 'seccion_id']);
    }

    /**
     * Gets query for [[Matriculas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMatriculas()
    {
        return $this->hasMany(Matricula::className(), ['docente_seccion_id' => 'id']);
    }
}
