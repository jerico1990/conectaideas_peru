<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipo_institucion_educativa".
 *
 * @property int $id
 * @property string|null $descripcion
 * @property int|null $estado
 */
class TipoInstitucionEducativa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipo_institucion_educativa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['estado'], 'integer'],
            [['descripcion'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descripcion' => 'Descripcion',
            'estado' => 'Estado',
        ];
    }
}
