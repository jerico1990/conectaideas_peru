<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "semana".
 *
 * @property int $id
 * @property string|null $semana
 * @property string|null $fecha_inicio
 * @property string|null $fecha_fin
 * @property int|null $estado
 *
 * @property Recurso[] $recursos
 */
class Semana extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'semana';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_inicio', 'fecha_fin'], 'safe'],
            [['estado_registro'], 'integer'],
            [['semana'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'semana' => 'Semana',
            'fecha_inicio' => 'Fecha Inicio',
            'fecha_fin' => 'Fecha Fin',
            'estado_registro' => 'Estado',
        ];
    }

    /**
     * Gets query for [[Recursos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecursos()
    {
        return $this->hasMany(Recurso::className(), ['semana_id' => 'id']);
    }
}
