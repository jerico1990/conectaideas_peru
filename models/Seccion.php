<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "seccion".
 *
 * @property int $id
 * @property int|null $grado_id
 * @property int|null $institucion_educativa_id
 * @property string|null $descripcion
 * @property int|null $estado
 * @property string|null $fecha_registro
 *
 * @property DocenteSeccion[] $docenteSeccions
 * @property Grado $grado
 * @property InstitucionEducativa $institucionEducativa
 */
class Seccion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'seccion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['grado_id', 'institucion_educativa_id', 'estado'], 'integer'],
            [['fecha_registro'], 'safe'],
            [['descripcion'], 'string', 'max' => 150],
            // [['grado_id'], 'exist', 'skipOnError' => true, 'targetClass' => Grado::className(), 'targetAttribute' => ['grado_id' => 'id']],
            // [['institucion_educativa_id'], 'exist', 'skipOnError' => true, 'targetClass' => InstitucionEducativa::className(), 'targetAttribute' => ['institucion_educativa_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'grado_id' => 'Grado ID',
            'institucion_educativa_id' => 'Institucion Educativa ID',
            'descripcion' => 'Descripcion',
            'estado' => 'Estado',
            'fecha_registro' => 'Fecha Registro',
        ];
    }

    /**
     * Gets query for [[DocenteSeccions]].
     *
     * @return \yii\db\ActiveQuery
     */
    // public function getDocenteSeccions()
    // {
    //     return $this->hasMany(DocenteSeccion::className(), ['seccion_id' => 'id']);
    // }

    // /**
    //  * Gets query for [[Grado]].
    //  *
    //  * @return \yii\db\ActiveQuery
    //  */
    // public function getGrado()
    // {
    //     return $this->hasOne(Grado::className(), ['id' => 'grado_id']);
    // }

    // /**
    //  * Gets query for [[InstitucionEducativa]].
    //  *
    //  * @return \yii\db\ActiveQuery
    //  */
    // public function getInstitucionEducativa()
    // {
    //     return $this->hasOne(InstitucionEducativa::className(), ['id' => 'institucion_educativa_id']);
    // }
}
