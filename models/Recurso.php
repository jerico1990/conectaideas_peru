<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "recurso".
 *
 * @property int $id
 * @property int|null $grado_id
 * @property int|null $semana_id
 * @property string|null $solucionario
 * @property string|null $matriz
 * @property string|null $video
 * @property string|null $fecha_registro
 * @property int|null $estado
 *
 * @property Grado $grado
 * @property Semana $semana
 */
class Recurso extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $titulo;
    public static function tableName()
    {
        return 'recurso';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['grado_id', 'semana_id', 'estado_registro'], 'integer'],
            [['fecha_registro'], 'safe'],
            [['solucionario', 'matriz', 'video'], 'string', 'max' => 250],
            [['grado_id'], 'exist', 'skipOnError' => true, 'targetClass' => Grado::className(), 'targetAttribute' => ['grado_id' => 'id']],
            [['semana_id'], 'exist', 'skipOnError' => true, 'targetClass' => Semana::className(), 'targetAttribute' => ['semana_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'grado_id' => 'Grado ID',
            'semana_id' => 'Semana ID',
            'solucionario' => 'Solucionario',
            'matriz' => 'Matriz',
            'video' => 'Video',
            'fecha_registro' => 'Fecha Registro',
            'estado_registro' => 'Estado',
        ];
    }

    /**
     * Gets query for [[Grado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGrado()
    {
        return $this->hasOne(Grado::className(), ['id' => 'grado_id']);
    }

    /**
     * Gets query for [[Semana]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSemana()
    {
        return $this->hasOne(Semana::className(), ['id' => 'semana_id']);
    }
}
