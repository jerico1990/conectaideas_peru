<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "institucion_educativa".
 *
 * @property int $id
 * @property string|null $departamento
 * @property string|null $provincia
 * @property string|null $distrito
 * @property string|null $nombre_ie
 * @property string|null $cod_mod
 *
 * @property Docente[] $docentes
 * @property Seccion[] $seccions
 */
class InstitucionEducativa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'institucion_educativa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['departamento', 'provincia', 'distrito', 'nombre_ie'], 'string', 'max' => 150],
            [['cod_mod'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'departamento' => 'Departamento',
            'provincia' => 'Provincia',
            'distrito' => 'Distrito',
            'nombre_ie' => 'Nombre Ie',
            'cod_mod' => 'Cod Mod',
        ];
    }

    /**
     * Gets query for [[Docentes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocentes()
    {
        return $this->hasMany(Docente::className(), ['institucion_educativa_id' => 'id']);
    }

    /**
     * Gets query for [[Seccions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSeccions()
    {
        return $this->hasMany(Seccion::className(), ['institucion_educativa_id' => 'id']);
    }
}
