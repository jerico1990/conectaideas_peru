<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "grado".
 *
 * @property int $id
 * @property int|null $grado
 * @property string|null $descripcion
 * @property int|null $estado
 *
 * @property Recurso[] $recursos
 * @property Seccion[] $seccions
 */
class Grado extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'grado';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['grado', 'estado'], 'integer'],
            [['descripcion'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'grado' => 'Grado',
            'descripcion' => 'Descripcion',
            'estado' => 'Estado',
        ];
    }

    /**
     * Gets query for [[Recursos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecursos()
    {
        return $this->hasMany(Recurso::className(), ['grado_id' => 'id']);
    }

    /**
     * Gets query for [[Seccions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSeccions()
    {
        return $this->hasMany(Seccion::className(), ['grado_id' => 'id']);
    }
}
