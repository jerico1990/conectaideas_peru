<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "actividad".
 *
 * @property int $id
 * @property int|null $id_experiencia
 * @property int|null $id_grado
 * @property int|null $correlativo
 * @property string|null $descripcion
 * @property string|null $fecha_inicio
 * @property string|null $fecha_fin
 * @property int|null $id_actividad_chile
 * @property int|null $estado
 * @property string|null $fecha_habilitacion
 * @property string|null $url_video
 * @property string|null $matriz
 * @property string|null $solucionario
 *
 * @property Grado $grado
 * @property Experiencia $experiencia
 */
class Actividad extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $titulo;
    public $temp;
    public $archivo_matriz;
    public $archivo_solucionario;
    public static function tableName()
    {
        return 'actividad';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_experiencia', 'id_grado', 'correlativo', 'id_actividad_chile', 'estado'], 'integer'],
            [['fecha_inicio', 'fecha_fin', 'fecha_habilitacion','temp','archivo_matriz','archivo_solucionario'], 'safe'],
            [['descripcion'], 'string', 'max' => 250],
            [['url_video', 'matriz', 'solucionario'], 'string', 'max' => 150],
            [['id_grado'], 'exist', 'skipOnError' => true, 'targetClass' => Grado::className(), 'targetAttribute' => ['id_grado' => 'id']],
            [['id_experiencia'], 'exist', 'skipOnError' => true, 'targetClass' => Experiencia::className(), 'targetAttribute' => ['id_experiencia' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_experiencia' => 'Id Experiencia',
            'id_grado' => 'Id Grado',
            'correlativo' => 'Correlativo',
            'descripcion' => 'Descripcion',
            'fecha_inicio' => 'Fecha Inicio',
            'fecha_fin' => 'Fecha Fin',
            'id_actividad_chile' => 'Id Actividad Chile',
            'estado' => 'Estado',
            'fecha_habilitacion' => 'Fecha Habilitacion',
            'url_video' => 'Url Video',
            'matriz' => 'Matriz',
            'solucionario' => 'Solucionario',
        ];
    }

    /**
     * Gets query for [[Grado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGrado()
    {
        return $this->hasOne(Grado::className(), ['id' => 'id_grado']);
    }

    /**
     * Gets query for [[Experiencia]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getExperiencia()
    {
        return $this->hasOne(Experiencia::className(), ['id' => 'id_experiencia']);
    }
}
