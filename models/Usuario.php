<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;
/**
 * This is the model class for table "usuario".
 *
 * @property int $id
 * @property string $usuario
 * @property string $clave
 * @property int $estado
 */
class Usuario extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public $auth_key;
    public static function tableName()
    {
        return 'usuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['estado','cargo_id'], 'integer'],
            [['usuario', 'clave'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario' => 'Usuario',
            'clave' => 'Clave',
            'estado' => 'Estado',
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getPerfil()
    {
        return $this->cargo_id;
    }

    public function getFoto()
    {
        if($this->usuario=='administrador'){
            return 'perfil.png';
        }else{
            $docente = (new \yii\db\Query())->select('foto')->from('docente')->where('dni=:dni',[':dni'=>$this->usuario])->one();
            if($docente['foto']!=''){
                return $docente['foto'];
            }else{
                return 'perfil.png';
            }
        }
    }

    public function getDocenteId(){
        if($this->usuario=='administrador'){
            return $this->usuario;
        }else{
            $docente = (new \yii\db\Query())->select('id')->from('docente')->where('dni=:dni',[':dni'=>$this->usuario])->one();
            return $docente['id'];
        } 
    }

    public function getNombres(){
        if($this->usuario=='administrador'){
            return $this->usuario;
        }else{
            $docente = (new \yii\db\Query())->select('cargo_id,nombres,apellido_paterno,apellido_materno')->from('docente')->where('dni=:dni',[':dni'=>$this->usuario])->one();
            return $docente['nombres'].' '.$docente['apellido_paterno'];
        } 
    }

    public function getUsername(){
        return $this->usuario;
    }


    public static function findByUsername($password,$username)
    {
      return static::find()->where('usuario=:usuario and estado=1',[':usuario' => $username])->one();
    }

    public function validatePassword($password,$username){

        $model=static::find()->where('usuario=:usuario and estado=1',[':usuario' => $username])->one();
        if (Yii::$app->getSecurity()->validatePassword($password, $model->clave)) {
            return $model;
        }

        // if($password == $model->clave){
        //     return $model;
        // }
        return false;
    }

    public static function findIdentityByAccessToken($token, $type = null){
        if ($user['accessToken'] === $token) {
           return new static($user);
        }
        return null;
    }

    public function getAuthKey(){
        return $this->auth_key;
    }

    public function validateAuthKey($authKey){
        return $this->getAuthKey() === $authKey;
    }
}
