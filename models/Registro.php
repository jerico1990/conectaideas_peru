<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property-read User|null $user This property is read-only.
 *
 */
class Registro extends Model
{
    public $dni;
    public $apellido_paterno;
    public $apellido_materno;
    public $nombres;
    public $correo_electronico;
    public $celular;
    public $fecha_nacimiento;
    public $clave;
    public $codigo_verificador;
    public $tipo_institucion_educativa;
    public $departamento;
    public $provincia;
    public $distrito;
    public $institucion_educativa_id;
    public $cargo_id;
    public $grado;
    public $seccion_4;
    public $seccion_5;
    public $seccion_6;
    public $cod_mod;
    public $aceptar_condicion;



    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['dni','apellido_paterno','apellido_materno','nombres','correo_electronico','celular' ,'codigo_verificador', 'clave', 'fecha_nacimiento','tipo_institucion_educativa','departamento','provincia','distrito','institucion_educativa_id','cargo_id','grado','seccion_4','seccion_5','seccion_6','cod_mod','aceptar_condicion'], 'safe'],
            // // username and password are both required
            // [['username', 'password'], 'required'],
            // // rememberMe must be a boolean value
            // ['rememberMe', 'boolean'],
            // // password is validated by validatePassword()
            // ['password', 'validatePassword'],
        ];
    }

}
