<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class Resetear extends Model
{
    public $dni;
    public $clave;
    public $clave2;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['dni','clave','clave2'], 'safe'],
        ];
    }

    
}
