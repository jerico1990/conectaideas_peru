<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "texto".
 *
 * @property int $id
 * @property string|null $texto_principal
 * @property string|null $enlace
 * @property int|null $tipo
 * @property string|null $archivo
 * @property string|null $video
 * @property int|null $pregunta
 * @property int|null $estado_registro
 * @property int|null $posicion
 */
class Texto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'texto';
    }

    /**
     * {@inheritdoc}
     */
    public $p02_texto_1;
    public $p02_enlace_1;
    public $p02_tipo_1;
    public $p02_enlace_video_1;
    public $p02_carga_archivo_1;

    public $p05_texto_1;
    public $p05_enlace_1;
    public $p05_tipo_1;
    public $p05_enlace_video_1;
    public $p05_carga_archivo_1;

    public $p05_texto_2;
    public $p05_enlace_2;
    public $p05_tipo_2;
    public $p05_enlace_video_2;
    public $p05_carga_archivo_2;

    public $archivo_documento;

    public function rules()
    {
        return [
            [['tipo', 'pregunta', 'estado_registro', 'posicion'], 'integer'],
            [['texto_principal', 'texto_enlace','enlace','archivo', 'video'], 'string', 'max' => 255],
            [['archivo_documento','p02_texto_1','p02_enlace_1','p02_tipo_1','p02_enlace_video_1','p02_carga_archivo_1','p05_texto_1','p05_enlace_1','p05_tipo_1','p05_enlace_video_1','p05_carga_archivo_1','p05_texto_2','p05_enlace_2','p05_tipo_2','p05_enlace_video_2','p05_carga_archivo_2'],'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'texto_principal' => 'Texto Principal',
            'enlace' => 'Enlace',
            'tipo' => 'Tipo',
            'archivo' => 'Archivo',
            'video' => 'Video',
            'pregunta' => 'Pregunta',
            'estado_registro' => 'Estado Registro',
            'posicion' => 'Posicion',
        ];
    }
}
