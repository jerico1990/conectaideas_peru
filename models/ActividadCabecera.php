<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "actividad_cabecera".
 *
 * @property int $id
 * @property int|null $experiencia_id
 * @property int|null $correlativo
 * @property string|null $fecha_inicio
 * @property string|null $fecha_fin
 * @property int|null $estado
 * @property string|null $fecha_habilitacion
 *
 * @property Experiencia $experiencia
 * @property ActividadDetalle[] $actividadDetalles
 */
class ActividadCabecera extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $titulo;
    public static function tableName()
    {
        return 'actividad_cabecera';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['experiencia_id', 'correlativo', 'estado'], 'integer'],
            [['fecha_inicio', 'fecha_fin', 'fecha_habilitacion'], 'safe'],
            [['experiencia_id'], 'exist', 'skipOnError' => true, 'targetClass' => Experiencia::className(), 'targetAttribute' => ['experiencia_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'experiencia_id' => 'Experiencia ID',
            'correlativo' => 'Correlativo',
            'fecha_inicio' => 'Fecha Inicio',
            'fecha_fin' => 'Fecha Fin',
            'estado' => 'Estado',
            'fecha_habilitacion' => 'Fecha Habilitacion',
        ];
    }

    /**
     * Gets query for [[Experiencia]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getExperiencia()
    {
        return $this->hasOne(Experiencia::className(), ['id' => 'experiencia_id']);
    }

    /**
     * Gets query for [[ActividadDetalles]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getActividadDetalles()
    {
        return $this->hasMany(ActividadDetalle::className(), ['actividad_cabecera_id' => 'id']);
    }
}
