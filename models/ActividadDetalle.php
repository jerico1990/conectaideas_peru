<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "actividad_detalle".
 *
 * @property int $id
 * @property int|null $actividad_cabecera_id
 * @property string|null $descripcion
 * @property int|null $actividad_chile_id
 * @property string|null $url_video
 * @property string|null $matriz
 * @property string|null $solucionario
 * @property int|null $grado_id
 *
 * @property Grado $grado
 * @property ActividadCabecera $actividadCabecera
 */
class ActividadDetalle extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $temp;
    public $archivo_matriz;
    public $archivo_solucionario;
    public $recurso_id;
    public $experiencia_correlativo;
    public $actividad_correlativo;
    public $grado_descripcion;
    public static function tableName()
    {
        return 'actividad_detalle';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['actividad_cabecera_id', 'actividad_chile_id', 'grado_id','recurso_id'], 'integer'],
            [['descripcion','url_video'], 'string', 'max' => 250],
            [['matriz', 'solucionario'], 'string', 'max' => 150],
            [['grado_id'], 'exist', 'skipOnError' => true, 'targetClass' => Grado::className(), 'targetAttribute' => ['grado_id' => 'id']],
            [['actividad_cabecera_id'], 'exist', 'skipOnError' => true, 'targetClass' => ActividadCabecera::className(), 'targetAttribute' => ['actividad_cabecera_id' => 'id']],
            [['temp','archivo_matriz','archivo_solucionario','experiencia_correlativo','actividad_correlativo','grado_descripcion'],'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'actividad_cabecera_id' => 'Actividad Cabecera ID',
            'descripcion' => 'Descripcion',
            'actividad_chile_id' => 'Actividad Chile ID',
            'url_video' => 'Url Video',
            'matriz' => 'Matriz',
            'solucionario' => 'Solucionario',
            'grado_id' => 'Grado ID',
        ];
    }

    /**
     * Gets query for [[Grado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGrado()
    {
        return $this->hasOne(Grado::className(), ['id' => 'grado_id']);
    }

    /**
     * Gets query for [[ActividadCabecera]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getActividadCabecera()
    {
        return $this->hasOne(ActividadCabecera::className(), ['id' => 'actividad_cabecera_id']);
    }
}
