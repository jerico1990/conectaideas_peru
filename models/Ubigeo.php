<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ubigeo".
 *
 * @property int $id
 * @property string|null $departamento
 * @property string|null $provincia
 * @property string|null $distrito
 */
class Ubigeo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ubigeo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['departamento', 'provincia', 'distrito'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'departamento' => 'Departamento',
            'provincia' => 'Provincia',
            'distrito' => 'Distrito',
        ];
    }
}
