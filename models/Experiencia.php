<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "experiencia".
 *
 * @property int $id
 * @property int|null $correlativo
 *
 * @property Actividad[] $actividads
 */
class Experiencia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $titulo;
    public static function tableName()
    {
        return 'experiencia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['correlativo'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'correlativo' => 'Correlativo',
        ];
    }

    /**
     * Gets query for [[Actividads]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getActividads()
    {
        return $this->hasMany(Actividad::className(), ['id_experiencia' => 'id']);
    }
}
