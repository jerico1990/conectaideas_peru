<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estudiante".
 *
 * @property int $id
 * @property int|null $institucion_educativa_id
 * @property string|null $nombres
 * @property string|null $apellido_paterno
 * @property string|null $apellido_materno
 * @property string|null $dni
 *
 * @property Matricula[] $matriculas
 */
class Estudiante extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $titulo;
    public $docente_seccion_id;
    public $masivamente;
    public $archivo;
    public $temp;
    public static function tableName()
    {
        return 'estudiante';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['institucion_educativa_id','docente_seccion_id'], 'integer'],
            [['nombres', 'apellido_paterno', 'apellido_materno', 'apellido_paterno_letra'], 'string', 'max' => 150],
            [['dni'], 'string', 'max' => 8],
            [['masivamente','archivo','temp'],'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'institucion_educativa_id' => 'Institucion Educativa ID',
            'nombres' => 'Nombres',
            'apellido_paterno' => 'Apellido Paterno',
            'apellido_materno' => 'Apellido Materno',
            'dni' => 'Dni',
        ];
    }

    /**
     * Gets query for [[Matriculas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMatriculas()
    {
        return $this->hasMany(Matricula::className(), ['estudiante_id' => 'id']);
    }
}
