<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "matricula".
 *
 * @property int $id
 * @property int|null $docente_seccion_id
 * @property int|null $estudiante_id
 * @property string|null $fecha_registro
 * @property int|null $estado
 *
 * @property DocenteSeccion $docenteSeccion
 * @property Estudiante $estudiante
 */
class Matricula extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'matricula';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['docente_seccion_id', 'estudiante_id', 'estado_registro'], 'integer'],
            [['fecha_registro'], 'safe'],
            [['docente_seccion_id'], 'exist', 'skipOnError' => true, 'targetClass' => DocenteSeccion::className(), 'targetAttribute' => ['docente_seccion_id' => 'id']],
            [['estudiante_id'], 'exist', 'skipOnError' => true, 'targetClass' => Estudiante::className(), 'targetAttribute' => ['estudiante_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'docente_seccion_id' => 'Docente Seccion ID',
            'estudiante_id' => 'Estudiante ID',
            'fecha_registro' => 'Fecha Registro',
            'estado_registro' => 'Estado',
        ];
    }

    /**
     * Gets query for [[DocenteSeccion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocenteSeccion()
    {
        return $this->hasOne(DocenteSeccion::className(), ['id' => 'docente_seccion_id']);
    }

    /**
     * Gets query for [[Estudiante]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstudiante()
    {
        return $this->hasOne(Estudiante::className(), ['id' => 'estudiante_id']);
    }
}
