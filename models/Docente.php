<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "docente".
 *
 * @property int $id
 * @property string|null $nombres
 * @property string|null $apellido_paterno
 * @property string|null $apellido_materno
 * @property int|null $institucion_educativa_id
 * @property int|null $cargo_id
 * @property string|null $dni
 * @property string|null $celular
 * @property string|null $correo_electronico
 * @property string|null $cod_mod
 * @property int|null $tipo_institucion_educativa_id
 *
 * @property InstitucionEducativa $institucionEducativa
 * @property Cargo $cargo
 * @property DocenteSeccion[] $docenteSeccions
 */
class Docente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $titulo;
    public $archivo;
    public $temp;
    public static function tableName()
    {
        return 'docente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['institucion_educativa_id', 'cargo_id', 'tipo_institucion_educativa_id'], 'integer'],
            [['nombres', 'apellido_paterno', 'apellido_materno', 'correo_electronico'], 'string', 'max' => 150],
            [['dni'], 'string', 'max' => 8],
            [['celular'], 'string', 'max' => 9],
            [['cod_mod','aceptar_condicion'], 'string', 'max' => 10],
            [['archivo','temp'], 'safe'],
            // [['institucion_educativa_id'], 'exist', 'skipOnError' => true, 'targetClass' => InstitucionEducativa::className(), 'targetAttribute' => ['institucion_educativa_id' => 'id']],
            // [['cargo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cargo::className(), 'targetAttribute' => ['cargo_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombres' => 'Nombres',
            'apellido_paterno' => 'Apellido Paterno',
            'apellido_materno' => 'Apellido Materno',
            'institucion_educativa_id' => 'Institucion Educativa ID',
            'cargo_id' => 'Cargo ID',
            'dni' => 'Dni',
            'celular' => 'Celular',
            'correo_electronico' => 'Correo Electronico',
            'cod_mod' => 'Cod Mod',
            'tipo_institucion_educativa_id' => 'Tipo Institucion Educativa ID',
        ];
    }

    /**
     * Gets query for [[InstitucionEducativa]].
     *
     * @return \yii\db\ActiveQuery
     */
    // public function getInstitucionEducativa()
    // {
    //     return $this->hasOne(InstitucionEducativa::className(), ['id' => 'institucion_educativa_id']);
    // }

    /**
     * Gets query for [[Cargo]].
     *
     * @return \yii\db\ActiveQuery
     */
    // public function getCargo()
    // {
    //     return $this->hasOne(Cargo::className(), ['id' => 'cargo_id']);
    // }

    // /**
    //  * Gets query for [[DocenteSeccions]].
    //  *
    //  * @return \yii\db\ActiveQuery
    //  */
    // public function getDocenteSeccions()
    // {
    //     return $this->hasMany(DocenteSeccion::className(), ['docente_id' => 'id']);
    // }
}
