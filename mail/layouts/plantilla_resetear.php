<img src="<?= Yii::$app->request->getHostInfo() . Yii::$app->getUrlManager()->getBaseUrl() ?>/img/logo_conectaideas.png" alt="">
<p>
    
    ¡Bienvenido(a) a Conecta Ideas! <br>
    
    Se envia enlace para resetear su contraseña:
    <a href="<?= $enlace ?>"><?= $enlace ?></a>

    Saludos,<br>
    Equipo Conecta Ideas
</p>