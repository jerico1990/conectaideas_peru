<div role="article" aria-roledescription="email" lang="en" style="text-size-adjust:100%;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#ffffff;">
    <table role="presentation" style="width:100%;border:none;border-spacing:0;">
      <tr>
        <td align="center" style="padding:0;">
          <table role="presentation" style="width:100%;max-width:640px;border:none;border-spacing:0;text-align:left;font-family:Arial,sans-serif;font-size:16px;line-height:22px;color:#363636;">
            <tr>
              <td style="padding:20px;text-align:center;font-size:12px;background-color:#ffffff;color:#cccccc;">
                <p style="margin:0 0 8px 0;"><a href="http://www.facebook.com/" style="text-decoration:none;">
                  <a href="http://www.twitter.com/" style="text-decoration:none;">  
                    <img src="http://monki.pe/imagenes_e/logo-conecta-ideas.png" width="200" height="25" alt="f" style="display:inline-block;color:#cccccc;">
                  </a> 
                </p>
              </td>
            </tr>
            <tr>
              <td style="padding:40px;text-align:center;background-color:#2AB7CA; border-collapse: collapse;">
                <p style="font-family:Arial,sans-serif;margin:0;font-size:20px;color:#ffffff;margin-block: 0;margin-inline: 0;">¡Bienvenido a Conecta Ideas!</p>
              </td>
            </tr>
            <tr>
              <td style="padding:35px;font-size:0;background-color:#ffffff;">
                <div class="col-sml" style="display:inline-block;width:100%;max-width:145px;vertical-align:top;text-align:left;font-family:Arial,sans-serif;font-size:14px;color:#363636;">
                  <img src="http://monki.pe/imagenes_e/conectito.png" width="125" alt="" style="width:80%;max-width:115px;margin-bottom:20px;">
                </div>
                <div class="col-lge" style="display:inline-block;width:100%;max-width:395px;vertical-align:top;padding-bottom:20px;font-family:Arial,sans-serif;font-size:16px;line-height:22px;color:#363636;">
                  <p style="font-family:Arial,sans-serif;margin-top:0;margin-bottom:12px;">Te has registrado correctamente.</p>
                  <p style="font-family:Arial,sans-serif;margin-top:0;margin-bottom:12px;"> <strong>Si eres Tutor,</strong> el siguiente paso es registrar a tus estudiantes para la creación de sus usuarios.</p>
                  <p style="font-family:Arial,sans-serif;margin-top:0;margin-bottom:12px;">Para ello, ingresa al portal Docente con los accesos que acabas de crear:</p>
                  <p style="font-family:Arial,sans-serif;margin-top:0;margin-bottom:12px;"><strong>Usuario:</strong> <span><?= $dni ?></span></p>
                  <p style="font-family:Arial,sans-serif;margin-top:0;margin-bottom:20px;"><strong>Contraseña:</strong> <span> La que creaste en el formulario</span></p>
                  <p style="font-family:Arial,sans-serif;margin:0;"><a href="http://portal.conectaideasperu.com/" target="_blank" style="box-shadow: 2px 2px 4px rgba(160, 168, 175, 0.43);background: #ff3884; text-decoration: none; padding: 13px 20px; color: #ffffff; border-radius: 10px; display:inline-block; mso-padding-alt:0;"><!--[if mso]><i style="letter-spacing: 25px;mso-font-width:-100%;mso-text-raise:20pt">&nbsp;</i><![endif]--><span style="mso-text-raise:10pt;font-weight:normal;">Ir al Portal Docente</span><!--[if mso]><i style="letter-spacing: 25px;mso-font-width:-100%">&nbsp;</i><![endif]--></a></p>
                </div>
              </td>
            </tr>
            <tr>
              <td>
                <table style="border-collapse: collapse; width: 100%;" bgcolor="#E6F4F8">
                  <tr>
                    <td align="center" style="padding:48px 42px 0px 42px; border-collapse: collapse; border: none">
                        <p style="font-family:Arial,sans-serif;margin:0;font-size:18px;line-height:20px;font-weight:bold;margin-bottom:28px;">¿Cómo registro a mis estudiantes?</p>
                        <p style="font-family:Arial,sans-serif;margin-top:0;margin-bottom:20px;">Para ver el paso a paso para registrar a tus estudiantes, puedes ver el siguiente video tutorial:</p>
                        <p style="font-family:Arial,sans-serif;margin:0;">
                          <a href="https://www.youtube.com/watch?v=PM5WDSGMeh8" target="_blank" style="box-shadow: 2px 2px 4px rgba(160, 168, 175, 0.43);background: #4F9C2E; text-decoration: none; padding: 15px 20px; color: #ffffff; border-radius: 10px; display:inline-block; mso-padding-alt:0;"><!--[if mso]><i style="letter-spacing: 25px;mso-font-width:-100%;mso-text-raise:20pt">&nbsp;</i><![endif]--><span style="mso-text-raise:10pt;font-weight:normal;">Ver video tutorial</span><!--[if mso]><i style="letter-spacing: 25px;mso-font-width:-100%">&nbsp;</i><![endif]--></a>
                        </p>
                    </td>
                  </tr>
                  <tr>
                    <td align="center" style="padding:48px 42px 48px 42px; border-collapse: collapse;">
                        <p style="font-family:Arial,sans-serif;margin:0;font-size:18px;line-height:28px;font-weight:bold;margin-bottom:3px;">Taller de capacitación
                          </p>
                        <p style="font-family:Arial,sans-serif;margin:0;font-size:18px;line-height:28px;font-weight:bold;margin-bottom:28px;">“Exploramos el Portal Docente”</p>
      
                        <p style="font-family:Arial,sans-serif;margin-top:0;margin-bottom:20px;">Además, puedes registrarte para asistir al taller semanal “Exploramos el Portal Docente” y recibir asesoría en el registro de tus estudiantes.</p>
                        <p style="font-family:Arial,sans-serif;margin:0;">
                          <a href="https://bit.ly/3er38B5" target="_blank" style="box-shadow: 2px 2px 4px rgba(160, 168, 175, 0.43);background: #FFAD12; text-decoration: none; padding: 15px 20px; color: #ffffff; border-radius: 10px; display:inline-block; mso-padding-alt:0;"><!--[if mso]><i style="letter-spacing: 25px;mso-font-width:-100%;mso-text-raise:20pt">&nbsp;</i><![endif]--><span style="mso-text-raise:10pt;font-weight:normal;">Regístrate aquí</span><!--[if mso]><i style="letter-spacing: 25px;mso-font-width:-100%">&nbsp;</i><![endif]--></a>
                        </p>
                    </td>
                  </tr>  
                </table>
              </td>
            </tr>
            <tr>
              <td style="padding:35px 0 ;font-size:0;background-color:#ffffff;" align="center">
                <div class="col-footer" style="display:inline-block;width:32%;max-width:200px;vertical-align:middle;padding-bottom:20px;font-family:Arial,sans-serif;font-size:16px;line-height:22px;color:#363636;">
                  <a href="www.facebook.com/ConectaIdeasPeru" style="text-decoration:none; font-family: Arial,sans-serif; font-size: 14px; color: #000;">  
                    <img src="http://monki.pe/imagenes_e/icon-facebook.png" alt="icon-facebook" style="vertical-align: middle;display:inline-block;color:#000;">
                    <span style="vertical-align: middle;display:inline-block;">&nbsp; Conecta Ideas Perú</span>
                  </a>
                </div>
                <div class="col-footer" style="display:inline-block;width:2%;max-width:10px;vertical-align:middle;padding-bottom:20px;font-family:Arial,sans-serif;font-size:16px;line-height:22px;color:#363636;">
                  <span class="show" style="vertical-align: middle;">
                    <img src="http://monki.pe/imagenes_e/line.png">
                  </span>
                </div>
                <div class="col-footer" style="display:inline-block;width:32%;max-width:200px;vertical-align:middle;padding-bottom:20px;font-family:Arial,sans-serif;font-size:16px;line-height:22px;color:#363636;">
                  <a href="https://www.conectaideasperu.com" style="text-decoration:none; font-family: Arial,sans-serif; font-size: 14px; color: #000;">
                    <span>www.conectaideasperu.com</span>
                  </a>
                </div>
                <div class="col-footer" style="display:inline-block;width:2%;max-width:10px;vertical-align:middle;padding-bottom:20px;font-family:Arial,sans-serif;font-size:16px;line-height:22px;color:#363636;">
                  <span class="show" style="vertical-align: middle;">
                    <img src="http://monki.pe/imagenes_e/line.png">
                  </span>
                </div>
                <div class="col-footer" style="display:inline-block;width:32%;max-width:200px;vertical-align:middle;padding-bottom:20px;font-family:Arial,sans-serif;font-size:16px;line-height:22px;color:#363636;">
                  <a href="https://www.youtube.com/channel/UC2PF514onE4vP7mOTg1AqfA" style="text-decoration:none; font-family: Arial,sans-serif; font-size: 14px; color: #000;">
                    <span style="vertical-align: middle;">
                      <img src="http://monki.pe/imagenes_e/icon-youtube.png" alt="icon-youtube" style="vertical-align: middle;display:inline-block;color:#000;">
                      <span style="vertical-align: middle;display:inline-block;"> &nbsp; Conecta Ideas Perú</span>
                    </span>
                  </a>
                </div>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>


<!-- <img src="<?= Yii::$app->request->getHostInfo() . Yii::$app->getUrlManager()->getBaseUrl() ?>/img/logo_conectaideas.png" alt="" height="60px">
<p>
    
    ¡Bienvenido(a) a Conecta Ideas! <br>
    Gracias por registrarte. <br> <br>

    Con tu usuario <?= $dni ?> y contraseña generada, puedes acceder al Portal Docente desde el Lunes 19 de abril para registrar a tus estudiantes. <br><br>

    Asimismo, te avisaremos pronto sobre algunos talleres en los que puedes participar para conocer más sobre el programa y los recursos que tendrás disponibles. <br><br>

    Recuerda que para ingresar al Portal Docente necesitarás los siguientes datos:<br><br>

    Usuario: <?= $dni ?><br>
    Contraseña: (La que creaste en el formulario de registro)<br><br>

    Si tuvieras alguna consulta puedes visitar nuestro <a href="https://www.conectaideasperu.com/centrodeayuda/">Centro de ayuda</a>  o nuestro canal de <a href="https://api.whatsapp.com/send?phone=+51964180225&text=Hola,%20quisiera%20más%20información">Whatsapp</a> <br><br>

    Saludos,<br>
    Equipo Conecta Ideas
</p> -->