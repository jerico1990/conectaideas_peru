<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="x-apple-disable-message-reformatting">
    <title>Conecta Ideas Perú</title>
    <style>
    table, td, div, h1, p {
      font-family: Arial, sans-serif;
    }

    table, td, tr {
      border-collapse: collapse;
    }

    @media screen and (max-width: 530px) {
      .unsub {
        display: block;
        padding: 8px;
        margin-top: 14px;
        border-radius: 6px;
        background-color: #555555;
        text-decoration: none !important;
        font-weight: bold;
      }
      .col-lge {
        max-width: 100% !important;
      }

      .col-footer {
        width: % !important;
      }
      .show {
        display: none;
      }
    }
    @media screen and (min-width: 531px) {
      .col-sml {
        max-width: 27% !important;
      }
      .col-lge {
        max-width: 73% !important;
      }

      .col-footer {
        max-width: 33% !important;
      }

      .show {
        padding: 0 6px;
        display: inline-block;
        vertical-align: middle;
      }
    }
  </style>
</head>
<body>
    <?php $this->beginBody() ?>
       
    <?= $content ?>
    
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
