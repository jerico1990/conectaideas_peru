<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

class CargoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','get-cargos','get-cargos-tipo-institucion'],
                'rules' => [
					[
                        'allow' => true,
                        'actions' => ['get-cargos','get-cargos-tipo-institucion'],
						'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index','get-cargos','get-cargos-tipo-institucion'],
                        'roles' => ['@'],
                    ],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='vacio';
        return $this->render('index');
    }

    public function actionGetCargos(){
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
       
        if($_POST){

            $cargos = (new \yii\db\Query())->select('*')->from('cargo')->where('estado=1');
            $cargos = $cargos->all();

            return ['success'=>true,'data'=>$cargos];
        }
    }

    public function actionGetCargosTipoInstitucion(){
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
       
        if($_POST){
            $tipo_institucion_id = (isset($_POST['tipo_institucion_id']) && $_POST['tipo_institucion_id']!='')?$_POST['tipo_institucion_id']:null;

            $cargos = (new \yii\db\Query())->select('*')->from('cargo')->where('estado=1');
            $cargos = $cargos->andWhere(['=', "tipo_institucion_id",$tipo_institucion_id]);
            $cargos = $cargos->all();

            return ['success'=>true,'data'=>$cargos];
        }
    }
}
