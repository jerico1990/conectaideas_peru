<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\CabeceraCargaArchivo;
use app\models\CrecimientoCabecera;
use app\models\Grado;
use app\models\Cargo;

class PanelController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        
        if(Yii::$app->user->identity->perfil==99){
            $this->layout='privado_skote';
            return $this->render('index_administrador');
        }else if(Yii::$app->user->identity->perfil==1 || Yii::$app->user->identity->perfil==2 || Yii::$app->user->identity->perfil==4 || Yii::$app->user->identity->perfil==5 || Yii::$app->user->identity->perfil==6 || Yii::$app->user->identity->perfil==7){
            $cargo = Cargo::findOne(Yii::$app->user->identity->perfil);
            $this->layout='privado';
            return $this->render('index_director',['prefijo_perfil'=>$cargo->descripcion_corta]);
        }else{
            $cargo = Cargo::findOne(Yii::$app->user->identity->perfil);
            $this->layout='privado';
            return $this->render('index',['prefijo_perfil'=>$cargo->descripcion_corta]);
        }
    }

    public function actionOpciones($docente_seccion_id=null,$grado=null,$seccion=null){
        
        $this->layout='privado';
        $grado = Grado::find()->where('grado=:grado',[':grado'=>$grado])->one();
        return $this->render('opciones',['docente_seccion_id'=>$docente_seccion_id,'grado'=>$grado,'seccion'=>$seccion]);
    }

    public function actionGetListaGradosSecciones(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            
            $gradosSecciones = (new \yii\db\Query())
                ->select(['docente_seccion.id,grado.grado,grado.nombre_corto nombre_corto,seccion.descripcion seccion_descripcion,count(estudiante.id) cantidad_estudiante'])
                ->from('usuario')
                ->innerJoin('docente','docente.dni=usuario.usuario')
                ->innerJoin('docente_seccion','docente_seccion.docente_id=docente.id')
                ->innerJoin('seccion','seccion.id=docente_seccion.seccion_id')
                ->innerJoin('grado','grado.id=seccion.grado_id')
                ->leftJoin('matricula','matricula.docente_seccion_id=docente_seccion.id and matricula.estado_registro=1')
                ->leftJoin('estudiante','estudiante.id=matricula.estudiante_id')
                ->where('usuario.id=:id',[':id'=>Yii::$app->user->identity->id])
                ->groupBy('docente_seccion.id,grado.grado,grado.descripcion,seccion.descripcion')
                ->all();
            return ['success'=>true,'gradosSecciones'=>$gradosSecciones];
        }
    }
}
