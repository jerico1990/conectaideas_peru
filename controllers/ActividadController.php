<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\ActividadCabecera;
use app\models\ActividadDetalle;
use app\models\Grado;
use yii\web\UploadedFile;

class ActividadController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($docente_seccion_id=null,$grado=null,$seccion=null){
        
        
        if(Yii::$app->user->identity->perfil==99){
            $this->layout='privado_skote';
            return $this->render('index');
        }else if(Yii::$app->user->identity->perfil==1 || Yii::$app->user->identity->perfil==2 || Yii::$app->user->identity->perfil==4 || Yii::$app->user->identity->perfil==5 || Yii::$app->user->identity->perfil==6 || Yii::$app->user->identity->perfil==7){
            $this->layout='privado';
            $grado = Grado::find()->where('grado=:grado',[':grado'=>$grado])->one();
            return $this->render('index_director',['grado'=>$grado]);
        }else{
            $this->layout='privado';
            $grado = Grado::find()->where('grado=:grado',[':grado'=>$grado])->one();
            return $this->render('index_docente',['docente_seccion_id'=>$docente_seccion_id,'grado'=>$grado,'seccion'=>$seccion]);
        }
    }

    public function actionCreate(){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = new ActividadCabecera();
        $model->titulo = 'Registrar actividad';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $model->estado=0;
                $model->estado_reporte=0;
                $model->fecha_habilitacion = date( 'Y-m-d H:i:s');
                if($model->save()){
                    $actividad_detalle = new ActividadDetalle;
                    $actividad_detalle->actividad_cabecera_id = $model->id;
                    $actividad_detalle->grado_id = 1;
                    $actividad_detalle->save();

                    $actividad_detalle = new ActividadDetalle;
                    $actividad_detalle->actividad_cabecera_id = $model->id;
                    $actividad_detalle->grado_id = 2;
                    $actividad_detalle->save();

                    $actividad_detalle = new ActividadDetalle;
                    $actividad_detalle->actividad_cabecera_id = $model->id;
                    $actividad_detalle->grado_id = 3;
                    $actividad_detalle->save();

                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionUpdate($id){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = ActividadCabecera::findOne($id);
        $model->titulo = 'Actualizar actividad';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    

    public function actionGetActividad(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $actividad_id = $_POST['id_actividad'];
            $actividad = (new \yii\db\Query())
                ->select('actividad_cabecera.*')
                ->from('actividad_cabecera')
                ->where('id=:id',[':id'=>$actividad_id])
                ->one();
            return ['success'=>true,'actividad'=>$actividad];
        }
    }

    public function actionGetListaActividades(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
                $actividades = (new \yii\db\Query())
                    ->select('*')
                    ->from('vw_lista_actividades');

                if(isset($_POST['experiencia_id']) && $_POST['experiencia_id']!=''){
                    $actividades = $actividades->andWhere(['=', "experiencia_id",$_POST['experiencia_id']]);
                }

                if(isset($_POST['grado_id']) && $_POST['grado_id']!=''){
                    $actividades = $actividades->andWhere(['=', "grado_id",$_POST['grado_id']]);
                }

                if(isset($_POST['actividad_id']) && $_POST['actividad_id']!=''){
                    $actividades = $actividades->andWhere(['=', "id",$_POST['actividad_id']]);
                }

                $actividades = $actividades->all();
            return ['success'=>true,'actividades'=>$actividades];
        }
    }


    public function actionGetListaExperienciaActividades(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
                $actividades = (new \yii\db\Query())
                    ->select('actividad_cabecera.*,experiencia.correlativo experiencia_correlativo,grado.nombre_corto')
                    ->from('actividad_cabecera')
                    ->innerJoin('experiencia','experiencia.id=actividad_cabecera.experiencia_id')
                    ->innerJoin('actividad_detalle','actividad_detalle.actividad_cabecera_id=actividad_cabecera.id')
                    ->innerJoin('grado','grado.id=actividad_detalle.grado_id');

                if(isset($_POST['id_experiencia']) && $_POST['id_experiencia']!=''){
                    $actividades = $actividades->andWhere(['=', "actividad_cabecera.experiencia_id",$_POST['id_experiencia']]);
                }

                if(isset($_POST['id_grado']) && $_POST['id_grado']!=''){
                    $actividades = $actividades->andWhere(['=', "actividad_detalle.grado_id",$_POST['id_grado']]);
                }

                if(isset($_POST['id_actividad']) && $_POST['id_actividad']!=''){
                    $actividades = $actividades->andWhere(['=', "actividad_cabecera.id",$_POST['id_actividad']]);
                }

                $actividades = $actividades->andWhere(['=', "actividad_cabecera.estado","1"]);
                $actividades = $actividades->all();
            return ['success'=>true,'actividades'=>$actividades];
        }
    }

    public function actionGetExperienciaActividades(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
                $lista_actividades_recursos = [];
                $actividades = (new \yii\db\Query())
                    ->select('actividad_cabecera.*')
                    ->from('actividad_cabecera')
                    ->innerJoin('experiencia','experiencia.id=actividad_cabecera.experiencia_id');

                if(isset($_POST['experiencia_id']) && $_POST['experiencia_id']!=''){
                    $actividades = $actividades->andWhere(['=', "actividad_cabecera.experiencia_id",$_POST['experiencia_id']]);
                }

                $actividades = $actividades->andWhere(['=', "actividad_cabecera.estado","1"]);
                $actividades = $actividades->all();


                foreach($actividades as $actividad){
                    $recursos = (new \yii\db\Query())
                            ->select('actividad_detalle.*,grado.nombre_corto')
                            ->from('actividad_detalle')
                            ->innerJoin('grado','grado.id=actividad_detalle.grado_id')
                            ->where('actividad_cabecera_id=:actividad_cabecera_id',[':actividad_cabecera_id'=>$actividad['id']]);
                    if(isset($_POST['grado_id']) && $_POST['grado_id']!=''){
                        $recursos = $recursos->andWhere(['=', "grado.id",$_POST['grado_id']]);
                    }
                    $recursos = $recursos->all();
                    
                    $actividad_detalle_id = ($recursos && $recursos[0])?$recursos[0]['id']:false;
                    $reporte_01 = Yii::$app->db->createCommand('
                    SELECT
                        estudiantes_reporte01.id,
                        estudiantes_reporte01.conectados,
                        estudiantes_reporte01.no_conectados,
                        estudiantes_reporte01.fecha_fin	
                    FROM (
                        SELECT
                            A.id,
                            R.conectados,
                            R.no_conectados,
                            DATE_FORMAT(A.fecha_inicio,"%d/%m") as fecha_fin
                        FROM reporte_01 R
                        INNER JOIN actividad_detalle AD ON R.actividad_detalle_id = AD.id
                        INNER JOIN actividad_cabecera A ON AD.actividad_cabecera_id = A.id
                        INNER JOIN docente_seccion DS ON R.docente_seccion_id = DS.id
                        INNER JOIN docente D ON DS.docente_id = D.id
                        INNER JOIN usuario U ON D.dni = U.usuario
                        WHERE U.id = "' . Yii::$app->user->identity->id . '" AND AD.grado_id = "'. $_POST['grado_id'] .'" AND R.conectados>0 AND A.id <= (
                        SELECT A.id FROM actividad_detalle AD
                        INNER JOIN actividad_cabecera A ON AD.actividad_cabecera_id = A.id WHERE AD.id = '.$actividad_detalle_id.')
                        ORDER BY A.id ASC
                        LIMIT 3) estudiantes_reporte01
                    order by estudiantes_reporte01.id asc
                    ')->queryAll();


                    $reporte_02 = Yii::$app->db->createCommand('
                        SELECT
                        E.nombres,
                        E.apellido_paterno_letra
                        FROM
                        reporte_02 R
                        INNER JOIN actividad_detalle AD ON R.actividad_detalle_id = AD.id
                        INNER JOIN actividad_cabecera A ON AD.actividad_cabecera_id = A.id
                        INNER JOIN docente_seccion DS ON R.docente_seccion_id = DS.id
                        INNER JOIN estudiante E on R.estudiante_id = E.id
                        INNER JOIN docente D ON DS.docente_id = D.id
                        INNER JOIN usuario U ON D.dni = U.usuario
                        WHERE U.id = "' . Yii::$app->user->identity->id . '" AND AD.grado_id = "'. $_POST['grado_id'] .'" AND A.id = '.$actividad['id'].'
                    ')->queryAll();


                    $reporte_03 = Yii::$app->db->createCommand('
                        SELECT
                            R.id,
                            C.descripcion,
                            R.avanzado,
                            R.intermedio,
                            R.inicial,
                            R.ninguno
                        FROM
                        reporte_03 R
                        INNER JOIN actividad_detalle AD ON R.actividad_detalle_id = AD.id
                        INNER JOIN actividad_cabecera A ON AD.actividad_cabecera_id = A.id
                        INNER JOIN docente_seccion DS ON R.docente_seccion_id = DS.id
                        INNER JOIN competencia C on R.competencia_id = C.id
                        INNER JOIN docente D ON DS.docente_id = D.id
                        INNER JOIN usuario U ON D.dni = U.usuario
                        WHERE U.id = "' . Yii::$app->user->identity->id . '" AND AD.grado_id = "'. $_POST['grado_id'] .'" AND AD.actividad_cabecera_id = '.$actividad['id'].'
                    ')->queryAll();

                    $reporte_04 = Yii::$app->db->createCommand('
                        SELECT
                            CA.descripcion descripcion_capacidad,
                            R.numero_problema,
                            R.numero_estudiantes
                        FROM
                        reporte_04 R
                        INNER JOIN actividad_detalle AD ON R.actividad_detalle_id = AD.id
                        INNER JOIN actividad_cabecera A ON AD.actividad_cabecera_id = A.id
                        INNER JOIN docente_seccion DS ON R.docente_seccion_id = DS.id
                        INNER JOIN docente D ON DS.docente_id = D.id
                        INNER JOIN usuario U ON D.dni = U.usuario
                        INNER JOIN capacidad CA ON CA.id = R.capacidad_id
                        WHERE U.id = "' . Yii::$app->user->identity->id . '" AND AD.grado_id = "'. $_POST['grado_id'] .'" AND AD.actividad_cabecera_id = '.$actividad['id'].'
                        ORDER BY R.numero_estudiantes desc;
                    ')->queryAll();

                    $reporte_05 = Yii::$app->db->createCommand('
                        SELECT
                            E.id,
                            E.nombres,
                            E.apellido_paterno_letra,
                            R.puntaje,
                            R.p01,
                            R.p02,
                            R.p03,
                            R.p04,
                            R.p05,
                            R.p06,
                            R.p07,
                            R.p08,
                            R.p09,
                            R.p10,
                            R.p11,
                            R.p12,
                            R.p13,
                            R.p14,
                            R.p15,
                            R.p16,
                            R.p17,
                            R.p18,
                            R.p19,
                            R.p20,
                            R.p21,
                            R.p22,
                            R.p23,
                            R.p24,
                            R.p25,
                            R.p26,
                            R.p27,
                            R.p28,
                            R.p29,
                            R.p30
                        FROM
                        reporte_05 R
                        INNER JOIN actividad_detalle AD ON R.actividad_detalle_id = AD.id
                        INNER JOIN actividad_cabecera A ON AD.actividad_cabecera_id = A.id
                        INNER JOIN docente_seccion DS ON R.docente_seccion_id = DS.id
                        INNER JOIN estudiante E on R.estudiante_id = E.id
                        INNER JOIN docente D ON DS.docente_id = D.id
                        INNER JOIN usuario U ON D.dni = U.usuario
                        WHERE U.id = "' . Yii::$app->user->identity->id . '" AND AD.grado_id = "'. $_POST['grado_id'] .'" AND AD.actividad_cabecera_id = '.$actividad['id'].'
                        ORDER BY R.puntaje desc;
                    ')->queryAll();

                    array_push($lista_actividades_recursos,['id'=>$actividad['id'],
                                                            'correlativo'=>$actividad['correlativo'],
                                                            'detalle'=>$recursos,
                                                            'reporte01'=>$reporte_01,
                                                            'reporte02'=>$reporte_02,
                                                            'reporte03'=>$reporte_03,
                                                            'reporte04'=>$reporte_04,
                                                            'reporte05'=>$reporte_05
                                                            ]);
                }

            return ['success'=>true,'actividades'=>$lista_actividades_recursos];
        }
    }


    public function actionGetExperienciaActividadesReportes(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
                $lista_actividades_recursos = [];
                $actividades = (new \yii\db\Query())
                    ->select('actividad_cabecera.*')
                    ->from('actividad_cabecera')
                    ->innerJoin('experiencia','experiencia.id=actividad_cabecera.experiencia_id');

                if(isset($_POST['experiencia_id']) && $_POST['experiencia_id']!=''){
                    $actividades = $actividades->andWhere(['=', "actividad_cabecera.experiencia_id",$_POST['experiencia_id']]);
                }

                $actividades = $actividades->andWhere(['=', "actividad_cabecera.estado_reporte","1"]);
                $actividades = $actividades->all();

                //var_dump($actividades);die;
                $p02_texto = (new \yii\db\Query())->select('*')->from('texto')->where('experiencia_id=:experiencia_id and grado_id=:grado_id and posicion=1 and pregunta=2',[':experiencia_id'=>$_POST['experiencia_id'],':grado_id'=>$_POST['grado_id']])->one();

                $p05_texto_01 = (new \yii\db\Query())->select('*')->from('texto')->where('experiencia_id=:experiencia_id and grado_id=:grado_id and posicion=1 and pregunta=5',[':experiencia_id'=>$_POST['experiencia_id'],':grado_id'=>$_POST['grado_id']])->one();

                $p05_texto_02 = (new \yii\db\Query())->select('*')->from('texto')->where('experiencia_id=:experiencia_id and grado_id=:grado_id and posicion=2 and pregunta=5',[':experiencia_id'=>$_POST['experiencia_id'],':grado_id'=>$_POST['grado_id']])->one();
                

                foreach($actividades as $actividad){

                    


                    $recursos = (new \yii\db\Query())
                            ->select('actividad_detalle.*,grado.nombre_corto')
                            ->from('actividad_detalle')
                            ->innerJoin('grado','grado.id=actividad_detalle.grado_id')
                            ->where('actividad_cabecera_id=:actividad_cabecera_id',[':actividad_cabecera_id'=>$actividad['id']]);
                    if(isset($_POST['grado_id']) && $_POST['grado_id']!=''){
                        $recursos = $recursos->andWhere(['=', "grado.id",$_POST['grado_id']]);
                    }
                    $recursos = $recursos->all();
                    
                    $actividad_detalle_id = ($recursos && $recursos[0])?$recursos[0]['id']:false;
                    $reporte_01 = Yii::$app->db->createCommand('

                        SELECT
                            estudiantes_reporte01.id,
                            estudiantes_reporte01.correlativo,
                            estudiantes_reporte01.conectados,
                            estudiantes_reporte01.no_conectados,
                            estudiantes_reporte01.fecha_fin
                        FROM (
                            SELECT
                                R.id,
                                A.correlativo,
                                R.conectados,
                                R.no_conectados,
                                DATE_FORMAT(A.fecha_inicio,"%d/%m") as fecha_fin
                            FROM reporte_01 R
                            INNER JOIN actividad_detalle AD ON R.actividad_detalle_id = AD.id
                            INNER JOIN actividad_cabecera A ON AD.actividad_cabecera_id = A.id
                            INNER JOIN docente_seccion DS ON R.docente_seccion_id = DS.id
                            INNER JOIN docente D ON DS.docente_id = D.id
                            INNER JOIN usuario U ON D.dni = U.usuario
                            WHERE U.id = "' . Yii::$app->user->identity->id . '" AND AD.grado_id = "'. $_POST['grado_id'] .'" AND R.conectados>0 AND A.id <= (
                            SELECT A.id FROM actividad_detalle AD
                            INNER JOIN actividad_cabecera A ON AD.actividad_cabecera_id = A.id WHERE AD.id = '.$actividad_detalle_id.')
                            ORDER BY A.id DESC
                            LIMIT 3) estudiantes_reporte01
                        order by estudiantes_reporte01.id asc
                    ')->queryAll();


                    $reporte_02 = Yii::$app->db->createCommand('
                        SELECT
                        E.nombres,
                        E.apellido_paterno_letra
                        FROM
                        reporte_02 R
                        INNER JOIN actividad_detalle AD ON R.actividad_detalle_id = AD.id
                        INNER JOIN actividad_cabecera A ON AD.actividad_cabecera_id = A.id
                        INNER JOIN docente_seccion DS ON R.docente_seccion_id = DS.id
                        INNER JOIN estudiante E on R.estudiante_id = E.id
                        INNER JOIN docente D ON DS.docente_id = D.id
                        INNER JOIN usuario U ON D.dni = U.usuario
                        WHERE U.id = "' . Yii::$app->user->identity->id . '" AND AD.grado_id = "'. $_POST['grado_id'] .'" AND A.id = '.$actividad['id'].'
                    ')->queryAll();


                    $reporte_03 = Yii::$app->db->createCommand('
                        SELECT
                            R.id,
                            C.descripcion,
                            R.avanzado,
                            R.intermedio,
                            R.inicial,
                            R.ninguno
                        FROM
                        reporte_03 R
                        INNER JOIN actividad_detalle AD ON R.actividad_detalle_id = AD.id
                        INNER JOIN actividad_cabecera A ON AD.actividad_cabecera_id = A.id
                        INNER JOIN docente_seccion DS ON R.docente_seccion_id = DS.id
                        INNER JOIN competencia C on R.competencia_id = C.id
                        INNER JOIN docente D ON DS.docente_id = D.id
                        INNER JOIN usuario U ON D.dni = U.usuario
                        WHERE U.id = "' . Yii::$app->user->identity->id . '" AND AD.grado_id = "'. $_POST['grado_id'] .'" AND AD.actividad_cabecera_id = '.$actividad['id'].'
                    ')->queryAll();

                    $reporte_04 = Yii::$app->db->createCommand('
                        SELECT
                            CA.descripcion descripcion_capacidad,
                            R.numero_problema,
                            R.numero_estudiantes
                        FROM
                        reporte_04 R
                        INNER JOIN actividad_detalle AD ON R.actividad_detalle_id = AD.id
                        INNER JOIN actividad_cabecera A ON AD.actividad_cabecera_id = A.id
                        INNER JOIN docente_seccion DS ON R.docente_seccion_id = DS.id
                        INNER JOIN docente D ON DS.docente_id = D.id
                        INNER JOIN usuario U ON D.dni = U.usuario
                        INNER JOIN capacidad CA ON CA.id = R.capacidad_id
                        WHERE U.id = "' . Yii::$app->user->identity->id . '" AND AD.grado_id = "'. $_POST['grado_id'] .'" AND AD.actividad_cabecera_id = '.$actividad['id'].'
                        ORDER BY R.numero_estudiantes desc
                    ')->queryAll();

                    $reporte_05 = Yii::$app->db->createCommand('
                        SELECT
                            E.id,
                            E.nombres,
                            E.apellido_paterno_letra,
                            R.puntaje,
                            R.p01,
                            R.p02,
                            R.p03,
                            R.p04,
                            R.p05,
                            R.p06,
                            R.p07,
                            R.p08,
                            R.p09,
                            R.p10,
                            R.p11,
                            R.p12,
                            R.p13,
                            R.p14,
                            R.p15,
                            R.p16,
                            R.p17,
                            R.p18,
                            R.p19,
                            R.p20,
                            R.p21,
                            R.p22,
                            R.p23,
                            R.p24,
                            R.p25,
                            R.p26,
                            R.p27,
                            R.p28,
                            R.p29,
                            R.p30
                        FROM
                        reporte_05 R
                        INNER JOIN actividad_detalle AD ON R.actividad_detalle_id = AD.id
                        INNER JOIN actividad_cabecera A ON AD.actividad_cabecera_id = A.id
                        INNER JOIN docente_seccion DS ON R.docente_seccion_id = DS.id
                        INNER JOIN estudiante E on R.estudiante_id = E.id
                        INNER JOIN docente D ON DS.docente_id = D.id
                        INNER JOIN usuario U ON D.dni = U.usuario
                        WHERE U.id = "' . Yii::$app->user->identity->id . '" AND AD.grado_id = "'. $_POST['grado_id'] .'" AND AD.actividad_cabecera_id = '.$actividad['id'].'
                        ORDER BY R.puntaje desc;
                    ')->queryAll();

                    array_push($lista_actividades_recursos,['id'=>$actividad['id'],
                                                            'correlativo'=>$actividad['correlativo'],
                                                            'detalle'=>$recursos,
                                                            'reporte01'=>$reporte_01,
                                                            'reporte02'=>$reporte_02,
                                                            'reporte03'=>$reporte_03,
                                                            'reporte04'=>$reporte_04,
                                                            'reporte05'=>$reporte_05
                                                            ]);
                    
                }

            return ['success'=>true,'actividades'=>$lista_actividades_recursos,'p02_texto'=>$p02_texto,'p05_texto_01'=>$p05_texto_01,'p05_texto_02'=>$p05_texto_02];
        }
    }

    public function actionGetExperienciaActividadesAdmin(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
                $lista_actividades_recursos = [];
                $actividades = (new \yii\db\Query())
                    ->select('actividad_cabecera.*')
                    ->from('actividad_cabecera')
                    ->innerJoin('experiencia','experiencia.id=actividad_cabecera.experiencia_id');

                if(isset($_POST['experiencia_id']) && $_POST['experiencia_id']!=''){
                    $actividades = $actividades->andWhere(['=', "actividad_cabecera.experiencia_id",$_POST['experiencia_id']]);
                }

                $actividades = $actividades->all();


                foreach($actividades as $actividad){
                    $recursos = (new \yii\db\Query())
                            ->select('actividad_detalle.*,grado.nombre_corto')
                            ->from('actividad_detalle')
                            ->innerJoin('grado','grado.id=actividad_detalle.grado_id')
                            ->where('actividad_cabecera_id=:actividad_cabecera_id',[':actividad_cabecera_id'=>$actividad['id']]);
                    if(isset($_POST['grado_id']) && $_POST['grado_id']!=''){
                        $recursos = $recursos->andWhere(['=', "grado.id",$_POST['grado_id']]);
                    }
                    $recursos = $recursos->all();
                    
                    array_push($lista_actividades_recursos,['id'=>$actividad['id'],'correlativo'=>$actividad['correlativo'],'detalle'=>$recursos]);
                }

            return ['success'=>true,'actividades'=>$lista_actividades_recursos];
        }
    }


    


    public function actionHabilitarActividad(){
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $actividad_id = $_POST['actividad_id'];
            $model = ActividadCabecera::findOne($actividad_id);
            $model->estado=1;
            $model->fecha_habilitacion = date ( 'Y-m-d H:i:s'); 
            if($model->save()){
                return ['success'=>true];
            }else{
                return ['success'=>false];
            }
        }
    }


    public function actionDeshabilitarActividad(){
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $actividad_id = $_POST['actividad_id'];
            $model = ActividadCabecera::findOne($actividad_id);
            $model->estado=0;
            if($model->save()){
                return ['success'=>true];
            }else{
                return ['success'=>false];
            }
        }
    }


    public function actionHabilitarActividadReporte(){
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $actividad_id = $_POST['actividad_id'];
            $model = ActividadCabecera::findOne($actividad_id);
            $model->estado_reporte=1;
            $model->fecha_habilitacion = date ( 'Y-m-d H:i:s'); 
            if($model->save()){
                return ['success'=>true];
            }else{
                return ['success'=>false];
            }
        }
    }


    public function actionDeshabilitarActividadReporte(){
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $actividad_id = $_POST['actividad_id'];
            $model = ActividadCabecera::findOne($actividad_id);
            $model->estado_reporte=0;
            if($model->save()){
                return ['success'=>true];
            }else{
                return ['success'=>false];
            }
        }
    }

}
