<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Recurso;
use app\models\Cargo;
class RecursoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){

        // if(Yii::$app->user->identity->perfil==1 || Yii::$app->user->identity->perfil==2 || Yii::$app->user->identity->perfil==5 || Yii::$app->user->identity->perfil==6 || Yii::$app->user->identity->perfil==7){
            
        //     $this->layout='privado';
        //     return $this->render('index_director');
        // }else{
        //     $this->layout='privado';
        //     return $this->render('index');
        // }
        $this->layout='privado';
        return $this->render('index_director');
    }

    public function actionView(){
        $this->layout='privado';
        return $this->render('view');
    }

    public function actionGetListaRecursos(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
                $recursos = (new \yii\db\Query())
                    ->select('recurso.*,grado.descripcion grado_descripcion,semana.semana semana_descripcion')
                    ->from('recurso')
                    ->leftJoin('grado','grado.id=recurso.grado_id')
                    ->leftJoin('semana','semana.id=recurso.semana_id')
                    ->where('recurso.estado_registro=1')
                    ->all();
            return ['success'=>true,'recursos'=>$recursos];
        }
    }

    public function actionGetRecurso(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $recurso_id = $_POST['recurso_id'];
            $recurso = (new \yii\db\Query())
                ->select('recurso.*')
                ->from('recurso')
                ->where('id=:id',[':id'=>$recurso_id])
                ->one();
            return ['success'=>true,'recurso'=>$recurso];
        }
    }

    public function actionCreate(){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = new Recurso();
        $model->titulo = 'Registrar recurso';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $model->estado_registro = 1 ;
                $model->fecha_registro = date ( 'Y-m-d H:i:s'); 
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }


    public function actionUpdate($id){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = Recurso::findOne($id);
        $model->titulo = 'Actualizar recurso';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionEliminarRecurso(){
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $recurso_id = $_POST['recurso_id'];
            $model = Recurso::findOne($recurso_id);
            $model->estado_registro=0;
            if($model->save()){
                return ['success'=>true];
            }else{
                return ['success'=>false];
            }
        }
    }
}
