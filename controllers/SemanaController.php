<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

class SemanaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='vacio';
        return $this->render('index');
    }

    public function actionGetListaSemanas(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
                $semanas = (new \yii\db\Query())
                    ->select('*')
                    ->from('semana')
                    ->all();
            return ['success'=>true,'semanas'=>$semanas];
        }
    }


    public function actionGetSemana(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $semana = $_POST['semana'];
            $recurso = (new \yii\db\Query())
                ->select('recurso.*')
                ->from('recurso')
                ->innerJoin('semana','semana.id=recurso.semana_id')
                ->where('semana.semana=:semana',[':semana'=>$semana])
                ->one();
            return ['success'=>true,'recurso'=>$recurso];
        }
    }
}
