<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

class TipoInstitucionEducativaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','get-tipos-instituciones-educativas'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['get-tipos-instituciones-educativas'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['get-tipos-instituciones-educativas'],
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index'                                 => ['post','get'],
                    'get-tipos-instituciones-educativas'    => ['post'],
                ],
            ],
            // 'corsFilter' => [
            //     'class' => \yii\filters\Cors::className(),
            //     'cors' => [
            //         // restrict access to
            //         'Origin' => ['https://www.pais.gob.pe', 'http://localhost','http://localhost:4200','https://www.myserver.com'],
            //         // Allow only POST and PUT methods
            //         'Access-Control-Request-Method' => ['POST','PUT','GET'],
            //         // // Allow only headers 'X-Wsse'
            //         'Access-Control-Request-Headers' => ['X-Wsse'],
            //         // // Allow credentials (cookies, authorization headers, etc.) to be exposed to the browser
            //         'Access-Control-Allow-Credentials' => true,
            //         // // Allow OPTIONS caching
            //         'Access-Control-Max-Age' => 3600,
            //         // // Allow the X-Pagination-Current-Page header to be exposed to the browser.
            //         'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
            //     ],
    
            // ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='vacio';
        return $this->render('index');
    }


    public function actionGetTiposInstitucionesEducativas(){
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
       
        if($_POST){

            $tipos_instituciones_educativas = (new \yii\db\Query())->select('*')->from('tipo_institucion_educativa');
            $tipos_instituciones_educativas = $tipos_instituciones_educativas->all();

            return ['success'=>true,'data'=>$tipos_instituciones_educativas];
        }
    }
}
