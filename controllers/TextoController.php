<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\models\Texto;
class TextoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='privado_skote';
        return $this->render('index');
    }

    public function actionUpdateTexto($id=null){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model =Texto::findOne($id);
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                
                // $text = Texto::findOne($model->id);
                // $text->texto_principal  =   $model->p02_texto_1;
                // $text->texto_enlace     =   $model->p02_enlace_1;
                // $text->tipo             =   $model->p02_tipo_1;
                // $text->video            =   $model->p02_enlace_video_1;
               
                $model->archivo_documento = UploadedFile::getInstance($model, 'archivo_documento');
                if($model->archivo_documento){
                    if($model->archivo){
                        if (file_exists(Yii::$app->basePath . '/web/archivo_texto/'.$model->archivo)) {
                            unlink(Yii::$app->basePath . '/web/archivo_texto/'.$model->archivo);
                        } 
                    }
                    if($model->archivo_documento->saveAs('archivo_texto/archivo_'.$model->id.'.' . $model->archivo_documento->extension)){
                        $model->archivo = 'archivo_'.$model->id.'.' . $model->archivo_documento->extension;
                    }
                }

                if($model->tipo==1){
                    $model->video = "";
                    $model->enlace = "";
                }else if($model->tipo==2){
                    if($model->archivo){
                        if (file_exists(Yii::$app->basePath . '/web/archivo_texto/'.$model->archivo)) {
                            unlink(Yii::$app->basePath . '/web/archivo_texto/'.$model->archivo);
                        } 
                    }
                    $model->archivo = "";
                    $model->enlace = "";
                }else if($model->tipo==3){
                    $model->archivo = "";
                    $model->video = "";
                }

                if($model->save()){
                    return ['success'=>true,'archivo'=>$model->archivo];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('_form_video', [
                    'model' => $model,
                ]);
            }
        }
    }


    public function actionUpdatePregunta02(){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model =new Texto;
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                
                $text = Texto::findOne(1);
                $text->texto_principal  =   $model->p02_texto_1;
                $text->enlace           =   $model->p02_enlace_1;
                $text->tipo             =   $model->p02_tipo_1;
                $text->video            =   $model->p02_enlace_video_1;
                
                
                
                $model->p02_carga_archivo_1 = UploadedFile::getInstance($model, 'p02_carga_archivo_1');
                if($model->p02_carga_archivo_1){
                    if($text->archivo){
                        if (file_exists(Yii::$app->basePath . '/web/archivo_texto/'.$text->archivo)) {
                            unlink(Yii::$app->basePath . '/web/archivo_texto/'.$text->archivo);
                        } 
                    }
                    if($model->p02_carga_archivo_1->saveAs('archivo_texto/p02_archivo_1.' . $model->p02_carga_archivo_1->extension)){
                        $text->archivo = 'p02_archivo_1.' . $model->p02_carga_archivo_1->extension;
                    }
                }

                if($model->tipo==1){
                    $text->video = "";
                }else if($model->tipo==2){
                    if($text->archivo){
                        if (file_exists(Yii::$app->basePath . '/web/archivo_texto/'.$text->archivo)) {
                            unlink(Yii::$app->basePath . '/web/archivo_texto/'.$text->archivo);
                        } 
                    }
                    $text->archivo = "";
                }

                if($text->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('_form_video', [
                    'model' => $model,
                ]);
            }
        }
    }


    public function actionUpdatePregunta05(){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model =new Texto;
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                
                $text = Texto::findOne(2);
                $text->texto_principal  =   $model->p05_texto_1;
                $text->enlace           =   $model->p05_enlace_1;
                $text->tipo             =   $model->p05_tipo_1;
                $text->video            =   $model->p05_enlace_video_1;
                
                $model->p05_carga_archivo_1 = UploadedFile::getInstance($model, 'p05_carga_archivo_1');
                if($model->p05_carga_archivo_1){
                    if($text->archivo){
                        if (file_exists(Yii::$app->basePath . '/web/archivo_texto/'.$text->archivo)) {
                            unlink(Yii::$app->basePath . '/web/archivo_texto/'.$text->archivo);
                        } 
                    }
                    if($model->p05_carga_archivo_1->saveAs('archivo_texto/p05_archivo_1.' . $model->p05_carga_archivo_1->extension)){
                        $text->archivo = 'p05_archivo_1.' . $model->p05_carga_archivo_1->extension;
                    }
                }

                if($model->p05_tipo_1==1){
                    $text->video = "";
                }else if($model->p05_tipo_1==2){
                    if($text->archivo){
                        if (file_exists(Yii::$app->basePath . '/web/archivo_texto/'.$text->archivo)) {
                            unlink(Yii::$app->basePath . '/web/archivo_texto/'.$text->archivo);
                        } 
                    }
                    //var_dump("asdas");die;
                    $text->archivo = "";
                }



                $text2 = Texto::findOne(3);
                $text2->texto_principal  =   $model->p05_texto_2;
                $text2->enlace           =   $model->p05_enlace_2;
                $text2->tipo             =   $model->p05_tipo_2;
                $text2->video            =   $model->p05_enlace_video_2;
                
                $model->p05_carga_archivo_2 = UploadedFile::getInstance($model, 'p05_carga_archivo_2');
                if($model->p05_carga_archivo_2){
                    if($text2->archivo){
                        if (file_exists(Yii::$app->basePath . '/web/archivo_texto/'.$text2->archivo)) {
                            unlink(Yii::$app->basePath . '/web/archivo_texto/'.$text2->archivo);
                        } 
                    }
                    if($model->p05_carga_archivo_2->saveAs('archivo_texto/p05_archivo_2.' . $model->p05_carga_archivo_2->extension)){
                        $text2->archivo = 'p05_archivo_2.' . $model->p05_carga_archivo_2->extension;
                    }
                }

                if($model->p05_tipo_2==1){
                    $text2->video = "";
                }else if($model->p05_tipo_2==2){
                    if($text2->archivo){
                        if (file_exists(Yii::$app->basePath . '/web/archivo_texto/'.$text2->archivo)) {
                            unlink(Yii::$app->basePath . '/web/archivo_texto/'.$text2->archivo);
                        } 
                    }
                    $text2->archivo = "";
                }

                if($text->save() && $text2->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('_form_video', [
                    'model' => $model,
                ]);
            }
        }
    }


    public function actionGetTexto(){
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
       
        if($_POST){
            $id = $_POST['id'];

            $texto = (new \yii\db\Query())->select('*')->from('texto')->where('id=:id',[':id'=>$id]);
            $texto = $texto->one();

            return ['success'=>true,'texto'=>$texto];
        }
    }



    public function actionGetExperienciaGradosTextos(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
                $lista_grados_textos = [];

                $grados = (new \yii\db\Query())
                    ->select('grado.id,grado.nombre_corto,grado.grado')
                    ->from('texto')
                    ->innerJoin('grado','grado.id=texto.grado_id')
                    ->innerJoin('experiencia','experiencia.id=texto.experiencia_id');
                $grados = $grados->andWhere(['=', "grado.estado",1]);
                if(isset($_POST['experiencia_id']) && $_POST['experiencia_id']!=''){
                    $grados = $grados->andWhere(['=', "texto.experiencia_id",$_POST['experiencia_id']]);
                }

                $grados = $grados->groupBy('grado.id,grado.nombre_corto,grado.grado')->all();


                foreach($grados as $grado){
                    $texto = (new \yii\db\Query())
                            ->select('texto.*')
                            ->from('texto')
                            ->innerJoin('grado','grado.id=texto.grado_id')
                            ->innerJoin('experiencia','experiencia.id=texto.experiencia_id');

                    if(isset($_POST['experiencia_id']) && $_POST['experiencia_id']!=''){
                        $texto = $texto->andWhere(['=', "texto.experiencia_id",$_POST['experiencia_id']]);
                    }
                    //if(isset($_POST['grado_id']) && $_POST['grado_id']!=''){
                        $texto = $texto->andWhere(['=', "grado.id",$grado['id']]);
                    //}
                    $texto = $texto->all();
                    
                    array_push($lista_grados_textos,['id'=>$grado['id'],'correlativo'=>$grado['grado'],'nombre_corto'=>$grado['nombre_corto'],'textos'=>$texto]);
                }

            return ['success'=>true,'grados'=>$lista_grados_textos];
        }
    }


    public function actionEliminarArchivo(){
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $texto_id = $_POST['texto_id'];
            $model = Texto::findOne($texto_id);
            $model->archivo='';
            if($model->save()){
                return ['success'=>true];
            }else{
                return ['success'=>false];
            }
        }
    }

}
