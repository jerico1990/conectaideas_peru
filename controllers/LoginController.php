<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class LoginController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if ($action->id == 'error'){
            $this->layout = 'login';
        }
        return parent::beforeAction($action);
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout='login';
        $model = new LoginForm();

        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['panel/index']);
        }

        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            return $this->redirect(['panel/index']);
        }
        return $this->render('index', [
            'model' => $model,
        ]);
    
    }

    public function actionGetValidarUsuarioClave(){
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
       
        if($_POST){
            $usuario = $_POST['usuario'];
            $clave = $_POST['clave'];
            $banderaUsuario = 0 ;
            $banderaClave = 0 ;
            $msg = 0 ;

            $usuarioValidar = (new \yii\db\Query())->select('*')->from('usuario')->where('estado=1 and usuario=:usuario',[':usuario'=>$usuario]);
            $claveValidar = $usuarioValidar->one();
            $usuarioValidar = $usuarioValidar->count();

            if ($usuarioValidar>0 && Yii::$app->getSecurity()->validatePassword($clave, $claveValidar['clave'])) {
                $banderaClave = 1 ;
            }

            if($usuarioValidar>0){
                $banderaUsuario = 1 ;
            }

            if($banderaClave==1 && $banderaUsuario==1){
                $msg = 1 ;
            }else if($banderaClave==0 && $banderaUsuario==1){
                $msg = 2 ;
            }else if($banderaClave==1 && $banderaUsuario==0){
                $msg = 3 ;
            }else if($banderaClave==0 && $banderaUsuario==0){
                $msg = 0 ;
            }

            return ['success'=>true,'msg'=>$msg];
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

}
