<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
// use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
// use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpWord\PhpWord;
use app\models\ActividadCabecera;
use app\models\ActividadDetalle;
use app\models\Grado;
//require __DIR__ . '/../web/pdf-master/src/Pdf.php';
// use Spipu\Html2Pdf\Html2Pdf;
// use Spipu\Html2Pdf\Exception\Html2PdfException;
// use Spipu\Html2Pdf\Exception\ExceptionFormatter;
// use rudissaar\fpdf\FPDF;
// use kartik\mpdf\Pdf;
use Mpdf\Mpdf;
// use miloschuman\highcharts\Highcharts;

class ReporteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($docente_seccion_id=null,$grado=null,$seccion=null){
        if($grado==null && Yii::$app->user->identity->perfil!=99){
            echo "no tiene permitido el ingreso";
            return false;
        }
        if(Yii::$app->user->identity->perfil==99){
            $this->layout='privado_skote';
            return $this->render('index');
        }else {
            $this->layout='privado';
            $grado = Grado::find()->where('grado=:grado',[':grado'=>$grado])->one();
            return $this->render('index_docente',['docente_seccion_id'=>$docente_seccion_id,'grado'=>$grado,'seccion'=>$seccion]);
        }
    }

    public function actionSemanal(){
        if(Yii::$app->user->identity->perfil!=99){
            echo "no tiene permitido el ingreso";
            return false;
        }

        $this->layout='privado_skote';
        return $this->render('semanal');
    }

    public function actionProcesarSemanal(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $reporte01 = Yii::$app->db->createCommand('
                insert into reporte_01 (docente_seccion_id,actividad_detalle_id,conectados,no_conectados)
                select 
                    docente_seccion.id docente_seccion_id,
                    actividad_detalle.id actividad_detalle_id,
                    rb01.conectados,
                    rb01.no_conectados
                from reporte_base_01 rb01
                inner JOIN docente on docente.dni=rb01.dni
                inner join docente_seccion on docente_seccion.docente_id=docente.id
                inner join seccion on seccion.id=docente_seccion.seccion_id and seccion.descripcion=rb01.seccion_curso
                inner join grado on grado.id=seccion.grado_id and grado.grado=rb01.grado_curso
                inner join actividad_detalle on actividad_detalle.actividad_chile_id=rb01.tarea_id and actividad_detalle.grado_id=grado.id
                where rb01.estado_registro is null;

                update reporte_base_01 rb01
                inner JOIN docente on docente.dni=rb01.dni
                inner join docente_seccion on docente_seccion.docente_id=docente.id
                inner join seccion on seccion.id=docente_seccion.seccion_id and seccion.descripcion=rb01.seccion_curso
                inner join grado on grado.id=seccion.grado_id and grado.grado=rb01.grado_curso
                inner join actividad_detalle on actividad_detalle.actividad_chile_id=rb01.tarea_id and actividad_detalle.grado_id=grado.id
                set rb01.estado_registro=1,fecha_procesamiento=NOW()
                where rb01.estado_registro is null;

                update reporte_base_01
                set estado_registro = -1,fecha_procesamiento=NOW()
                where estado_registro is null;
            ')->execute();


            $reporte02 = Yii::$app->db->createCommand('
                insert into reporte_02 (docente_seccion_id,actividad_detalle_id,estudiante_id)
                select 
                    docente_seccion.id docente_seccion_id,
                    actividad_detalle.id actividad_detalle_id,
                    estudiante.id estudiante_id
                from reporte_base_02 rb02
                inner JOIN docente on docente.dni=rb02.dni
                inner join docente_seccion on docente_seccion.docente_id=docente.id
                inner join seccion on seccion.id=docente_seccion.seccion_id and seccion.descripcion=rb02.seccion_curso
                inner join grado on grado.id=seccion.grado_id and grado.grado=rb02.grado_curso
                inner join actividad_detalle on actividad_detalle.actividad_chile_id=rb02.tarea_id and actividad_detalle.grado_id=grado.id
                inner join matricula on matricula.docente_seccion_id=docente_seccion.id
                inner join estudiante on estudiante.id=matricula.estudiante_id and rb02.username=estudiante.usuario
                where rb02.estado_registro is null;

                update reporte_base_02 rb02
                inner JOIN docente on docente.dni=rb02.dni
                inner join docente_seccion on docente_seccion.docente_id=docente.id
                inner join seccion on seccion.id=docente_seccion.seccion_id and seccion.descripcion=rb02.seccion_curso
                inner join grado on grado.id=seccion.grado_id and grado.grado=rb02.grado_curso
                inner join actividad_detalle on actividad_detalle.actividad_chile_id=rb02.tarea_id and actividad_detalle.grado_id=grado.id
                inner join matricula on matricula.docente_seccion_id=docente_seccion.id
                inner join estudiante on estudiante.id=matricula.estudiante_id and rb02.username=estudiante.usuario
                set rb02.estado_registro=1,fecha_procesamiento=NOW()
                where rb02.estado_registro is null;

                update reporte_base_02
                set estado_registro = -1,fecha_procesamiento=NOW()
                where estado_registro is null;
            ')->execute();


            $reporte03 = Yii::$app->db->createCommand('
                insert into reporte_03 (docente_seccion_id,actividad_detalle_id,competencia_id,avanzado,intermedio,inicial,ninguno)
                select 
                    docente_seccion.id docente_seccion_id,
                    actividad_detalle.id actividad_detalle_id,
                    competencia.id competencia_id,
                    rb03.avanzado,
                    rb03.intermedio,
                    rb03.inicial,
                    rb03.no_uso ninguno
                from reporte_base_03 rb03
                inner JOIN docente on docente.dni=rb03.dni
                inner join docente_seccion on docente_seccion.docente_id=docente.id
                inner join seccion on seccion.id=docente_seccion.seccion_id and seccion.descripcion=rb03.seccion_curso
                inner join grado on grado.id=seccion.grado_id and grado.grado=rb03.grado_curso
                inner join actividad_detalle on actividad_detalle.actividad_chile_id=rb03.tarea_id and actividad_detalle.grado_id=grado.id
                inner join competencia on competencia.id=rb03.competencia
                where rb03.estado_registro is null;

                update reporte_base_03 rb03
                inner JOIN docente on docente.dni=rb03.dni
                inner join docente_seccion on docente_seccion.docente_id=docente.id
                inner join seccion on seccion.id=docente_seccion.seccion_id and seccion.descripcion=rb03.seccion_curso
                inner join grado on grado.id=seccion.grado_id and grado.grado=rb03.grado_curso
                inner join actividad_detalle on actividad_detalle.actividad_chile_id=rb03.tarea_id and actividad_detalle.grado_id=grado.id
                inner join competencia on competencia.id=rb03.competencia
                set rb03.estado_registro=1,fecha_procesamiento=NOW()
                where rb03.estado_registro is null;

                update reporte_base_03
                set estado_registro = -1,fecha_procesamiento=NOW()
                where estado_registro is null;
            ')->execute();


            $reporte04 = Yii::$app->db->createCommand('
                insert into reporte_04 (docente_seccion_id,actividad_detalle_id,numero_problema,capacidad_id,numero_estudiantes)
                select 
                    docente_seccion.id docente_seccion_id,
                    actividad_detalle.id actividad_detalle_id,
                    rb04.posicion_ejercicio numero_problema,
                    capacidad.id capacidad_id,
                    rb04.incorrectos numero_estudiantes
                from reporte_base_04 rb04
                inner JOIN docente on docente.dni=rb04.dni
                inner join docente_seccion on docente_seccion.docente_id=docente.id
                inner join seccion on seccion.id=docente_seccion.seccion_id and seccion.descripcion=rb04.seccion_curso
                inner join grado on grado.id=seccion.grado_id and grado.grado=rb04.grado_curso
                inner join actividad_detalle on actividad_detalle.actividad_chile_id=rb04.tarea_id and actividad_detalle.grado_id=grado.id
                inner join capacidad on capacidad.id=rb04.cap_num
                where rb04.estado_registro is null;

                update reporte_base_04 rb04
                inner JOIN docente on docente.dni=rb04.dni
                inner join docente_seccion on docente_seccion.docente_id=docente.id
                inner join seccion on seccion.id=docente_seccion.seccion_id and seccion.descripcion=rb04.seccion_curso
                inner join grado on grado.id=seccion.grado_id and grado.grado=rb04.grado_curso
                inner join actividad_detalle on actividad_detalle.actividad_chile_id=rb04.tarea_id and actividad_detalle.grado_id=grado.id
                inner join capacidad on capacidad.id=rb04.cap_num
                set rb04.estado_registro=1,fecha_procesamiento=NOW()
                where rb04.estado_registro is null;

                update reporte_base_04
                set estado_registro = -1,fecha_procesamiento=NOW()
                where estado_registro is null;
            ')->execute();


            $reporte05 = Yii::$app->db->createCommand("
                insert into reporte_05 (docente_seccion_id,actividad_detalle_id,estudiante_id,puntaje,p01,p02,p03,p04,p05,p06,p07,p08,p09,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19,p20,p21,p22,p23,p24,p25,p26,p27,p28,p29,p30)
                select 
                    docente_seccion.id docente_seccion_id,
                    actividad_detalle.id actividad_detalle_id,
                    estudiante.id estudiante_id,
                    rb05.calificacion_final puntaje,
                    case rb05.P01 when 'C' then 1 when 'I' then -1 when 'V' then 0 end p01,
                    case rb05.P02 when 'C' then 1 when 'I' then -1 when 'V' then 0 end p02,
                    case rb05.P03 when 'C' then 1 when 'I' then -1 when 'V' then 0 end p03,
                    case rb05.P04 when 'C' then 1 when 'I' then -1 when 'V' then 0 end p04,
                    case rb05.P05 when 'C' then 1 when 'I' then -1 when 'V' then 0 end p05,
                    case rb05.P06 when 'C' then 1 when 'I' then -1 when 'V' then 0 end p06,
                    case rb05.P07 when 'C' then 1 when 'I' then -1 when 'V' then 0 end p07,
                    case rb05.P08 when 'C' then 1 when 'I' then -1 when 'V' then 0 end p08,
                    case rb05.P09 when 'C' then 1 when 'I' then -1 when 'V' then 0 end p09,
                    case rb05.P10 when 'C' then 1 when 'I' then -1 when 'V' then 0 end p10,
                    case rb05.P11 when 'C' then 1 when 'I' then -1 when 'V' then 0 end p11,
                    case rb05.P12 when 'C' then 1 when 'I' then -1 when 'V' then 0 end p12,
                    case rb05.P13 when 'C' then 1 when 'I' then -1 when 'V' then 0 end p13,
                    case rb05.P14 when 'C' then 1 when 'I' then -1 when 'V' then 0 end p14,
                    case rb05.P15 when 'C' then 1 when 'I' then -1 when 'V' then 0 end p15,
                    case rb05.P16 when 'C' then 1 when 'I' then -1 when 'V' then 0 end p16,
                    case rb05.P17 when 'C' then 1 when 'I' then -1 when 'V' then 0 end p17,
                    case rb05.P18 when 'C' then 1 when 'I' then -1 when 'V' then 0 end p18,
                    case rb05.P19 when 'C' then 1 when 'I' then -1 when 'V' then 0 end p19,
                    case rb05.P20 when 'C' then 1 when 'I' then -1 when 'V' then 0 end p20,
                    case rb05.P21 when 'C' then 1 when 'I' then -1 when 'V' then 0 end p21,
                    case rb05.P22 when 'C' then 1 when 'I' then -1 when 'V' then 0 end p22,
                    case rb05.P23 when 'C' then 1 when 'I' then -1 when 'V' then 0 end p23,
                    case rb05.P24 when 'C' then 1 when 'I' then -1 when 'V' then 0 end p24,
                    case rb05.P25 when 'C' then 1 when 'I' then -1 when 'V' then 0 end p25,
                    case rb05.P26 when 'C' then 1 when 'I' then -1 when 'V' then 0 end p26,
                    case rb05.P27 when 'C' then 1 when 'I' then -1 when 'V' then 0 end p27,
                    case rb05.P28 when 'C' then 1 when 'I' then -1 when 'V' then 0 end p28,
                    case rb05.P29 when 'C' then 1 when 'I' then -1 when 'V' then 0 end p29,
                    case rb05.P30 when 'C' then 1 when 'I' then -1 when 'V' then 0 end p30
                from reporte_base_05 rb05
                inner JOIN docente on docente.dni=rb05.dni
                inner join docente_seccion on docente_seccion.docente_id=docente.id
                inner join seccion on seccion.id=docente_seccion.seccion_id and seccion.descripcion=rb05.seccion_curso
                inner join grado on grado.id=seccion.grado_id and grado.grado=rb05.grado_curso
                inner join actividad_detalle on actividad_detalle.actividad_chile_id=rb05.tarea_id and actividad_detalle.grado_id=grado.id
                inner join matricula on matricula.docente_seccion_id=docente_seccion.id
                inner join estudiante on estudiante.id=matricula.estudiante_id and estudiante.usuario=rb05.username
                where rb05.estado_registro is null;

                update reporte_base_05 rb05
                inner JOIN docente on docente.dni=rb05.dni
                inner join docente_seccion on docente_seccion.docente_id=docente.id
                inner join seccion on seccion.id=docente_seccion.seccion_id and seccion.descripcion=rb05.seccion_curso
                inner join grado on grado.id=seccion.grado_id and grado.grado=rb05.grado_curso
                inner join actividad_detalle on actividad_detalle.actividad_chile_id=rb05.tarea_id and actividad_detalle.grado_id=grado.id
                inner join matricula on matricula.docente_seccion_id=docente_seccion.id
                inner join estudiante on estudiante.id=matricula.estudiante_id and estudiante.usuario=rb05.username
                set rb05.estado_registro=1,fecha_procesamiento=NOW()
                where rb05.estado_registro is null;

                update reporte_base_05
                set estado_registro = -1,fecha_procesamiento=NOW()
                where estado_registro is null;
            ")->execute();
            return ['success'=>true];
        }
    }

    public function actionGetListaReporteSemanalPorProcesar(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $reportes = [];

            $reporte01 = Yii::$app->db->createCommand('
            select 
                count(rb01.estado_registro) cantidad_total,
                count(if(rb01.estado_registro is null, 1, NULL))  cantidad_por_procesar
            from reporte_base_01 rb01
            ')->queryOne();

            $reporte02 = Yii::$app->db->createCommand('
            select 
                count(rb02.estado_registro) cantidad_total,
                count(if(rb02.estado_registro is null, 1, NULL))  cantidad_por_procesar
            from reporte_base_02 rb02
            ')->queryOne();

            $reporte03 = Yii::$app->db->createCommand('
            select 
                count(rb03.estado_registro) cantidad_total,
                count(if(rb03.estado_registro is null, 1, NULL))  cantidad_por_procesar
            from reporte_base_03 rb03
            ')->queryOne();

            $reporte04 = Yii::$app->db->createCommand('
            select 
                count(rb04.estado_registro) cantidad_total,
                count(if(rb04.estado_registro is null, 1, NULL))  cantidad_por_procesar
            from reporte_base_04 rb04
            ')->queryOne();

            $reporte05 = Yii::$app->db->createCommand('
            select 
                count(rb05.estado_registro) cantidad_total,
                count(if(rb05.estado_registro is null, 1, NULL))  cantidad_por_procesar
            from reporte_base_05 rb05
            ')->queryOne();

            array_push($reportes, ['reporte'=>'Pregunta 1','cantidad_total'=>$reporte01['cantidad_total'],'cantidad_por_procesar'=>$reporte01['cantidad_por_procesar']]);
            array_push($reportes, ['reporte'=>'Pregunta 2','cantidad_total'=>$reporte02['cantidad_total'],'cantidad_por_procesar'=>$reporte02['cantidad_por_procesar']]);
            array_push($reportes, ['reporte'=>'Pregunta 3','cantidad_total'=>$reporte03['cantidad_total'],'cantidad_por_procesar'=>$reporte03['cantidad_por_procesar']]);
            array_push($reportes, ['reporte'=>'Pregunta 4','cantidad_total'=>$reporte04['cantidad_total'],'cantidad_por_procesar'=>$reporte04['cantidad_por_procesar']]);
            array_push($reportes, ['reporte'=>'Pregunta 5','cantidad_total'=>$reporte05['cantidad_total'],'cantidad_por_procesar'=>$reporte05['cantidad_por_procesar']]);
            
            return ['success'=>true,'reportes'=>$reportes];
        }
    }


    public function actionGetListaReporteSemanalResultadoProcesamiento(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $reportes = [];
            $fecha = Yii::$app->db->createCommand('
            select 
                MAX(rb01.fecha_procesamiento) fecha
            from reporte_base_01 rb01;
            ')->queryOne();


            $reporte01 = Yii::$app->db->createCommand('
            select 
                count(rb01.dni) cantidad_total,
                count(if(rb01.estado_registro=-1, 1, null))  cantidad_por_procesar
            from reporte_base_01 rb01
            where rb01.fecha_procesamiento="'.$fecha['fecha'].'"
            ')->queryOne();

            $reporte02 = Yii::$app->db->createCommand('
            select 
                count(rb02.dni) cantidad_total,
                count(if(rb02.estado_registro=-1, 1, null))  cantidad_por_procesar
            from reporte_base_02 rb02
            where rb02.fecha_procesamiento="'.$fecha['fecha'].'"
            ')->queryOne();

            $reporte03 = Yii::$app->db->createCommand('
            select 
                count(rb03.dni) cantidad_total,
                count(if(rb03.estado_registro=-1, 1, null))  cantidad_por_procesar
            from reporte_base_03 rb03
            where rb03.fecha_procesamiento="'.$fecha['fecha'].'"
            ')->queryOne();

            $reporte04 = Yii::$app->db->createCommand('
            select 
                count(rb04.dni) cantidad_total,
                count(if(rb04.estado_registro=-1, 1, null))  cantidad_por_procesar
            from reporte_base_04 rb04
            where rb04.fecha_procesamiento="'.$fecha['fecha'].'"
            ')->queryOne();

            $reporte05 = Yii::$app->db->createCommand('
            select 
                count(rb05.dni) cantidad_total,
                count(if(rb05.estado_registro=-1, 1, null))  cantidad_por_procesar
            from reporte_base_05 rb05
            where rb05.fecha_procesamiento="'.$fecha['fecha'].'"
            ')->queryOne();

            array_push($reportes, ['reporte'=>'Pregunta 1','cantidad_total'=>$reporte01['cantidad_total'],'cantidad_por_procesar'=>$reporte01['cantidad_por_procesar']]);
            array_push($reportes, ['reporte'=>'Pregunta 2','cantidad_total'=>$reporte02['cantidad_total'],'cantidad_por_procesar'=>$reporte02['cantidad_por_procesar']]);
            array_push($reportes, ['reporte'=>'Pregunta 3','cantidad_total'=>$reporte03['cantidad_total'],'cantidad_por_procesar'=>$reporte03['cantidad_por_procesar']]);
            array_push($reportes, ['reporte'=>'Pregunta 4','cantidad_total'=>$reporte04['cantidad_total'],'cantidad_por_procesar'=>$reporte04['cantidad_por_procesar']]);
            array_push($reportes, ['reporte'=>'Pregunta 5','cantidad_total'=>$reporte05['cantidad_total'],'cantidad_por_procesar'=>$reporte05['cantidad_por_procesar']]);
            
            return ['success'=>true,'reportes'=>$reportes];
        }
    }

    public function actionReporteRegistrosDocentes(){
        $inputFileName = \Yii::$app->basePath.'/plantilla/Formulario_docentes_estructura.xlsx';
        $inputFileType = IOFactory::identify($inputFileName);
        $reader = IOFactory::createReader($inputFileType);

        $spread = $reader->load($inputFileName);
      

        $encuestas = (new \yii\db\Query())
            ->select(['*'])
            ->from('vw_reporte_registros_docentes')
            ->all();

        
        $i = 2;
        foreach($encuestas as $encuesta){
            $spread->getActiveSheet()
                ->setCellValue('A'.$i, $encuesta['id'])
                ->setCellValue('B'.$i, $encuesta['fecha'])
                ->setCellValue('C'.$i, $encuesta['dni'])
                ->setCellValue('D'.$i, $encuesta['apellido_paterno'])
                ->setCellValue('E'.$i, $encuesta['apellido_materno'])
                ->setCellValue('F'.$i, $encuesta['nombres'])
                ->setCellValue('G'.$i, $encuesta['dia'])
                ->setCellValue('H'.$i, $encuesta['mes'])
                ->setCellValue('I'.$i, $encuesta['anio'])
                ->setCellValue('J'.$i, $encuesta['correo_electronico'])
                ->setCellValue('K'.$i, $encuesta['celular'])
                ->setCellValue('L'.$i, $encuesta['departamento'])
                ->setCellValue('M'.$i, $encuesta['provincia'])
                ->setCellValue('N'.$i, $encuesta['distrito'])
                ->setCellValue('O'.$i, $encuesta['nombre_ie'])
                ->setCellValue('P'.$i, $encuesta['cod_mod'])
                ->setCellValue('Q'.$i, $encuesta['tipo_ie_id'])
                ->setCellValue('R'.$i, $encuesta['tipo_ie_descripcion'])
                ->setCellValue('S'.$i, $encuesta['cargo_id'])
                ->setCellValue('T'.$i, $encuesta['cargo_descripcion'])
                ->setCellValue('U'.$i, $encuesta['grado'])
                ->setCellValue('V'.$i, $encuesta['seccion']);
            $i++;
        }

        
        #$writer = new Xlsx($spread);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment;filename="Reporte01_Registros_Docentes_'.date('Ymd').'.xlsx"');
        header('Cache-Control: max-age=0');
        
        #$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spread, 'Xls');
        
        $writer = IOFactory::createWriter($spread, 'Xlsx');
        #$writer->setOffice2003Compatibility(true);

        $response = Yii::$app->getResponse();
        $headers = $response->getHeaders();
        $headers->set('Content-Type', 'application/octet-stream');
        $headers->set('Content-Disposition', 'attachment;filename="Reporte01_Registros_Docentes_'.date('Ymd').'.xlsx"');

        ob_start();
        $writer->save("php://output");
        $content = ob_get_contents();
        ob_clean();
        return $content;


        /*
        $fileType = 'Xlsx';
        $writer = IOFactory::createWriter($spread, 'Xlsx');
        
        // if ($fileType == 'Excel2007' || $fileType == 'Xlsx') {
        //     header('Content-Type: application/octet-stream');
        //     header('Content-Disposition: attachment;filename="Reporte01_Registros_Docentes_'.date('Ymd').'.xlsx"');
        //     header('Cache-Control: max-age=0');
        // } else {
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="Reporte01_Registros_Docentes_'.date('Ymd').'.xlsx"');
            header('Cache-Control: max-age=0');
        //}

        //var_dump($writer);die;

        
        $writer->save('php://output');
        $content = ob_get_contents();
        ob_clean();
        return $content;*/
    }

    public function actionReporteRegistrosDocentesSafari(){
        $inputFileName = \Yii::$app->basePath.'/plantilla/Formulario_docentes_estructura.xlsx';
        $inputFileType = IOFactory::identify($inputFileName);
        $reader = IOFactory::createReader($inputFileType);

        $spread = $reader->load($inputFileName);
      

        $encuestas = (new \yii\db\Query())
            ->select(['*'])
            ->from('vw_reporte_registros_docentes')
            ->all();

        
        $i = 2;
        foreach($encuestas as $encuesta){
            $spread->getActiveSheet()
                ->setCellValue('A'.$i, $encuesta['id'])
                ->setCellValue('B'.$i, $encuesta['fecha'])
                ->setCellValue('C'.$i, $encuesta['dni'])
                ->setCellValue('D'.$i, $encuesta['apellido_paterno'])
                ->setCellValue('E'.$i, $encuesta['apellido_materno'])
                ->setCellValue('F'.$i, $encuesta['nombres'])
                ->setCellValue('G'.$i, $encuesta['dia'])
                ->setCellValue('H'.$i, $encuesta['mes'])
                ->setCellValue('I'.$i, $encuesta['anio'])
                ->setCellValue('J'.$i, $encuesta['correo_electronico'])
                ->setCellValue('K'.$i, $encuesta['celular'])
                ->setCellValue('L'.$i, $encuesta['departamento'])
                ->setCellValue('M'.$i, $encuesta['provincia'])
                ->setCellValue('N'.$i, $encuesta['distrito'])
                ->setCellValue('O'.$i, $encuesta['nombre_ie'])
                ->setCellValue('P'.$i, $encuesta['cod_mod'])
                ->setCellValue('Q'.$i, $encuesta['tipo_ie_id'])
                ->setCellValue('R'.$i, $encuesta['tipo_ie_descripcion'])
                ->setCellValue('S'.$i, $encuesta['cargo_id'])
                ->setCellValue('T'.$i, $encuesta['cargo_descripcion'])
                ->setCellValue('U'.$i, $encuesta['grado'])
                ->setCellValue('V'.$i, $encuesta['seccion']);
            $i++;
        }

        // Redirect output to a client’s web browser (Xlsx)
        
        
        #$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spread, 'Xls');
        
        $writer = IOFactory::createWriter($spread, 'Xlsx');
        #$writer->setOffice2003Compatibility(true);

        // $response = Yii::$app->getResponse();
        // $headers = $response->getHeaders();
        // $headers->set('Content-Type', 'application/force-download');
        // $headers->set('Content-Type', 'application/vnd.ms-excel');
        // $headers->set('Content-Type', 'application/octet-stream');
        // $headers->set('Content-Disposition', 'attachment;filename="Reporte01_Registros_Docentes_'.date('Ymd').'.xlsx"');
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control:must-revalidate, post-check=0, pre-check=0");
        header("Content-Type:application/force-download");
        header("Content-Type:application/vnd.ms-excel");
        header("Content-Type:application/octet-stream");
        header("Content-Type:application/download");
        header('Content-Disposition: attachment;filename=Reporte01_Registros_Docentes_'.date('Ymd').'.xlsx');
        header("Content-Transfer-Encoding:binary");
        $writer->save('php://output');
        exit;


    }

    public function actionReporteRegistrosEstudiantes(){
        $inputFileName = \Yii::$app->basePath.'/plantilla/Estudiantes_total.xlsx';
        $inputFileType = IOFactory::identify($inputFileName);
        $reader = IOFactory::createReader($inputFileType);

        $spread = $reader->load($inputFileName);
      

        $agregados = (new \yii\db\Query())
            ->select(['*'])
            ->from('vw_reporte_estudiantes_total')
            ->all();

        
        $i = 2;
        foreach($agregados as $agregado){
            $spread->getSheet(0)
                ->setCellValue('A'.$i, $agregado['fecha_inscripcion'])
                ->setCellValue('B'.$i, $agregado['username'])
                ->setCellValue('C'.$i, $agregado['email'])
                ->setCellValue('D'.$i, $agregado['rut'])
                ->setCellValue('E'.$i, $agregado['rbd'])
                ->setCellValue('F'.$i, $agregado['pais_insti'])
                ->setCellValue('G'.$i, $agregado['inst_id'])
                ->setCellValue('H'.$i, $agregado['institucion'])
                ->setCellValue('I'.$i, $agregado['nombres'])
                ->setCellValue('J'.$i, $agregado['apellido_paterno'])
                ->setCellValue('K'.$i, $agregado['apellido_materno'])
                ->setCellValue('L'.$i, $agregado['genero'])
                ->setCellValue('M'.$i, $agregado['pais_usuario'])
                ->setCellValue('N'.$i, $agregado['idioma'])
                ->setCellValue('O'.$i, $agregado['clave'])
                ->setCellValue('P'.$i, $agregado['dia_nac'])
                ->setCellValue('Q'.$i, $agregado['mes_nac'])
                ->setCellValue('R'.$i, $agregado['anio_nac'])
                ->setCellValue('S'.$i, $agregado['celular'])
                ->setCellValue('T'.$i, $agregado['cargo'])
                ->setCellValue('U'.$i, $agregado['rol'])
                ->setCellValue('V'.$i, $agregado['nivel_crs'])
                ->setCellValue('W'.$i, $agregado['nombre_crs'])
                ->setCellValue('X'.$i, $agregado['estado_registro'])
                ->setCellValue('Y'.$i, $agregado['dni']);
            $i++;
        }


        // $eliminados = (new \yii\db\Query())
        //     ->select(['*'])
        //     ->from('vw_reporte_estudiantes_eliminados')
        //     ->all();

        
        // $i = 2;
        // foreach($eliminados as $eliminado){
        //     $spread->getSheet(1)
        //         ->setCellValue('A'.$i, $eliminado['fecha_eliminacion'])
        //         ->setCellValue('B'.$i, $eliminado['username'])
        //         ->setCellValue('C'.$i, $eliminado['email'])
        //         ->setCellValue('D'.$i, $eliminado['rut'])
        //         ->setCellValue('E'.$i, $eliminado['rbd'])
        //         ->setCellValue('F'.$i, $eliminado['pais_insti'])
        //         ->setCellValue('G'.$i, $eliminado['inst_id'])
        //         ->setCellValue('H'.$i, $eliminado['nombres'])
        //         ->setCellValue('I'.$i, $eliminado['apellido_paterno'])
        //         ->setCellValue('J'.$i, $eliminado['apellido_materno']);
        //     $i++;
        // }


        $writer = IOFactory::createWriter($spread, 'Xlsx');
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control:must-revalidate, post-check=0, pre-check=0");
        header("Content-Type:application/force-download");
        header("Content-Type:application/vnd.ms-excel");
        header("Content-Type:application/octet-stream");
        header("Content-Type:application/download");
        header('Content-Disposition: attachment;filename=Lista_Estudiantes_'.date('Ymd').'.xlsx');
        header("Content-Transfer-Encoding:binary");
        $writer->save('php://output');
        exit;


    }

    public function actionReporteRegistrosEstudiantesAgregados(){
        $inputFileName = \Yii::$app->basePath.'/plantilla/Estudiantes_agregados.xlsx';
        $inputFileType = IOFactory::identify($inputFileName);
        $reader = IOFactory::createReader($inputFileType);

        $spread = $reader->load($inputFileName);
      

        $agregados = (new \yii\db\Query())
            ->select(['*'])
            ->from('vw_reporte_estudiantes_agregados')
            ->all();

        
        $i = 2;
        foreach($agregados as $agregado){
            $spread->getSheet(0)
                ->setCellValue('A'.$i, $agregado['fecha_inscripcion'])
                ->setCellValue('B'.$i, $agregado['username'])
                ->setCellValue('C'.$i, $agregado['email'])
                ->setCellValue('D'.$i, $agregado['rut'])
                ->setCellValue('E'.$i, $agregado['rbd'])
                ->setCellValue('F'.$i, $agregado['pais_insti'])
                ->setCellValue('G'.$i, $agregado['inst_id'])
                ->setCellValue('H'.$i, $agregado['institucion'])
                ->setCellValue('I'.$i, $agregado['nombres'])
                ->setCellValue('J'.$i, $agregado['apellido_paterno'])
                ->setCellValue('K'.$i, $agregado['apellido_materno'])
                ->setCellValue('L'.$i, $agregado['genero'])
                ->setCellValue('M'.$i, $agregado['pais_usuario'])
                ->setCellValue('N'.$i, $agregado['idioma'])
                ->setCellValue('O'.$i, $agregado['clave'])
                ->setCellValue('P'.$i, $agregado['dia_nac'])
                ->setCellValue('Q'.$i, $agregado['mes_nac'])
                ->setCellValue('R'.$i, $agregado['anio_nac'])
                ->setCellValue('S'.$i, $agregado['celular'])
                ->setCellValue('T'.$i, $agregado['cargo'])
                ->setCellValue('U'.$i, $agregado['rol'])
                ->setCellValue('V'.$i, $agregado['nivel_crs'])
                ->setCellValue('W'.$i, $agregado['nombre_crs']);
            $i++;
        }



        $writer = IOFactory::createWriter($spread, 'Xlsx');
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control:must-revalidate, post-check=0, pre-check=0");
        header("Content-Type:application/force-download");
        header("Content-Type:application/vnd.ms-excel");
        header("Content-Type:application/octet-stream");
        header("Content-Type:application/download");
        header('Content-Disposition: attachment;filename=Estudiantes_agregados_'.date('Ymd').'.xlsx');
        header("Content-Transfer-Encoding:binary");
        $writer->save('php://output');
        exit;


    }

    public function actionReporteRegistrosEstudiantesEliminados(){
        $inputFileName = \Yii::$app->basePath.'/plantilla/Estudiantes_eliminados.xlsx';
        $inputFileType = IOFactory::identify($inputFileName);
        $reader = IOFactory::createReader($inputFileType);

        $spread = $reader->load($inputFileName);
      
        $eliminados = (new \yii\db\Query())
            ->select(['*'])
            ->from('vw_reporte_estudiantes_eliminados')
            ->all();

        
        $i = 2;
        foreach($eliminados as $eliminado){
            $spread->getSheet(0)
                ->setCellValue('A'.$i, $eliminado['fecha_eliminacion'])
                ->setCellValue('B'.$i, $eliminado['username'])
                ->setCellValue('C'.$i, $eliminado['email'])
                ->setCellValue('D'.$i, $eliminado['rut'])
                ->setCellValue('E'.$i, $eliminado['rbd'])
                ->setCellValue('F'.$i, $eliminado['pais_insti'])
                ->setCellValue('G'.$i, $eliminado['inst_id'])
                ->setCellValue('H'.$i, $eliminado['nombres'])
                ->setCellValue('I'.$i, $eliminado['apellido_paterno'])
                ->setCellValue('J'.$i, $eliminado['apellido_materno']);
            $i++;
        }


        $writer = IOFactory::createWriter($spread, 'Xlsx');
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control:must-revalidate, post-check=0, pre-check=0");
        header("Content-Type:application/force-download");
        header("Content-Type:application/vnd.ms-excel");
        header("Content-Type:application/octet-stream");
        header("Content-Type:application/download");
        header('Content-Disposition: attachment;filename=Estudiantes_eliminados_'.date('Ymd').'.xlsx');
        header("Content-Transfer-Encoding:binary");
        $writer->save('php://output');
        exit;


    }

    public function actionReporteListaEstudiantes($docente_seccion_id){
        $inputFileName = \Yii::$app->basePath.'/plantilla/Lista_estudiantes.xlsx';
        $inputFileType = IOFactory::identify($inputFileName);
        $reader = IOFactory::createReader($inputFileType);

        $spread = $reader->load($inputFileName);
      

        $estudiantes = (new \yii\db\Query())
            ->select(['*'])
            ->from('vw_lista_estudiantes_docente')
            ->where('usuario_id=:usuario_id and docente_seccion_id=:docente_seccion_id',[':usuario_id'=>Yii::$app->user->identity->id,':docente_seccion_id'=>$docente_seccion_id])
            ->all();

        
        $i = 2;
        foreach($estudiantes as $estudiante){
            $spread->getSheet(0)
                ->setCellValue('A'.$i, $estudiante['grado'])
                ->setCellValue('B'.$i, $estudiante['seccion'])
                ->setCellValue('C'.$i, $estudiante['nombres'])
                ->setCellValue('D'.$i, $estudiante['apellido_paterno_letra'])
                ->setCellValue('E'.$i, $estudiante['usuario'])
                ->setCellValue('F'.$i, $estudiante['estado_registro']);
            $i++;
        }


        $writer = IOFactory::createWriter($spread, 'Xlsx');
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control:must-revalidate, post-check=0, pre-check=0");
        header("Content-Type:application/force-download");
        header("Content-Type:application/vnd.ms-excel");
        header("Content-Type:application/octet-stream");
        header("Content-Type:application/download");
        header('Content-Disposition: attachment;filename=Lista_Estudiantes_'.date('Ymd').'.xlsx');
        header("Content-Transfer-Encoding:binary");
        $writer->save('php://output');
        exit;


    }

    public function actionReporteResultadosSincronizacion(){
        $inputFileName = \Yii::$app->basePath.'/plantilla/Resultado_sincronizacion.xlsx';
        $inputFileType = IOFactory::identify($inputFileName);
        $reader = IOFactory::createReader($inputFileType);

        $spread = $reader->load($inputFileName);
        $fecha = Yii::$app->db->createCommand('
            select 
                MAX(rb01.fecha_procesamiento) fecha
            from reporte_base_01 rb01;
            ')->queryOne();

        $reportes01 = (new \yii\db\Query())
            ->select('*')
            ->from('reporte_base_01')
            ->where('estado_registro=-1 and fecha_procesamiento=:fecha_procesamiento',[':fecha_procesamiento'=>$fecha['fecha']])
            ->all();

        
        $i = 2;
        foreach($reportes01 as $rec){
            $spread->getSheet(0)
                ->setCellValue('A'.$i, $rec['row_names'])
                ->setCellValue('B'.$i, $rec['cod_mod'])
                ->setCellValue('C'.$i, $rec['dni'])
                ->setCellValue('D'.$i, $rec['grado_curso'])
                ->setCellValue('E'.$i, $rec['seccion_curso'])
                ->setCellValue('F'.$i, $rec['semana'])
                ->setCellValue('G'.$i, $rec['tarea_id'])
                ->setCellValue('H'.$i, $rec['conectados'])
                ->setCellValue('I'.$i, $rec['no_conectados'])
                ->setCellValue('J'.$i, $rec['estado_registro'])
                ->setCellValue('K'.$i, $rec['fecha_procesamiento']);
            $i++;
        }


        $reportes02 = (new \yii\db\Query())
            ->select('*')
            ->from('reporte_base_02')
            ->where('estado_registro=-1 and fecha_procesamiento=:fecha_procesamiento',[':fecha_procesamiento'=>$fecha['fecha']])
            ->all();

        
        $i = 2;
        foreach($reportes02 as $rec){
            $spread->getSheet(1)
                ->setCellValue('A'.$i, $rec['row_names'])
                ->setCellValue('B'.$i, $rec['cod_mod'])
                ->setCellValue('C'.$i, $rec['dni'])
                ->setCellValue('D'.$i, $rec['grado_curso'])
                ->setCellValue('E'.$i, $rec['seccion_curso'])
                ->setCellValue('F'.$i, $rec['tarea_id'])
                ->setCellValue('G'.$i, $rec['username'])
                ->setCellValue('H'.$i, $rec['estado_registro'])
                ->setCellValue('I'.$i, $rec['fecha_procesamiento']);
            $i++;
        }


        $reportes03 = (new \yii\db\Query())
            ->select('*')
            ->from('reporte_base_03')
            ->where('estado_registro=-1 and fecha_procesamiento=:fecha_procesamiento',[':fecha_procesamiento'=>$fecha['fecha']])
            ->all();

        
        $i = 2;
        foreach($reportes03 as $rec){
            $spread->getSheet(2)
                ->setCellValue('A'.$i, $rec['row_names'])
                ->setCellValue('B'.$i, $rec['cod_mod'])
                ->setCellValue('C'.$i, $rec['dni'])
                ->setCellValue('D'.$i, $rec['grado_curso'])
                ->setCellValue('E'.$i, $rec['seccion_curso'])
                ->setCellValue('F'.$i, $rec['tarea_id'])
                ->setCellValue('G'.$i, $rec['competencia'])
                ->setCellValue('H'.$i, $rec['inicial'])
                ->setCellValue('I'.$i, $rec['intermedio'])
                ->setCellValue('J'.$i, $rec['avanzado'])
                ->setCellValue('K'.$i, $rec['no_uso'])
                ->setCellValue('L'.$i, $rec['estado_registro'])
                ->setCellValue('M'.$i, $rec['fecha_procesamiento']);
            $i++;
        }


        $reportes04 = (new \yii\db\Query())
            ->select('*')
            ->from('reporte_base_04')
            ->where('estado_registro=-1 and fecha_procesamiento=:fecha_procesamiento',[':fecha_procesamiento'=>$fecha['fecha']])
            ->all();

        
        $i = 2;
        foreach($reportes04 as $rec){
            $spread->getSheet(3)
                ->setCellValue('A'.$i, $rec['row_names'])
                ->setCellValue('B'.$i, $rec['cod_mod'])
                ->setCellValue('C'.$i, $rec['dni'])
                ->setCellValue('D'.$i, $rec['grado_curso'])
                ->setCellValue('E'.$i, $rec['seccion_curso'])
                ->setCellValue('F'.$i, $rec['tarea_id'])
                ->setCellValue('G'.$i, $rec['cap_num'])
                ->setCellValue('H'.$i, $rec['posicion_ejercicio'])
                ->setCellValue('I'.$i, $rec['incorrectos'])
                ->setCellValue('J'.$i, $rec['estado_registro'])
                ->setCellValue('K'.$i, $rec['fecha_procesamiento']);
            $i++;
        }


        $reportes05 = (new \yii\db\Query())
            ->select('*')
            ->from('reporte_base_05')
            ->where('estado_registro=-1 and fecha_procesamiento=:fecha_procesamiento',[':fecha_procesamiento'=>$fecha['fecha']])
            ->all();

        
        $i = 2;
        foreach($reportes05 as $rec){
            $spread->getSheet(4)
                ->setCellValue('A'.$i, $rec['row_names'])
                ->setCellValue('B'.$i, $rec['cod_mod'])
                ->setCellValue('C'.$i, $rec['dni'])
                ->setCellValue('D'.$i, $rec['grado_curso'])
                ->setCellValue('E'.$i, $rec['seccion_curso'])
                ->setCellValue('F'.$i, $rec['tarea_id'])
                ->setCellValue('G'.$i, $rec['username'])
                ->setCellValue('H'.$i, $rec['calificacion_final'])
                ->setCellValue('I'.$i, $rec['P01'])
                ->setCellValue('J'.$i, $rec['P02'])
                ->setCellValue('K'.$i, $rec['P03'])
                ->setCellValue('L'.$i, $rec['P04'])
                ->setCellValue('M'.$i, $rec['P05'])
                ->setCellValue('N'.$i, $rec['P06'])
                ->setCellValue('O'.$i, $rec['P07'])
                ->setCellValue('P'.$i, $rec['P08'])
                ->setCellValue('Q'.$i, $rec['P09'])
                ->setCellValue('R'.$i, $rec['P10'])
                ->setCellValue('S'.$i, $rec['P11'])
                ->setCellValue('T'.$i, $rec['P12'])
                ->setCellValue('U'.$i, $rec['P13'])
                ->setCellValue('V'.$i, $rec['P14'])
                ->setCellValue('W'.$i, $rec['P15'])
                ->setCellValue('X'.$i, $rec['P16'])
                ->setCellValue('Y'.$i, $rec['P17'])
                ->setCellValue('Z'.$i, $rec['P18'])
                ->setCellValue('AA'.$i, $rec['P19'])
                ->setCellValue('AB'.$i, $rec['P20'])
                ->setCellValue('AC'.$i, $rec['P21'])
                ->setCellValue('AD'.$i, $rec['P22'])
                ->setCellValue('AE'.$i, $rec['P23'])
                ->setCellValue('AF'.$i, $rec['P24'])
                ->setCellValue('AG'.$i, $rec['P25'])
                ->setCellValue('AH'.$i, $rec['P26'])
                ->setCellValue('AI'.$i, $rec['P27'])
                ->setCellValue('AJ'.$i, $rec['P28'])
                ->setCellValue('AK'.$i, $rec['P29'])
                ->setCellValue('AL'.$i, $rec['P30'])
                ->setCellValue('AM'.$i, $rec['estado_registro'])
                ->setCellValue('AN'.$i, $rec['fecha_procesamiento']);
            $i++;
        }



        $writer = IOFactory::createWriter($spread, 'Xlsx');
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control:must-revalidate, post-check=0, pre-check=0");
        header("Content-Type:application/force-download");
        header("Content-Type:application/vnd.ms-excel");
        header("Content-Type:application/octet-stream");
        header("Content-Type:application/download");
        header('Content-Disposition: attachment;filename=Resultados_Sincronizacion'.date('Ymd').'.xlsx');
        header("Content-Transfer-Encoding:binary");
        $writer->save('php://output');
        exit;


    }

    public function actionReporteListaEstudiantesPdf($docente_seccion_id=null) {
        
        $informacionGeneral = (new \yii\db\Query())
                    ->select('docente.*,institucion_educativa.nombre_ie,grado.nombre_corto,seccion.descripcion seccion')
                    ->from('usuario')
                    ->innerJoin('docente','docente.dni=usuario.usuario')
                    ->innerJoin('institucion_educativa','institucion_educativa.id=docente.institucion_educativa_id')
                    ->innerJoin('docente_seccion','docente_seccion.docente_id=docente.id')
                    ->innerJoin('seccion','seccion.id=docente_seccion.seccion_id')
                    ->innerJoin('grado','grado.id=seccion.grado_id')
                    ->where('docente_seccion.id=:id',[':id'=>$docente_seccion_id])
                    ->one();

        $estudiantes = (new \yii\db\Query())
                    ->select('estudiante.nombres,estudiante.apellido_paterno_letra,matricula.estado_registro,estudiante.usuario,estudiante.clave,matricula.id matricula_id')
                    ->from('estudiante')
                    ->innerJoin('matricula','estudiante.id = matricula.estudiante_id')
                    ->innerJoin('docente_seccion','docente_seccion.id=matricula.docente_seccion_id')
                    ->where('matricula.estado_registro in (1,2) and docente_seccion.docente_id=:docente_id and docente_seccion.id=:id',[':id'=>$docente_seccion_id,':docente_id'=>Yii::$app->user->identity->docenteid]);
        $estudiantes_contar = $estudiantes->count();

        $estudiantes = $estudiantes->orderBy('matricula.estado_registro desc')->all();

        // $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
        // $fontDirs = $defaultConfig['fontDir'];

        // $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
        // $fontData = $defaultFontConfig['fontdata'];

        // $mpdf = new Mpdf\Mpdf([
        //     'fontDir' => array_merge($fontDirs, [
        //         \Yii::$app->basePath . '/web/Montserrat',
        //     ]),
        //     'fontdata' => $fontData + [
        //         'Montserrat' => [
        //             'R' => "Montserrat-Regular.ttf",
		// 			'B' => "Montserrat-Bold.ttf",
		// 			'I' => "Montserrat-Italic.ttf",
        //             'BI' => "Montserrat-BoldItalic.ttf",
        //         ]
        //     ],
        //     'default_font_size' => '12',
        //     'default_font' => 'Montserrat,sans-serif'
        //     ]);

        $mpdf = new Mpdf([
            'default_font_size' => '12',
            'default_font' => 'Montserrat,sans-serif']);


        $content = $this->renderPartial('_reporteListaEstudiantesPdf');
        $mpdf->WriteFixedPosHTML(date('d-m-Y'), 95, 46.2, 50, 90, 'auto');
        $mpdf->WriteFixedPosHTML($informacionGeneral['nombre_ie'], 57.2, 63.7, 90, 90, 'auto');
        $mpdf->WriteFixedPosHTML($informacionGeneral['seccion'], 41, 76.7, 50, 90, 'auto');
        $mpdf->WriteFixedPosHTML($estudiantes_contar, 164, 76.8, 50, 90, 'auto');
        $mpdf->WriteFixedPosHTML($informacionGeneral['nombres'] . ' ' . $informacionGeneral['apellido_paterno'] , 50, 89.8, 50, 90, 'auto');
        $mpdf->WriteHTML($content);
        $contador = 0;
        $cantidad_estudiantes = 1;
        foreach($estudiantes as $estudiante){

            $estado_registro = '';
            $img_estado_registro = '';
            if($estudiante['estado_registro']==1){
                $estado_registro = 'Habilitado';
                $img_estado_registro = 'correcto.png';
            }else if($estudiante['estado_registro']==2){
                $estado_registro = 'En espera';
                $img_estado_registro = 'en_espera.png';
            }
            $mpdf->WriteFixedPosHTML('<div style="width:140px;">'.$estudiante['nombres'] .' '. $estudiante['apellido_paterno_letra'] .'</div>', 21, 125 + $contador, 50, 90, 'auto');
            $mpdf->WriteFixedPosHTML('<div style="width:115px;">'.$estudiante['usuario'] .'</div>', 63, 125 + $contador, 50, 90, 'auto');
            $mpdf->WriteFixedPosHTML('<div style="width:130px;">'.$estudiante['clave'] .'</div>', 102, 125 + $contador, 50, 90, 'auto');
            $mpdf->WriteFixedPosHTML('<div style="width:140px;"> <img height="10px" src="'.\Yii::$app->basePath.'/web/img/'.$img_estado_registro.'"> '.$estado_registro .'</div>', 148, 125 + $contador, 50, 90, 'auto');
         
            $contador = $contador + 8;
            if($cantidad_estudiantes == 20){
                
                $contador = 0 ;
                $mpdf->AddPage();
                $mpdf->WriteFixedPosHTML(date('d-m-Y'), 95, 46.5, 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML($informacionGeneral['nombre_ie'], 57.2, 64.1, 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML($informacionGeneral['seccion'], 41, 76.9, 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML($estudiantes_contar, 164, 77, 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML($informacionGeneral['nombres'] . ' ' . $informacionGeneral['apellido_paterno'] , 50, 89.8, 50, 90, 'auto');

                

                $mpdf->WriteHTML($content);
            }
            $cantidad_estudiantes ++ ;
        }
        $mpdf->Output();
    }

    public function actionReporteSemanalPdf($docente_seccion_id=null,$grado_id=null,$actividad_cabecera_id=null) {



        $actividad = (new \yii\db\Query())->select('*')->from('actividad_cabecera')->where('id=:id',[':id'=>$actividad_cabecera_id])->one();
        $informacionGeneral = (new \yii\db\Query())
                    ->select('docente.*,institucion_educativa.nombre_ie,grado.nombre_corto,seccion.descripcion seccion')
                    ->from('usuario')
                    ->innerJoin('docente','docente.dni=usuario.usuario')
                    ->innerJoin('institucion_educativa','institucion_educativa.id=docente.institucion_educativa_id')
                    ->innerJoin('docente_seccion','docente_seccion.docente_id=docente.id')
                    ->innerJoin('seccion','seccion.id=docente_seccion.seccion_id')
                    ->innerJoin('grado','grado.id=seccion.grado_id')
                    ->where('docente_seccion.id=:id',[':id'=>$docente_seccion_id])
                    ->one();

        $estudiantes = (new \yii\db\Query())
                    ->select('estudiante.nombres,estudiante.apellido_paterno_letra,matricula.estado_registro,estudiante.usuario,estudiante.clave,matricula.id matricula_id')
                    ->from('estudiante')
                    ->innerJoin('matricula','estudiante.id = matricula.estudiante_id')
                    ->innerJoin('docente_seccion','docente_seccion.id=matricula.docente_seccion_id')
                    ->where('matricula.estado_registro in (1,2) and docente_seccion.docente_id=:docente_id and docente_seccion.id=:id',[':id'=>$docente_seccion_id,':docente_id'=>Yii::$app->user->identity->docenteid]);
        $estudiantes_contar = $estudiantes->count();

        $estudiantes = $estudiantes->orderBy('matricula.estado_registro desc')->all();

        

        

        $mpdf = new Mpdf(['default_font_size' => '12',
        'default_font' => 'Montserrat,sans-serif']);
        $mpdf->showImageErrors = true;
        //$mascara_01 = $this->renderPartial('_mascara01');
        //$mpdf->WriteHTML($mascara_01);
        $mpdf->WriteFixedPosHTML('<img height="450px" width="900px" src="'.\Yii::$app->basePath.'/web/img/01.01.Home.png"> ', 1, 1 , 210, 800, 'auto');

        $mpdf->WriteFixedPosHTML(date('d-m-Y'), 100, 45, 50, 90, 'auto');
        $mpdf->WriteFixedPosHTML($informacionGeneral['nombre_ie'], 57.2, 62.1, 50, 90, 'auto');
        $mpdf->WriteFixedPosHTML($informacionGeneral['seccion'], 41, 76.2, 50, 90, 'auto');
        $mpdf->WriteFixedPosHTML($estudiantes_contar, 165, 76.2, 50, 90, 'auto');
        $mpdf->WriteFixedPosHTML($informacionGeneral['nombres'] . ' ' . $informacionGeneral['apellido_paterno'] , 50, 89.5, 50, 90, 'auto');
        $mpdf->WriteFixedPosHTML($actividad['correlativo'], 52, 103, 50, 90, 'auto');
        
        $pagina_inicio = file_get_contents('http://localhost:8081/reporte/index');
        //var_dump(base64_decode($pagina_inicio));die;
        //
        header("Content-Type: image/jpeg;");
        $mpdf->WriteFixedPosHTML('<img src="data:image/jpeg;base64,'.$pagina_inicio .'">', 52, 143, 50, 90, 'auto');
        $mpdf->AddPage();
        $mpdf->WriteFixedPosHTML('<img height="250px" width="900px" src="'.\Yii::$app->basePath.'/web/img/02.01.Cabecera.png"> ', 1, 1 , 250, 80, 'auto');
        $reporte02 = Yii::$app->db->createCommand('
                SELECT
                    E.nombres,
                    E.apellido_paterno_letra
                FROM
                reporte_02 R
                INNER JOIN actividad_detalle AD ON R.actividad_detalle_id = AD.id
                INNER JOIN actividad_cabecera A ON AD.actividad_cabecera_id = A.id
                INNER JOIN docente_seccion DS ON R.docente_seccion_id = DS.id
                INNER JOIN estudiante E on R.estudiante_id = E.id
                INNER JOIN docente D ON DS.docente_id = D.id
                INNER JOIN usuario U ON D.dni = U.usuario
                WHERE U.id = "' . Yii::$app->user->identity->id . '" AND AD.grado_id = "'. $grado_id .'" AND A.id = '.$actividad_cabecera_id.'
            ')->queryAll();
        $reporte02_cantidad = Yii::$app->db->createCommand('
                SELECT
                    count(E.nombres) cantidad
                FROM
                reporte_02 R
                INNER JOIN actividad_detalle AD ON R.actividad_detalle_id = AD.id
                INNER JOIN actividad_cabecera A ON AD.actividad_cabecera_id = A.id
                INNER JOIN docente_seccion DS ON R.docente_seccion_id = DS.id
                INNER JOIN estudiante E on R.estudiante_id = E.id
                INNER JOIN docente D ON DS.docente_id = D.id
                INNER JOIN usuario U ON D.dni = U.usuario
                WHERE U.id = "' . Yii::$app->user->identity->id . '" AND AD.grado_id = "'. $grado_id .'" AND A.id = '.$actividad_cabecera_id.'
            ')->queryOne();
        // $mpdf->WriteFixedPosHTML('<div style="width:140px;">111</div>', 38, 48 , 50, 90, 'auto');
        // $mpdf->WriteFixedPosHTML('<div style="width:130px;">222</div>', 140, 48 , 50, 90, 'auto');
        $contador_reporte02_=0;
        $cantidad_reporte02_=1;
        //var_dump($reporte02_cantidad['cantidad']);die;
        foreach($reporte02 as $registro){
            $mpdf->WriteFixedPosHTML('<img height="30px" width="900px" src="'.\Yii::$app->basePath.'/web/img/02.02.Relleno.png"> ', 38, 45 + $contador_reporte02_ , 250, 40, 'auto');
            $contador_reporte02_ = $contador_reporte02_ + 8 ;
            if($cantidad_reporte02_==(int)$reporte02_cantidad['cantidad']){
                $mpdf->WriteFixedPosHTML('<img height="350px" width="900px" src="'.\Yii::$app->basePath.'/web/img/02.03.Pie.png"> ', 38, 45 + $contador_reporte02_ , 250, 95, 'auto');
            }
            $cantidad_reporte02_ ++;
        }

        $contador_reporte02=0;
        $espacio_reporte02=0;
        foreach($reporte02 as $registro){
            
            if($contador_reporte02==0){
                $mpdf->WriteFixedPosHTML('<div style="width:140px;">'.$registro['nombres'].' '.$registro['apellido_paterno_letra'].'</div>', 38, 48 + $contador_reporte02 , 50, 90, 'auto');
            }
            if($contador_reporte02==1){
                $mpdf->WriteFixedPosHTML('<div style="width:130px;">'.$registro['nombres'].' '.$registro['apellido_paterno_letra'].'</div>', 140, 48 + $contador_reporte02 , 50, 90, 'auto');
                $contador_reporte02 = 0;
            }
            $espacio_reporte02 = $contador_reporte02 + 8;
            $contador_reporte02++;
        }
        
        
        $mpdf->AddPage();
        $reporte03 = Yii::$app->db->createCommand('
                        SELECT
                            R.id,
                            C.descripcion,
                            R.avanzado,
                            R.intermedio,
                            R.inicial,
                            R.ninguno
                        FROM
                        reporte_03 R
                        INNER JOIN actividad_detalle AD ON R.actividad_detalle_id = AD.id
                        INNER JOIN actividad_cabecera A ON AD.actividad_cabecera_id = A.id
                        INNER JOIN docente_seccion DS ON R.docente_seccion_id = DS.id
                        INNER JOIN competencia C on R.competencia_id = C.id
                        INNER JOIN docente D ON DS.docente_id = D.id
                        INNER JOIN usuario U ON D.dni = U.usuario
                        WHERE U.id = "' . Yii::$app->user->identity->id . '" AND AD.grado_id = "'. $grado_id .'" AND AD.actividad_cabecera_id = '.$actividad_cabecera_id.'
                    ')->queryAll();

        foreach($reporte03 as $registro){
            $mpdf->WriteFixedPosHTML('<img height="450px" width="900px" src="'.\Yii::$app->basePath.'/web/img/03.01.Completa.png"> ', 1, 1 , 210, 800, 'auto');
            $mpdf->AddPage();
        }
        


        


        /* PAGINA 04 */
        $reporte04 = Yii::$app->db->createCommand('
                        SELECT
                            R.descripcion_capacidad,
                            R.numero_problema,
                            R.numero_estudiantes
                        FROM
                        reporte_04 R
                        INNER JOIN actividad_detalle AD ON R.actividad_detalle_id = AD.id
                        INNER JOIN actividad_cabecera A ON AD.actividad_cabecera_id = A.id
                        INNER JOIN docente_seccion DS ON R.docente_seccion_id = DS.id
                        INNER JOIN docente D ON DS.docente_id = D.id
                        INNER JOIN usuario U ON D.dni = U.usuario
                        WHERE U.id = "' . Yii::$app->user->identity->id . '" AND AD.grado_id = "'. $grado_id .'" AND AD.actividad_cabecera_id = '.$actividad_cabecera_id.'
                    ')->queryAll();
        $reporte04_cantidad = Yii::$app->db->createCommand('
                        SELECT
                            count(R.descripcion_capacidad) cantidad
                        FROM
                        reporte_04 R
                        INNER JOIN actividad_detalle AD ON R.actividad_detalle_id = AD.id
                        INNER JOIN actividad_cabecera A ON AD.actividad_cabecera_id = A.id
                        INNER JOIN docente_seccion DS ON R.docente_seccion_id = DS.id
                        INNER JOIN docente D ON DS.docente_id = D.id
                        INNER JOIN usuario U ON D.dni = U.usuario
                        WHERE U.id = "' . Yii::$app->user->identity->id . '" AND AD.grado_id = "'. $grado_id .'" AND AD.actividad_cabecera_id = '.$actividad_cabecera_id.'
                    ')->queryOne();

        $mpdf->WriteFixedPosHTML('<img height="450px" width="900px" src="'.\Yii::$app->basePath.'/web/img/04.01.Cabecera.png"> ', 1, 1 , 250, 180, 'auto');

        $contador_reporte04_=0;
        $cantidad_reporte04_=1;
        foreach($reporte04 as $registro){
            $mpdf->WriteFixedPosHTML('<img height="30px" width="900px" src="'.\Yii::$app->basePath.'/web/img/04.02.Relleno.png"> ', 38, 105 + $contador_reporte04_ , 250, 40, 'auto');
            $contador_reporte04_ = $contador_reporte04_ + 8 ;
            if($cantidad_reporte04_==(int)$reporte04_cantidad['cantidad']){
                $mpdf->WriteFixedPosHTML('<img height="350px" width="900px" src="'.\Yii::$app->basePath.'/web/img/04.03.Pie.png"> ', 38, 105 + $contador_reporte04_ , 250, 95, 'auto');
            }
            $cantidad_reporte04_ ++;
        }


        $contador_reporte04=0;
        $espacio_reporte04=0;
        foreach($reporte04 as $registro){
            
            //if($contador_reporte04==0){
                $mpdf->WriteFixedPosHTML('<div style="width:140px;">'.$registro['numero_problema'].' </div>', 53, 105 + $contador_reporte04 , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('<div style="width:130px;">'.$registro['numero_estudiantes'].'</div>', 137, 105 + $contador_reporte04 , 50, 90, 'auto');

            //}
            // if($contador_reporte04==1){
            //     $mpdf->WriteFixedPosHTML('<div style="width:130px;">'.$registro['nombres'].' '.$registro['apellido_paterno_letra'].'</div>', 140, 48 + $contador_reporte02 , 50, 90, 'auto');
            //     $contador_reporte04 = 0;
            // }
            $contador_reporte04 = $contador_reporte04 + 8;
            //$contador_reporte04++;
        }

        $mpdf->AddPage();
        $mpdf->WriteFixedPosHTML('<img height="450px" width="900px" src="'.\Yii::$app->basePath.'/web/img/05.01.Cabecera.png"> ', 1, 1 , 250, 180, 'auto');
        $reporte05 = Yii::$app->db->createCommand('
                        SELECT
                            E.id,
                            E.nombres,
                            E.apellido_paterno_letra,
                            R.puntaje,
                            R.p01,
                            R.p02,
                            R.p03,
                            R.p04,
                            R.p05,
                            R.p06,
                            R.p07,
                            R.p08,
                            R.p09,
                            R.p10,
                            R.p11,
                            R.p12,
                            R.p13,
                            R.p14,
                            R.p15,
                            R.p16,
                            R.p17,
                            R.p18,
                            R.p19,
                            R.p20,
                            R.p21,
                            R.p22,
                            R.p23,
                            R.p24,
                            R.p25,
                            R.p26,
                            R.p27,
                            R.p28,
                            R.p29,
                            R.p30
                        FROM
                        reporte_05 R
                        INNER JOIN actividad_detalle AD ON R.actividad_detalle_id = AD.id
                        INNER JOIN actividad_cabecera A ON AD.actividad_cabecera_id = A.id
                        INNER JOIN docente_seccion DS ON R.docente_seccion_id = DS.id
                        INNER JOIN estudiante E on R.estudiante_id = E.id
                        INNER JOIN docente D ON DS.docente_id = D.id
                        INNER JOIN usuario U ON D.dni = U.usuario
                        WHERE U.id = "' . Yii::$app->user->identity->id . '" AND AD.grado_id = "'. $grado_id .'" AND AD.actividad_cabecera_id = '.$actividad_cabecera_id.'
                    ')->queryAll();

        $reporte05_cantidad = Yii::$app->db->createCommand('
                        SELECT
                            count(E.id) cantidad
                        FROM
                        reporte_05 R
                        INNER JOIN actividad_detalle AD ON R.actividad_detalle_id = AD.id
                        INNER JOIN actividad_cabecera A ON AD.actividad_cabecera_id = A.id
                        INNER JOIN docente_seccion DS ON R.docente_seccion_id = DS.id
                        INNER JOIN estudiante E on R.estudiante_id = E.id
                        INNER JOIN docente D ON DS.docente_id = D.id
                        INNER JOIN usuario U ON D.dni = U.usuario
                        WHERE U.id = "' . Yii::$app->user->identity->id . '" AND AD.grado_id = "'. $grado_id .'" AND AD.actividad_cabecera_id = '.$actividad_cabecera_id.'
                    ')->queryOne();
        
        $contador_reporte05_= 0;
        $espacio_reporte05_ = 0;
        $cantidad_registros_reporte05_ = 1;
        $espacio_preguntas = 0 ;
        foreach($reporte05 as $registro){
            $puntaje_color = '';
            if($registro['puntaje'] >= 0 && $registro['puntaje'] <= 10){
                $puntaje_color = '05.05.CuadroRojo.png';
            }else if($registro['puntaje'] >= 11 && $registro['puntaje'] <= 20){
                $puntaje_color = '05.05.CuadroAmarillo.png';
            }else if($registro['puntaje'] >= 21 && $registro['puntaje'] <= 30){
                $puntaje_color = '05.05.CuadroVerde.png';
            }

            $p01 = $this->puntaje($registro['p01']);
            $p02 = $this->puntaje($registro['p02']);
            $p03 = $this->puntaje($registro['p03']);
            $p04 = $this->puntaje($registro['p04']);
            $p05 = $this->puntaje($registro['p05']);
            $p06 = $this->puntaje($registro['p06']);
            $p07 = $this->puntaje($registro['p07']);
            $p08 = $this->puntaje($registro['p08']);
            $p09 = $this->puntaje($registro['p09']);
            $p10 = $this->puntaje($registro['p10']);
            $p11 = $this->puntaje($registro['p11']);
            $p12 = $this->puntaje($registro['p12']);
            $p13 = $this->puntaje($registro['p13']);
            $p14 = $this->puntaje($registro['p14']);
            $p15 = $this->puntaje($registro['p15']);
            $p16 = $this->puntaje($registro['p16']);
            $p17 = $this->puntaje($registro['p17']);
            $p18 = $this->puntaje($registro['p18']);
            $p19 = $this->puntaje($registro['p19']);
            $p20 = $this->puntaje($registro['p20']);
            $p21 = $this->puntaje($registro['p21']);
            $p22 = $this->puntaje($registro['p22']);
            $p23 = $this->puntaje($registro['p23']);
            $p24 = $this->puntaje($registro['p24']);
            $p25 = $this->puntaje($registro['p25']);
            $p26 = $this->puntaje($registro['p26']);
            $p27 = $this->puntaje($registro['p27']);
            $p28 = $this->puntaje($registro['p28']);
            $p29 = $this->puntaje($registro['p29']);
            $p30 = $this->puntaje($registro['p30']);



            if($cantidad_registros_reporte05_==5){
                $espacio_reporte05_ = 0;
                $mpdf->AddPage();
                $mpdf->WriteFixedPosHTML('<img height="450px" width="900px" src="'.\Yii::$app->basePath.'/web/img/05.01.Cabecera.png"> ', 1, 1 , 250, 180, 'auto');
            }
            if($contador_reporte05_ == 2){
                $contador_reporte05_ = 0;
            }

            if($contador_reporte05_==0){
                $mpdf->WriteFixedPosHTML('<img height="580px"  src="'.\Yii::$app->basePath.'/web/img/'.$puntaje_color.'"> ', 30, 47 + $espacio_reporte05_ , 150, 110, 'auto');
                $mpdf->WriteFixedPosHTML($registro['nombres'].' '.$registro['apellido_paterno_letra'], 35, 62 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML($registro['puntaje'], 80, 62 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p01.'">', 37, 80 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p02.'">', 37, 87 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p03.'">', 37, 94.3 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p04.'">', 37, 101.5 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p05.'">', 37, 109 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('6&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p06.'">', 37, 116.2 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('7&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p07.'">', 37, 123.5 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('8&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p08.'">', 37, 131 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('9&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p09.'">', 37, 138.1 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('10&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p10.'">', 37, 145.2 + $espacio_reporte05_ , 50, 90, 'auto');

                $mpdf->WriteFixedPosHTML('11&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p11.'">', 56, 80 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('12&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p12.'">', 56, 87 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('13&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p13.'">', 56, 94.3 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('14&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p14.'">', 56, 101.5 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('15&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p15.'">', 56, 109 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('16&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p16.'">', 56, 116.2 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('17&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p17.'">', 56, 123.5 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('18&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p18.'">', 56, 131 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('19&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p19.'">', 56, 138.1 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('20&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p20.'">', 56, 145.2 + $espacio_reporte05_ , 50, 90, 'auto');

                $mpdf->WriteFixedPosHTML('21&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p21.'">', 75, 80 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('22&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p22.'">', 75, 87 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('23&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p23.'">', 75, 94.3 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('24&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p24.'">', 75, 101.5 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('25&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p25.'">', 75, 109 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('26&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p26.'">', 75, 116.2 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('27&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p27.'">', 75, 123.5 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('28&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p28.'">', 75, 131 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('29&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p29.'">', 75, 138.1 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('30&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p30.'">', 75, 145.2 + $espacio_reporte05_ , 50, 90, 'auto');
            }
            
            if($contador_reporte05_==1){
                $mpdf->WriteFixedPosHTML('<img height="580px"  src="'.\Yii::$app->basePath.'/web/img/'.$puntaje_color.'"> ', 105, 47 + $espacio_reporte05_ , 150, 110, 'auto');
                $mpdf->WriteFixedPosHTML($registro['nombres'].' '.$registro['apellido_paterno_letra'], 110, 62 + $espacio_reporte05_  , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML($registro['puntaje'], 155, 62 + $espacio_reporte05_ , 50, 90, 'auto');

                $mpdf->WriteFixedPosHTML('1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p01.'">', 112, 80 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p02.'">', 112, 87 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p03.'">', 112, 94.3 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p04.'">', 112, 101.5 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p05.'">', 112, 109 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('6&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p06.'">', 112, 116.2 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('7&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p07.'">', 112, 123.5 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('8&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p08.'">', 112, 131 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('9&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p09.'">', 112, 138.1 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('10&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p10.'">', 112, 145.2 + $espacio_reporte05_ , 50, 90, 'auto');

                $mpdf->WriteFixedPosHTML('11&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p11.'">', 131, 80 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('12&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p12.'">', 131, 87 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('13&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p13.'">', 131, 94.3 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('14&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p14.'">', 131, 101.5 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('15&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p15.'">', 131, 109 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('16&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p16.'">', 131, 116.2 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('17&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p17.'">', 131, 123.5 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('18&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p18.'">', 131, 131 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('19&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p19.'">', 131, 138.1 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('20&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p20.'">', 131, 145.2 + $espacio_reporte05_ , 50, 90, 'auto');

                $mpdf->WriteFixedPosHTML('21&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p21.'">', 150, 80 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('22&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p22.'">', 150, 87 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('23&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p23.'">', 150, 94.3 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('24&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p24.'">', 150, 101.5 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('25&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p25.'">', 150, 109 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('26&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p26.'">', 150, 116.2 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('27&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p27.'">', 150, 123.5 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('28&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p28.'">', 150, 131 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('29&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p29.'">', 150, 138.1 + $espacio_reporte05_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML('30&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p30.'">', 150, 145.2 + $espacio_reporte05_ , 50, 90, 'auto');


                $espacio_reporte05_ = $espacio_reporte05_ + 120;
            }

            $contador_reporte05_++;
            $cantidad_registros_reporte05_++;
        }

        



        // $mpdf->WriteFixedPosHTML('<img height="580px"  src="'.\Yii::$app->basePath.'/web/img/05.05.CuadroVerde.png"> ', 30, 45 , 150, 110, 'auto');
        // $mpdf->WriteFixedPosHTML('<img height="580px"  src="'.\Yii::$app->basePath.'/web/img/05.05.CuadroVerde.png"> ', 105, 45 , 150, 110, 'auto');
        
        // $mpdf->WriteFixedPosHTML('<img height="580px"  src="'.\Yii::$app->basePath.'/web/img/05.05.CuadroVerde.png"> ', 30, 165 , 150, 110, 'auto');

        //$mpdf->WriteFixedPosHTML(, 1, 80 , 250, 180, 'auto');
        // $content = $this->renderPartial('_grafico');
        // $mpdf->WriteHTML($content);
        //$mpdf->WriteFixedPosHTML('<img height="350px" width="900px" src="'.\Yii::$app->basePath.'/web/img/04.03.Pie.png"> ', 38, 205, 250, 95, 'auto');

        $mpdf->Output();
    }


    public function actionReporteSemanalPdfDemo() {

        
        if($_POST){
            //var_dump();
            $docente_seccion_id = $_POST['docente_seccion_id'];
            $grado_id = $_POST['grado_id'];
            $actividad_cabecera_id = $_POST['actividad_cabecera_id'];

            $actividad = (new \yii\db\Query())->select('*')->from('actividad_cabecera')->where('id=:id',[':id'=>$actividad_cabecera_id])->one();
            $actividad_detalle = (new \yii\db\Query())->select('*')->from('actividad_detalle')->where('actividad_cabecera_id=:actividad_cabecera_id and grado_id=:grado_id',[
                ':actividad_cabecera_id'=>$actividad_cabecera_id,
                ':grado_id'=>$grado_id
                ])->one();
            $informacionGeneral = (new \yii\db\Query())
                        ->select('docente.*,institucion_educativa.nombre_ie,grado.nombre_corto,seccion.descripcion seccion')
                        ->from('usuario')
                        ->innerJoin('docente','docente.dni=usuario.usuario')
                        ->innerJoin('institucion_educativa','institucion_educativa.id=docente.institucion_educativa_id')
                        ->innerJoin('docente_seccion','docente_seccion.docente_id=docente.id')
                        ->innerJoin('seccion','seccion.id=docente_seccion.seccion_id')
                        ->innerJoin('grado','grado.id=seccion.grado_id')
                        ->where('docente_seccion.id=:id',[':id'=>$docente_seccion_id])
                        ->one();
    
            $estudiantes = (new \yii\db\Query())
                        ->select('estudiante.nombres,estudiante.apellido_paterno_letra,matricula.estado_registro,estudiante.usuario,estudiante.clave,matricula.id matricula_id')
                        ->from('estudiante')
                        ->innerJoin('matricula','estudiante.id = matricula.estudiante_id')
                        ->innerJoin('docente_seccion','docente_seccion.id=matricula.docente_seccion_id')
                        ->where('matricula.estado_registro in (1,2) and docente_seccion.docente_id=:docente_id and docente_seccion.id=:id',[':id'=>$docente_seccion_id,':docente_id'=>Yii::$app->user->identity->docenteid]);
            $estudiantes_contar = $estudiantes->count();
    
            $estudiantes = $estudiantes->orderBy('matricula.estado_registro desc')->all();
    
            
    
            
    
            $mpdf = new Mpdf(['default_font_size' => '12',
            'default_font' => 'Montserrat,sans-serif']);
            $mpdf->showImageErrors = true;
            //$mascara_01 = $this->renderPartial('_mascara01');
            //$mpdf->WriteHTML($mascara_01);
            $mpdf->WriteFixedPosHTML('<img height="450px" width="900px" src="'.\Yii::$app->basePath.'/web/img/01.01.Home.png"> ', 1, 1 , 210, 800, 'auto');
    
            $mpdf->WriteFixedPosHTML(date('d-m-Y'), 100, 45, 50, 90, 'auto');
            $mpdf->WriteFixedPosHTML($informacionGeneral['nombre_ie'], 57.2, 62.1, 120, 120, 'auto');
            $mpdf->WriteFixedPosHTML($informacionGeneral['nombre_corto'].' '.$informacionGeneral['seccion'], 41, 76.2, 50, 90, 'auto');
            $mpdf->WriteFixedPosHTML($estudiantes_contar, 165, 76.2, 50, 90, 'auto');
            $mpdf->WriteFixedPosHTML($informacionGeneral['nombres'] . ' ' . $informacionGeneral['apellido_paterno'] , 50, 89.5, 120, 120, 'auto');
            $mpdf->WriteFixedPosHTML($actividad_detalle['descripcion'], 52, 103, 120, 150, 'auto');
            
            //$pagina_inicio = file_get_contents('http://localhost:8081/reporte/index');
            //var_dump(base64_decode($pagina_inicio));die;
            //
            header("Content-Type: image/jpeg;");
            //$mpdf->WriteFixedPosHTML('<img src="data:image/jpeg;base64,'.$pagina_inicio .'">', 52, 143, 50, 90, 'auto');
            //$mpdf->WriteFixedPosHTML('<img src="data:image/svg+xml;utf8,'. $_POST['imagen01_svg'] .'">', 52, 143, 50, 90, 'auto');

            $mpdf->WriteFixedPosHTML( $_POST['imagen01_svg'] , 32, 143, 180, 180, 'auto');
            $mpdf->AddPage();
            $mpdf->WriteFixedPosHTML('<img height="250px" width="900px" src="'.\Yii::$app->basePath.'/web/img/02.01.Cabecera.png"> ', 1, 1 , 250, 80, 'auto');
            $reporte02 = Yii::$app->db->createCommand('
                    SELECT
                        E.nombres,
                        E.apellido_paterno_letra
                    FROM
                    reporte_02 R
                    INNER JOIN actividad_detalle AD ON R.actividad_detalle_id = AD.id
                    INNER JOIN actividad_cabecera A ON AD.actividad_cabecera_id = A.id
                    INNER JOIN docente_seccion DS ON R.docente_seccion_id = DS.id
                    INNER JOIN estudiante E on R.estudiante_id = E.id
                    INNER JOIN docente D ON DS.docente_id = D.id
                    INNER JOIN usuario U ON D.dni = U.usuario
                    WHERE U.id = "' . Yii::$app->user->identity->id . '" AND AD.grado_id = "'. $grado_id .'" AND A.id = '.$actividad_cabecera_id.'
                ')->queryAll();
            $reporte02_cantidad = Yii::$app->db->createCommand('
                    SELECT
                        count(E.nombres) cantidad
                    FROM
                    reporte_02 R
                    INNER JOIN actividad_detalle AD ON R.actividad_detalle_id = AD.id
                    INNER JOIN actividad_cabecera A ON AD.actividad_cabecera_id = A.id
                    INNER JOIN docente_seccion DS ON R.docente_seccion_id = DS.id
                    INNER JOIN estudiante E on R.estudiante_id = E.id
                    INNER JOIN docente D ON DS.docente_id = D.id
                    INNER JOIN usuario U ON D.dni = U.usuario
                    WHERE U.id = "' . Yii::$app->user->identity->id . '" AND AD.grado_id = "'. $grado_id .'" AND A.id = '.$actividad_cabecera_id.'
                ')->queryOne();
            $p02_texto = (new \yii\db\Query())->select('*')->from('texto')->where('experiencia_id=:experiencia_id and grado_id=:grado_id and posicion=1 and pregunta=2',[':experiencia_id'=>$actividad['experiencia_id'],':grado_id'=>$grado_id])->one();

            $p02_teto_principal = $p02_texto['texto_principal'];
            $p02_texto_enlace = $p02_texto['texto_enlace'];
            $p02_tipo = $p02_texto['tipo'];
            $p02_textox = '';
            $p02_archivo = $p02_texto['archivo'];
            $p02_video = $p02_texto['video'];
            $p02_enlace = $p02_texto['enlace'];

            if($p02_tipo==1){
                $p02_textox = $p02_teto_principal.' <a href="'.\Yii::$app->request->BaseUrl.'/archivo_texto/'.$p02_archivo.'" target="_blank">'.$p02_texto_enlace.'</a>';
            }else if($p02_tipo==2){
                $p02_textox = $p02_teto_principal.' <a href="'. $p02_video.'" target="_blank">'.$p02_texto_enlace.'</a>';
            }else if($p02_tipo==3){
                $p02_textox = $p02_teto_principal.' <a href="'. $p02_enlace.'" target="_blank">'.$p02_texto_enlace.'</a>';
            }

            $contador_reporte02_=0;
            $cantidad_reporte02_=0;

            for($i=0;$i<=($reporte02_cantidad['cantidad']/2);$i++){
            //foreach($reporte02 as $registro){
                $mpdf->WriteFixedPosHTML('<img height="20px" width="900px" src="'.\Yii::$app->basePath.'/web/img/02.02.Relleno.png"> ', 38, 45 + $contador_reporte02_ , 250, 30, 'auto');
                $contador_reporte02_ = $contador_reporte02_ + 8 ;
                // $mpdf->WriteFixedPosHTML('<img height="30px" width="900px" src="'.\Yii::$app->basePath.'/web/img/02.02.Relleno.png"> ', 38, 45 + $contador_reporte02_ , 250, 40, 'auto');
                // $cont
                if($cantidad_reporte02_==((int)($reporte02_cantidad['cantidad']/2))){
                    $mpdf->WriteFixedPosHTML('<img height="350px" width="900px" src="'.\Yii::$app->basePath.'/web/img/02.03.Pie.png"> ', 38, 45 + $contador_reporte02_ , 250, 95, 'auto');
                    $mpdf->WriteFixedPosHTML($p02_textox, 21, 45 + $contador_reporte02_ + 60 , 165, 90, 'auto');
                }
                $cantidad_reporte02_ ++;
            }
    
            $contador_reporte02=0;
            $espacio_reporte02=0;
            foreach($reporte02 as $registro){
                //$mpdf->WriteFixedPosHTML('<img height="30px" width="900px" src="'.\Yii::$app->basePath.'/web/img/02.02.Relleno.png"> ', 38, 45 + $contador_reporte02_ , 250, 40, 'auto');

                if($contador_reporte02==0){
                    $mpdf->WriteFixedPosHTML($registro['nombres'].' '.$registro['apellido_paterno_letra'], 38, 48 + $espacio_reporte02 , 50, 90, 'auto');
                }
                if($contador_reporte02==1){
                    $mpdf->WriteFixedPosHTML($registro['nombres'].' '.$registro['apellido_paterno_letra'], 140, 48 + $espacio_reporte02 , 50, 90, 'auto');
                    //$contador_reporte02 = 0;
                    $espacio_reporte02 = $espacio_reporte02 + 8;
                }

                $contador_reporte02++;
                if($contador_reporte02==2){
                    $contador_reporte02 = 0;
                }
            }

            
            
            
            $mpdf->AddPage();
            $reporte03 = Yii::$app->db->createCommand('
                            SELECT
                                R.id,
                                C.descripcion,
                                R.avanzado,
                                R.intermedio,
                                R.inicial,
                                R.ninguno
                            FROM
                            reporte_03 R
                            INNER JOIN actividad_detalle AD ON R.actividad_detalle_id = AD.id
                            INNER JOIN actividad_cabecera A ON AD.actividad_cabecera_id = A.id
                            INNER JOIN docente_seccion DS ON R.docente_seccion_id = DS.id
                            INNER JOIN competencia C on R.competencia_id = C.id
                            INNER JOIN docente D ON DS.docente_id = D.id
                            INNER JOIN usuario U ON D.dni = U.usuario
                            WHERE U.id = "' . Yii::$app->user->identity->id . '" AND AD.grado_id = "'. $grado_id .'" AND AD.actividad_cabecera_id = '.$actividad_cabecera_id.'
                        ')->queryAll();
            
            $contador_reporte03=0;
            foreach($reporte03 as $registro){
                $mpdf->WriteFixedPosHTML('<img height="450px" width="900px" src="'.\Yii::$app->basePath.'/web/img/03.01.Completa.png"> ', 1, 1 , 210, 800, 'auto');
                $mpdf->WriteFixedPosHTML( $registro['descripcion'] , 28, 55, 160, 160, 'auto');
                if($contador_reporte03 == 0){

                    $mpdf->WriteFixedPosHTML( $_POST['imagen03_1_svg'] , 28, 73, 180, 180, 'auto');
                }

                if($contador_reporte03 == 1){
                    $mpdf->WriteFixedPosHTML( $_POST['imagen03_2_svg'] , 28, 73, 180, 180, 'auto');
                }
                $contador_reporte03++;
                $mpdf->AddPage();
            }
            
            /* PAGINA 04 */
            $reporte04 = Yii::$app->db->createCommand('
                            SELECT
                                R.numero_problema,
                                R.numero_estudiantes,
                                CA.descripcion descripcion_capacidad
                            FROM
                            reporte_04 R
                            INNER JOIN actividad_detalle AD ON R.actividad_detalle_id = AD.id
                            INNER JOIN actividad_cabecera A ON AD.actividad_cabecera_id = A.id
                            INNER JOIN docente_seccion DS ON R.docente_seccion_id = DS.id
                            INNER JOIN docente D ON DS.docente_id = D.id
                            INNER JOIN usuario U ON D.dni = U.usuario
                            INNER JOIN capacidad CA ON CA.id = R.capacidad_id
                            WHERE U.id = "' . Yii::$app->user->identity->id . '" AND AD.grado_id = "'. $grado_id .'" AND AD.actividad_cabecera_id = '.$actividad_cabecera_id.'
                            ORDER BY R.numero_estudiantes desc;
                        ')->queryAll();
            $reporte04_cantidad = Yii::$app->db->createCommand('
                            SELECT
                                count(CA.descripcion) cantidad
                            FROM
                            reporte_04 R
                            INNER JOIN actividad_detalle AD ON R.actividad_detalle_id = AD.id
                            INNER JOIN actividad_cabecera A ON AD.actividad_cabecera_id = A.id
                            INNER JOIN docente_seccion DS ON R.docente_seccion_id = DS.id
                            INNER JOIN docente D ON DS.docente_id = D.id
                            INNER JOIN usuario U ON D.dni = U.usuario
                            INNER JOIN capacidad CA ON CA.id = R.capacidad_id
                            WHERE U.id = "' . Yii::$app->user->identity->id . '" AND AD.grado_id = "'. $grado_id .'" AND AD.actividad_cabecera_id = '.$actividad_cabecera_id.'
                        ')->queryOne();
    
            $mpdf->WriteFixedPosHTML('<img height="450px" width="900px" src="'.\Yii::$app->basePath.'/web/img/04.01.Cabecera.png"> ', 1, 1 , 250, 180, 'auto');
    
            $contador_reporte04_=0;
            $cantidad_reporte04_=1;
            $conta_04=1;
            foreach($reporte04 as $registro){
                $mpdf->WriteFixedPosHTML('<img height="10px" width="900px" src="'.\Yii::$app->basePath.'/web/img/04.02.Relleno.png"> ', 38, 105 + $contador_reporte04_ , 250, 40, 'auto');

                if($cantidad_reporte04_ == 1){
                    $mpdf->WriteFixedPosHTML($registro['descripcion_capacidad'], 28, 78 , 160, 160, 'auto');
                }
                $mpdf->WriteFixedPosHTML($registro['numero_problema'], 53, 105 + $contador_reporte04_ , 50, 90, 'auto');
                $mpdf->WriteFixedPosHTML($registro['numero_estudiantes'], 137, 105 + $contador_reporte04_ , 50, 90, 'auto');
                $contador_reporte04_ = $contador_reporte04_ + 8 ;
                

                if($conta_04==(int)$reporte04_cantidad['cantidad']){
                    $mpdf->WriteFixedPosHTML('<img height="350px" width="900px" src="'.\Yii::$app->basePath.'/web/img/04.03.Pie.png"> ', 38, 105 + $contador_reporte04_ , 250, 95, 'auto');
                }
                $cantidad_reporte04_ ++;
                if($cantidad_reporte04_==15){
                    $mpdf->WriteFixedPosHTML('<img height="350px" width="900px" src="'.\Yii::$app->basePath.'/web/img/04.03.Pie.png"> ', 38, 105 + $contador_reporte04_ , 250, 95, 'auto');
                    $mpdf->AddPage();
                    $mpdf->WriteFixedPosHTML('<img height="450px" width="900px" src="'.\Yii::$app->basePath.'/web/img/04.01.Cabecera.png"> ', 1, 1 , 250, 180, 'auto');
                    $cantidad_reporte04_=1;
                    $contador_reporte04_=0;
                }
                $conta_04++;
            }
    
    
            // $contador_reporte04=0;
            // $espacio_reporte04=0;
            // foreach($reporte04 as $registro){
                
            //     $contador_reporte04 = $contador_reporte04 + 8;
            // }
    
            $mpdf->AddPage();
            $reporte05 = Yii::$app->db->createCommand('
                            SELECT
                                E.id,
                                E.nombres,
                                E.apellido_paterno_letra,
                                R.puntaje,
                                R.p01,
                                R.p02,
                                R.p03,
                                R.p04,
                                R.p05,
                                R.p06,
                                R.p07,
                                R.p08,
                                R.p09,
                                R.p10,
                                R.p11,
                                R.p12,
                                R.p13,
                                R.p14,
                                R.p15,
                                R.p16,
                                R.p17,
                                R.p18,
                                R.p19,
                                R.p20,
                                R.p21,
                                R.p22,
                                R.p23,
                                R.p24,
                                R.p25,
                                R.p26,
                                R.p27,
                                R.p28,
                                R.p29,
                                R.p30
                            FROM
                            reporte_05 R
                            INNER JOIN actividad_detalle AD ON R.actividad_detalle_id = AD.id
                            INNER JOIN actividad_cabecera A ON AD.actividad_cabecera_id = A.id
                            INNER JOIN docente_seccion DS ON R.docente_seccion_id = DS.id
                            INNER JOIN estudiante E on R.estudiante_id = E.id
                            INNER JOIN docente D ON DS.docente_id = D.id
                            INNER JOIN usuario U ON D.dni = U.usuario
                            WHERE U.id = "' . Yii::$app->user->identity->id . '" AND AD.grado_id = "'. $grado_id .'" AND AD.actividad_cabecera_id = '.$actividad_cabecera_id.'
                            ORDER BY R.puntaje desc;
                        ')->queryAll();
    
            $reporte05_cantidad = Yii::$app->db->createCommand('
                            SELECT
                                count(E.id) cantidad
                            FROM
                            reporte_05 R
                            INNER JOIN actividad_detalle AD ON R.actividad_detalle_id = AD.id
                            INNER JOIN actividad_cabecera A ON AD.actividad_cabecera_id = A.id
                            INNER JOIN docente_seccion DS ON R.docente_seccion_id = DS.id
                            INNER JOIN estudiante E on R.estudiante_id = E.id
                            INNER JOIN docente D ON DS.docente_id = D.id
                            INNER JOIN usuario U ON D.dni = U.usuario
                            WHERE U.id = "' . Yii::$app->user->identity->id . '" AND AD.grado_id = "'. $grado_id .'" AND AD.actividad_cabecera_id = '.$actividad_cabecera_id.'
                        ')->queryOne();
            
            if($reporte05_cantidad['cantidad']==0){
                $mpdf->WriteFixedPosHTML('<img height="450px" width="900px" src="'.\Yii::$app->basePath.'/web/img/06.01Cabeceradeuno.png"> ', 1, 1 , 250, 180, 'auto');
                $mpdf->WriteFixedPosHTML('<img height="450px" width="900px" src="'.\Yii::$app->basePath.'/web/img/06.03.Pie.png"> ', 1, 277 , 250, 180, 'auto');
            }else{
                $mpdf->WriteFixedPosHTML('<img height="450px" width="900px" src="'.\Yii::$app->basePath.'/web/img/06.01Cabeceradedos.png"> ', 1, 1 , 250, 180, 'auto');
                $mpdf->WriteFixedPosHTML('<img height="450px" width="900px" src="'.\Yii::$app->basePath.'/web/img/06.03.Pie.png"> ', 1, 277 , 250, 180, 'auto');
            }

            $contador_reporte05_= 0;
            $espacio_reporte05_ = 0;
            $cantidad_registros_reporte05_ = 1;
            $espacio_preguntas = 0 ;
            $cantidad_sumada = round($reporte05_cantidad['cantidad']/2);
            // for($i=0;$i<$reporte05_cantidad['cantidad'];$i++){

            //     $puntaje_color = '';
            //         if($reporte05[$i]['puntaje'] >= 0 && $reporte05[$i]['puntaje'] <= 10){
            //             $puntaje_color = '06.02Individualrojo.png';
            //         }else if($reporte05[$i]['puntaje'] >= 11 && $reporte05[$i]['puntaje'] <= 20){
            //             $puntaje_color = '06.02Individualamarillo.png';
            //         }else if($reporte05[$i]['puntaje'] >= 21 && $reporte05[$i]['puntaje'] <= 30){
            //             $puntaje_color = '06.02Individualverde.png';
            //         }


            //     if($cantidad_sumada>=$cantidad_registros_reporte05_){
            //         $mpdf->WriteFixedPosHTML('<img height="181px"  src="'.\Yii::$app->basePath.'/web/img/'.$puntaje_color.'"> ', 30, 60 + $espacio_reporte05_ , 70, 110, 'auto');
            //         $mpdf->WriteFixedPosHTML($reporte05[$i]['nombres'].' '.$reporte05[$i]['apellido_paterno_letra'], 35, 62 + $espacio_reporte05_ , 50, 90, 'auto');
            //         $mpdf->WriteFixedPosHTML($reporte05[$i]['puntaje'], 85.5, 62 + $espacio_reporte05_ , 50, 90, 'auto');
            //         $espacio_reporte05_ = $espacio_reporte05_ + 10;
            //     }

            //     if($cantidad_sumada==$cantidad_registros_reporte05_){
            //         $espacio_reporte05_ = 0;
            //     }
                
            //     if($cantidad_sumada<$cantidad_registros_reporte05_){
            //         $mpdf->WriteFixedPosHTML('<img height="181px"  src="'.\Yii::$app->basePath.'/web/img/'.$puntaje_color.'"> ', 115, 60 + $espacio_reporte05_ , 70, 110, 'auto');
            //         $mpdf->WriteFixedPosHTML($reporte05[$i]['nombres'].' '.$reporte05[$i]['apellido_paterno_letra'], 120, 62 + $espacio_reporte05_  , 50, 90, 'auto');
            //         $mpdf->WriteFixedPosHTML($reporte05[$i]['puntaje'], 170.5, 62 + $espacio_reporte05_ , 50, 90, 'auto');
    
    
            //         $espacio_reporte05_ = $espacio_reporte05_ + 10;
            //     }

            //     $contador_reporte05_++;
            //     $cantidad_registros_reporte05_++;

            //     if($contador_reporte05_ == 2){
            //         $contador_reporte05_ = 0;
            //     }

            // }

            foreach($reporte05 as $registro){

                $puntaje_color = '';
                if($registro['puntaje'] >= 0 && $registro['puntaje'] <= 10){
                    $puntaje_color = '06.02Individualrojo.png';
                }else if($registro['puntaje'] >= 11 && $registro['puntaje'] <= 20){
                    $puntaje_color = '06.02Individualamarillo.png';
                }else if($registro['puntaje'] >= 21 && $registro['puntaje'] <= 30){
                    $puntaje_color = '06.02Individualverde.png';
                }
    
    
                if($cantidad_sumada>=$cantidad_registros_reporte05_){
                    $mpdf->WriteFixedPosHTML('<img height="181px"  src="'.\Yii::$app->basePath.'/web/img/'.$puntaje_color.'"> ', 30, 60 + $espacio_reporte05_ , 70, 110, 'auto');
                    $mpdf->WriteFixedPosHTML($registro['nombres'].' '.$registro['apellido_paterno_letra'], 35, 62 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML($registro['puntaje'], 85.5, 62 + $espacio_reporte05_ , 50, 90, 'auto');
                    $espacio_reporte05_ = $espacio_reporte05_ + 10;
                }

                if($cantidad_sumada==$cantidad_registros_reporte05_){
                    $espacio_reporte05_ = 0;
                }
                
                if($cantidad_sumada<$cantidad_registros_reporte05_){
                    $mpdf->WriteFixedPosHTML('<img height="181px"  src="'.\Yii::$app->basePath.'/web/img/'.$puntaje_color.'"> ', 115, 60 + $espacio_reporte05_ , 70, 110, 'auto');
                    $mpdf->WriteFixedPosHTML($registro['nombres'].' '.$registro['apellido_paterno_letra'], 120, 62 + $espacio_reporte05_  , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML($registro['puntaje'], 170.5, 62 + $espacio_reporte05_ , 50, 90, 'auto');
    
    
                    $espacio_reporte05_ = $espacio_reporte05_ + 10;
                }
    
                $contador_reporte05_++;
                $cantidad_registros_reporte05_++;

                if($contador_reporte05_ == 2){
                    $contador_reporte05_ = 0;
                }

                if($cantidad_registros_reporte05_==41){
                    $espacio_reporte05_ = 0;
                    $cantidad_registros_reporte05_ = 1;
                    $mpdf->AddPage();
                    $mpdf->WriteFixedPosHTML('<img height="450px" width="900px" src="'.\Yii::$app->basePath.'/web/img/06.01Cabeceradedos.png"> ', 1, 1 , 250, 180, 'auto');
                    $mpdf->WriteFixedPosHTML('<img height="450px" width="900px" src="'.\Yii::$app->basePath.'/web/img/05.03.Pie.png"> ', 1, 277 , 250, 180, 'auto');
                }
            }


            
            $mpdf->AddPage();

            $mpdf->WriteFixedPosHTML('<img height="450px" width="900px" src="'.\Yii::$app->basePath.'/web/img/05.01.Cabecera.png"> ', 1, 1 , 250, 180, 'auto');
            $mpdf->WriteFixedPosHTML('<img height="450px" width="900px" src="'.\Yii::$app->basePath.'/web/img/05.03.Pie.png"> ', 1, 277 , 250, 180, 'auto');

            
            
            $contador_reporte05_= 0;
            $espacio_reporte05_ = 0;
            $cantidad_registros_reporte05_ = 1;
            $cantidad_registros_reporte05 = 1;
            $espacio_preguntas = 0 ;
            foreach($reporte05 as $registro){

                $puntaje_color = '';
                if($registro['puntaje'] >= 0 && $registro['puntaje'] <= 10){
                    $puntaje_color = '05.05.CuadroRojo.png';
                }else if($registro['puntaje'] >= 11 && $registro['puntaje'] <= 20){
                    $puntaje_color = '05.05.CuadroAmarillo.png';
                }else if($registro['puntaje'] >= 21 && $registro['puntaje'] <= 30){
                    $puntaje_color = '05.05.CuadroVerde.png';
                }
    
                $p01 = $this->puntaje($registro['p01']);
                $p02 = $this->puntaje($registro['p02']);
                $p03 = $this->puntaje($registro['p03']);
                $p04 = $this->puntaje($registro['p04']);
                $p05 = $this->puntaje($registro['p05']);
                $p06 = $this->puntaje($registro['p06']);
                $p07 = $this->puntaje($registro['p07']);
                $p08 = $this->puntaje($registro['p08']);
                $p09 = $this->puntaje($registro['p09']);
                $p10 = $this->puntaje($registro['p10']);
                $p11 = $this->puntaje($registro['p11']);
                $p12 = $this->puntaje($registro['p12']);
                $p13 = $this->puntaje($registro['p13']);
                $p14 = $this->puntaje($registro['p14']);
                $p15 = $this->puntaje($registro['p15']);
                $p16 = $this->puntaje($registro['p16']);
                $p17 = $this->puntaje($registro['p17']);
                $p18 = $this->puntaje($registro['p18']);
                $p19 = $this->puntaje($registro['p19']);
                $p20 = $this->puntaje($registro['p20']);
                $p21 = $this->puntaje($registro['p21']);
                $p22 = $this->puntaje($registro['p22']);
                $p23 = $this->puntaje($registro['p23']);
                $p24 = $this->puntaje($registro['p24']);
                $p25 = $this->puntaje($registro['p25']);
                $p26 = $this->puntaje($registro['p26']);
                $p27 = $this->puntaje($registro['p27']);
                $p28 = $this->puntaje($registro['p28']);
                $p29 = $this->puntaje($registro['p29']);
                $p30 = $this->puntaje($registro['p30']);
    
                if($contador_reporte05_<=1){
                    $mpdf->WriteFixedPosHTML('<img height="580px"  src="'.\Yii::$app->basePath.'/web/img/'.$puntaje_color.'"> ', 30, 47 + $espacio_reporte05_ , 150, 110, 'auto');
                    $mpdf->WriteFixedPosHTML($registro['nombres'].' '.$registro['apellido_paterno_letra'], 35, 62 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML($registro['puntaje'], 80, 62 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p01.'">', 37, 80 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p02.'">', 37, 87 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p03.'">', 37, 94.3 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p04.'">', 37, 101.5 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p05.'">', 37, 109 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('6&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p06.'">', 37, 116.2 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('7&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p07.'">', 37, 123.5 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('8&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p08.'">', 37, 131 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('9&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p09.'">', 37, 138.1 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('10&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p10.'">', 37, 145.2 + $espacio_reporte05_ , 50, 90, 'auto');
    
                    $mpdf->WriteFixedPosHTML('11&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p11.'">', 56, 80 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('12&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p12.'">', 56, 87 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('13&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p13.'">', 56, 94.3 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('14&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p14.'">', 56, 101.5 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('15&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p15.'">', 56, 109 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('16&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p16.'">', 56, 116.2 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('17&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p17.'">', 56, 123.5 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('18&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p18.'">', 56, 131 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('19&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p19.'">', 56, 138.1 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('20&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p20.'">', 56, 145.2 + $espacio_reporte05_ , 50, 90, 'auto');
    
                    $mpdf->WriteFixedPosHTML('21&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p21.'">', 75, 80 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('22&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p22.'">', 75, 87 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('23&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p23.'">', 75, 94.3 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('24&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p24.'">', 75, 101.5 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('25&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p25.'">', 75, 109 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('26&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p26.'">', 75, 116.2 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('27&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p27.'">', 75, 123.5 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('28&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p28.'">', 75, 131 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('29&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p29.'">', 75, 138.1 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('30&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p30.'">', 75, 145.2 + $espacio_reporte05_ , 50, 90, 'auto');
                    $espacio_reporte05_ = $espacio_reporte05_ + 120;
                }

                if($contador_reporte05_ == 2){
                    $espacio_reporte05_ = 0 ;
                }
                
                if($contador_reporte05_>1){
                    $mpdf->WriteFixedPosHTML('<img height="580px"  src="'.\Yii::$app->basePath.'/web/img/'.$puntaje_color.'"> ', 105, 47 + $espacio_reporte05_ , 150, 110, 'auto');
                    $mpdf->WriteFixedPosHTML($registro['nombres'].' '.$registro['apellido_paterno_letra'], 110, 62 + $espacio_reporte05_  , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML($registro['puntaje'], 155, 62 + $espacio_reporte05_ , 50, 90, 'auto');
    
                    $mpdf->WriteFixedPosHTML('1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p01.'">', 112, 80 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p02.'">', 112, 87 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p03.'">', 112, 94.3 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p04.'">', 112, 101.5 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p05.'">', 112, 109 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('6&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p06.'">', 112, 116.2 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('7&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p07.'">', 112, 123.5 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('8&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p08.'">', 112, 131 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('9&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p09.'">', 112, 138.1 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('10&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p10.'">', 112, 145.2 + $espacio_reporte05_ , 50, 90, 'auto');
    
                    $mpdf->WriteFixedPosHTML('11&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p11.'">', 131, 80 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('12&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p12.'">', 131, 87 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('13&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p13.'">', 131, 94.3 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('14&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p14.'">', 131, 101.5 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('15&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p15.'">', 131, 109 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('16&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p16.'">', 131, 116.2 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('17&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p17.'">', 131, 123.5 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('18&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p18.'">', 131, 131 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('19&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p19.'">', 131, 138.1 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('20&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p20.'">', 131, 145.2 + $espacio_reporte05_ , 50, 90, 'auto');
    
                    $mpdf->WriteFixedPosHTML('21&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p21.'">', 150, 80 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('22&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p22.'">', 150, 87 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('23&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p23.'">', 150, 94.3 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('24&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p24.'">', 150, 101.5 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('25&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p25.'">', 150, 109 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('26&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p26.'">', 150, 116.2 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('27&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p27.'">', 150, 123.5 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('28&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p28.'">', 150, 131 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('29&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p29.'">', 150, 138.1 + $espacio_reporte05_ , 50, 90, 'auto');
                    $mpdf->WriteFixedPosHTML('30&nbsp;&nbsp;&nbsp;&nbsp;<img height="10px"  src="'.\Yii::$app->basePath.'/web/img/'.$p30.'">', 150, 145.2 + $espacio_reporte05_ , 50, 90, 'auto');
    
                    $espacio_reporte05_ = $espacio_reporte05_ + 120;
                }
    
                $contador_reporte05_++;
                $cantidad_registros_reporte05_++;

                // if($contador_reporte05_ == 2){
                    
                // }

                if($cantidad_registros_reporte05_==5 && $reporte05_cantidad['cantidad']!=$cantidad_registros_reporte05){
                    $contador_reporte05_ = 0;
                    $espacio_reporte05_ = 0;
                    $cantidad_registros_reporte05_ = 1;
                    $mpdf->AddPage();
                    $mpdf->WriteFixedPosHTML('<img height="450px" width="900px" src="'.\Yii::$app->basePath.'/web/img/05.01.Cabecera.png"> ', 1, 1 , 250, 180, 'auto');
                    $mpdf->WriteFixedPosHTML('<img height="450px" width="900px" src="'.\Yii::$app->basePath.'/web/img/05.03.Pie.png"> ', 1, 277 , 250, 180, 'auto');
                }
                $cantidad_registros_reporte05++;
            }

            $mpdf->AddPage();

            $p05_texto_01 = (new \yii\db\Query())->select('*')->from('texto')->where('experiencia_id=:experiencia_id and grado_id=:grado_id and posicion=1 and pregunta=5',[':experiencia_id'=>$actividad['experiencia_id'],':grado_id'=>$grado_id])->one();

            $p05_texto_principal_01 = $p05_texto_01['texto_principal'];
            $p05_texto_enlace_01 = $p05_texto_01['texto_enlace'];
            $p05_tipo_01 = $p05_texto_01['tipo'];
            $p05_textox_01 = '';
            $p05_archivo_01 = $p05_texto_01['archivo'];
            $p05_video_01 = $p05_texto_01['video'];
            $p05_enlace_01 = $p05_texto_01['enlace'];

            if($p05_tipo_01==1){
                $p05_textox_01 = $p05_texto_principal_01.' <a href="'.\Yii::$app->request->BaseUrl.'/archivo_texto/'.$p05_archivo_01.'" target="_blank">'.$p05_texto_enlace_01.'</a>';
            }else if($p05_tipo_01==2){
                $p05_textox_01 = $p05_texto_principal_01.' <a href="'. $p05_video_01.'" target="_blank">'.$p05_texto_enlace_01.'</a>';
            }else if($p05_tipo_01==3){
                $p05_textox_01 = $p05_texto_principal_01.' <a href="'. $p05_enlace_01.'" target="_blank">'.$p05_texto_enlace_01.'</a>';
            }


            $p05_texto_02 = (new \yii\db\Query())->select('*')->from('texto')->where('experiencia_id=:experiencia_id and grado_id=:grado_id and posicion=2 and pregunta=5',[':experiencia_id'=>$actividad['experiencia_id'],':grado_id'=>$grado_id])->one();

            $p05_texto_principal_02 = $p05_texto_02['texto_principal'];
            $p05_texto_enlace_02 = $p05_texto_02['texto_enlace'];
            $p05_tipo_02 = $p05_texto_02['tipo'];
            $p05_textox_02 = '';
            $p05_archivo_02 = $p05_texto_02['archivo'];
            $p05_video_02 = $p05_texto_02['video'];
            $p05_enlace_02 = $p05_texto_02['enlace'];

            if($p05_tipo_02==1){
                $p05_textox_02 = $p05_texto_principal_02.' <a href="'.\Yii::$app->request->BaseUrl.'/archivo_texto/'.$p05_archivo_02.'" target="_blank">'.$p05_texto_enlace_02.'</a>';
            }else if($p05_tipo_02==2){
                $p05_textox_02 = $p05_texto_principal_02.' <a href="'. $p05_video_02.'" target="_blank">'.$p05_texto_enlace_02.'</a>';
            }else if($p05_tipo_02==3){
                $p05_textox_02 = $p05_texto_principal_02.' <a href="'. $p05_enlace_02.'" target="_blank">'.$p05_texto_enlace_02.'</a>';
            }


            $mpdf->WriteFixedPosHTML('<img height="450px" width="900px" src="'.\Yii::$app->basePath.'/web/img/07.01._Cabecera.png"> ', 1, 1 , 250, 180, 'auto');
            $mpdf->WriteFixedPosHTML($p05_textox_01. ' ' .$p05_textox_02, 18, 30 , 170, 90, 'auto');
    
            
            $mpdf->Output('demo.pdf','D');
            exit(0);
        }
        
    }

    public function puntaje($valor){
        if($valor == -1){
            return '05.06.RedondoRojo.png';
        }elseif($valor == 0){
            return '05.06.RedondoGris.png';
        }elseif($valor == 1){
            return '05.06.RedondoVerde.png';
        }
    }

    public function actionDemo(){
        // $phpWord = new PhpWord();
        // $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/formatos/formato-anexo-4-soe-resumen.docx');
        // $template->setValue('meta', '20,550.00');
        // $template->saveAs(Yii::$app->basePath . '/web/formatos/formato_docx_convertido.docx');

        // Pdf::convert(Yii::$app->basePath . '/web/formatos/formato_docx_convertido.docx', Yii::$app->basePath . '/web/formatos/formato-anexo-4-soe-resumen.pdf');
        // $file_url = Yii::$app->basePath . '/web/formatos/formato-anexo-4-soe-resumen.pdf';
        // $pdf_name = 'formato-anexo-4-soe-resumen.pdf';

        // header('Content-Type: application/pdf');
        // header("Content-Transfer-Encoding: Binary");
        // header("Content-disposition: attachment; filename=".$pdf_name);
        // readfile($file_url);

        // unlink(Yii::$app->basePath . '/web/formatos/formato_docx_convertido.docx');
        // unlink($file_url);

        // return true;
        $this->layout = 'privado_pdf';
        return $this->render('_demo');
    }

    public function actionUpload(){
        if(isset($_POST['data'])){
            $data = base64_decode($_POST['data']);
            if (isset($_POST['fileName'])) {
                $file = $_POST['fileName'];
            } else {
                $file = "order-" . rand(10,1000) . ".pdf";
            }
            file_put_contents($file, $data);
            echo "Succes";
        } else {
            echo "No Data Sent";
        }
        exit();
    }

    // public function actionDemo2(){
    //     $content = $this->renderPartial('_demo2');
    
    //     // setup kartik\mpdf\Pdf component
    //     $pdf = new Pdf([
    //         // set to use core fonts only
    //         'mode' => Pdf::MODE_CORE, 
    //         // A4 paper format
    //         'format' => Pdf::FORMAT_A4, 
    //         // portrait orientation
    //         'orientation' => Pdf::ORIENT_PORTRAIT, 
    //         // stream to browser inline
    //         'destination' => Pdf::DEST_BROWSER, 
    //         // your html content input
    //         'cssFile' => \Yii::$app->basePath.'/web/personalizado/demo.css',
    //         'content' => $content,  
    //         // format content from your own css file if needed or use the
    //         // enhanced bootstrap css built by Krajee for mPDF formatting 
    //         // any css to be embedded if required
            
            
    //         // set mPDF properties on the fly
    //         'options' => ['title' => 'Krajee Report Title'],
    //         // call mPDF methods on the fly
    //         'methods' => [ 
    //             'SetHeader'=>['Krajee Report Header'], 
    //             'SetFooter'=>['{PAGENO}'],
    //         ]
    //     ]);
        
    //     // return the pdf output as per the destination setting
    //     return $pdf->render(); 
    // }

    public function actionDemo2($docente_seccion_id=null){

        $informacionGeneral = (new \yii\db\Query())
                    ->select('docente.*,institucion_educativa.nombre_ie,grado.nombre_corto,seccion.descripcion seccion')
                    ->from('usuario')
                    ->innerJoin('docente','docente.dni=usuario.usuario')
                    ->innerJoin('institucion_educativa','institucion_educativa.id=docente.institucion_educativa_id')
                    ->innerJoin('docente_seccion','docente_seccion.docente_id=docente.id')
                    ->innerJoin('seccion','seccion.id=docente_seccion.seccion_id')
                    ->innerJoin('grado','grado.id=seccion.grado_id')
                    ->where('docente_seccion.id=:id',[':id'=>$docente_seccion_id])
                    ->one();

        $estudiantes = (new \yii\db\Query())
                    ->select('estudiante.nombres,estudiante.apellido_paterno_letra,matricula.estado_registro,estudiante.usuario,estudiante.clave,matricula.id matricula_id')
                    ->from('estudiante')
                    ->innerJoin('matricula','estudiante.id = matricula.estudiante_id')
                    ->innerJoin('docente_seccion','docente_seccion.id=matricula.docente_seccion_id')
                    ->where('matricula.estado_registro in (1,2) and docente_seccion.docente_id=:docente_id and docente_seccion.id=:id',[':id'=>$docente_seccion_id,':docente_id'=>Yii::$app->user->identity->docenteid]);
        $estudiantes_contar = $estudiantes->count();

        $estudiantes = $estudiantes->orderBy('matricula.estado_registro desc')->all();

        $mpdf = new Mpdf();
        $content = $this->renderPartial('_demo2');
        $mpdf->WriteFixedPosHTML(date('d-m-Y'), 102, 48, 50, 90, 'auto');
        $mpdf->WriteFixedPosHTML($informacionGeneral['nombre_ie'], 58, 65, 50, 90, 'auto');
        $mpdf->WriteFixedPosHTML($informacionGeneral['seccion'], 42, 78, 50, 90, 'auto');
        $mpdf->WriteFixedPosHTML($estudiantes_contar, 165, 78, 50, 90, 'auto');
        $mpdf->WriteFixedPosHTML($informacionGeneral['nombres'] . ' ' . $informacionGeneral['apellido_paterno'] , 52, 91.5, 50, 90, 'auto');
        $mpdf->WriteFixedPosHTML('', 54, 104.8, 50, 90, 'auto');
        // $mpdf->SetDefaultBodyCSS('background-img', "url('".\Yii::$app->basePath."/web/img/fondo_de_pdf.png')");
        // $mpdf->SetDefaultBodyCSS('background-repeat', 'no-repeat');
        // $mpdf->SetDefaultBodyCSS('background-size', '300px');
        // $mpdf->SetDefaultBodyCSS('height', '300px');

        $mpdf->WriteHTML($content);

        $mpdf->Output();
    }


}
