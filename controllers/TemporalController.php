<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Registro;
use app\models\Usuario;
use app\models\Resetear;
use app\models\Docente;
class TemporalController extends Controller
{
    /**
     * {@inheritdoc}
     */
    

    /**
     * Displays homepage.
     *
     * @return string
     */

    
    public function actionIndex()
    {
        $this->layout='vacio';
        return $this->render('index');
    }

    public function actionResetearClave()
    {
        $this->layout='login';
        $model = new Resetear;
        if ($model->load(Yii::$app->request->post())) {
            $docente = Docente::find()->where('dni=:dni',[':dni'=>$model->dni])->one();
            $usuario = Usuario::find()->where('usuario=:usuario',[':usuario'=>$model->dni])->one();
            if($usuario){
                $correo_electronico = preg_replace('/(?<=..).(?=...*@)/', '*', $docente->correo_electronico);

                $usuario->auth = Yii::$app->security->generatePasswordHash($docente->dni);
                if($usuario->save()){
                    $body = Yii::$app->view->renderFile ( '@app/mail/layouts/plantilla_resetear.php' , [
                        'enlace' => Yii::$app->request->getHostInfo() . Yii::$app->getUrlManager()->getBaseUrl() .'/temporal/validar?auth='. $usuario->auth
                    ] );
    
                    Yii::$app->mail->compose('@app/mail/layouts/html',['content' => $body ])
                        ->setFrom('conectaideasperu@grade.org.pe')
                        ->setTo($docente->correo_electronico)
                        ->setSubject('Bienvenidos a la plataforma ConectaIdeas Perú')
                        ->send();
    
                    return $this->render('resetear-enviado',['correo_electronico'=>$correo_electronico]);
                }
            }
            return $this->render('resetear-error');
        }


        return $this->render('resetear-clave');
    }

    public function actionValidar($auth=null){

        if($auth){
            $this->layout='login';
            
            $validar = Usuario::find()->where('auth=:auth',[':auth'=>$auth])->one();
            if($validar){
                $model = new Resetear;
                if ($model->load(Yii::$app->request->post())) {
                    $validar->clave = Yii::$app->security->generatePasswordHash($model->clave);
                    $validar->auth = '';
                    $validar->update();
                    return $this->redirect(['login/index']);
                }
                return $this->render('validar');
            }
            
        }
        
    }


}
