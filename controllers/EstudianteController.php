<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Estudiante;
use app\models\Matricula;
use app\models\Grado;
use yii\base\ErrorException;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


use yii\web\UploadedFile;
class EstudianteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        if(Yii::$app->user->identity->perfil==99){
            $this->layout='privado_skote';
            return $this->render('index');
        }else{
            $this->layout='privado';
            return $this->render('index');
        }
    }

    public function actionHabilitarMasivamente(){
        $this->layout='privado_skote'; 
        return $this->render('habilitar-masivamente');  
    }

    public function actionLista($docente_seccion_id=null,$grado=null,$seccion=null){
        $this->layout='privado';
        $request = Yii::$app->request;
        $model = new Estudiante();
        $grado = Grado::find()->where('grado=:grado',[':grado'=>$grado])->one();
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $nombres = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $model->nombres);
                //var_dump($stripped);die;


                $model->nombres =   mb_strtoupper(trim($nombres)); // mb_strtoupper(trim($model->nombres));
                $model->apellido_paterno_letra = mb_strtoupper ($model->apellido_paterno_letra);
                $model->fecha_registro = date ( 'Y-m-d H:i:s'); 
                $model->estado_registro = 1 ;


                $numeracion = $this->NumeracionEstudiante(mb_strtolower(trim($model->nombres)),mb_strtolower(trim($model->apellido_paterno_letra)));
                $model->numeracion = $numeracion;
                $model->usuario = $this->eliminarAcentos(mb_strtolower(trim($model->nombres)).mb_strtolower($model->apellido_paterno_letra).$numeracion);
                $model->clave = $this->eliminarAcentos(mb_strtolower(trim($model->nombres)).mb_strtolower($model->apellido_paterno_letra).$numeracion);
                $institucion_educativa = (new \yii\db\Query())
                    ->select('seccion.institucion_educativa_id')
                    ->from('docente_seccion')
                    ->innerJoin('seccion','seccion.id=docente_seccion.seccion_id')
                    ->where('docente_seccion.id=:id',[':id'=>$docente_seccion_id])
                    ->one();
               
                $model->institucion_educativa_id = $institucion_educativa['institucion_educativa_id'];
                if($model->save()){
                    $matricula = new Matricula;
                    $matricula->docente_seccion_id=$model->docente_seccion_id;
                    $matricula->estudiante_id=$model->id;
                    $matricula->fecha_registro = date ( 'Y-m-d H:i:s'); 
                    $matricula->estado_registro = 2 ;
                    $matricula->save();
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('lista',['docente_seccion_id'=>$docente_seccion_id,'grado'=>$grado,'seccion'=>$seccion]);
            }
        }else{
            return $this->render('lista',['docente_seccion_id'=>$docente_seccion_id,'grado'=>$grado,'seccion'=>$seccion]);
        }
    }

    public function actionGetListaEstudiantes(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $estudiantes = (new \yii\db\Query())
                ->select(['estudiante.*,matricula.estado_registro,matricula.id matricula_id,institucion_educativa.departamento,institucion_educativa.provincia,institucion_educativa.distrito,institucion_educativa.nombre_ie, DATE_FORMAT(matricula.fecha_registro, "%d-%m-%Y") as fecha_registro'])
                ->from('estudiante')
                ->innerJoin('institucion_educativa','institucion_educativa.id=estudiante.institucion_educativa_id')
                ->innerJoin('matricula','estudiante.id = matricula.estudiante_id');
            
            if(isset($_POST['departamento']) && $_POST['departamento']!=''){
                $estudiantes = $estudiantes->andWhere(['=', "departamento",$_POST['departamento']]);
            }
            if(isset($_POST['provincia']) && $_POST['provincia']!=''){
                $estudiantes = $estudiantes->andWhere(['=', "provincia",$_POST['provincia']]);
            }
            if(isset($_POST['distrito']) && $_POST['distrito']!=''){
                $estudiantes = $estudiantes->andWhere(['=', "distrito",$_POST['distrito']]);
            }
            if(isset($_POST['iiee']) && $_POST['iiee']!=''){
                $estudiantes = $estudiantes->andWhere(['=', "institucion_educativa.id",$_POST['iiee']]);
            }
            if(isset($_POST['usuario']) && $_POST['usuario']!=''){
                $estudiantes = $estudiantes->andWhere(['like', "usuario",$_POST['usuario']]);
            }


            $estudiantes = $estudiantes->orderBy('matricula.estado_registro desc')
                ->all();
            return ['success'=>true,'estudiantes'=>$estudiantes];
        }
    }


    public function actionGetListaEstudiantesDocente(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $docente_seccion_id = $_POST['docente_seccion_id'];
            $estudiantes = (new \yii\db\Query())
                ->select('estudiante.nombres,estudiante.apellido_paterno_letra,matricula.estado_registro,estudiante.usuario,estudiante.clave,matricula.id matricula_id')
                ->from('estudiante')
                ->innerJoin('matricula','estudiante.id = matricula.estudiante_id')
                ->innerJoin('docente_seccion','docente_seccion.id=matricula.docente_seccion_id')
                ->where('matricula.estado_registro in (1,2) and docente_seccion.docente_id=:docente_id and docente_seccion.id=:id',[':id'=>$docente_seccion_id,':docente_id'=>Yii::$app->user->identity->docenteid])
                ->orderBy('matricula.estado_registro desc')
                ->all();
            return ['success'=>true,'estudiantes'=>$estudiantes];
        }
    }

    public function actionGetEstudiante(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $estudiante_id = $_POST['estudiante_id'];
            $estudiante = (new \yii\db\Query())
                ->select('estudiante.*')
                ->from('estudiante')
                ->where('id=:id',[':id'=>$estudiante_id])
                ->one();
            return ['success'=>true,'estudiante'=>$estudiante];
        }
    }

    public function actionCreate(){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = new Estudiante();
        $model->titulo = 'Registrar estudiante';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $model->estado_registro = 1 ;
                // $model->nombres = strtoupper($model->nombres);
                // $model->apellido_paterno_letra = strtoupper($model->apellido_paterno_letra);

                $model->fecha_registro = date ( 'Y-m-d H:i:s'); 
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }
    
    public function actionMasivamenteArchivo(){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = new Estudiante();
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                //create a dir temporal
                $tmpHandle = tmpfile();
                $metaDatas = stream_get_meta_data($tmpHandle);
                $tmpFilename = $metaDatas['uri'];
                $model->archivo = UploadedFile::getInstance($model, 'archivo');
                if($model->archivo){
                    $model->archivo->saveAs($tmpFilename);
                    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($tmpFilename);
                    unlink($tmpFilename);
                    $data  = $spreadsheet->getActiveSheet()->toArray();
                    $lista_usuarios = [];
                    $i = 0 ;
                    foreach($data as $row)
                    {
                        if($row[1]!=NULL){
                            if($i >0){
                                array_push($lista_usuarios,$row[1]);
                            }
                            $i++;
                            
                        }
                    }

                    
                    $lista_usuarios_bd = [];
                    $lista_errores = $lista_usuarios;
                    $usuarios = (new \yii\db\Query())
                            ->select('estudiante.id,estudiante.usuario')
                            ->from('estudiante')
                            ->innerJoin('matricula','matricula.estudiante_id=estudiante.id and matricula.estado_registro=2')
                            ->where(['usuario'=>$lista_usuarios])
                            ->all();
                    
                    
                    foreach($usuarios as $usuario){
                        //array_push($lista_usuarios_bd,$usuario['usuario']);

                        Yii::$app->db->createCommand()
                        ->update('matricula', ['estado_registro' => 1,'fecha_habilitacion'=>date ( 'Y-m-d H:i:s')], ['=', 'estudiante_id',$usuario['id']])
                        ->execute();

                        $lista_errores = array_diff($lista_errores, [$usuario['usuario']]);
                    }
                    

                    return ['success'=>true,'lista_errores'=>$lista_errores];
                }
            }
        }
    }

    public function actionMasivamente(){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = new Estudiante();
        $model->titulo = 'Habilitar masivamente';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $lista_usuarios = explode("\r\n", $model->masivamente);

                $lista_usuarios_bd = [];
                $lista_errores = $lista_usuarios;
                $usuarios = (new \yii\db\Query())
                        ->select('estudiante.id,estudiante.usuario')
                        ->from('estudiante')
                        ->innerJoin('matricula','matricula.estudiante_id=estudiante.id and matricula.estado_registro=2')
                        ->where(['usuario'=>$lista_usuarios])
                        ->all();
                
                
                foreach($usuarios as $usuario){
                    //array_push($lista_usuarios_bd,$usuario['usuario']);

                    Yii::$app->db->createCommand()
                    ->update('matricula', ['estado_registro' => 1,'fecha_habilitacion'=>date ( 'Y-m-d H:i:s')], ['=', 'estudiante_id',$usuario['id']])
                    ->execute();

                    $lista_errores = array_diff($lista_errores, [$usuario['usuario']]);
                }
                return ['success'=>true,'lista_errores'=>$lista_errores];
            } else {
                return $this->render('masivamente',['model'=>$model]);
            }
        }
    }


    public function actionUpdate($id){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = Estudiante::findOne($id);
        $model->titulo = 'Actualizar estudiante';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionEliminarEstudiante(){
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $matricula_id = $_POST['matricula_id'];
            $model = Matricula::findOne($matricula_id);
            $model->estado_registro=0;
            $model->fecha_eliminacion = date ( 'Y-m-d H:i:s'); 
            if($model->save()){
                return ['success'=>true];
            }else{
                return ['success'=>false];
            }
        }
    }

    public function actionHabilitarEstudiante(){
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $matricula_id = $_POST['matricula_id'];
            $model = Matricula::findOne($matricula_id);
            $model->estado_registro=1;
            $model->fecha_habilitacion = date ( 'Y-m-d H:i:s'); 
            if($model->save()){
                return ['success'=>true];
            }else{
                return ['success'=>false];
            }
        }
    }


    public function actionDeshabilitarEstudiante(){
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $matricula_id = $_POST['matricula_id'];
            $model = Matricula::findOne($matricula_id);
            $model->estado_registro=0;
            if($model->save()){
                return ['success'=>true];
            }else{
                return ['success'=>false];
            }
        }
    }

    public function NumeracionEstudiante($nombre, $letra){
        $model =  (new \yii\db\Query())
                    ->select(['MAX(estudiante.numeracion) numeracion'])
                    ->from('estudiante')
                    ->where('nombres=:nombres and apellido_paterno_letra=:apellido_paterno_letra',[':nombres'=>$nombre,':apellido_paterno_letra'=>$letra])
                    //->where('usuario like ":nombres:apellido_paterno_letra%" ',[':nombres'=>$nombre,':apellido_paterno_letra'=>$letra])
                    ->one();
        $longitud = mb_strlen($nombre.$letra);
        //var_dump($longitud,$model,$nombre.$letra);
        if($longitud==2 && $model['numeracion']==null){
            return 100001;
        }else if($longitud==3 && $model['numeracion']==null){
            return 10001;
        }else if($longitud==4 && $model['numeracion']==null){
            return 1001;
        }else if($longitud==5 && $model['numeracion']==null){
            return 101;
        }else if($longitud>=6 && $model['numeracion']==null){
            return 11;
        }else{
            $numeracion = (int) $model['numeracion'] + 1;
            return $numeracion;
        }


        
        
    }


    public function actionEnviarCorreo(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $correo = $_POST['correo'];
            $usuario= $_POST['usuario'];
            $clave  = $_POST['clave'];
            $nombres = $_POST['nombres'];
            $body = Yii::$app->view->renderFile ( '@app/mail/layouts/enviar_estudiante.php' , [
                'usuario' => $usuario,
                'clave' => $clave,
                'nombres' => $nombres,
            ] );

            Yii::$app->mail->compose('@app/mail/layouts/html',['content' => $body ])
                ->setFrom('conectaideasperu@grade.org.pe')
                ->setTo($correo)
                ->setSubject('Usuario y contraseña para el app de Conecta Ideas')
                ->send();
            return ['success'=>true];
        }

        
    }

    public function eliminarAcentos($cadena){
		
		//Reemplazamos la A y a
		$cadena = str_replace(
            array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
            array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
            $cadena
		);
 
		//Reemplazamos la E y e
		$cadena = str_replace(
            array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
            array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
            $cadena 
        );
 
		//Reemplazamos la I y i
		$cadena = str_replace(
		array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
		array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
		$cadena );
 
		//Reemplazamos la O y o
		$cadena = str_replace(
            array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
            array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
            $cadena 
        );
 
		//Reemplazamos la U y u
		$cadena = str_replace(
            array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
            array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
            $cadena 
        );
 
		//Reemplazamos la N, n, C y c
		$cadena = str_replace(
            array('Ñ', 'ñ', 'Ç', 'ç'),
            array('N', 'n', 'C', 'c'),
            $cadena
        );
        
        $cadena = str_replace(
            array(' '),
            array(''),
            $cadena
		);
        
        $cadena = trim(preg_replace('/\t/', '', $cadena));
        $cadena = trim(preg_replace('/\n/', '', $cadena));
        //$cadena = trim(preg_replace(' ', '', $cadena));

		return $cadena;
	}
}
