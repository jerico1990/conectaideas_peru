<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Registro;
use app\models\Docente;
use app\models\Seccion;
use app\models\DocenteSeccion;
use app\models\Usuario;
class RegistrarController extends Controller
{
    /**
     * {@inheritdoc}
     */
    

    /**
     * Displays homepage.
     *
     * @return string
     */

    public function actionIndexDiseno(){
        $this->layout='publico';
        return $this->render('index-diseno');
    }

    public function actionFinal(){
        $this->layout='registrar';
        return $this->render('final');
    }

    public function actionIndex()
    {
        $this->layout='registrar';
        $request = Yii::$app->request;
        $model = new Registro();
        if($request->isAjax){
            if ($model->load($request->post())) {

                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

                $contar_docente = (new \yii\db\Query())->from('docente')->andWhere(['=', "dni",$model->dni])->count();
                $contar_usuario = (new \yii\db\Query())->from('usuario')->andWhere(['=', "usuario",strtolower($model->correo_electronico)])->count();
                if($contar_docente==1 || $contar_usuario==1){
                    return ['success'=>false,'msg'=>'Información ya registrada','status'=>2];
                }

                if($model->tipo_institucion_educativa=="3"){
                    $model->cargo_id                    = 9;
                }

                $docente                                = new Docente;
                $docente->dni                           = $model->dni;
                $docente->nombres                       = mb_strtoupper($model->nombres);
                $docente->apellido_paterno              = mb_strtoupper($model->apellido_paterno);
                $docente->apellido_materno              = mb_strtoupper($model->apellido_materno);
                $docente->celular                       = $model->celular;
                $docente->correo_electronico            = strtolower($model->correo_electronico);
                $docente->cargo_id                      = $model->cargo_id;
                $docente->institucion_educativa_id      = $model->institucion_educativa_id;
                $docente->cod_mod                       = $model->cod_mod ;
                $docente->tipo_institucion_educativa_id = $model->tipo_institucion_educativa;
                $docente->fecha_nacimiento              = date('Y-m-d',strtotime($model->fecha_nacimiento));
                $docente->aceptar_condicion             = $model->aceptar_condicion;
                $docente->fecha_registro                = date( 'Y-m-d H:i:s');
                if($docente->save()){
                    if($model->tipo_institucion_educativa=="2" && $model->cargo_id=="8"){
                        $listaGrados = explode(',',$model->grado);
                        
                        foreach($listaGrados as $grado){
                            //var_dump($grado);
                            $seccion                            = new Seccion;
                            if($grado=="4"){
                                $seccion->grado_id = 1;
                            }else if($grado=="5"){
                                $seccion->grado_id = 2;
                            }else if($grado=="6"){
                                $seccion->grado_id = 3;
                            }
                            $seccion->institucion_educativa_id  = $model->institucion_educativa_id;
                            $seccion->descripcion               = 'A';
                            $seccion->estado                    = 1 ;
                            $seccion->fecha_registro            = date( 'Y-m-d H:i:s');
                            $seccion->save();
    
                            $docente_seccion                    = new DocenteSeccion;
                            $docente_seccion->docente_id        = $docente->id;
                            $docente_seccion->seccion_id        = $seccion->id;
                            $docente_seccion->fecha_registro    = date( 'Y-m-d H:i:s');
                            $docente_seccion->estado            = 1 ;
                            $docente_seccion->save();
                        }
                    }

                    if($model->tipo_institucion_educativa=="1" && $model->cargo_id=="3"){
                        
                        $seccion                            = new Seccion;
                        if($model->grado=="4"){
                            $seccion->grado_id = 1;
                        }else if($model->grado=="5"){
                            $seccion->grado_id = 2;
                        }else if($model->grado=="6"){
                            $seccion->grado_id = 3;
                        }
                        $seccion->institucion_educativa_id  = $model->institucion_educativa_id;
                        if($model->grado=="4"){
                            $seccion->descripcion           = $this->eliminarAcentos(mb_strtoupper($model->seccion_4));
                        }else if($model->grado=="5"){
                            $seccion->descripcion           = $this->eliminarAcentos(mb_strtoupper($model->seccion_5));
                        }else if($model->grado=="6"){
                            $seccion->descripcion           = $this->eliminarAcentos(mb_strtoupper($model->seccion_6));
                        }
                        
                        $seccion->estado                    = 1 ;
                        $seccion->fecha_registro            = date( 'Y-m-d H:i:s');
                        $seccion->save();

                        $docente_seccion                    = new DocenteSeccion;
                        $docente_seccion->docente_id        = $docente->id;
                        $docente_seccion->seccion_id        = $seccion->id;
                        $docente_seccion->fecha_registro    = date( 'Y-m-d H:i:s');
                        $docente_seccion->estado            = 1 ;
                        $docente_seccion->save();
                    }

                    if($model->tipo_institucion_educativa=="1" && $model->cargo_id=="4"){
                        //var_dump($model->seccion_4);die;
                        $listaGrados = explode(',',$model->grado);
                        foreach($listaGrados as $grado){
                            if($grado=="4"){
                                $listaSecciones = explode(',',$model->seccion_4);
                            }else if($grado=="5"){
                                $listaSecciones = explode(',',$model->seccion_5);
                            }else if($grado=="6"){
                                $listaSecciones = explode(',',$model->seccion_6);
                            }

                            foreach($listaSecciones as $secc){
                                $seccion                            = new Seccion;
                                if($grado=="4"){
                                    $seccion->grado_id = 1;
                                }else if($grado=="5"){
                                    $seccion->grado_id = 2;
                                }else if($grado=="6"){
                                    $seccion->grado_id = 3;
                                }
                                $seccion->institucion_educativa_id  = $model->institucion_educativa_id;
                                $seccion->descripcion               = $this->eliminarAcentos(mb_strtoupper($secc));
                                $seccion->estado                    = 1 ;
                                $seccion->fecha_registro            = date( 'Y-m-d H:i:s');
                                $seccion->save();
        
                                $docente_seccion                    = new DocenteSeccion;
                                $docente_seccion->docente_id        = $docente->id;
                                $docente_seccion->seccion_id        = $seccion->id;
                                $docente_seccion->fecha_registro    = date( 'Y-m-d H:i:s');
                                $docente_seccion->estado            = 1 ;
                                $docente_seccion->save();
                            }
                        }
                    }

                    if($model->tipo_institucion_educativa=="3"){
                        
                        $seccion                            = new Seccion;
                        $seccion->grado_id                  = 1;
                        $seccion->institucion_educativa_id  = $model->institucion_educativa_id;
                        $seccion->descripcion               = 'UNIDOCENTE';
                        $seccion->estado                    = 1 ;
                        $seccion->fecha_registro            = date( 'Y-m-d H:i:s');
                        $seccion->save();

                        $docente_seccion                    = new DocenteSeccion;
                        $docente_seccion->docente_id        = $docente->id;
                        $docente_seccion->seccion_id        = $seccion->id;
                        $docente_seccion->fecha_registro    = date( 'Y-m-d H:i:s');
                        $docente_seccion->estado            = 1 ;
                        $docente_seccion->save();


                        $seccion                            = new Seccion;
                        $seccion->grado_id                  = 2;
                        $seccion->institucion_educativa_id  = $model->institucion_educativa_id;
                        $seccion->descripcion               = 'UNIDOCENTE';
                        $seccion->estado                    = 1 ;
                        $seccion->fecha_registro            = date( 'Y-m-d H:i:s');
                        $seccion->save();

                        $docente_seccion                    = new DocenteSeccion;
                        $docente_seccion->docente_id        = $docente->id;
                        $docente_seccion->seccion_id        = $seccion->id;
                        $docente_seccion->fecha_registro    = date( 'Y-m-d H:i:s');
                        $docente_seccion->estado            = 1 ;
                        $docente_seccion->save();

                        $seccion                            = new Seccion;
                        $seccion->grado_id                  = 3;
                        $seccion->institucion_educativa_id  = $model->institucion_educativa_id;
                        $seccion->descripcion               = 'UNIDOCENTE';
                        $seccion->estado                    = 1 ;
                        $seccion->fecha_registro            = date( 'Y-m-d H:i:s');
                        $seccion->save();

                        $docente_seccion                    = new DocenteSeccion;
                        $docente_seccion->docente_id        = $docente->id;
                        $docente_seccion->seccion_id        = $seccion->id;
                        $docente_seccion->fecha_registro    = date( 'Y-m-d H:i:s');
                        $docente_seccion->estado            = 1 ;
                        $docente_seccion->save();
                    }

                    $usuario            = new Usuario;
                    $usuario->usuario   = $model->dni;
                    $usuario->clave     = Yii::$app->security->generatePasswordHash($model->clave);
                    $usuario->estado    = 1;
                    $usuario->cargo_id  = $model->cargo_id;
                    $usuario->save();

                    

                    $body = Yii::$app->view->renderFile ( '@app/mail/layouts/plantilla.php' , [
                        'dni' => $model->dni 
                    ] );
                    //cesar.gago.egocheaga@gmai.com
                    Yii::$app->mail->compose('@app/mail/layouts/html',['content' => $body ])
                        ->setFrom('conectaideasperu@grade.org.pe')
                        ->setTo(strtolower($model->correo_electronico))
                        ->setCc('conectaideasperu@grade.org.pe')
                        ->setSubject('Bienvenidos a la plataforma ConectaIdeas Perú')
                        ->send();

                    return ['success'=>true,'msg'=>'Grabado existoso','status'=>1];
                }else{
                    return ['success'=>false,'msg'=>'Error al grabar la información','status'=>0];
                }
            }
        }else{
            return $this->render('index');
        }
    
    }

    public function actionGetValidarDni(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $dni = $_POST['dni'];
            $contar_docente = (new \yii\db\Query())->from('docente')->andWhere(['=', "dni",$dni])->count();

            if($contar_docente==1){
                return ['success'=>true,'msg'=>'Información ya registrada','status'=>1];
            }

            return ['success'=>true,'msg'=>'Información no registrada','status'=>0];
        }
    }

    public function actionGetValidarCorreo(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $correo_electronico = $_POST['correo_electronico'];
            $contar_docente = (new \yii\db\Query())->from('docente')->andWhere(['=', "correo_electronico",strtolower($correo_electronico)])->count();

            if($contar_docente==1){
                return ['success'=>true,'msg'=>'Información ya registrada','status'=>1];
            }

            return ['success'=>true,'msg'=>'Información no registrada','status'=>0];
        }
    }

    public function eliminarAcentos($cadena){
		
		//Reemplazamos la A y a
		$cadena = str_replace(
            array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
            array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
            $cadena
		);
 
		//Reemplazamos la E y e
		$cadena = str_replace(
            array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
            array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
            $cadena 
        );
 
		//Reemplazamos la I y i
		$cadena = str_replace(
		array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
		array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
		$cadena );
 
		//Reemplazamos la O y o
		$cadena = str_replace(
            array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
            array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
            $cadena 
        );
 
		//Reemplazamos la U y u
		$cadena = str_replace(
            array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
            array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
            $cadena 
        );
 
		//Reemplazamos la N, n, C y c
		$cadena = str_replace(
            array('Ñ', 'ñ', 'Ç', 'ç'),
            array('N', 'n', 'C', 'c'),
            $cadena
		);
		
		return $cadena;
	}
}
