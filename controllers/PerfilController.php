<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Docente;
use yii\web\UploadedFile;
class PerfilController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='privado';

        return $this->render('index');
    }

    public function actionGetInformacionGeneral(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
                $informacionGeneral = (new \yii\db\Query())
                    ->select('docente.*,institucion_educativa.nombre_ie')
                    ->from('usuario')
                    ->innerJoin('docente','docente.dni=usuario.usuario')
                    ->innerJoin('institucion_educativa','institucion_educativa.id=docente.institucion_educativa_id')
                    ->where('usuario.id=:id',[':id'=>Yii::$app->user->identity->id])
                    ->all();
            return ['success'=>true,'informacionGeneral'=>$informacionGeneral];
        }
    }

    public function actionUpdateCorreo(){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = Docente::findOne(Yii::$app->user->identity->docenteid);
        $model->titulo = 'Actualizar correo';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                if($model->correo_electronico && $model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('_form_correo', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionUpdateNombres(){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = Docente::findOne(Yii::$app->user->identity->docenteid);
        $model->titulo = 'Actualizar nombres';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                if($model->nombres && $model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            }
        }
    }

    public function actionUpdateApellido(){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = Docente::findOne(Yii::$app->user->identity->docenteid);
        $model->titulo = 'Actualizar apellido';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                if($model->apellido_paterno && $model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            }
        }
    }

    public function actionUpdateCelular(){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = Docente::findOne(Yii::$app->user->identity->docenteid);
        $model->titulo = 'Actualizar celular';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                if($model->celular && $model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('_form_celular', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionUpdateFoto(){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = Docente::findOne(Yii::$app->user->identity->docenteid);
        $model->titulo = 'Foto de perfil';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $model->archivo = UploadedFile::getInstance($model, 'archivo');
                if($model->archivo){
                    if($model->foto){
                        if (file_exists(Yii::$app->basePath . '/web/fotos/'.$model->foto)) {
                            unlink(Yii::$app->basePath . '/web/fotos/'.$model->foto);
                        } 
                    }
                    
                    
                    if($model->archivo->saveAs('fotos/' . $model->id . '.' . $model->archivo->extension)){
                        $model->foto = $model->id . '.' . $model->archivo->extension;
                        if($model->save()){
                            return ['success'=>true,'foto'=>$model->foto];
                        }else{
                            return ['success'=>false];
                        }
                    }
                }
                return ['success'=>false];
            } else {
                return $this->render('_form_foto', [
                    'model' => $model,
                ]);
            }
        }
    }

}
