<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

class GradoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','get-grados','get-lista-grados'],
                'rules' => [
					[
                        'allow' => true,
                        'actions' => ['get-grados','get-lista-grados'],
						'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index','get-grados','get-lista-grados'],
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='vacio';
        return $this->render('index');
    }

    public function actionGetGrados(){
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
       
        if($_POST){

            $grados = (new \yii\db\Query())->select('*')->from('grado')->where('estado=1');
            $grados = $grados->all();

            return ['success'=>true,'data'=>$grados];
        }
    }

    public function actionGetListaGrados(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
                $grados = (new \yii\db\Query())
                    ->select('*')
                    ->from('grado')
                    ->where('estado=1')
                    ->all();
            return ['success'=>true,'grados'=>$grados];
        }
    }
}
