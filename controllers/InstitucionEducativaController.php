<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

class InstitucionEducativaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','get-institucion-educativa','get-institucion-educativa-codigo-modular','get-instituciones-educativas','get-lista-instituciones-educativas'],
                'rules' => [
					[
                        'allow' => true,
                        'actions' => ['get-institucion-educativa','get-institucion-educativa-codigo-modular','get-instituciones-educativas','get-lista-instituciones-educativas'],
						'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index','get-institucion-educativa','get-institucion-educativa-codigo-modular','get-instituciones-educativas','get-lista-instituciones-educativas'],
                        'roles' => ['@'],
                    ],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='vacio';
        return $this->render('index');
    }

    public function actionGetInstitucionEducativa(){
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
       
        if($_POST){
            $id = (isset($_POST['id']) && $_POST['id']!='')?$_POST['id']:null;

            $institucion_educativa = (new \yii\db\Query())->select('*')->from('institucion_educativa');
            $institucion_educativa = $institucion_educativa->andWhere(['=', "id",$id]);
            $institucion_educativa = $institucion_educativa->orderBy('nombre_ie asc')->all();

            return ['success'=>true,'data'=>$institucion_educativa];
        }
    }

    public function actionGetInstitucionEducativaCodigoModular(){
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
       
        if($_POST){
            $codigo_modular = (isset($_POST['codigo_modular']) && $_POST['codigo_modular']!='')?$_POST['codigo_modular']:null;

            $institucion_educativa = (new \yii\db\Query())->select('*')->from('institucion_educativa');
            $institucion_educativa = $institucion_educativa->andWhere(['=', "cod_mod",$codigo_modular]);
            $institucion_educativa = $institucion_educativa->orderBy('nombre_ie asc')->all();

            return ['success'=>true,'data'=>$institucion_educativa];
        }
    }



    public function actionGetInstitucionesEducativas(){
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
       
        if($_POST){
            $departamento = (isset($_POST['departamento']) && $_POST['departamento']!='')?$_POST['departamento']:null;
            $provincia = (isset($_POST['provincia']) && $_POST['provincia']!='')?$_POST['provincia']:null;
            $distrito = (isset($_POST['distrito']) && $_POST['distrito']!='')?$_POST['distrito']:null;

            $instituciones_educativas = (new \yii\db\Query())->select('*')->from('institucion_educativa');
            $instituciones_educativas = $instituciones_educativas->andWhere(['=', "departamento",$departamento]);
            $instituciones_educativas = $instituciones_educativas->andWhere(['=', "provincia",$provincia]);
            $instituciones_educativas = $instituciones_educativas->andWhere(['=', "distrito",$distrito]);
            $instituciones_educativas = $instituciones_educativas->orderBy('nombre_ie asc')->all();

            return ['success'=>true,'data'=>$instituciones_educativas];
        }
    }

    public function actionGetListaInstitucionesEducativas(){
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
       
        if($_POST){
            $q = (isset($_GET['q']) && $_GET['q']!='')?$_GET['q']:null;

            $instituciones_educativas = (new \yii\db\Query())->select(['id,concat(cod_mod,"-",nombre_ie) as text,departamento,provincia,distrito'])->from('institucion_educativa');
            $instituciones_educativas = $instituciones_educativas->andWhere(['like', 'cod_mod',$q]);
            $total_count = $instituciones_educativas->count();
            $instituciones_educativas = $instituciones_educativas->orderBy('nombre_ie asc')->all();

            return ['success'=>true,'results'=>$instituciones_educativas];
        }
    }
}
