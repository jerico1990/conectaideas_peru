<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

class UbigeoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','get-regiones','get-provincias','get-distritos'],
                'rules' => [
					[
                        'allow' => true,
                        'actions' => ['get-regiones','get-provincias','get-distritos'],
						'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['get-regiones','get-provincias','get-distritos'],
                        'roles' => ['@'],
                    ],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='vacio';
        return $this->render('index');
    }

    public function actionGetRegiones(){
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
       
        if($_POST){

            $regiones = (new \yii\db\Query())->select('ubigeo.departamento')->from('ubigeo')->innerJoin('institucion_educativa','institucion_educativa.departamento=ubigeo.departamento');
            $regiones = $regiones->distinct('ubigeo.departamento')->orderBy('ubigeo.departamento asc')->all();

            return ['success'=>true,'data'=>$regiones];
        }
    }

    public function actionGetProvincias(){
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
       
        if($_POST){
            $departamento = (isset($_POST['departamento']) && $_POST['departamento']!='')?$_POST['departamento']:null;

            $provincias = (new \yii\db\Query())->select('ubigeo.provincia')->from('ubigeo')->innerJoin('institucion_educativa','institucion_educativa.provincia=ubigeo.provincia');
            $provincias = $provincias->andWhere(['=', "ubigeo.departamento",$departamento]);
            $provincias = $provincias->distinct('ubigeo.provincia')->orderBy('ubigeo.provincia asc')->all();

            return ['success'=>true,'data'=>$provincias];
        }
    }

    public function actionGetDistritos(){
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
       
        if($_POST){
            $provincia = (isset($_POST['provincia']) && $_POST['provincia']!='')?$_POST['provincia']:null;

            $distritos = (new \yii\db\Query())->select('ubigeo.distrito')->from('ubigeo')->innerJoin('institucion_educativa','institucion_educativa.distrito=ubigeo.distrito');
            $distritos = $distritos->andWhere(['=', "ubigeo.provincia",$provincia]);
            $distritos = $distritos->distinct('ubigeo.distrito')->orderBy('ubigeo.distrito asc')->all();

            return ['success'=>true,'data'=>$distritos];
        }
    }
}
