<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\ActividadCabecera;
use app\models\ActividadDetalle;
use yii\web\UploadedFile;
class ActividadDetalleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='privado_skote';
        return $this->render('index');
    }

    public function actionEliminarVideo(){
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $actividad_id = $_POST['actividad_id'];
            $model = ActividadDetalle::findOne($actividad_id);
            $model->url_video='';
            if($model->save()){
                return ['success'=>true];
            }else{
                return ['success'=>false];
            }
        }
    }

    public function actionUpdateRecurso(){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model =new ActividadDetalle;
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                
                $actividadDetalle = ActividadDetalle::findOne($model->recurso_id);
                $actividadDetalle->url_video    = $model->url_video;
                $actividadDetalle->descripcion  = $model->descripcion;
                $actividadDetalle->actividad_chile_id  = $model->actividad_chile_id;

                $model->archivo_matriz = UploadedFile::getInstance($model, 'archivo_matriz');
                if($model->archivo_matriz){
                    if($actividadDetalle->matriz){
                        if (file_exists(Yii::$app->basePath . '/web/recursos/matriz/'.$actividadDetalle->matriz)) {
                            unlink(Yii::$app->basePath . '/web/recursos/matriz/'.$actividadDetalle->matriz);
                        } 
                    }
                    if($model->archivo_matriz->saveAs('recursos/matriz/' . $model->grado_descripcion . '_Matriz_E' . $model->experiencia_correlativo .'_Actividad' . $model->actividad_correlativo . '.' . $model->archivo_matriz->extension)){
                        $actividadDetalle->matriz = $model->grado_descripcion . '_Matriz_E' . $model->experiencia_correlativo .'_Actividad' . $model->actividad_correlativo . '.' . $model->archivo_matriz->extension;
                    }
                }

                $model->archivo_solucionario = UploadedFile::getInstance($model, 'archivo_solucionario');
                if($model->archivo_solucionario){
                    if($actividadDetalle->solucionario){
                        if (file_exists(Yii::$app->basePath . '/web/recursos/solucionario/'.$actividadDetalle->solucionario)) {
                            unlink(Yii::$app->basePath . '/web/recursos/solucionario/'.$actividadDetalle->solucionario);
                        }
                    }
                    if($model->archivo_solucionario->saveAs('recursos/solucionario/' . $model->grado_descripcion . '_Solucionario_E' . $model->experiencia_correlativo .'_Actividad' . $model->actividad_correlativo . '.' . $model->archivo_solucionario->extension)){
                        $actividadDetalle->solucionario = $model->grado_descripcion . '_Solucionario_E' . $model->experiencia_correlativo .'_Actividad' . $model->actividad_correlativo . '.' . $model->archivo_solucionario->extension;
                    }
                }



                if($actividadDetalle->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('_form_video', [
                    'model' => $model,
                ]);
            }
        }
    }


    public function actionEliminarMatriz(){
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $actividad_id = $_POST['actividad_id'];
            $model = ActividadDetalle::findOne($actividad_id);
            $model->matriz='';
            if($model->save()){
                return ['success'=>true];
            }else{
                return ['success'=>false];
            }
        }
    }

    public function actionEliminarSolucionario(){
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $actividad_id = $_POST['actividad_id'];
            $model = ActividadDetalle::findOne($actividad_id);
            $model->solucionario='';
            if($model->save()){
                return ['success'=>true];
            }else{
                return ['success'=>false];
            }
        }
    }



}
