<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Grado;
use app\models\Experiencia;
use app\models\Texto;
class ExperienciaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='privado_skote';
        return $this->render('index');
    }


    public function actionGetListaExperiencias(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
                $experiencias = (new \yii\db\Query())
                    ->select('*')
                    ->from('wv_lista_experiencias')
                    ->where('cantidad_actividades_activos>0')
                    ->orderBy('correlativo desc')
                    ->all();
            return ['success'=>true,'experiencias'=>$experiencias];
        }
    }

    public function actionGetListaExperienciasReportes(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
                $experiencias = (new \yii\db\Query())
                    ->select('*')
                    ->from('wv_lista_experiencias_reportes')
                    ->where('cantidad_actividades_activos>0')
                    ->orderBy('correlativo desc')
                    ->all();
            return ['success'=>true,'experiencias'=>$experiencias];
        }
    }

    public function actionGetListaExp(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
                $experiencias = (new \yii\db\Query())
                    ->select('*')
                    ->from('wv_lista_experiencias')
                    ->all();
            return ['success'=>true,'experiencias'=>$experiencias];
        }
    }

    public function actionCreate(){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = new Experiencia();
        $model->titulo = 'Agregar experiencia';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                if($model->save()){
                    /* 4to */
                    $texto = new Texto();
                    $texto->pregunta = 2;
                    $texto->estado_registro = 1;
                    $texto->posicion = 1;
                    $texto->experiencia_id = $model->id;
                    $texto->grado_id = 1;
                    $texto->save();

                    $texto = new Texto();
                    $texto->pregunta = 5;
                    $texto->estado_registro = 1;
                    $texto->posicion = 1;
                    $texto->experiencia_id = $model->id;
                    $texto->grado_id = 1;
                    $texto->save();

                    $texto = new Texto();
                    $texto->pregunta = 5;
                    $texto->estado_registro = 1;
                    $texto->posicion = 2;
                    $texto->experiencia_id = $model->id;
                    $texto->grado_id = 1;
                    $texto->save();

                    /* 5to */
                    $texto = new Texto();
                    $texto->pregunta = 2;
                    $texto->estado_registro = 1;
                    $texto->posicion = 1;
                    $texto->experiencia_id = $model->id;
                    $texto->grado_id = 2;
                    $texto->save();

                    $texto = new Texto();
                    $texto->pregunta = 5;
                    $texto->estado_registro = 1;
                    $texto->posicion = 1;
                    $texto->experiencia_id = $model->id;
                    $texto->grado_id = 2;
                    $texto->save();

                    $texto = new Texto();
                    $texto->pregunta = 5;
                    $texto->estado_registro = 1;
                    $texto->posicion = 2;
                    $texto->experiencia_id = $model->id;
                    $texto->grado_id = 2;
                    $texto->save();


                    /* 5to */
                    $texto = new Texto();
                    $texto->pregunta = 2;
                    $texto->estado_registro = 1;
                    $texto->posicion = 1;
                    $texto->experiencia_id = $model->id;
                    $texto->grado_id = 3;
                    $texto->save();

                    $texto = new Texto();
                    $texto->pregunta = 5;
                    $texto->estado_registro = 1;
                    $texto->posicion = 1;
                    $texto->experiencia_id = $model->id;
                    $texto->grado_id = 3;
                    $texto->save();

                    $texto = new Texto();
                    $texto->pregunta = 5;
                    $texto->estado_registro = 1;
                    $texto->posicion = 2;
                    $texto->experiencia_id = $model->id;
                    $texto->grado_id = 3;
                    $texto->save();

                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionUpdate($id){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = Experiencia::findOne($id);
        $model->titulo = 'Actualizar experiencia';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionGetExperiencia(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $id_experiencia = $_POST['id_experiencia'];
            $experiencia = (new \yii\db\Query())
                ->select('experiencia.*')
                ->from('experiencia')
                ->where('id=:id',[':id'=>$id_experiencia])
                ->one();
            return ['success'=>true,'experiencia'=>$experiencia];
        }
    }

}
